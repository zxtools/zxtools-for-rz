#pragma once

#include "task.h"

Date^ ReadDate(int version, BinaryReader^ br)
{
	Date^ date = gcnew Date();

	date->year = br->ReadInt32();
	date->month = br->ReadInt32();
	date->day = br->ReadInt32();
	date->hour = br->ReadInt32();
	date->minute = br->ReadInt32();
	date->weekday = br->ReadInt32();

	return date;
}

Map_Location^ ReadLocation(int version, BinaryReader^ br)
{
	Map_Location^ location = gcnew Map_Location();

	location->map_id = br->ReadInt32();
	location->x = br->ReadSingle();
	location->altitude = br->ReadSingle();
	location->z = br->ReadSingle();

	return location;
}

DynamicsTask^ ReadDynamics(int version, BinaryReader^ br)
{
	DynamicsTask^ value_set = gcnew DynamicsTask();
	value_set->id = br->ReadInt32();
	value_set->unknown_1 = br->ReadBoolean();
	value_set->unknown_2 = br->ReadBoolean();
	value_set->unknown_3 = br->ReadBoolean();
	value_set->unknown_4 = br->ReadBoolean();
	value_set->unknown_5 = br->ReadBoolean();
	value_set->point = ReadLocation(version, br);
	value_set->times = br->ReadInt32();
	return value_set;
}

TeamMembers^ ReadTeamMembers(int version, BinaryReader^ br)
{
	TeamMembers^ value_set = gcnew TeamMembers();
    value_set->level_min = br->ReadInt32();
	value_set->level_max = br->ReadInt32();
	value_set->race = br->ReadInt32();
	value_set->job = br->ReadInt32();
	value_set->gender = br->ReadInt32();
	value_set->Unknown_02 = br->ReadBoolean();
	value_set->Unknown_01 = br->ReadInt32();
    value_set->amount_min = br->ReadInt32();
	value_set->amount_max = br->ReadInt32();
    value_set->quest = br->ReadInt32();
	return value_set;
}

ValueSet^ ReadValueSet(int version, BinaryReader^ br)
{
	ValueSet^ value_set = gcnew ValueSet();
	value_set->val_1 = br->ReadInt32();
	value_set->val_2 = br->ReadInt32();
	value_set->val_3 = br->ReadInt32();
	value_set->val_count = br->ReadInt32();
	return value_set;
}

ValueItem^ ReadValueItem(int version, BinaryReader^ br)
{
	ValueItem^ value_item = gcnew ValueItem();
	value_item->val_1 = br->ReadInt32();
	value_item->val_2 = br->ReadInt32();
	value_item->val_3 = br->ReadBoolean();
	return value_item;
}

DateSpan^ ReadDateSpan(int version, BinaryReader^ br)
{
	DateSpan^ date_span = gcnew DateSpan();
	date_span->from = ReadDate(version, br);
	date_span->to = ReadDate(version, br);
	return date_span;
}

RewardItem^ ReadRewardItem(int version, BinaryReader^ br)
{
	RewardItem^ item = gcnew RewardItem();

	item->id = br->ReadInt32(); // 4
	item->unknown = br->ReadBoolean();  // 5
	item->amount = br->ReadInt32(); // 9
	item->probability = br->ReadSingle(); // 13
	item->flg = br->ReadBoolean(); // 14
	item->expiration = br->ReadInt32(); // 18
	item->unkown_01 = br->ReadInt32();  // 22
	item->unkown_02 =  br->ReadByte(); // 26
    item->RefLevel = br->ReadInt32(); // 30
	item->unkown_03 = br->ReadInt32(); // 34
	return item;
}

Item^ ReadItem(int version, BinaryReader^ br)
{
	Item^ item = gcnew Item();
	item->id = br->ReadInt32();
	item->unknown = br->ReadBoolean();
	item->amount = br->ReadInt32();
	item->probability = br->ReadSingle();
	item->expiration = br->ReadInt32();
	item->unkown_01 = br->ReadBytes(14);
	return item;
}

ItemGroup^ ReadItemGroup(int version, BinaryReader^ br)
{
	ItemGroup^ item_group = gcnew ItemGroup();
	item_group->type = br->ReadBoolean();
	item_group->items_count = br->ReadInt32();
	item_group->items = gcnew array<RewardItem^>(item_group->items_count);
	for(int n=0; n<item_group->items->Length; n++)
	{
		item_group->items[n] = ReadRewardItem(version, br); //ReadItem
	}
	return item_group;
}


Chase^ ReadChase(int version, BinaryReader^ br)
{
	Chase^ chase = gcnew Chase();
	chase->id_monster = br->ReadInt32();
	chase->amount_monster = br->ReadInt32();
	chase->id_drop = br->ReadInt32();
	chase->amount_drop = br->ReadInt32();
	chase->unknown_1 = br->ReadByte();
	chase->probability = br->ReadSingle();
	chase->unknown_2 = br->ReadByte();
	return chase;
}

Span^ ReadSpan(int version, BinaryReader^ br)
{
	Span^ location_span = gcnew Span();

	location_span->east = br->ReadSingle();
	location_span->bottom = br->ReadSingle();
	location_span->south = br->ReadSingle();
	location_span->west = br->ReadSingle();
	location_span->top = br->ReadSingle();
	location_span->north = br->ReadSingle();

	return location_span;
}

ShengWang^ ReadSW(int version, BinaryReader^ br)
        {
            ShengWang^ wang = gcnew ShengWang();
            wang->GuiDao = br->ReadInt32();
            wang->QingYun = br->ReadInt32();
            wang->TianYin = br->ReadInt32();
            wang->GuiWang = br->ReadInt32();
            wang->HeHuan = br->ReadInt32();
            wang->ZhongYi = br->ReadInt32();
            wang->QingYuan = br->ReadInt32();
            wang->WenCai = br->ReadInt32();
            wang->ShiDe = br->ReadInt32();
            wang->DaoXin = br->ReadInt32();
            wang->MoXing = br->ReadInt32();
            wang->FoYuan = br->ReadInt32();
            wang->unknow_15 = br->ReadInt32();
            wang->JiuLi = br->ReadInt32();
            wang->LieShan = br->ReadInt32();
            wang->ChenHuang = br->ReadInt32();
            wang->TaiHao = br->ReadInt32();
            wang->HuaiGuang = br->ReadInt32();
            wang->TianHua = br->ReadInt32();
            wang->FenXiang = br->ReadInt32();
            wang->ZhanJi = br->ReadInt32();
            wang->ZhanXun = br->ReadInt32();
            wang->TianZong = br->ReadInt32();
            wang->DiZong = br->ReadInt32();
            wang->RenZong = br->ReadInt32();
            return wang;
 }

EXP_Formalu^ ReadExpValItems(int version, BinaryReader^ br)
{
	EXP_Formalu^ formalu = gcnew EXP_Formalu();
    formalu->val_1 = br->ReadInt32();
    formalu->val_2 = br->ReadSingle();
    return formalu;
}
Reward^ ReadReward(int version, BinaryReader^ br)
{
	Reward^ reward = gcnew Reward();
	reward->coins = br->ReadInt32();
	reward->experience = br->ReadInt64();
	reward->ExpLv = br->ReadSingle();
	reward->RCLv = br->ReadSingle();
	reward->APLv = br->ReadSingle();
	reward->new_quest = br->ReadInt32();
	reward->unknow_01 = br->ReadInt32();
    reward->unknow_02 = br->ReadInt32();
	reward->unknow_03 = br->ReadInt32();
    reward->unknow_04 = br->ReadInt32();
    reward->unknow_05 = br->ReadInt32();
    reward->unknow_06 = br->ReadInt32();
    reward->unknow_07 = br->ReadInt32();
    reward->unknow_08 = br->ReadInt32();
    reward->unknow_09 = br->ReadInt32();
    reward->unknow_10 = br->ReadInt32();
	if(version>=165){
		reward->unkn_1380_1 = br->ReadBytes(65);
	}
	reward->group_integral = br->ReadInt32();
	reward->unknow_12 = br->ReadInt32();
    reward->unknow_12_1 = br->ReadInt32();
	reward->unkn_1330_6 = br->ReadInt32();
	reward->fation_gongxian = br->ReadInt32();
    reward->family_gongxian = br->ReadInt32();
    reward->unknow_13 = br->ReadInt32();
	reward->title = br->ReadInt32();
    reward->removePK = br->ReadInt32(); 
    reward->clearPK = br->ReadBoolean();
    reward->Divorce = br->ReadBoolean(); //82
	reward->sw = ReadSW(version, br); //182 +8
	reward->unknow_20 = br->ReadInt32();
    reward->unknow_21 = br->ReadInt32();
    reward->unknow_22 = br->ReadInt32();
    reward->unknow_23 = br->ReadInt32();
    reward->unknow_31 = br->ReadInt32();
    reward->unknow_25 = br->ReadInt32();
    reward->unknow_26 = br->ReadInt32();
    reward->unknow_27 = br->ReadInt32();
    reward->unknow_28 = br->ReadInt32();
    reward->unknow_29 = br->ReadInt32();
    reward->Cangku = br->ReadInt32();
    reward->unknow_30 = br->ReadInt32();
    reward->Beibao = br->ReadInt32();
    reward->CWBeibao = br->ReadInt32();
    reward->CWlan = br->ReadInt32();
    reward->ZuoqiBeibao = br->ReadInt32();
    reward->unknow_31_1 = br->ReadInt32(); //250+8
    reward->ShengChan_UP = br->ReadBoolean();
    reward->unknow_32 = br->ReadInt32();
    reward->jobtmp_up = br->ReadInt32();
    reward->unknow_33 = br->ReadInt32();
    reward->SetJob = br->ReadInt32(); //267
    reward->teleport = ReadLocation(version, br);
    reward->ai_trigger = br->ReadInt32(); //287
    reward->trigger_on = br->ReadBoolean(); //288
    reward->unknow_35 = br->ReadInt32();
    reward->unknow_36 = br->ReadInt32();
    reward->ShuLianDu = br->ReadInt32();
    reward->SkillsLV = br->ReadInt32();
    reward->family_skill_id = br->ReadInt32();
    reward->family_lingqi = br->ReadInt32();//
	reward->unkn_1330_7 = br->ReadInt32();
	reward->unkn_1330_8 = br->ReadInt32();
    reward->unknow_40 = br->ReadInt32();
    reward->fation_lingqi = br->ReadInt32(); //328
    reward->Notice = br->ReadBoolean(); 
    reward->Notice_Channel = br->ReadInt32();
    reward->removetasks = br->ReadInt32();
    reward->DoubleTimes = br->ReadInt32();
    reward->item_groups_count = br->ReadInt32();//165
    reward->unknow_42 = br->ReadInt32();
    reward->unknow_43 = br->ReadInt32();
    reward->ChuShi = br->ReadBoolean();
    reward->PanShi = br->ReadBoolean();
    reward->Shitu_01 = br->ReadBoolean();
    reward->Shitu_02 = br->ReadBoolean();
    reward->is_special_reward = br->ReadBoolean(); //149
    reward->special_flg = br->ReadInt32();
    reward->unknow_46 = br->ReadInt32();
    reward->Camp_on = br->ReadBoolean();
    reward->Camp = br->ReadInt32();
    reward->skillcount = br->ReadInt32();
    reward->skill_on = br->ReadBoolean(); //131
    reward->skill_Tianshu_on = br->ReadBoolean(); 
    reward->is_exp_formula = br->ReadInt32();
    reward->exp_formula_len = br->ReadInt32();
    reward->unknow_48 = br->ReadBytes(12); //119
	reward->value_set = gcnew array<ValueSet^>(3); 
	for(int i=0;i<reward->value_set->Length;i++){
		reward->value_set[i]=gcnew ValueSet();
		reward->value_set[i] = ReadValueSet(version, br);
	}
	reward->unknow_49 = br->ReadBytes(87); //217
    reward->Bianshen = br->ReadInt32();
    reward->BianshenTime = br->ReadInt32();
    reward->unknow_50 = br->ReadInt32();
    reward->unknow_51 = br->ReadInt32();
    reward->unknow_52 = br->ReadBoolean();
    reward->YuanShen = br->ReadInt32();
    reward->YuanShenlv = br->ReadBoolean();
    reward->OpenYuanYing = br->ReadBoolean();
    reward->HongLiIngot = br->ReadInt32(); //244
	if(version>=165){
		reward->unkn_1380_2 =  br->ReadInt32(); 
		reward->first_time_count =  br->ReadInt32(); 
	}
    reward->unknown_53 = br->ReadInt32();
	reward->unknown_55 = br->ReadInt32();
    reward->unknown_56 = br->ReadInt32();
    reward->unknown_n1 = br->ReadInt32();
    reward->unknown_n2 = br->ReadInt32();
    reward->unknown_n3 = br->ReadSingle();
    reward->unknown_n4 = br->ReadInt32(); //272
	reward->nval_1 = br->ReadBytes(61); 
    reward->is_nval = br->ReadBoolean();
    reward->nval_2 = br->ReadBytes(26);
    reward->nval_len_1 = br->ReadInt32();
    reward->nval_3 = br->ReadBytes(32);
    reward->nval_len_2 = br->ReadInt32();
	if(version>=165){
		reward->nval_4 = br->ReadBytes(140);
	}
	else{
		reward->nval_4 = br->ReadBytes(28);
	}
	reward->unkn_1330_10 = br->ReadBoolean();
	reward->unkn_1330_9 = br->ReadInt32();
	if (reward->unknown_55 <= 0)
    {
		reward->item_groups = gcnew array<ItemGroup^>(reward->item_groups_count);
		for (int j = 0; (j < reward->item_groups->Length); j++)
        {
			reward->item_groups[j] = ReadItemGroup(version, br);
        }
		reward->Valitems = gcnew array<ValueItem^>(reward->value_set[1]->val_count);
        for (int k = 0; (k < reward->Valitems->Length); k++)
        {
			reward->Valitems[k] = ReadValueItem(version, br);
        }
		
		if (reward->is_nval)
        {
			reward->nval_5 = br->ReadBytes(reward->nval_len_1);
			reward->nval_6 = br->ReadBytes(reward->nval_len_2);
			reward->noticetext_len = br->ReadInt32();
			reward->noticetext = br->ReadBytes((reward->noticetext_len * 2));
        }
		
        if ((reward->is_exp_formula > 0 || reward->exp_formula_len > 0)&&!reward->Shitu_01)
        {
			reward->exp_val = gcnew EXP_formalu();
			reward->exp_val->name = br->ReadBytes(reward->exp_formula_len);
			reward->exp_val->count = br->ReadInt32();
            reward->exp_val->val = gcnew array<EXP_Formalu^>(reward->exp_val->count);
            for (int m = 0; (m < reward->exp_val->val->Length); m++)
            {
				reward->exp_val->val[m] = ReadExpValItems(version, br);
            }
        }
		reward->first_time_reward = gcnew array<ItemGroup^>(reward->first_time_count);
		for (int j = 0; (j < reward->first_time_reward->Length); j++)
        {
			reward->first_time_reward[j] = ReadItemGroup(version, br);
		}
    }
    else
    {
		if (reward->is_nval)
		{
			reward->nval_5 = br->ReadBytes(reward->nval_len_1);
			reward->nval_6 = br->ReadBytes(reward->nval_len_2+36);
        }
		reward->item_groups = gcnew array<ItemGroup^>(reward->item_groups_count);
		for (int j = 0; (j < reward->item_groups->Length); j++)
        {
			reward->item_groups[j] = ReadItemGroup(version, br);
        }
        reward->noticetext_len = br->ReadInt32();
		reward->noticetext = br->ReadBytes((reward->noticetext_len * 2));
        return reward;
    }
    if (!reward->is_nval && (!reward->is_special_reward || (reward->special_flg <= 0)))
    {
		reward->unknown_54 = br->ReadInt32();
    }
	return reward;
}

void Task::Load(int version, BinaryReader^ br, int stream_position, TreeNodeCollection^ nodes)
{
	//try{
	br->BaseStream->Position = stream_position;
	// ################# GENERAL #############################
	id = br->ReadInt32();
	name = br->ReadBytes(60);
	author_mode=br->ReadBoolean();
	UNKNOWN_01=br->ReadInt32();
	type = br->ReadInt32();
	time_limit = br->ReadInt32();
	UNKNOWN_02=br->ReadBoolean();
	// add row
	if(nodes)
	{
		Drawing::Color c = Drawing::Color::White;
		String^ node = Name;

		if(node->StartsWith("^"))
		{
			try
			{
				c = Drawing::Color::FromArgb(int::Parse(node->Substring(1, 6), Globalization::NumberStyles::HexNumber));
				node = node->Substring(7);
			}
			catch(...)
			{
				c = Drawing::Color::White;
			}
		}

		nodes->Add(id.ToString() + " - " + node);
		nodes[nodes->Count-1]->ForeColor = c;
	}
	// ################# DATES #############################
	date_spans_count = br->ReadInt32();
	UNKNOWN_157 = br->ReadBytes(20); //hex set
	xunhanType = br->ReadInt32();
	cooldown = br->ReadInt32();
	activate_first_subquest = br->ReadBoolean();
	activate_random_subquest = br->ReadBoolean();
	activate_next_subquest = br->ReadBoolean();
	on_give_up_parent_fails = br->ReadBoolean();
	on_success_parent_success = br->ReadBoolean();
	can_give_up = br->ReadBoolean();
	repeatable = br->ReadBoolean();
	repeatable_after_failure = br->ReadBoolean();
	fail_on_death = br->ReadBoolean();
	on_fail_parent_fail = br->ReadBoolean();
	UNKNOWN_10 = br->ReadBoolean();
	player_limit = br->ReadInt32();
	reset_type = br->ReadInt32();
	reset_cycle = br->ReadInt32();
	trigger_locations = gcnew LocationSpan();
	trigger_locations->has_location = br->ReadBoolean();
	trigger_locations->map_id = br->ReadInt32();
	trigger_locations->count = 1;
	trigger_locations->spans = gcnew array<Span^>(trigger_locations->count);
	trigger_locations->spans[0] = ReadSpan(version, br);
	has_instant_teleport = br->ReadBoolean();
	instant_teleport_location = ReadLocation(version, br);
	ai_trigger = br->ReadInt32();
	has_trigger_on = br->ReadBoolean();
	if(version>=165){
		UNKNOWN_ZEROS=br->ReadBytes(136); //148
	}
	else
	{
		UNKNOWN_ZEROS=br->ReadBytes(76); //all zeros?
	}
	UNKNOWN_18 = br->ReadBoolean();
	UNKNOWN_19 = br->ReadBoolean();
	autotask_unknown = br->ReadInt32();
	UNKNOWN_20 = br->ReadBoolean();
	cleartask_unknown = br->ReadInt32();
	mark_available_icon = br->ReadBoolean();
	mark_available_point = br->ReadBoolean();
	quest_npc = br->ReadInt32();
	reward_npc = br->ReadInt32();
	UNKNOWN_17 = br->ReadBoolean(); ///mission_task
	UNKNOWN_21 = br->ReadBoolean();
	UNKNOWN_22 = br->ReadBoolean();
	component = br->ReadSingle();
	classify = br->ReadInt32();
	if(version>=165){
		unkn_1380_2 =br->ReadBoolean();
	}
	UNKNOWN_23 = br->ReadBoolean();
	clan_task = br->ReadBoolean();
	player_limit_on = br->ReadBoolean();
	all_zone = br->ReadBoolean();
	player_limit_num = br->ReadInt32();
	UNKNOWN_112_1 = br->ReadBytes(12); //hex set
	all_zone_num = br->ReadInt32();
	unkpenalty = br->ReadInt32();
	playerpenalty = br->ReadInt32();
	allkillnum = br->ReadInt32();
	bool1380=br->ReadBoolean();
	UNKNOWN_113 = br->ReadBytes(31);  //hex set
	haslimit = br->ReadBoolean();
	unkn_1330_11 = br->ReadInt32();
	fail_locations = gcnew LocationSpan();
	fail_locations->has_location = br->ReadBoolean();
	fail_locations->map_id = br->ReadInt32();
	fail_locations->count = 1;
	fail_locations->spans = gcnew array<Span^>(fail_locations->count);
	fail_locations->spans[0] = ReadSpan(version, br);
	fail_locations->unknown_1 = gcnew array<unsigned char>(4); //todelete?
	GlobalVal1 = br->ReadBytes(30); //hex set 30/34
	global_value_set1 = gcnew ValueSet();
	global_value_set1 = ReadValueSet(version, br);
	global_value_set2 = gcnew ValueSet();
	global_value_set2 = ReadValueSet(version, br);
	global_value_set3 = gcnew ValueSet();
	global_value_set3 = ReadValueSet(version, br);
	GlobalVal2 = br->ReadBytes(193); //hex set 125 /1330
	val_unknown_01 = br->ReadBoolean();
	val_unknown_02 = br->ReadBoolean(); 
	val_unknown_03 = br->ReadBoolean();
	new_type = br->ReadInt32();
	new_type2 = br->ReadInt32();
	unkn_1330_1 = br->ReadInt32();
	unkn_1330_2 = br->ReadBoolean();
	level_max = br->ReadInt32();
	level_min = br->ReadInt32();
	has_show_level = br->ReadBoolean();
	bloodbound_min = br->ReadInt32();
	bloodbound_max = br->ReadInt32();
	required_items_count = br->ReadInt32();
	required_items_unknown = br->ReadInt32();
	UNKNOWN_27 = br->ReadBoolean(); 
	items_unknown_4 = br->ReadBoolean();
	dynamics_count = br->ReadInt32(); 
	items_unknown_6 = br->ReadInt32();
	items_unknown_5 = br->ReadBoolean(); 
	items_unknown_7 = br->ReadInt32(); 
	given_items_count = br->ReadInt32();
	given_items_needBags = br->ReadInt32(); 
	items_unknown_9 = br->ReadInt32(); 
	items_unknown_10 = br->ReadInt32();
	items_unknown_11 = br->ReadInt32(); 
	required_reputation = br->ReadInt32(); 
	instant_pay_coins = br->ReadInt32(); 
	required_items_bags = br->ReadBoolean(); 
	required_need_next =  br->ReadInt32();
	has_next = br->ReadBoolean();
	clan_required = br->ReadBoolean(); 
	required_need_contribution = br->ReadInt32();  //counter
	has_contribution = br->ReadBoolean();
	required_need_working = br->ReadInt64(); 
	has_working = br->ReadBoolean(); 
	required_need_xianji = br->ReadInt64();
	has_xianji = br->ReadBoolean(); 
	required_need_gender = br->ReadInt32();  
	has_gender = br->ReadBoolean(); 
	shengwang_guidao = br->ReadInt32(); //99
    shengwang_qingyun = br->ReadInt32();
    shengwang_tianyin = br->ReadInt32();
    shengwang_guiwang = br->ReadInt32();
    shengwang_hehuan = br->ReadInt32();
    shengwang_zhongyi = br->ReadInt32();
    shengwang_qingyuan = br->ReadInt32();
    shengwang_wencai = br->ReadInt32();
    shengwang_shide = br->ReadInt32();
    shengwang_yuliu_01 = br->ReadInt32();
    shengwang_yuliu_02 = br->ReadInt32();
    shengwang_yuliu_03 = br->ReadInt32();
    shengwang_yuliu_04 = br->ReadInt32();
    shengwang_jiuli = br->ReadInt32();
    shengwang_lieshan = br->ReadInt32();
    shengwang_chenhuang = br->ReadInt32();
    shengwang_taihao = br->ReadInt32();
    shengwang_huaiguang = br->ReadInt32();
    shengwang_tianhua = br->ReadInt32();
    shengwang_fenxiang = br->ReadInt32();
    shengwang_unknown_48 = br->ReadBytes(48); //223
	has_shengwang = br->ReadBoolean();
	front_tasks_count = br->ReadInt32();
	front_tasks = gcnew array<int>(5);
	for(int j=0; j<front_tasks->Length; j++ )//conuter check?
	{	
		front_tasks[j] = br->ReadInt32();
	}
	has_show_tasks = br->ReadBoolean();
	front_tasks_num_count = br->ReadInt32();
	front_tasks_a = gcnew array<int>(5);
	front_tasks_num = gcnew array<__int16>(5);
	for (int k = 0; (k < front_tasks_a->Length); k++)
    {
        front_tasks_a[k] = br->ReadInt32();
        front_tasks_num[k] = br->ReadInt16();
    }
	//283
	front_tasks_yaoqiu = br->ReadInt32();
	front_tasks_renwu = br->ReadInt32();
	front_tasks_zhuanchonglv = br->ReadInt32();
	front_tasks_showbangpai = br->ReadBoolean();
	UNKNOWN_114 = br->ReadBytes(4);
	front_tasks_showgender = br->ReadBoolean();
	front_tasks_yaoqiubanpai = br->ReadBoolean();
	front_tasks_xingbie = br->ReadInt32();
	how_unknown01 = br->ReadBoolean();
	job_nums = br->ReadInt32();
	jobs = gcnew array<int>(32);
	for(int j=0; j<jobs->Length; j++ )//conuter check?
	{	
		jobs[j] = br->ReadInt32();
	}
	required_be_married = br->ReadBoolean(); //440
	front_tasks_marry = br->ReadBoolean(); //till now
	if(version>=165){
		UNKNOWN_115 = br->ReadBytes(61);
	}
	else
	{
		UNKNOWN_115 = br->ReadBytes(9);
	}
	mutex_tasks_num = br->ReadInt32();
	mutexs = gcnew array<int>(5);
	for(int j=0; j<mutexs->Length; j++ )
	{	
		mutexs[j] = br->ReadInt32();
	}
	UNKNOWN_116 = br->ReadBytes(32);
	UNKNOWN_31 = br->ReadBoolean();
	pk_min = br->ReadInt32();
	pk_max = br->ReadInt32(); 
	required_be_gm = br->ReadBoolean();
	Team_asks = br->ReadBoolean();
	Team_share_again = br->ReadBoolean();
	Team_UNKNUWN_01 = br->ReadBoolean();
	Team_share_now = br->ReadBoolean();
	UNKNOWN_118 = br->ReadBytes(5);
	Team_check_mem = br->ReadBoolean();
	Team_duizhang_shibai = br->ReadBoolean();
	Team_quan_shibai = br->ReadBoolean();
	UNKNOWN_119 = br->ReadBytes(6);
	Team_leave_shibai = br->ReadBoolean(); //20
	UNKNOWN_120 = br->ReadBytes(10);
	required_team_member_groups_count = br->ReadInt32();
	UNKNOWN_121 = br->ReadBytes(4);
    UNKNOWN_122 = br->ReadBytes(4);
    UNKNOWN_123 = br->ReadBytes(4);
	Shitu_yaoqiu = br->ReadBoolean();
    Shitu_shitu = br->ReadBoolean();
	Shitu_chushi = br->ReadBoolean();
	Shitu_pantaohui = br->ReadBoolean();
    Shitu_UNKNOWN_01 = br->ReadBoolean();
    Shitu_UNKNOWN_02 = br->ReadBoolean(); //52
    Shitu_lv = br->ReadInt32();
    Shitu_tasks = br->ReadInt32();
    UNKNOWN_124 = br->ReadBytes(56); //116
    Jiazu_jiazu_tasks = br->ReadBoolean();
    Jiazu_zuzhang_tasks = br->ReadBoolean();
    Jiazu_jineng_lv_min = br->ReadInt32();
    Jiazu_jineng_lv_max = br->ReadInt32();
    Jiazu_jineng_shulian_min = br->ReadInt32();
    Jiazu_jineng_shulian_max = br->ReadInt32();
	Jiazu_jineng_id = br->ReadInt32();
    Jiazu_map_id = br->ReadInt32();
    Jiazu_tiaozhan_min = br->ReadInt32();
    Jiazu_tiaozhan_max = br->ReadInt32();
    UNKNOWN_125 = br->ReadBytes(4);
    BangLing_kouchu = br->ReadBoolean(); //155
    BangLing_shuliang = br->ReadInt32();
    UNKNOWN_126 = br->ReadBytes(4);
    front_tasks_feisheng_01 = br->ReadBoolean();
    front_tasks_feisheng_02 = br->ReadBoolean();
    front_tasks_feisheng = br->ReadBoolean(); //166
    UNKNOWN_127 = br->ReadBytes(103); //269
    zhenying = br->ReadInt32();
    Ingot_min = br->ReadInt32();
    Ingot_max = br->ReadInt32();
	unkn_1330_3 =  br->ReadInt32();
	unkn_1330_4 =  br->ReadInt32();
	if(version>=165){
		unkn_1380_1=br->ReadBytes(39);
	}
    is_val = br->ReadBoolean(); //290
    Nval_1 = br->ReadBytes(16);
    Nval_1_len = br->ReadInt32();
    Nval_2 = br->ReadBytes(32);
    Nval_2_len = br->ReadInt32();
    Nval_3 = br->ReadBytes(28);
    Nval_3_len = br->ReadInt32();
    Nval_4 = br->ReadBytes(32);
    Nval_4_len = br->ReadInt32();
	UNKNOWN_128 = br->ReadBytes(90); 
    UNKNOWN_Soul_01 = br->ReadInt32();
    Soul_min = br->ReadInt32();
    Soul_max = br->ReadInt32();
    Soul_min_on = br->ReadBoolean();
    Soul_max_on = br->ReadBoolean();
	if(version>=165){
		unk1380int1 = br->ReadInt32();
		unk1380int2 = br->ReadInt32(); //alliance building
		unk1380int3 = br->ReadInt32(); //quest lvl?
		Unknown_1628 = br->ReadBytes(51); 
	}
	else
	{
		Unknown_1628 = br->ReadBytes(21); 
	}
    JinXingFangShi = br->ReadInt32();
    WanChengLeiXing = br->ReadInt32();
    required_chases_count = br->ReadInt32();
    UNKNOWN_129 = br->ReadBytes(5);
    required_get_items_count = br->ReadInt32();
    UNKNOWN_234556 = br->ReadBytes(4);
    HuaFeiJinQian = br->ReadInt32();
    SafeNPC = br->ReadInt32();
    SafeNpcTime = br->ReadInt32();
	if(version>=165){
		UNKNOWN_130a = br->ReadBytes(17); //41
		UNKNOWN_130b = br->ReadInt32();
		UNKNOWN_130 = br->ReadBytes(91); //41
	}
	else{
		UNKNOWN_130 = br->ReadBytes(8); //41
	}
	reach_locations = gcnew LocationSpan();
	reach_locations->has_location = br->ReadBoolean();
	reach_locations->count = 1;
	reach_locations->spans = gcnew array<Span^>(fail_locations->count);
	reach_locations->spans[0] = ReadSpan(version, br);
	reach_locations->unknown_1 = gcnew array<unsigned char>(4); //todelete?
	UNKNOWN_131 = br->ReadBytes(73); //73
	reach_locations->map_id = br->ReadInt32();
    required_wait_time = br->ReadInt32();
	UNKNOWN_132 = br->ReadBytes(28); 
	Title_count = br->ReadInt32();
	TitleS = gcnew array<__int16>(5);
	for (int num25 = 0; (num25 < TitleS->Length); num25++)
    {
		TitleS[num25] = br->ReadInt16();
    }
	needlv = br->ReadInt32();
    UNKNOWN_133 = br->ReadBytes(39); //38
    is_val_1 = br->ReadBoolean();
    sNval_1 = br->ReadBytes(0x10);
    sNval_1_len = br->ReadInt32();
    sNval_2 = br->ReadBytes(0x20);
    sNval_2_len = br->ReadInt32();
    sNval_3 = br->ReadBytes(0x1c);
    sNval_3_len = br->ReadInt32();
    sNval_4 = br->ReadBytes(0x20);
    sNval_4_len = br->ReadInt32();
    sNval_5 = br->ReadBytes(32); //24 1330
    reward_type = br->ReadInt32();
    reward_type2 = br->ReadInt32();
    UNKNOWN_134 = br->ReadBytes(32); 
	parent_quest = br->ReadInt32();
	previous_quest = br->ReadInt32();
    next_quest = br->ReadInt32();
	sub_quest_first = br->ReadInt32();
	if(author_mode)
	{
		author_text=br->ReadBytes(60);
	}
	else author_text = gcnew array<unsigned char>(0);
	if(version>=165 && UNKNOWN_130b > 0){
		NEW_FLY_TYPE1380 = br->ReadBytes(8*UNKNOWN_130b); 
	}
	date_spans = gcnew array<DateSpan^>(date_spans_count);
	for(int i=0; i<date_spans->Length; i++)
	{
		date_spans[i] = ReadDateSpan(version, br);
	}
	tops = gcnew array<__int16>(required_reputation); //required_need_contribution
	for(int i=0; i<tops->Length; i++)
	{
		tops[i] = br->ReadInt16();
	}
	if(global_value_set1->val_count>0){
		global_value_1_items = gcnew array<ValueItem^>(global_value_set1->val_count); //ValueItem();
		for(int num=0;num<global_value_1_items->Length;num++)
		{
			global_value_1_items[num]=gcnew ValueItem();
			global_value_1_items[num] = ReadValueItem(version, br);
		}
	}
	//########### REQUIRED ITEMS #############################
	required_items = gcnew array<Item^>(required_items_count);
	for(int i=0; i<required_items->Length; i++)
	{
		required_items[i] = ReadItem(version, br);
	}
	// ################# GIVEN ITEMS #############################
	given_items = gcnew array<Item^>(given_items_count);
	for(int i=0; i<given_items->Length; i++)
	{
		given_items[i] = ReadItem(version, br);
	}
	// ################# DYNAMICS #############################
	dynamics = gcnew array<DynamicsTask^>(dynamics_count);
	//if(id==14144) ByteArray_to_HexString(SelectedTask->UNKNOWN_129);
	for(int i=0; i<dynamics->Length; i++)
	{
		dynamics[i] = ReadDynamics(version, br);
	}
	if (is_val)
    {
		Nval_unknown_01 = br->ReadBytes(Nval_1_len);
		Nval_unknown_02 = br->ReadBytes(Nval_2_len);
        Nval_unknown_03 = br->ReadBytes(Nval_3_len);
        Nval_unknown_04 = br->ReadBytes(Nval_4_len);
    }
	if (is_val_1)
    {
		Nval_unknown_05 = br->ReadBytes(sNval_1_len);
		Nval_unknown_06 = br->ReadBytes(sNval_2_len);
        Nval_unknown_07 = br->ReadBytes(sNval_3_len);
        Nval_unknown_08 = br->ReadBytes(sNval_4_len);
    }
	//######## TEAM MEMBERS #############################
	required_team_member_groups = gcnew array<TeamMembers^>(required_team_member_groups_count);
	for(int i=0; i<required_team_member_groups->Length; i++)
	{
		required_team_member_groups[i] = ReadTeamMembers(version, br);
	}
	// ################# CHASE #############################
	required_chases = gcnew array<Chase^>(required_chases_count);
	for(int i=0; i<required_chases->Length; i++)
	{
		required_chases[i] = ReadChase(version, br);
	}
	// ################# GET ITEMS #############################
	required_get_items = gcnew array<Item^>(required_get_items_count);
	for(int i=0; i<required_get_items->Length; i++)
	{
		required_get_items[i] = ReadItem(version, br);
	}
	//################# SUCCESS REWARDS #############################

	reward_success = ReadReward(version, br);
	if(reward_success->is_special_reward)
    {
		if (reward_success->special_flg == 1)
        {
			reward_team_member = ReadReward(version, br);
			UNKNOWN_7 = br->ReadBytes(4);
        }
        if (reward_success->special_flg == 2)
        {
			reward_team_leader = ReadReward(version, br);
			UNKNOWN_7 = br->ReadBytes(4);
        }
        if (reward_success->special_flg == 4)
        {
			reward_teacher = ReadReward(version, br);
			if(reward_success->Shitu_01&&reward_success->exp_formula_len>0){
				reward_teacher->exp_val = gcnew EXP_formalu();
				reward_teacher->exp_val->name = br->ReadBytes(reward_success->exp_formula_len);
				reward_teacher->exp_val->count = br->ReadInt32();
				reward_teacher->exp_val->val = gcnew array<EXP_Formalu^>(reward_teacher->exp_val->count);
				for (int m = 0; (m < reward_teacher->exp_val->val->Length); m++)
				{
					reward_teacher->exp_val->val[m] = ReadExpValItems(version, br);
				}
			}
			UNKNOWN_7 = br->ReadBytes(4);
        }
    }
	//if(reward_success->special_flg == 4) MessageBox::Show("TEST: " + reward_success->Shitu_01);
	// ################# FAILED REWARDS #############################
	reward_failed = ReadReward(version, br);
	unknown_202=br->ReadBytes(48); 
	if (reward_type != 0)
    {
		if (reward_type == 1)
		{
			UNKNOWN_end = br->ReadBytes(28);
			reward_items_count_2 = br->ReadInt32();
			reward_items_id_2 = br->ReadInt32();
			reward_items_group_2 = gcnew array<int>(5);
			for (int num13 = 0; (num13 < reward_items_group_2->Length); num13++)
			{
				reward_items_group_2[num13] = br->ReadInt32();
			}
			rewards_items_2 = gcnew array<Reward^>(reward_items_count_2);
			for (int num12 = 0; (num12 < rewards_items_2->Length); num12++)
			{
				rewards_items_2[num12] = ReadReward(version, br);
			}
			UNKNOWN_end2 = br->ReadBytes(48);
          }
    }
	else
	{
                if (reward_type2 != 3)
                {
                    UNKNOWN_end = br->ReadBytes(104);
                }
                else
                {
                    UNKNOWN_end = br->ReadBytes(28);
                    reward_items_count_2 = br->ReadInt32();
                    reward_items_id = br->ReadInt32();
                    reward_items_id_2 = reward_items_id;
                    reward_items_group_2 = gcnew array<int>(5);
                    for (int num13 = 0; (num13 < reward_items_group_2->Length); num13++)
                    {
                        reward_items_group_2[num13] = br->ReadInt32();
                    }
                    rewards_items_2 = gcnew array<Reward^>(reward_items_count_2);
                    for (int num12 = 0; (num12 < rewards_items_2->Length); num12++)
                    {
                        rewards_items_2[num12] = ReadReward(version, br);
                    }
                    UNKNOWN_end2 = br->ReadBytes(48);
                }
                goto Label_1451; 
	}
	if (reward_type == 2)
	{		
		// ################# TIMED REWARDS #############################
		//MessageBox::Show("timed!"+id+"/"+reward_success->is_special_reward);
		rewards_timed_count = br->ReadInt32();
		rewards_timed_factors = gcnew array<float>(5);
		for(int i=0; i<rewards_timed_factors->Length; i++)
		{
			rewards_timed_factors[i] = br->ReadSingle();
		}
		rewards_timed = gcnew array<Reward^>(rewards_timed_count);
		for(int i=0; i<rewards_timed->Length; i++)
		{
			rewards_timed[i] = ReadReward(version, br);
		}
		UNKNOWN_end2 = br->ReadBytes(0x30);
	}
	if (reward_type == 3)
	{
                reward_items_count = br->ReadInt32();
				reward_items_id = br->ReadInt32();
				reward_items_group = gcnew array<int>(5);
				for (int num13 = 0; (num13 < reward_items_group->Length); num13++)
                {
					reward_items_group[num13] = br->ReadInt32();
                }
				rewards_items_new = gcnew array<Reward^>(reward_items_count);
                for (int num12 = 0; (num12 < rewards_items_new->Length); num12++)
                {
					rewards_items_new[num12] = ReadReward(version, br);
                }
                UNKNOWN_end2 = br->ReadBytes(0x4c);
	}
	if (reward_type == 4)
	{
				UNKNOWN_end = br->ReadBytes(0x38);
                reward_num_count = br->ReadInt32();
                reward_num  = gcnew array<int>(5);
                for (int num7 = 0; (num7 < reward_num->Length); num7++)
                {
                    reward_num[num7] = br->ReadInt32();
                }
                rewards_num = gcnew array<Reward^>(reward_num_count);
                for (int num6 = 0; (num6 < rewards_num->Length); num6++)
                {
                    rewards_num[num6] = ReadReward(version, br);
                }
                UNKNOWN_end2 = br->ReadBytes(0x18);
	}
Label_1451:
		conversation = gcnew Conversation();
		conversation->crypt_key = id;
		conversation->prompt_character_count = br->ReadInt32();
		//if(id==29175) MessageBox::Show("promt!"+id+"\n"+conversation->prompt_character_count+"\n");
		conversation->prompt_text = br->ReadBytes(2*conversation->prompt_character_count);
		conversation->agern_character_count = br->ReadInt32();
		conversation->agern_text = br->ReadBytes(2*conversation->agern_character_count);
		conversation->seperator = br->ReadBytes(4);
		conversation->general_character_count = br->ReadInt32();
		conversation->general_text = br->ReadBytes(2*conversation->general_character_count);
		conversation->xuanfu_character_count = br->ReadInt32();
		conversation->xuanfu_text = br->ReadBytes(conversation->xuanfu_character_count * 2);
		conversation->new_character_count = br->ReadInt32();
		conversation->new_text = br->ReadBytes(conversation->new_character_count * 2);
		conversation->dialogs = gcnew array<Dialog^>(5);
		for(int d=0; d<conversation->dialogs->Length; d++)
		{
			conversation->dialogs[d] = gcnew Dialog();
			conversation->dialogs[d]->crypt_key = id;
			conversation->dialogs[d]->unknown = br->ReadInt32();
			conversation->dialogs[d]->dialog_text = br->ReadBytes(128);
			conversation->dialogs[d]->question_count = br->ReadInt32();
			conversation->dialogs[d]->questions = gcnew array<Question^>(conversation->dialogs[d]->question_count);
			for(int q=0; q<conversation->dialogs[d]->questions->Length; q++)
			{
				conversation->dialogs[d]->questions[q] = gcnew Question();
				conversation->dialogs[d]->questions[q]->crypt_key = id;
				conversation->dialogs[d]->questions[q]->question_id = br->ReadInt32();
				conversation->dialogs[d]->questions[q]->previous_question = br->ReadInt32();
				conversation->dialogs[d]->questions[q]->question_character_count = br->ReadInt32();
				conversation->dialogs[d]->questions[q]->question_text = br->ReadBytes(2*conversation->dialogs[d]->questions[q]->question_character_count);
				conversation->dialogs[d]->questions[q]->answer_count = br->ReadInt32();
				conversation->dialogs[d]->questions[q]->answers = gcnew array<Answer^>(conversation->dialogs[d]->questions[q]->answer_count);
				for(int a=0; a<conversation->dialogs[d]->questions[q]->answer_count; a++)
				{
					conversation->dialogs[d]->questions[q]->answers[a] = gcnew Answer();
					conversation->dialogs[d]->questions[q]->answers[a]->crypt_key = id;
					conversation->dialogs[d]->questions[q]->answers[a]->question_link = br->ReadInt32();
					conversation->dialogs[d]->questions[q]->answers[a]->answer_text = br->ReadBytes(128);
					conversation->dialogs[d]->questions[q]->answers[a]->task_link = br->ReadInt32();
				}
			}
		}
	
		// ################# SUB TASKS #############################
		sub_quest_count = br->ReadInt32();

		//if(id==12954) MessageBox::Show("sub["+id+"]"+sub_quest_count);
		sub_quests = gcnew array<Task^>(sub_quest_count);
		for(int i=0; i<sub_quest_count; i++)
		{
			if(nodes)
			{
				sub_quests[i] = gcnew Task(version, br, (int)br->BaseStream->Position, nodes[nodes->Count-1]->Nodes);
			}
			else
			{
				sub_quests[i] = gcnew Task(version, br, (int)br->BaseStream->Position, nullptr);
			}
		}
	//}
	//catch(Exception^ e)
	//{
	//	MessageBox::Show("ERROR loading task ID: "+id+"\n" + e->Message);
	//}
}