#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Text;
using namespace System::Windows::Forms;

String^ decrypt(int key, array<unsigned char>^ text);
array<unsigned char>^ encrypt(int key, String^ text, int length, bool appendZero);
String^ ByteArray_to_HexString(array<unsigned char>^ value);
array<unsigned char>^ HexString_to_ByteArray(String^ value);
String^ ByteArray_to_GbkString(array<unsigned char>^ text);
array<unsigned char>^ GbkString_to_ByteArray(String^ text, int length);
String^ ByteArray_to_UnicodeString(array<unsigned char>^ text);
array<unsigned char>^ UnicodeString_to_ByteArray(String^ text, int length);

public ref struct Map_Location
{
	int map_id;
	float x;
	float altitude;
	float z;
};

public ref struct RewardItem
{
	int id;
	bool unknown;
	int amount;
	float probability;
	bool flg;
	int expiration;
	int unkown_01;
	unsigned char unkown_02;
	int RefLevel;
	int unkown_03;
};

public ref struct Item
{
	int id;
	bool unknown;
	int amount;
	float probability;
	int expiration;
	array<unsigned char>^ unkown_01;
};

public ref struct DynamicsTask
{
	int id;
	Map_Location^ point;
	int times;
	bool unknown_1;
	bool unknown_2;
	bool unknown_3;
	bool unknown_4;
	bool unknown_5;
};


public ref struct ItemGroup
{
	bool type;
	int items_count;
	array<RewardItem^>^ items;
};

public ref struct MoraiPK
{
	int unknown_1;
	int unknown_2;
	int unknown_3;
	unsigned char unknown_4;
	float probability;
	int class_mask;
	int level_min;
	int level_max;
	int unknown_5;
	int type;
};

public ref struct Chase
{
	int id_monster;
	int amount_monster;
	int id_drop;
	int amount_drop;
	unsigned char unknown_1;
	float probability;
	unsigned char unknown_2;
	array<unsigned char>^ unknown_3;
};

public ref struct Date
{
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int weekday;
};

public ref struct DateSpan
{
	__int16 period;
	Date^ from;
	Date^ to;
};

public ref struct Span
{
	float east;
	float bottom;
	float south;
	float west;
	float top;
	float north;
};

public ref struct ValueSet
{
	int val_1;
	int val_2;
	int val_3;
	int val_count;
};

public ref struct ValueItem
{
	int val_1;
	int val_2;
	bool val_3;
};

public ref struct LocationSpan
{
	bool has_location;
	int map_id;
	int count;
	array<Span^>^ spans;
	array<unsigned char>^ unknown_1; // 4 Byte
};

public ref struct TeamMembers
{
	int amount_max;
    int amount_min;
    int gender;
    int job;
    int level_max;
    int level_min;
    int quest;
    int race;
    int Unknown_01;
    bool Unknown_02;
};
/*
public ref struct TeamMembers
{
	int level_min;
	int level_max;
	int unknown_1;
	int unknown_2;
	int unknown_3;
	int amount_min;
	int amount_max;
	int quest;
	int force;
};
*/

public ref struct PQ_Chase
{
	int id_monster;
	int amount_monster;
	float probability;
	int amount_unknown;
};
public ref struct PQ_Item
{
	int ranking_from;
	int ranking_to;
	bool unknown_3;
	int id;
	int amount;
	float probability;
};

public ref struct EXP_Formalu
{
	int val_1;
	float val_2;
};

public ref struct EXP_formalu
{
	int count;
	array<unsigned char>^ name;
	array<EXP_Formalu^>^ val;
};



public ref struct PQ_Special
{
	int id_pq;
	int unknown_2;
	unsigned char unknown_3;
};

public ref struct PQ_Reward
{
	int chase_count; // public reward chases count
	array<unsigned char>^ unknown_0; // 6 Byte
	int event_gold;
	array<unsigned char>^ unknown_1; // 9 Byte
	int unknown_quest; // New Quest ???
	array<unsigned char>^ unknown_2; // 4 Byte
	int unknown_level; // Level ???
	int contribution_random_min; // random contribution min
	int contribution_random_max; // random contribution max
	int contribution_required; // required reward contribution
	array<unsigned char>^ unknown_3; // 8 Byte
	int item_count; // public reward items count
	array<unsigned char>^ unknown_4; // 4 Byte
	int special_count; // public reward specials count
	array<unsigned char>^ unknown_5; // 29 Byte
	int script_count; // public reward scripts count
	array<unsigned char>^ unknown_6; // 8 Byte
	int message_count; // public reward messages count
	array<unsigned char>^ unknown_7; // 4 Byte

	bool chase_unknown_1;
	int chase_unknown_2;
	bool chase_unknown_3;
	array<PQ_Chase^>^ chases;

	bool item_unknown_1;
	array<PQ_Item^>^ items;

	array<PQ_Special^>^ specials;

	array<array<unsigned char>^>^ scripts; // 576 byte / script

	array<array<unsigned char>^>^ messages; // 128 byte / message
};


public ref struct ShengWang
    {
        // Fields
        int ChenHuang;
        int DaoXin;
        int DiZong;
        int FenXiang;
        int FoYuan;
        int GuiDao;
        int GuiWang;
        int HeHuan;
        int HuaiGuang;
        int JiuLi;
        int LieShan;
        int MoXing;
        int QingYuan;
		int QingYun;
        int RenZong;
        int ShiDe;
        int TaiHao;
        int TianHua;
        int TianYin;
        int TianZong;
        int unknow_15;
        int WenCai;
        int ZhanJi;
        int ZhanXun;
        int ZhongYi;
		int Etherkin_01;
		int Etherkin_02;
		int Etherkin_03;
		int Etherkin_04;
};

public ref struct Reward
{
	int coins;
	__int64 experience;
	int unkn_1330_6;
	int unkn_1330_7;
	int unkn_1330_8;
	int unkn_1330_9;
	array<unsigned char>^ unkn_1380_1;
	int unkn_1380_2;
	int first_time_count;
	bool unkn_1330_10;
	float ExpLv;
	float RCLv;
	float APLv;
	int new_quest;
	int unknow_01;
	int unknow_02;
	int unknow_03;
	int unknow_04;
	int unknow_05;
	int unknow_06;
	int unknow_07;
	int unknow_08;
	int unknow_09;
	int unknow_10;
	int group_integral;
	int unknow_12;
	int unknow_12_1;
	int fation_gongxian;
	int family_gongxian;
	int unknow_13;
	int title;
	int removePK;
	bool clearPK;
	bool Divorce;
	ShengWang^ sw;
	int unknow_20;
	int unknow_21;
	int unknow_22;
	int unknow_23;
	int unknow_31;
	int unknow_25;
	int unknow_26;
	int unknow_27;
	int unknow_28;
	int unknow_29;
	int Cangku;
	int unknow_30;
	int Beibao;
	int CWBeibao;
	int CWlan;
	int ZuoqiBeibao;
	int unknow_31_1;
	bool ShengChan_UP;
	int unknow_32;
	int jobtmp_up;
	int unknow_33;
	int SetJob;
	bool trigger_on;
	int unknow_35;
	int unknow_36;
	int ShuLianDu;
	int SkillsLV;
	int family_skill_id;
	int family_lingqi;
	int unknow_40;
	int fation_lingqi;
	bool Notice;
	int Notice_Channel;
	int removetasks;
	int DoubleTimes;
	int unknow_42;
	int unknow_43;
	bool ChuShi;
	bool PanShi;
	bool Shitu_01;
	bool Shitu_02;
	bool is_special_reward;
	int special_flg;
	int unknow_46;
	bool Camp_on;
	int Camp;
	int skillcount;
	bool skill_on;
	bool skill_Tianshu_on;
	int is_exp_formula;
	int exp_formula_len;
	array<ValueSet^>^ value_set;
	array<unsigned char>^ unknow_48;
	array<unsigned char>^ unknow_49;
	int Bianshen;
	int BianshenTime;
	int unknow_50;
	int unknow_51;
	bool unknow_52;
	int YuanShen;
	bool YuanShenlv;
	bool OpenYuanYing;
	int HongLiIngot;
	int unknown_53;
	int unknown_54; //
	int unknown_55;
	int unknown_56;
	int unknown_n1;
	int unknown_n2;
	float unknown_n3;
	int unknown_n4;
	array<unsigned char>^ nval_1;
	array<unsigned char>^ nval_2;
	array<unsigned char>^ nval_3;
	array<unsigned char>^ nval_4;
	bool is_nval;
	int nval_len_1;
	int nval_len_2;
	array<unsigned char>^ nval_5;
	array<unsigned char>^ nval_6;
	int noticetext_len;
	array<unsigned char>^ noticetext;
	int spirit;
	int reputation;
	int cultivation;
	int new_waypoint; // 4 Byte
	int storage_slots;
	int cupboard_slots;
	int wardrobe_slots;
	int account_stash_slots;
	int inventory_slots;
	int petbag_slots;
	int vigor;
	Map_Location^ teleport;
	int ai_trigger;
	array<unsigned char>^ UNKNOWN_2a; // 3 Byte
	array<unsigned char>^ UNKNOWN_2b; // 5 Byte
	int item_groups_count;
	array<unsigned char>^ SEPERATOR; // 4 Byte
	PQ_Reward^ pq;
	int influence;
	int prestige;
	int UNKNOWN_5;
	int UNKNOWN_6;
	int quest_slot_expansion;
	array<ItemGroup^>^ item_groups;
	array<ItemGroup^>^ first_time_reward;
	array<ValueItem^>^ Valitems;
	EXP_formalu^ exp_val;
};

public ref struct PQ_AuditScriptInfo
{
	int id;
	int unknown_1;
	array<unsigned char>^ unknown_2; // 1 Byte
};

public ref struct PQ_AuditScript
{
	array<unsigned char>^ name; // 64 Byte
	public: property String^ Name
	{
		String^ get()
		{
			return ByteArray_to_GbkString(name);
		}
		void set(String^ value)
		{
			name = GbkString_to_ByteArray(value, 64);
		}
	}
	int count;
	float id;
	array<unsigned char>^ seperator; // 4 Byte
	float reference_id;
	array<unsigned char>^ code; // 496 Byte
	public: property String^ Code
	{
		String^ get()
		{
			//return ByteArray_to_GbkString(code);
			return ByteArray_to_HexString(code);
		}
		void set(String^ value)
		{
			//code = GbkString_to_ByteArray(value, 496);
			code = HexString_to_ByteArray(value);
		}
	}
};

public ref struct PQ_AuditChase
{
	int id_monster;
	int amount_1;
	int contribution;
	int amount_3;
};

public ref struct PQ_Audit
{
	int script_info_count; // Unknown Count
	array<PQ_AuditScriptInfo^>^ script_infos;
	array<unsigned char>^ unknown_1; // 12 Byte
	int unknown_2;
	array<unsigned char>^ unknown_3; // 15 Byte
	int script_count; // Scripts Count
	array<PQ_AuditScript^>^ scripts;
	array<unsigned char>^ unknown_4; // 8 Byte
	array<unsigned char>^ unknown_5; // 1 Byte
	array<unsigned char>^ unknown_6; // 1 Byte
	int chase_count;
	array<PQ_AuditChase^>^ chases;
	array<unsigned char>^ unknown_7; // 4 Byte
	int required_quests_completed;
	array<unsigned char>^ unknown_8; // 1 Byte
	array<unsigned char>^ unknown_9; // 5 Byte
	array<unsigned char>^ unknown_10; // 5 Byte
	array<unsigned char>^ unknown_11; // 4 Byte
	LocationSpan^ location;
	int unknown_12;
	array<unsigned char>^ unknown_13; // 4 Byte
	int id_script;
	int unknown_14;
	int unknown_15;
	int unknown_16;
	array<unsigned char>^ unknown_17; // 20 Byte
	int special_script_count;
	array<PQ_AuditScript^>^ special_scripts;
	array<unsigned char>^ unknown_18; // 4 Byte
	array<unsigned char>^ unknown_19; // 4 Byte
	int message_count;
	array<array<unsigned char>^>^ messages; // 128 byte / message
	array<unsigned char>^ unknown_20; // 4 Byte
};

public ref struct Answer
{
	int crypt_key;
	int question_link;
	array<unsigned char>^ answer_text;
	// 128 Byte Unicode
	public: property String^ AnswerText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(answer_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(128);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			answer_text = target;
		}
		/*
		String^ get()
		{
			return decrypt(crypt_key, answer_text);
		}
		void set(String^ value)
		{
			answer_text = encrypt(crypt_key, value, 128, false);
		}
		*/
	}
	int task_link;
};

public ref struct Question
{
	int crypt_key;
	int question_id;
	int previous_question;
	int question_character_count;
	array<unsigned char>^ question_text;
	// 2*question_character_count Byte Unicode
	public: property String^ QuestionText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(question_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			question_text = target;
			question_character_count = question_text->Length/2;
		}
		/*
		String^ get()
		{
			return decrypt(crypt_key, question_text);
		}
		void set(String^ value)
		{
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			question_text = encrypt(crypt_key, value, 2*value->Length + 2, true);
			question_character_count = question_text->Length/2;
		}
		*/
	}
	int answer_count;
	array<Answer^>^ answers;
};

public ref struct Dialog
{
	int crypt_key;
	int unknown;
	array<unsigned char>^ dialog_text;
	// 128 Byte Unicode
	public: property String^ DialogText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(dialog_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(128);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			dialog_text = target;
		}
		/*
		String^ get()
		{
			return decrypt(crypt_key, dialog_text);
		}
		void set(String^ value)
		{
			dialog_text = encrypt(crypt_key, value, 128, false);
		}
		*/
	}
	int question_count;
	array<Question^>^ questions;
};

public ref struct Conversation
{
	int crypt_key;
	int prompt_character_count;
	int agern_character_count;
	int xuanfu_character_count;
	int new_character_count;
	array<unsigned char>^ prompt_text;
	array<unsigned char>^ agern_text;
	array<unsigned char>^ xuanfu_text;
	array<unsigned char>^ new_text;
	// 2*prompt_character_count Byte Unicode
	public: property String^ NewText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(new_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			new_text = target;
			new_character_count = new_text->Length/2;
		}
	}
	public: property String^ XuanfuText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(xuanfu_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			xuanfu_text = target;
			xuanfu_character_count = xuanfu_text->Length/2;
		}
	}
	public: property String^ AgernText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(agern_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			agern_text = target;
			agern_character_count = agern_text->Length/2;
		}
	}
	public: property String^ PromptText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(prompt_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			prompt_text = target;
			prompt_character_count = prompt_text->Length/2;
		}
		/*
		String^ get()
		{
			return decrypt(crypt_key, prompt_text);
		}
		void set(String^ value)
		{
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			prompt_text = encrypt(crypt_key, value, 2*value->Length + 2, true);
			prompt_character_count = prompt_text->Length/2;
		}
		*/
	}
	array<unsigned char>^ seperator;
	int general_character_count;
	array<unsigned char>^ general_text;
	// 2*general_character_count Byte Unicode
	public: property String^ GeneralText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(general_text);
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(2*value->Length + 2);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			general_text = target;
			general_character_count = general_text->Length/2;
		}
		/*
		String^ get()
		{
			return decrypt(crypt_key, general_text);
		}
		void set(String^ value)
		{
			value = value->TrimEnd(gcnew array<wchar_t>{'\0'});
			general_text = encrypt(crypt_key, value, 2*value->Length+2, true);
			general_character_count = general_text->Length/2;
		}
		*/
	}
	array<Dialog^>^ dialogs;
};

public ref class Task
{
	public: Task(void)
	{
	}
	public: Task(int version, BinaryReader^ BinaryStream, int BaseStreamPosition, TreeNodeCollection^ Nodes)
	{
		Load(version, BinaryStream, BaseStreamPosition, Nodes);
	}	

	protected: ~Task()
	{
	}

	private: int id;
	public: property int ID
	{
		int get()
		{
			return id;
		}
		void set(int value)
		{
			// Load strings with old encryption key
			String^ tmp_name = this->Name;
			String^ tmp_author = this->AuthorText;
			String^ tmp_prompt = this->conversation->PromptText;
			String^ tmp_general = this->conversation->GeneralText;

			// Change id / encryption key
			this->id = value;
			this->conversation->crypt_key = value;

			// Recode strings with new encryption key
			this->Name = tmp_name;
			this->AuthorText = tmp_author;
			this->conversation->PromptText = tmp_prompt;
			this->conversation->GeneralText = tmp_general;

			for(int d=0; d<this->conversation->dialogs->Length; d++)
			{
				String^ tmp_dialog = this->conversation->dialogs[d]->DialogText;
				this->conversation->dialogs[d]->crypt_key = value;
				this->conversation->dialogs[d]->DialogText = tmp_dialog;
				for(int q=0; q<this->conversation->dialogs[d]->questions->Length; q++)
				{
					String^ tmp_question = this->conversation->dialogs[d]->questions[q]->QuestionText;
					this->conversation->dialogs[d]->questions[q]->crypt_key = value;
					this->conversation->dialogs[d]->questions[q]->QuestionText = tmp_question;
					for(int a=0; a<this->conversation->dialogs[d]->questions[q]->answers->Length; a++)
					{
						String^ tmp_answer = this->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText;
						this->conversation->dialogs[d]->questions[q]->answers[a]->crypt_key = value;
						this->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText = tmp_answer;
					}
				}
			}
		}
	}
	private: array<unsigned char>^ name;
	// 60 Byte Unicode
	public: property String^ Name
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(name);//->Replace("\0", "");
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(60);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			name = target;
		}
	}
	public: bool author_mode;
	public: int UNKNOWN_01; // 4 Byte
	// [0] Normal
	// [1] Cycle
	// [2] Spiritual Cultivation
	// [3] Hero
	// [4] Challenge
	// [5] Adventure
	// [6] Errand
	// [7] Legendary
	// [8] Battle
	// [9] Public
	// [10] Divine Order?
	// [11] Guild Base?
	// [12] ?
	public: int constant1;
	public: int task_count;
	public: int counter2;
	public: int cunknown_1;
	public: int type;
	public: int time_limit;
	public: int cooldown;
	public: int max_player;
	public: int reset_type;
	public: int reset_cycle;
	public: int loop_mode;
	public: int	autotask_unknown;
	public: int	cleartask_unknown;
	public: bool has_trigger_on;
	public: bool cunknown_2;
	public: bool UNKNOWN_02;
	public: bool has_date_fail;
	public: Date^ date_fail;
	public: bool UNKNOWN_03;
	public: bool has_date_spans;
	public: int date_spans_count;
	public: int xunhanType;
	public: array<unsigned char>^ UNKNOWN_04; // 4 Bytes
	public: array<unsigned char>^ UNKNOWN_05; // 8 Bytes -> All Zeros
	public: array<unsigned char>^ UNKNOWN_06; // 12 Bytes -> All Zeros
	public: array<unsigned char>^ UNKNOWN_07; // 8 Bytes
	public: int UNKNOWN_08; // couple_type
	public: int UNKNOWN_09;
	public: bool activate_first_subquest;
	public: bool activate_random_subquest;
	public: bool activate_next_subquest;
	public: bool on_give_up_parent_fails;
	public: bool on_success_parent_success;
	public: bool can_give_up;
	public: bool repeatable;
	public: bool repeatable_after_failure;
	public: bool fail_on_death;
	public: bool on_fail_parent_fail;
	public: bool UNKNOWN_10;
	public: bool player_limit_on;
	public: bool all_zone;
	public: int player_limit;
	public: int player_limit_num;
	public: int all_zone_num;
	public: int unkpenalty;
	public: int playerpenalty;
	public: int allkillnum;
	public: bool haslimit;
	public: array<__int16>^ repu_tmp;
	public: array<unsigned char>^ UNKNOWN_157; 
	public: array<unsigned char>^ UNKNOWN_ZEROS; 
	public: array<unsigned char>^ UNKNOWN_112_1; 
	public: array<unsigned char>^ UNKNOWN_113;
	public: array<unsigned char>^ GlobalVal1;
	public: array<unsigned char>^ GlobalVal2;
	public: LocationSpan^ trigger_locations; // start on enter
	public: LocationSpan^ fail_locations; // fail on enter
	public: LocationSpan^ valid_locations; // fail on leave
	public: ValueSet^ global_value_set1;
	public: ValueSet^ global_value_set2;
	public: ValueSet^ global_value_set3;
	public: bool val_unknown_01;
	public: bool val_unknown_02;
	public: bool val_unknown_03;
	public: int new_type;
	public: int new_type2;
	public: bool UNKNOWN_17; // 1 Byte
	public: bool has_instant_teleport;
	public: Map_Location^ instant_teleport_location;
	public: int ai_trigger;
	public: bool UNKNOWN_18; // start/stop ai trigger ?
	public: bool UNKNOWN_19;
	public: bool UNKNOWN_20;
	public: bool UNKNOWN_21;
	public: bool UNKNOWN_22; // suicide required ?
	public: bool UNKNOWN_23;
	public: float component;
	public: int classify;
	public: int unkn_1330_1;
	public: bool unkn_1330_2;
	public: int unkn_1330_3;
	public: int unkn_1330_4;
	public: int unkn_1330_11;
	public: array<unsigned char>^ unkn_1380_1;
	public: bool unkn_1380_2;
	public: bool clan_task;
	public: int UNKNOWN_LEVEL;
	public: bool mark_available_icon;
	public: bool mark_available_point;
	public: int quest_npc;
	public: int reward_npc;
	public: bool craft_skill;
	public: bool UNKNOWN_24;
	public: bool UNKNOWN_25;
	public: bool UNKNOWN_26;
	public: PQ_Audit^ pq;
	public: bool LeaveFactionFail;
	public: bool NotClearItemWhenFailed;
	public: array<unsigned char>^ UNKNOWN_26_01;
	public: int level_min;
	public: int level_max;
	public: bool has_show_level;
	public: int bloodbound_min;
	public: int bloodbound_max;
	public: bool UNKNOWN_27; // required items?
	public: int required_items_count;
	public: int required_items_unknown;
	public: bool UNKNOWN_28; // given items?
	public: bool items_unknown_4;
	public: bool items_unknown_5;
	public: int items_unknown_6;
	public: int items_unknown_7;
	public: bool UNKNOWN_28_01;
	public: int dynamics_count;
	public: int given_items_count;
	public: int given_items_needBags;
	public: int items_unknown_9;
	public: int items_unknown_10;
	public: int items_unknown_11;
	public: bool required_items_bags;
	public: int instant_pay_coins;
	public: int	required_need_next;
	public: bool has_next;
	public: bool clan_required;
	public: int required_need_contribution;
	public: bool has_contribution;
	public: __int64 required_need_working;
	public: __int64 required_need_xianji;
	public: bool has_working;
	public: bool has_xianji;
	public: int required_need_gender;
	public: bool has_gender;
	public: int shengwang_guidao;
	public: int shengwang_qingyun;
	public: int shengwang_tianyin;
	public: int shengwang_guiwang;
	public: int shengwang_hehuan;
	public: int shengwang_zhongyi;
	public: int shengwang_qingyuan;
	public: int shengwang_wencai;
	public: int shengwang_shide;
	public: int shengwang_yuliu_01;
	public: int shengwang_yuliu_02;
	public: int shengwang_yuliu_03;
	public: int shengwang_yuliu_04;
	public: int shengwang_jiuli;
	public: int shengwang_lieshan;
	public: int shengwang_chenhuang;
	public: int shengwang_taihao;
	public: int shengwang_huaiguang;
	public: int shengwang_tianhua;
	public: int shengwang_fenxiang;
	public: array<unsigned char>^ shengwang_unknown_48;
	public: bool has_shengwang;
	public: int front_tasks_count;
	public: array<int>^ front_tasks;
	public: bool has_show_tasks;
	public: int front_tasks_num_count;
	public: array<int>^ front_tasks_a;
	public: array<__int16>^ front_tasks_num;
	public: int front_tasks_yaoqiu;
	public: int front_tasks_renwu;
	public: int front_tasks_zhuanchonglv;
	public: bool front_tasks_showbangpai;
	public: array<unsigned char>^ UNKNOWN_114;
	public: bool front_tasks_showgender;
	public: bool front_tasks_yaoqiubanpai;
	public: int front_tasks_xingbie;
	public: bool how_unknown01;
	public: int job_nums;
	public: bool front_tasks_marry;
	public: array<int>^ jobs;
	public: array<unsigned char>^ UNKNOWN_115;
	public: int mutex_tasks_num;
	public: array<int>^ mutexs;
	public: array<unsigned char>^ UNKNOWN_116;
	public: bool UNKNOWN_31;
	public: int pk_min;
	public: int pk_max;
	public: bool Team_asks;
	public: bool Team_share_again;
	public: bool Team_UNKNUWN_01;
	public: bool Team_share_now;
	public: array<unsigned char>^ UNKNOWN_118;
	public: bool Team_check_mem;
	public: bool Team_duizhang_shibai;
	public: bool Team_quan_shibai;
	public: array<unsigned char>^ UNKNOWN_119;
	public: bool Team_leave_shibai;
	public: array<unsigned char>^ UNKNOWN_120;
	public: int required_team_member_groups_count;
	public: array<unsigned char>^ UNKNOWN_121;
	public: array<unsigned char>^ UNKNOWN_122;
	public: array<unsigned char>^ UNKNOWN_123;
	public: bool Shitu_yaoqiu;
	public: bool Shitu_shitu;
	public: bool Shitu_chushi;
	public: bool Shitu_pantaohui;
	public: bool Shitu_UNKNOWN_01;
	public: bool Shitu_UNKNOWN_02;
	public: int Shitu_lv;
	public: int Shitu_tasks;
	public: array<unsigned char>^ UNKNOWN_124;
	public: bool Jiazu_jiazu_tasks;
	public: bool Jiazu_zuzhang_tasks;
	public: int Jiazu_jineng_lv_min;
	public: int Jiazu_jineng_lv_max;
	public: int Jiazu_jineng_shulian_min;
	public: int Jiazu_jineng_shulian_max;
	public: int Jiazu_jineng_id;
	public: int Jiazu_map_id;
	public: int Jiazu_tiaozhan_min;
	public: int Jiazu_tiaozhan_max;
	array<unsigned char>^ unknown_2055;
	public: array<unsigned char>^ UNKNOWN_125;
	public: bool BangLing_kouchu;
	public: int BangLing_shuliang;
	public: array<unsigned char>^ UNKNOWN_126;
	public: bool front_tasks_feisheng_01;
	public: bool front_tasks_feisheng_02;
	public: bool front_tasks_feisheng;
	public: array<unsigned char>^ UNKNOWN_127;
	public: int zhenying;
	public: int Ingot_min;
	public: int Ingot_max;
	public: bool is_val;
	public: array<unsigned char>^ Nval_1;
	public: array<unsigned char>^Nval_2;
	public: array<unsigned char>^Nval_3;
	public: array<unsigned char>^Nval_4;
	public: int Nval_1_len;
	public: int Nval_2_len;
	public: int Nval_3_len;
	public: int Nval_4_len;
	public: array<unsigned char>^ UNKNOWN_128;
	public: int UNKNOWN_Soul_01;
	public: int Soul_min;
	public: int Soul_max;
	public: bool Soul_min_on;
	public: bool Soul_max_on;
	public: array<unsigned char>^ Unknown_1628;
	public: int unk1380int3;
	public: int JinXingFangShi;
	public: int WanChengLeiXing;
	public: int required_chases_count;
	public: array<unsigned char>^ UNKNOWN_129;
	public: array<unsigned char>^ UNKNOWN_234556;
	public: int HuaFeiJinQian;
	public: int SafeNPC;
	public: int SafeNpcTime;
	public: array<unsigned char>^ UNKNOWN_130;
	public: array<unsigned char>^ UNKNOWN_130a;
	public: int UNKNOWN_130b;
	public: array<unsigned char>^ UNKNOWN_130c;
	public: float UNKNOWN_130d;
	public: array<unsigned char>^ UNKNOWN_131;
	public: int UNKNOWN_131a;
	public: array<unsigned char>^ UNKNOWN_131b;
	public: array<unsigned char>^ UNKNOWN_132;
	public: array<unsigned char>^ NEW_FLY_TYPE1380;
	public: int Title_count;
	public: array<__int16>^ TitleS;
	public: int needlv;
	public: array<unsigned char>^ UNKNOWN_133;
	public: bool is_val_1;
	public: array<unsigned char>^ sNval_1;
	public: array<unsigned char>^ sNval_2;
	public: array<unsigned char>^ sNval_3;
	public: array<unsigned char>^ sNval_4;
	public: array<unsigned char>^ sNval_5;
	public: array<unsigned char>^ Nval_unknown_01;
	public: array<unsigned char>^ Nval_unknown_02;
	public: array<unsigned char>^ Nval_unknown_03;
	public: array<unsigned char>^ Nval_unknown_04;
	public: array<unsigned char>^ Nval_unknown_05;
	public: array<unsigned char>^ Nval_unknown_06;
	public: array<unsigned char>^ Nval_unknown_07;
	public: array<unsigned char>^ Nval_unknown_08;
	public: int sNval_1_len;
	public: int sNval_2_len;
	public: int sNval_3_len;
	public: int sNval_4_len;
	public: int reward_type;
	public: int reward_type2;
	public: array<unsigned char>^ UNKNOWN_134;
	public: array<ValueItem^>^ global_value_1_items;
	public: array<DynamicsTask^>^ dynamics;
	public: array<TeamMembers^>^ required_team_member_groups;
	public: int UNKNOWN_29;
	public: int UNKNOWN_30;
	public: array<unsigned char>^ given_items_unknown; // 4 Byte
	public: array<__int16>^ tops;
	public: int required_reputation;
	public: array<unsigned char>^ UNKNOWN_32; // 4 Byte
	public: bool UNKNOWN_33;
	public: int required_quests_done_count;
	public: array<int>^ required_quests_done;
	public: array<unsigned char>^ UNKNOWN_34; // 60 Byte
	public: bool UNKNOWN_35;
	public: int UNKNOWN_36;
// required spiritual cultivation?
	public: int required_cultivation;
	public: array<unsigned char>^ UNKNOWN_37; // 5 Byte
	public: int UNKNOWN_38;
	public: bool UNKNOWN_39;
	public: int required_gender;
	public: bool UNKNOWN_40;
	public: int required_occupations_count;
	public: array<int>^ required_occupations;
	public: bool UNKNOWN_41;
	public: bool required_be_married;
	public: bool UNKNOWN_42;
	//correct position of UNKNOWN_42_1 and UNKNOWN_42_2 not confirmed!
	public: bool UNKNOWN_42_1; // wedding owner ???
	public: bool UNKNOWN_42_2; // show by wedding owner
	public: bool required_be_gm;
	public: bool UNKNOWN_43;
	public: array<unsigned char>^ UNKNOWN_44; // 19 Byte
	public: Date^ date_unknown;
	public: int UNKNOWN_45;
	public: bool UNKNOWN_46;
	public: bool bool1380;
	public: int unk1380int1;
	public: int unk1380int2;
	public: bool bool1380_2;
	public: array<unsigned char>^ UNKNOWN_47; // 7 Byte -> All Zeros
	public: int required_quests_undone_count;
	public: array<int>^ required_quests_undone;
	public: int required_blacksmith_level;
	public: int required_tailor_level;
	public: int required_craftsman_level;
	public: int required_apothecary_level;
	public: array<unsigned char>^ UNKNOWN_48; // 32 Byte (team member requirements?)
	public: array<unsigned char>^ UNKNOWN_49; // 3 Byte (team member requirements?)
	public: array<unsigned char>^ UNKNOWN_50; // 4 Byte
	public: bool UNKNOWN_51;
	public: array<unsigned char>^ UNKNOWN_52; // 9 Byte
	public: int resource_pq_audit_id;
	public: int UNKNOWN_53;
	public: int UNKNOWN_54;
	public: int required_pq_contribution;
	public: array<unsigned char>^ UNKNOWN_55; // 20 Byte
	public: array<unsigned char>^ UNKNOWN_55_02_01; // bool CheckForce
	public: int required_force; // 4 Byte
	public: array<unsigned char>^ UNKNOWN_55_02_02; // bool ShowByForce
	public: int required_prestige; // 4 Byte
	public: array<unsigned char>^ UNKNOWN_55_03; // bool ShowByForceReputation
	public: int required_influence_fee; // 4 Byte
	public: array<unsigned char>^ UNKNOWN_55_04; // 11 Byte: bool ShowByForceContrib; int ForcePremExp; bool ShowByForceExp; int ForcePremSP; bool ShowByForceSP;
	public: array<unsigned char>^ UNKNOWN_55_05; // int ForceActivityLevel
	public: array<unsigned char>^ UNKNOWN_55_06; // bool ShowByForceActivityLevel
	public: bool PremIsKing;
	public: bool ShowByKing;
	public: bool PremNotInTeam;
	public: bool ShowByNotInTeam;

	// 0 - ?
	// 1 - Chase
	// 2 - Get
	// 3 - Requires (done, unactivated) quests, race, class and gender requirements
	// 4 - Reach Coordinate
	// 5 - Auto Success
	// ...
	// 11 - Leave Coordinate
	public: int required_success_type;
	// 0 - None (Gift Box Reward)
	// 1 - Reward NPC
	public: int required_npc_type;
	public: int required_morai_pk_count;
	public: array<unsigned char>^ required_morai_pk_unknown; // 4 Byte
	public: array<unsigned char>^ required_chases_unknown; // 4 Byte
	public: int required_get_items_count;
	public: array<unsigned char>^ required_get_items_unknown; // 4 Byte
	public: int required_coins;
	public: array<unsigned char>^ UNKNOWN_56; // 16 Byte
	public: array<unsigned char>^ UNKNOWN_57; // 12 Byte
	public: LocationSpan^ reach_locations;
	public: int required_wait_time;
	public: array<unsigned char>^ UNKNOWN_57_01; // 15 Byte
	public: array<unsigned char>^ UNKNOWN_58; // 8 Byte
	public: array<unsigned char>^ TransformedForm; // 1 Byte
	public: array<unsigned char>^ UNKNOWN_59; // 24 Byte
	public: array<unsigned char>^ UNKNOWN_7; 
	public: int parent_quest;
	public: int previous_quest;
	public: int next_quest;
	public: int sub_quest_first;
	public: int reward_items_count_2;
	public: int reward_items_id_2;
	public: int reward_items_id;
	public: array<int>^ reward_items_group_2;
	public: array<int>^ reward_num;
	public: array<int>^ reward_items_group;
	public: bool UNKNOWN_60; // is divine quest with probability?
	public: float receive_quest_probability;
	public: bool UNKNOWN_60_01; 
	public: int reward_num_count;
	public: array<unsigned char>^ unknown_202;
	public: array<unsigned char>^ UNKNOWN_end;
	public: array<unsigned char>^ UNKNOWN_end2;
	private: array<unsigned char>^ author_text;
	// 60 Byte Unicode
	public: property String^ AuthorText
	{
		String^ get()
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			return enc->GetString(author_text);//->Replace("\0", "");
		}
		void set(String^ value)
		{
			Encoding^ enc = Encoding::GetEncoding("Unicode");
			array<unsigned char>^ target = gcnew array<unsigned char>(60);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			author_text = target;
		}
	}
	public: array<DateSpan^>^ date_spans;
	public: array<Item^>^ required_items;
	public: array<Item^>^ given_items;
	public: array<MoraiPK^>^ required_morai_pk;
	public: array<Chase^>^ required_chases;
	public: array<Item^>^ required_get_items;
	public: Reward^ reward_success;
	public: Reward^ reward_team_member;
	public: Reward^ reward_failed;
	public: Reward^ reward_team_leader;
	public: Reward^ reward_teacher;
	public: array<Reward^>^ rewards_items_new;
	public: array<Reward^>^ rewards_items_2;
	public: array<Reward^>^ rewards_num;

	public: int rewards_timed_count;
	public: int reward_items_count;
	public: array<float>^ rewards_timed_factors;
	public: array<Reward^>^ rewards_timed;

	public: array<unsigned char>^ UNKNOWN_61; // 80 Byte //public: Reward^ REWARD_UNKNOWN;
	
	Conversation^ conversation;
	int sub_quest_count;
	array<Task^>^ sub_quests;

	private: void Load(int version, BinaryReader^ br, int stream_position, TreeNodeCollection^ nodes);
	public: void Save(int version, BinaryWriter^ bw);
	public: Task^ Clone();
};