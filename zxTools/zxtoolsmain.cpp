#pragma once

#include "zxtools_menu.h"

[STAThreadAttribute]
int zxtoolsmain(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	// Select international culture Profile (NumberFormat, Localization,...)
	Application::CurrentCulture = System::Globalization::CultureInfo::InvariantCulture;

	// Create the main window and run it
	Application::Run(gcnew zxtools_menu());
	return 0;
}
