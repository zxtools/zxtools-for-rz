#pragma once

#include "..\..\COMMON\task.h"
#include "..\..\COMMON\DebugWindow.h"

using namespace System;
using namespace System::IO;
using namespace System::Security::Cryptography;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Text;


public ref class zxTASKeditWindow : public System::Windows::Forms::Form
{
	public:
	zxTASKeditWindow(void)
	{
		InitializeComponent();
		this->Icon = (Drawing::Icon^)((gcnew Resources::ResourceManager("zxTools.icons", Reflection::Assembly::GetExecutingAssembly()))->GetObject("app"));
		comboBox_search->SelectedIndex = 0;

		for(int i=0; i<versions->Count-1; i++)
		{
			this->exportToolStripMenuItem->DropDownItems->Add("v" + ((int)versions[i]).ToString());
			this->exportToolStripMenuItem->DropDownItems[i]->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_save);
		}
	}

	protected:
	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	~zxTASKeditWindow()
	{
		if (components)
		{
			delete components;
		}
	}

	static ArrayList^ versions = gcnew ArrayList(gcnew array<int>{159,165}); //version 
	array<Task^>^ Tasks;
	array<int>^ ItemStreamPositions;
	int version;

	Task^ SelectedTask;
	Reward^ SelectedReward;

	private: System::Windows::Forms::MenuStrip^  menuStrip_mainMenu;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
	private: System::Windows::Forms::ProgressBar^  progressBar_progress;
	private: System::Windows::Forms::TextBox^  textBox_search;
	private: System::Windows::Forms::Button^  button_search;
	private: System::Windows::Forms::TreeView^  treeView_tasks;
	private: System::Windows::Forms::DataGridView^  dataGridView_date_spans;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
	private: System::Windows::Forms::ToolStripMenuItem^  taskSplitToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox_reward;
	private: System::Windows::Forms::DataGridView^  dataGridView_reward_item_group_items;
private: System::Windows::Forms::TextBox^  textBox_reward_rclvl;

	private: System::Windows::Forms::Label^  label19;
private: System::Windows::Forms::TextBox^  textBox_rewardExpLv;

	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::TextBox^  textBox_reward_experience;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::TextBox^  textBox_reward_coins;
	private: System::Windows::Forms::Label^  label16;
private: System::Windows::Forms::TextBox^  textBox_reward_newtask;

	private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::TextBox^  textBox_reward_APLv;

	private: System::Windows::Forms::Label^  label20;

	private: System::Windows::Forms::Label^  label22;





	private: System::Windows::Forms::Label^  label25;
	private: System::Windows::Forms::TextBox^  textBox_conversation_prompt_text;
	private: System::Windows::Forms::TextBox^  textBox_reward_teleport_map_id;
	private: System::Windows::Forms::TextBox^  textBox_reward_teleport_z;
	private: System::Windows::Forms::TextBox^  textBox_reward_teleport_x;
	private: System::Windows::Forms::TextBox^  textBox_reward_teleport_altitude;
	private: System::Windows::Forms::Label^  label26;
	private: System::Windows::Forms::Label^  label37;
	private: System::Windows::Forms::ToolStripMenuItem^  exportToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox_answers;
	private: System::Windows::Forms::GroupBox^  groupBox_dialogs;
	private: System::Windows::Forms::GroupBox^  groupBox_conversation;
	private: System::Windows::Forms::Label^  label40;
	private: System::Windows::Forms::TextBox^  textBox_conversation_general_text;
	private: System::Windows::Forms::Label^  label42;
	private: System::Windows::Forms::ListBox^  listBox_conversation_dialogs;
	private: System::Windows::Forms::Label^  label43;
	private: System::Windows::Forms::TextBox^  textBox_conversation_dialog_unknown;
	private: System::Windows::Forms::TextBox^  textBox_conversation_answer_task_link;
	private: System::Windows::Forms::Label^  label46;
	private: System::Windows::Forms::TextBox^  textBox_conversation_answer_question_link;
	private: System::Windows::Forms::Label^  label47;
	private: System::Windows::Forms::ListBox^  listBox_conversation_answers;
	private: System::Windows::Forms::GroupBox^  groupBox_questions;
	private: System::Windows::Forms::TextBox^  textBox_conversation_question_previous;
	private: System::Windows::Forms::Label^  label45;
	private: System::Windows::Forms::TextBox^  textBox_conversation_question_id;
	private: System::Windows::Forms::Label^  label44;
	private: System::Windows::Forms::ListBox^  listBox_conversation_questions;
	private: System::Windows::Forms::TextBox^  textBox_conversation_answer_text;
	private: System::Windows::Forms::TextBox^  textBox_conversation_question_text;
	private: System::Windows::Forms::TextBox^  textBox_conversation_dialog_text;
	private: System::Windows::Forms::GroupBox^  groupBox_reward_selector;
	private: System::Windows::Forms::ListBox^  listBox_reward_timed;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown_time_factor;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::RadioButton^  radioButton_timed;
	private: System::Windows::Forms::RadioButton^  radioButton_failed;
	private: System::Windows::Forms::RadioButton^  radioButton_success;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_date_spans;
	private: System::Windows::Forms::ToolStripMenuItem^  addRowToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteRowToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  textBox_time_limit;
	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::TextBox^  textBox_author_text;
	private: System::Windows::Forms::TextBox^  textBox_name;
	private: System::Windows::Forms::Label^  label29;
	private: System::Windows::Forms::TextBox^  textBox_id;
	private: System::Windows::Forms::Label^  label28;
	private: System::Windows::Forms::CheckBox^  checkBox_author_mode;
	private: System::Windows::Forms::ToolTip^  toolTip;



	private: System::Windows::Forms::Label^  label55;
	private: System::Windows::Forms::Label^  label57;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown_reward_item_groups_count;
	private: System::Windows::Forms::Label^  label56;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_task;
	private: System::Windows::Forms::ToolStripMenuItem^  cloneSelectedToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteSelectedToolStripMenuItem;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_conversation_question;
	private: System::Windows::Forms::ToolStripMenuItem^  addToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  removeToolStripMenuItem;






	private: System::Windows::Forms::TextBox^  textBox_unknown_07;
	private: System::Windows::Forms::Label^  label76;
	private: System::Windows::Forms::ToolStripMenuItem^  creatureBuilderListToolStripMenuItem;
	private: System::Windows::Forms::ComboBox^  comboBox_search;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::ToolStripMenuItem^  exportSelectedItemToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  replaceSelectedItemToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox_general;
	private: System::Windows::Forms::TextBox^  textBox_unknown_01;
	private: System::Windows::Forms::Label^  label81;
	private: System::Windows::Forms::CheckBox^  checkBox_has_date_fail;
	private: System::Windows::Forms::CheckBox^  checkBox_has_date_spans;
	private: System::Windows::Forms::TextBox^  textBox_unknown_05;
	private: System::Windows::Forms::Label^  label84;
	private: System::Windows::Forms::TextBox^  textBox_unknown_04;
	private: System::Windows::Forms::Label^  label31;
	private: System::Windows::Forms::TextBox^  textBox_unknown_06;
	private: System::Windows::Forms::Label^  label85;

	private: System::Windows::Forms::Label^  label35;
	private: System::Windows::Forms::TextBox^  textBox_level_min;
	private: System::Windows::Forms::Label^  label36;
	private: System::Windows::Forms::TextBox^  textBox_level_max;


	private: System::Windows::Forms::TextBox^  textBox_sub_quest_first;
	private: System::Windows::Forms::Label^  label52;
	private: System::Windows::Forms::TextBox^  textBox_next_quest;
	private: System::Windows::Forms::Label^  label51;
	private: System::Windows::Forms::TextBox^  textBox_previous_quest;
	private: System::Windows::Forms::Label^  label50;
	private: System::Windows::Forms::TextBox^  textBox_parent_quest;
	private: System::Windows::Forms::Label^  label49;
	private: System::Windows::Forms::CheckBox^  checkBox_unknown_02;
	private: System::Windows::Forms::CheckBox^  checkBox_unknown_03;
	private: System::Windows::Forms::Label^  label82;



	private: System::Windows::Forms::Label^  label88;
private: System::Windows::Forms::DataGridView^  dataGridView_globals;

	private: System::Windows::Forms::TextBox^  textBox_unknown_08;
	private: System::Windows::Forms::Label^  label89;
	private: System::Windows::Forms::TextBox^  textBox_unknown_09;
	private: System::Windows::Forms::Label^  label90;
	private: System::Windows::Forms::GroupBox^  groupBox_flags;
	private: System::Windows::Forms::Label^  label91;
	private: System::Windows::Forms::CheckBox^  checkBox_activate_first_subquest;
	private: System::Windows::Forms::Label^  label100;
	private: System::Windows::Forms::CheckBox^  checkBox_on_fail_parent_fail;
	private: System::Windows::Forms::Label^  label99;
	private: System::Windows::Forms::CheckBox^  checkBox_fail_on_death;
	private: System::Windows::Forms::Label^  label98;
	private: System::Windows::Forms::CheckBox^  checkBox_repeatable_after_failure;
	private: System::Windows::Forms::Label^  label97;
	private: System::Windows::Forms::CheckBox^  checkBox_repeatable;
	private: System::Windows::Forms::Label^  label96;
	private: System::Windows::Forms::CheckBox^  checkBox_can_give_up;
	private: System::Windows::Forms::Label^  label95;
	private: System::Windows::Forms::CheckBox^  checkBox_on_success_parent_success;
	private: System::Windows::Forms::Label^  label94;
	private: System::Windows::Forms::CheckBox^  checkBox_on_give_up_parent_fails;
	private: System::Windows::Forms::Label^  label93;
	private: System::Windows::Forms::CheckBox^  checkBox_activate_next_subquest;
	private: System::Windows::Forms::Label^  label92;
	private: System::Windows::Forms::CheckBox^  checkBox_activate_random_subquest;
	private: System::Windows::Forms::Label^  label101;
	private: System::Windows::Forms::CheckBox^  checkBox_unknown_10;
	private: System::Windows::Forms::Label^  label102;
	private: System::Windows::Forms::DataGridView^  dataGridView_trigger_location_spans;
	private: System::Windows::Forms::CheckBox^  checkBox_trigger_locations_has_spans;
	private: System::Windows::Forms::Label^  label106;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn37;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn38;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn39;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn40;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn41;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn42;
	private: System::Windows::Forms::Label^  label107;
	private: System::Windows::Forms::TextBox^  textBox_trigger_locations_map_id;
	private: System::Windows::Forms::GroupBox^  groupBox_trigger_location;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_trigger_location;
	private: System::Windows::Forms::ToolStripMenuItem^  addToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  removeToolStripMenuItem1;
























	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column5;
	private: System::Windows::Forms::DataGridViewComboBoxColumn^  Column6;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column7;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column8;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column9;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column10;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column11;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column12;
	private: System::Windows::Forms::DataGridViewComboBoxColumn^  Column13;






	private: System::Windows::Forms::CheckBox^  checkBox_unknown_17;

	private: System::Windows::Forms::CheckBox^  checkBox_has_instant_teleport;
	private: System::Windows::Forms::Label^  label109;






	private: System::Windows::Forms::CheckBox^  checkBox_unknown_18;

	private: System::Windows::Forms::CheckBox^  checkBox_unknown_20;

	private: System::Windows::Forms::CheckBox^  checkBox_unknown_19;

	private: System::Windows::Forms::TextBox^  textBox_ai_trigger;
	private: System::Windows::Forms::Label^  label113;
	private: System::Windows::Forms::GroupBox^  groupBox_basic_1;
	private: System::Windows::Forms::CheckBox^  checkBox_unknown_23;

	private: System::Windows::Forms::CheckBox^  checkBox_unknown_22;

	private: System::Windows::Forms::CheckBox^  checkBox_unknown_21;

	private: System::Windows::Forms::TextBox^  textBox_unknown_level;
	private: System::Windows::Forms::Label^  label117;
	private: System::Windows::Forms::TextBox^  textBox_instant_teleport_location_z;
	private: System::Windows::Forms::Label^  label121;
	private: System::Windows::Forms::TextBox^  textBox_instant_teleport_location_alt;
	private: System::Windows::Forms::Label^  label120;
	private: System::Windows::Forms::TextBox^  textBox_instant_teleport_location_x;
	private: System::Windows::Forms::Label^  label119;
	private: System::Windows::Forms::TextBox^  textBox_instant_teleport_location_map_id;
	private: System::Windows::Forms::Label^  label118;
	private: System::Windows::Forms::Label^  label123;
	private: System::Windows::Forms::CheckBox^  checkBox_mark_available_point;
	private: System::Windows::Forms::Label^  label122;
	private: System::Windows::Forms::CheckBox^  checkBox_mark_available_icon;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::TextBox^  textBox_quest_npc;
	private: System::Windows::Forms::Label^  label34;
	private: System::Windows::Forms::TextBox^  textBox_reward_npc;










	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_valid_location;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem5;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem6;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_fail_location;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem3;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem4;






























































































































	private: System::Windows::Forms::GroupBox^  groupBox_basic_2;





	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_required_items;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem7;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem8;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_given_items;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem9;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem10;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_required_get_items;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem11;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem12;













	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_team_members;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem13;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem14;

	private: System::Windows::Forms::GroupBox^  groupBox_reach_location;
	private: System::Windows::Forms::CheckBox^  checkBox_reach_locations_has_spans;
	private: System::Windows::Forms::Label^  label4;

	private: System::Windows::Forms::DataGridView^  dataGridView_reach_location_spans;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn60;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn61;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn62;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn63;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn64;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn65;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reach_location;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem15;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem16;
	private: System::Windows::Forms::TextBox^  textBox_reach_locations_map_id;

	private: System::Windows::Forms::Label^  label6;































































































	private: System::Windows::Forms::TextBox^  textBox_player_limit;
	private: System::Windows::Forms::ToolStripMenuItem^  developerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  developerSearchToolStripMenuItem;
	private: System::Windows::Forms::ToolStripComboBox^  toolStripComboBox_developer_search;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem17;
	private: System::Windows::Forms::ToolStripMenuItem^  changeConfirmationToolStripMenuItem;












	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::TabPage^  tabPage3;
	private: System::Windows::Forms::TabPage^  tabPage4;

	private: System::Windows::Forms::TabPage^  tabPage6;
	private: System::Windows::Forms::TabPage^  tabPage7;























	private: System::Windows::Forms::GroupBox^  groupBox_reward_items;


	private: System::Windows::Forms::Label^  label192;






	private: System::Windows::Forms::Label^  label196;
	private: System::Windows::Forms::Label^  label198;
	private: System::Windows::Forms::Label^  label197;
















	private: System::Windows::Forms::CheckedListBox^  checkedListBox_reward_item_groups_flag;





















































	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_items;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem18;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem19;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_pq_chases;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem20;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem21;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_pq_items;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem22;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem23;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_chases;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem24;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem25;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_chases;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem26;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem27;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_location;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem28;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem29;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_script_infos;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem30;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem31;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_scripts;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem32;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem33;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_special_scripts;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem34;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem35;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_pq_messages;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem36;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem37;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_timed;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem38;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem39;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_pq_scripts;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem40;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem41;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_pq_specials;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem42;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem43;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_reward_pq_messages;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem44;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem45;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_conversation_answer;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem46;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem47;






























































private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip_morai_pk;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem48;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem49;










private: System::Windows::Forms::Label^  label238;
private: System::Windows::Forms::TextBox^  textBox_xunhan;
private: System::Windows::Forms::TextBox^  textBox_cooldown;
private: System::Windows::Forms::Label^  label239;
private: System::Windows::Forms::TextBox^  textBox_resetcycle;
private: System::Windows::Forms::TextBox^  textBox_resettype;
private: System::Windows::Forms::Label^  label241;
private: System::Windows::Forms::Label^  label240;
private: System::Windows::Forms::CheckBox^  checkBox_trigger_on;

private: System::Windows::Forms::TextBox^  textBox_autounk;
private: System::Windows::Forms::TextBox^  textBox_cleartask;
private: System::Windows::Forms::TextBox^  textBox_classify;
private: System::Windows::Forms::Label^  label110;
private: System::Windows::Forms::Label^  label108;
private: System::Windows::Forms::TextBox^  textBox_component;
private: System::Windows::Forms::CheckBox^  checkBox_clantask;
private: System::Windows::Forms::CheckBox^  checkBox_haslimit;
private: System::Windows::Forms::Label^  label114;
private: System::Windows::Forms::TextBox^  textBox_allkillnum;
private: System::Windows::Forms::Label^  label112;
private: System::Windows::Forms::Label^  label111;
private: System::Windows::Forms::TextBox^  textBox_playerpenalty;
private: System::Windows::Forms::TextBox^  textBox_unkpenalty;
private: System::Windows::Forms::TextBox^  textBox_allzonenum;
private: System::Windows::Forms::CheckBox^  checkBox_allzone;
private: System::Windows::Forms::CheckBox^  checkBox_plimit;
private: System::Windows::Forms::TextBox^  textBox_plimitnum;
private: System::Windows::Forms::TextBox^  textBox_newtype2;
private: System::Windows::Forms::TextBox^  textBox_newtype;
private: System::Windows::Forms::Label^  label116;
private: System::Windows::Forms::Label^  label115;
private: System::Windows::Forms::TextBox^  textBox_bldbmax;
private: System::Windows::Forms::TextBox^  textBox_bldbmin;
private: System::Windows::Forms::Label^  label242;
private: System::Windows::Forms::CheckBox^  checkBox_showlvl;
private: System::Windows::Forms::Label^  label62;


















private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column33;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column34;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column35;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column36;
















private: System::Windows::Forms::CheckBox^  checkBox_valunk3;




























































































private: System::Windows::Forms::TabPage^  tabPage5;
private: System::Windows::Forms::TextBox^  textBox_unkn120;
private: System::Windows::Forms::Label^  label134;
private: System::Windows::Forms::TextBox^  textBox_unkn119;
private: System::Windows::Forms::Label^  label132;
private: System::Windows::Forms::TextBox^  textBox_unkn118;
private: System::Windows::Forms::Label^  label167;
private: System::Windows::Forms::CheckBox^  checkBox_unknown31;
private: System::Windows::Forms::TextBox^  textBox_unkn116;
private: System::Windows::Forms::Label^  label75;
private: System::Windows::Forms::Label^  label54;
private: System::Windows::Forms::TextBox^  textBox_unkn115;











private: System::Windows::Forms::TextBox^  textBox_ubkb124;
private: System::Windows::Forms::Label^  label78;
private: System::Windows::Forms::TextBox^  textBox_unkn123;
private: System::Windows::Forms::Label^  label170;
private: System::Windows::Forms::TextBox^  textBox_unkn122;
private: System::Windows::Forms::Label^  label169;
private: System::Windows::Forms::TextBox^  textBox_unkn121;
private: System::Windows::Forms::Label^  label168;



















private: System::Windows::Forms::Label^  label141;










private: System::Windows::Forms::TextBox^  textBox_unkn127;
private: System::Windows::Forms::Label^  label72;
private: System::Windows::Forms::TextBox^  textBox_unkn126;
private: System::Windows::Forms::Label^  label142;
private: System::Windows::Forms::TextBox^  textBox_unkn125;


private: System::Windows::Forms::Label^  label144;
private: System::Windows::Forms::TextBox^  textBox_nval2;

private: System::Windows::Forms::TextBox^  textBox_nval1;
private: System::Windows::Forms::CheckBox^  checkBox_nval;
private: System::Windows::Forms::TextBox^  textBox_nval4;

private: System::Windows::Forms::TextBox^  textBox_nval3;

private: System::Windows::Forms::Label^  label147;
private: System::Windows::Forms::Label^  label146;
private: System::Windows::Forms::Label^  label145;
private: System::Windows::Forms::TextBox^  textBox_val1len;























private: System::Windows::Forms::TextBox^  textBox_sval4len;
private: System::Windows::Forms::TextBox^  textBox_sval3len;
private: System::Windows::Forms::TextBox^  textBox_svallen2;
private: System::Windows::Forms::TextBox^  textBox_sva1len;
private: System::Windows::Forms::TextBox^  textBox_sval4;
private: System::Windows::Forms::TextBox^  textBox_sval3;
private: System::Windows::Forms::Label^  label188;
private: System::Windows::Forms::Label^  label231;
private: System::Windows::Forms::Label^  label232;
private: System::Windows::Forms::Label^  label233;
private: System::Windows::Forms::TextBox^  textBox_sval2;
private: System::Windows::Forms::TextBox^  textBox_sval1;
private: System::Windows::Forms::CheckBox^  checkBox_conval2;
private: System::Windows::Forms::TextBox^  textBox_unkn133;
private: System::Windows::Forms::Label^  label187;
private: System::Windows::Forms::TextBox^  textBox_unkn132;
private: System::Windows::Forms::Label^  label184;
private: System::Windows::Forms::TextBox^  textBox_unkn131;
private: System::Windows::Forms::Label^  label182;
private: System::Windows::Forms::TextBox^  textBox_unkn130;
private: System::Windows::Forms::Label^  label181;
private: System::Windows::Forms::TextBox^  textBox_unkn23456;
private: System::Windows::Forms::Label^  label177;
private: System::Windows::Forms::TextBox^  textBox_unkn129;
private: System::Windows::Forms::Label^  label174;
private: System::Windows::Forms::TextBox^  textBox_unkn1628;
private: System::Windows::Forms::Label^  label150;
private: System::Windows::Forms::TextBox^  textBox_unkn128;
private: System::Windows::Forms::Label^  label148;
private: System::Windows::Forms::TextBox^  textBox_val4len;
private: System::Windows::Forms::TextBox^  textBox_val3len;
private: System::Windows::Forms::TextBox^  textBox_val2len;
private: System::Windows::Forms::TextBox^  textBox_rewardtype2;
private: System::Windows::Forms::Label^  label261;
private: System::Windows::Forms::TextBox^  textBox_rewardtype;
private: System::Windows::Forms::Label^  label260;
private: System::Windows::Forms::TextBox^  textBox_unkn134;
private: System::Windows::Forms::Label^  label259;
private: System::Windows::Forms::TextBox^  textBox_sval5;
private: System::Windows::Forms::Label^  label258;
private: System::Windows::Forms::GroupBox^  groupBox_requirements;
private: System::Windows::Forms::TextBox^  textBox3;
private: System::Windows::Forms::Label^  label235;
private: System::Windows::Forms::ListBox^  listBox_tops;
private: System::Windows::Forms::TextBox^  textBox_needlvl;
private: System::Windows::Forms::Label^  label186;
private: System::Windows::Forms::TextBox^  textBox_titlechange;

private: System::Windows::Forms::Label^  label185;
private: System::Windows::Forms::ListBox^  listBox_titles;
private: System::Windows::Forms::TextBox^  textBox_waittime;
private: System::Windows::Forms::Label^  label183;
private: System::Windows::Forms::TextBox^  textBox_npctime;
private: System::Windows::Forms::Label^  label180;
private: System::Windows::Forms::TextBox^  textBox_safenpc;
private: System::Windows::Forms::Label^  label179;
private: System::Windows::Forms::TextBox^  textBox_noncash;
private: System::Windows::Forms::Label^  label178;
private: System::Windows::Forms::TextBox^  textBox_completetype;
private: System::Windows::Forms::Label^  label173;
private: System::Windows::Forms::TextBox^  textBox_method;
private: System::Windows::Forms::Label^  label171;
private: System::Windows::Forms::TextBox^  textBox_soulmax;
private: System::Windows::Forms::CheckBox^  checkBox_soulmaxon;
private: System::Windows::Forms::CheckBox^  checkBox_soulminom;
private: System::Windows::Forms::TextBox^  textBox_soulmin;
private: System::Windows::Forms::Label^  label149;
private: System::Windows::Forms::TextBox^  textBox_soulunkn1;
private: System::Windows::Forms::TextBox^  textBox_ingotmax;
private: System::Windows::Forms::TextBox^  textBox_ingotmin;
private: System::Windows::Forms::Label^  label131;
private: System::Windows::Forms::Label^  label130;
private: System::Windows::Forms::Label^  label73;
private: System::Windows::Forms::TextBox^  textBox_zhenying;
private: System::Windows::Forms::CheckBox^  checkBox_frongfang03;
private: System::Windows::Forms::CheckBox^  checkBox_frontfeng02;
private: System::Windows::Forms::CheckBox^  checkBox_frontfeng01;
private: System::Windows::Forms::Label^  label143;
private: System::Windows::Forms::TextBox^  textBox_blamon;
private: System::Windows::Forms::CheckBox^  checkBox_blded;
private: System::Windows::Forms::TextBox^  textBox_clanchmax;
private: System::Windows::Forms::TextBox^  textBox_clanchmin;
private: System::Windows::Forms::Label^  label140;
private: System::Windows::Forms::Label^  label139;
private: System::Windows::Forms::TextBox^  textBox_clanmap;
private: System::Windows::Forms::Label^  label138;
private: System::Windows::Forms::TextBox^  textBox_clanskillid;
private: System::Windows::Forms::Label^  label137;
private: System::Windows::Forms::TextBox^  textBox_clanskillexpmax;
private: System::Windows::Forms::TextBox^  textBox_clanskillexpmin;
private: System::Windows::Forms::Label^  label136;
private: System::Windows::Forms::Label^  label135;
private: System::Windows::Forms::TextBox^  textBox_clanskillmax;
private: System::Windows::Forms::TextBox^  textBox_clanskillmin;
private: System::Windows::Forms::Label^  label133;
private: System::Windows::Forms::Label^  label80;
private: System::Windows::Forms::CheckBox^  checkBox_clanlead;
private: System::Windows::Forms::CheckBox^  checkBox_clanc;
private: System::Windows::Forms::TextBox^  textBox_vtask;
private: System::Windows::Forms::Label^  label77;
private: System::Windows::Forms::Label^  label30;
private: System::Windows::Forms::TextBox^  textBox_viewlvl;
private: System::Windows::Forms::CheckBox^  checkBox_vunkn2;
private: System::Windows::Forms::CheckBox^  checkBox_vunkn1;
private: System::Windows::Forms::CheckBox^  checkBox_vpeach;
private: System::Windows::Forms::CheckBox^  checkBox_vchef;
private: System::Windows::Forms::CheckBox^  checkBox_view;
private: System::Windows::Forms::CheckBox^  checkBox_viewreq;
private: System::Windows::Forms::CheckBox^  checkBox_leavefail;
private: System::Windows::Forms::CheckBox^  checkBox_tamaccfail;
private: System::Windows::Forms::CheckBox^  checkBox_leaderfail;
private: System::Windows::Forms::CheckBox^  checkBox_teamchecknum;
private: System::Windows::Forms::CheckBox^  checkBox_teamshare2;
private: System::Windows::Forms::CheckBox^  checkBox_teamunk;
private: System::Windows::Forms::CheckBox^  checkBox_teamshare;
private: System::Windows::Forms::CheckBox^  checkBox_teamask;
private: System::Windows::Forms::Label^  label166;
private: System::Windows::Forms::Label^  label164;
private: System::Windows::Forms::TextBox^  textBox_maxpk;
private: System::Windows::Forms::TextBox^  textBox_minpk;
private: System::Windows::Forms::CheckBox^  checkBox_frontmarried;
private: System::Windows::Forms::CheckBox^  checkBox_frontunkn01;
private: System::Windows::Forms::CheckBox^  checkBox_licreq;
private: System::Windows::Forms::CheckBox^  checkBox_frontgen;
private: System::Windows::Forms::TextBox^  textBox_unknfront;
private: System::Windows::Forms::CheckBox^  checkBox_frontalliance;
private: System::Windows::Forms::TextBox^  textBox_frontgender;
private: System::Windows::Forms::TextBox^  textBox_zhuanchonglv;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::TextBox^  textBox_renwu;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::TextBox^  textBox_yaoqiu;
private: System::Windows::Forms::CheckBox^  checkBox_tasknum;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::ListBox^  listBox_jobs;
private: System::Windows::Forms::Label^  label257;
private: System::Windows::Forms::Label^  label256;
private: System::Windows::Forms::Label^  label255;
private: System::Windows::Forms::Label^  label254;
private: System::Windows::Forms::Label^  label253;
private: System::Windows::Forms::Label^  label252;
private: System::Windows::Forms::Label^  label251;
private: System::Windows::Forms::Label^  label250;
private: System::Windows::Forms::Label^  label249;
private: System::Windows::Forms::Label^  label248;
private: System::Windows::Forms::Label^  label247;
private: System::Windows::Forms::Label^  label246;
private: System::Windows::Forms::Label^  label245;
private: System::Windows::Forms::Label^  label244;
private: System::Windows::Forms::Label^  label243;
private: System::Windows::Forms::Label^  label87;
private: System::Windows::Forms::Label^  label86;
private: System::Windows::Forms::Label^  label83;
private: System::Windows::Forms::Label^  label69;
private: System::Windows::Forms::Label^  label58;
private: System::Windows::Forms::TextBox^  textBox_rep20;
private: System::Windows::Forms::TextBox^  textBox_rep19;
private: System::Windows::Forms::TextBox^  textBox_rep13;
private: System::Windows::Forms::TextBox^  textBox_rep14;
private: System::Windows::Forms::TextBox^  textBox_rep15;
private: System::Windows::Forms::TextBox^  textBox_rep17;
private: System::Windows::Forms::TextBox^  textBox_rep16;
private: System::Windows::Forms::TextBox^  textBox_rep18;
private: System::Windows::Forms::CheckBox^  checkBox_hassheng;
private: System::Windows::Forms::TextBox^  textBox_rep7;
private: System::Windows::Forms::TextBox^  textBox_rep8;
private: System::Windows::Forms::TextBox^  textBox_rep12;
private: System::Windows::Forms::TextBox^  textBox_rep11;
private: System::Windows::Forms::TextBox^  textBox_rep10;
private: System::Windows::Forms::TextBox^  textBox_rep9;
private: System::Windows::Forms::TextBox^  textBox_rep6;
private: System::Windows::Forms::TextBox^  textBox_rep5;
private: System::Windows::Forms::TextBox^  textBox_rep4;
private: System::Windows::Forms::TextBox^  textBox_rep3;
private: System::Windows::Forms::TextBox^  textBox_rep2;
private: System::Windows::Forms::TextBox^  textBox_rep1;
private: System::Windows::Forms::CheckBox^  checkBox_needgender;
private: System::Windows::Forms::TextBox^  textBox_autoval;
private: System::Windows::Forms::CheckBox^  checkBox_hasauto;
private: System::Windows::Forms::TextBox^  textBox_workval;
private: System::Windows::Forms::CheckBox^  checkBox_haswork;
private: System::Windows::Forms::CheckBox^  checkBox_contribu;
private: System::Windows::Forms::CheckBox^  checkBox_needclan;
private: System::Windows::Forms::TextBox^  textBox_hasnextval;
private: System::Windows::Forms::CheckBox^  checkBox_hasnext;
private: System::Windows::Forms::CheckBox^  checkBox_rineedbag;
private: System::Windows::Forms::TextBox^  textBox_iunk11;
private: System::Windows::Forms::TextBox^  textBox_iunk10;
private: System::Windows::Forms::TextBox^  textBox_iunk09;
private: System::Windows::Forms::TextBox^  textBox_itunk07;
private: System::Windows::Forms::TextBox^  textBox_itunk06;
private: System::Windows::Forms::CheckBox^  checkBox_iunk4;
private: System::Windows::Forms::CheckBox^  checkBox_iunk5;
private: System::Windows::Forms::Label^  label222;
private: System::Windows::Forms::Label^  label74;
private: System::Windows::Forms::DataGridView^  dataGridView_team_members;










private: System::Windows::Forms::Label^  label68;
private: System::Windows::Forms::DataGridView^  dataGridView_given_items;





private: System::Windows::Forms::DataGridView^  dataGridView_required_items;

















private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::DataGridView^  dataGridView_required_get_items;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn16;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  dataGridViewTextBoxColumn17;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn18;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn19;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column17;


private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::DataGridView^  dataGridView_required_chases;










private: System::Windows::Forms::Label^  label48;
private: System::Windows::Forms::TextBox^  textBox_required_quests_undone_1;
private: System::Windows::Forms::TextBox^  textBox_required_quests_undone_2;
private: System::Windows::Forms::TextBox^  textBox_required_quests_undone_3;
private: System::Windows::Forms::TextBox^  textBox_required_quests_undone_5;
private: System::Windows::Forms::TextBox^  textBox_required_quests_undone_4;
private: System::Windows::Forms::TextBox^  textBox_unknown_44;
private: System::Windows::Forms::CheckBox^  checkBox_required_be_gm;
private: System::Windows::Forms::CheckBox^  checkBox_required_be_married;
private: System::Windows::Forms::ComboBox^  comboBox_required_gender;
private: System::Windows::Forms::Label^  label41;
private: System::Windows::Forms::TextBox^  textBox_required_quests_done_1;
private: System::Windows::Forms::TextBox^  textBox_required_quests_done_2;
private: System::Windows::Forms::TextBox^  textBox_required_quests_done_3;
private: System::Windows::Forms::TextBox^  textBox_required_quests_done_4;
private: System::Windows::Forms::TextBox^  textBox_required_quests_done_5;






private: System::Windows::Forms::TextBox^  textBox_required_items_unknown;
private: System::Windows::Forms::Label^  label70;

private: System::Windows::Forms::CheckBox^  checkBox_unknown_27;

private: System::Windows::Forms::TextBox^  textBox_instant_pay_coins;
private: System::Windows::Forms::Label^  label65;
private: System::Windows::Forms::DataGridView^  dataGridView_tasknum;


private: System::Windows::Forms::Label^  label151;
private: System::Windows::Forms::DataGridView^  dataGridView_dynamics;




































private: System::Windows::Forms::TextBox^  textBox_reward_ai_trigger;
private: System::Windows::Forms::TextBox^  textBox_run31;
private: System::Windows::Forms::Label^  label152;
private: System::Windows::Forms::TextBox^  textBox_run23;
private: System::Windows::Forms::Label^  label128;
private: System::Windows::Forms::TextBox^  textBox_run22;
private: System::Windows::Forms::Label^  label127;
private: System::Windows::Forms::TextBox^  textBox_run21;
private: System::Windows::Forms::Label^  label24;
private: System::Windows::Forms::TextBox^  textBox_run20;
private: System::Windows::Forms::Label^  label23;
private: System::Windows::Forms::CheckBox^  checkBox_rewdivorce;
private: System::Windows::Forms::CheckBox^  checkBox_rewremallinf;
private: System::Windows::Forms::TextBox^  textBox_clearpk;
private: System::Windows::Forms::Label^  label126;
private: System::Windows::Forms::TextBox^  textBox_rewardtitle;
private: System::Windows::Forms::Label^  label125;
private: System::Windows::Forms::TextBox^  textBox_run13;
private: System::Windows::Forms::Label^  label124;
private: System::Windows::Forms::TextBox^  textBox_clancontribut;
private: System::Windows::Forms::TextBox^  textBox_rewclancont;
private: System::Windows::Forms::TextBox^  textBox_run121;
private: System::Windows::Forms::TextBox^  textBox_grint;
private: System::Windows::Forms::TextBox^  textBox_run12;
private: System::Windows::Forms::TextBox^  textBox_run10;
private: System::Windows::Forms::TextBox^  textBox_run09;
private: System::Windows::Forms::TextBox^  textBox_run08;
private: System::Windows::Forms::TextBox^  textBox_run07;
private: System::Windows::Forms::TextBox^  textBox_run06;
private: System::Windows::Forms::TextBox^  textBox_run05;
private: System::Windows::Forms::TextBox^  textBox_run04;
private: System::Windows::Forms::TextBox^  textBox_un03;
private: System::Windows::Forms::TextBox^  textBox_un02;
private: System::Windows::Forms::TextBox^  textBox_un01;
private: System::Windows::Forms::Label^  label105;
private: System::Windows::Forms::Label^  label104;
private: System::Windows::Forms::Label^  label103;
private: System::Windows::Forms::Label^  label67;
private: System::Windows::Forms::Label^  label66;
private: System::Windows::Forms::Label^  label61;
private: System::Windows::Forms::Label^  label60;
private: System::Windows::Forms::Label^  label53;
private: System::Windows::Forms::Label^  label39;
private: System::Windows::Forms::Label^  label27;
private: System::Windows::Forms::Label^  label13;
private: System::Windows::Forms::Label^  label12;
private: System::Windows::Forms::GroupBox^  groupBox1;
private: System::Windows::Forms::TextBox^  textBox_rclanskillid;
private: System::Windows::Forms::Label^  label208;
private: System::Windows::Forms::TextBox^  textBox_rskill1;
private: System::Windows::Forms::Label^  label207;
private: System::Windows::Forms::TextBox^  textBox_proff;
private: System::Windows::Forms::Label^  label206;
private: System::Windows::Forms::TextBox^  textBox_runk36;
private: System::Windows::Forms::TextBox^  textBox_runk35;
private: System::Windows::Forms::Label^  label205;
private: System::Windows::Forms::Label^  label63;
private: System::Windows::Forms::CheckBox^  checkBox_triggeron;
private: System::Windows::Forms::TextBox^  textBox_rsetjob;
private: System::Windows::Forms::Label^  label204;
private: System::Windows::Forms::TextBox^  textBox_runk33;
private: System::Windows::Forms::Label^  label59;

private: System::Windows::Forms::TextBox^  textBox_runk32;
private: System::Windows::Forms::Label^  label262;
private: System::Windows::Forms::CheckBox^  checkBox_craftup;
private: System::Windows::Forms::TextBox^  textBox_runk301;
private: System::Windows::Forms::Label^  label237;
private: System::Windows::Forms::TextBox^  textBox_mountbag;
private: System::Windows::Forms::Label^  label236;
private: System::Windows::Forms::TextBox^  textBox_petslot;
private: System::Windows::Forms::Label^  label234;
private: System::Windows::Forms::TextBox^  textBox_rewpetbag;
private: System::Windows::Forms::Label^  label230;
private: System::Windows::Forms::TextBox^  textBox_rewbag;
private: System::Windows::Forms::Label^  label229;
private: System::Windows::Forms::TextBox^  textBox_rewstash;
private: System::Windows::Forms::Label^  label228;
private: System::Windows::Forms::TextBox^  textBox_runk30;
private: System::Windows::Forms::TextBox^  textBox_runk29;
private: System::Windows::Forms::TextBox^  textBox_runk28;
private: System::Windows::Forms::TextBox^  textBox_runk27;
private: System::Windows::Forms::TextBox^  textBox_runk26;
private: System::Windows::Forms::TextBox^  textBox_runk25;
private: System::Windows::Forms::Label^  label227;
private: System::Windows::Forms::Label^  label226;
private: System::Windows::Forms::Label^  label225;
private: System::Windows::Forms::Label^  label224;
private: System::Windows::Forms::Label^  label223;
private: System::Windows::Forms::Label^  label193;
private: System::Windows::Forms::TextBox^  textBox_rewRenZong;
private: System::Windows::Forms::TextBox^  textBox_rewDiZong;
private: System::Windows::Forms::TextBox^  textBox_TianZong;
private: System::Windows::Forms::TextBox^  textBox_ZhanXun;
private: System::Windows::Forms::TextBox^  textBox_ZhanJi;
private: System::Windows::Forms::TextBox^  textBox_FenXiang;
private: System::Windows::Forms::TextBox^  textBox_TianHua;
private: System::Windows::Forms::TextBox^  textBox_HuaiGuang;
private: System::Windows::Forms::TextBox^  textBox_rewreptaihao;
private: System::Windows::Forms::TextBox^  textBox_ChenHuang;
private: System::Windows::Forms::TextBox^  textBox_LieShan;
private: System::Windows::Forms::TextBox^  textBox_rewJiuLi;
private: System::Windows::Forms::Label^  label203;
private: System::Windows::Forms::Label^  label202;
private: System::Windows::Forms::Label^  label201;
private: System::Windows::Forms::Label^  label200;
private: System::Windows::Forms::Label^  label199;
private: System::Windows::Forms::Label^  label195;
private: System::Windows::Forms::Label^  label194;
private: System::Windows::Forms::Label^  label191;
private: System::Windows::Forms::Label^  label190;
private: System::Windows::Forms::Label^  label189;
private: System::Windows::Forms::Label^  label176;
private: System::Windows::Forms::Label^  label175;
private: System::Windows::Forms::TextBox^  textBox_runk15;
private: System::Windows::Forms::Label^  label172;
private: System::Windows::Forms::TextBox^  textBox_rewrepfoyuan;
private: System::Windows::Forms::Label^  label165;
private: System::Windows::Forms::TextBox^  textBox_rewmoxing;
private: System::Windows::Forms::Label^  label163;
private: System::Windows::Forms::TextBox^  textBox_DaoXin;
private: System::Windows::Forms::Label^  label162;
private: System::Windows::Forms::TextBox^  textBox_rewshide;
private: System::Windows::Forms::Label^  label161;
private: System::Windows::Forms::TextBox^  textBox_rewwencai;
private: System::Windows::Forms::Label^  label160;
private: System::Windows::Forms::TextBox^  textBox_rewQingyuan;
private: System::Windows::Forms::Label^  label159;
private: System::Windows::Forms::TextBox^  textBox_rewrepzhongyi;
private: System::Windows::Forms::Label^  label158;
private: System::Windows::Forms::TextBox^  textBox_rewreplupi;
private: System::Windows::Forms::Label^  label157;
private: System::Windows::Forms::TextBox^  textBox_rewrepvim;
private: System::Windows::Forms::Label^  label156;
private: System::Windows::Forms::TextBox^  textBox_rewrepsky;
private: System::Windows::Forms::Label^  label155;
private: System::Windows::Forms::TextBox^  textBox_rewrepjad;
private: System::Windows::Forms::Label^  label154;
private: System::Windows::Forms::TextBox^  textBox_rewrepmodo;
private: System::Windows::Forms::Label^  label153;
private: System::Windows::Forms::TextBox^  textBox_runk56;
private: System::Windows::Forms::Label^  label270;
private: System::Windows::Forms::TextBox^  textBox_runk55;
private: System::Windows::Forms::Label^  label269;
private: System::Windows::Forms::TextBox^  textBox_runk53;
private: System::Windows::Forms::Label^  label268;
private: System::Windows::Forms::TextBox^  textBox_rhuli;
private: System::Windows::Forms::Label^  label267;
private: System::Windows::Forms::CheckBox^  checkBox_openreason;
private: System::Windows::Forms::CheckBox^  checkBox_rsound;
private: System::Windows::Forms::TextBox^  textBox_rsound;


private: System::Windows::Forms::TextBox^  textBox_runk51;
private: System::Windows::Forms::Label^  label265;
private: System::Windows::Forms::TextBox^  textBox_run50;
private: System::Windows::Forms::Label^  label264;
private: System::Windows::Forms::TextBox^  textBox_transftime;
private: System::Windows::Forms::Label^  label263;
private: System::Windows::Forms::TextBox^  textBox_rtransf;
private: System::Windows::Forms::Label^  label221;
private: System::Windows::Forms::TextBox^  textBox_risexpf;
private: System::Windows::Forms::Label^  label217;
private: System::Windows::Forms::CheckBox^  checkBox_rtomeon;
private: System::Windows::Forms::TextBox^  textBox_rskillcnt;
private: System::Windows::Forms::CheckBox^  checkBox_rskillon;
private: System::Windows::Forms::TextBox^  textBox_rcampv;
private: System::Windows::Forms::CheckBox^  checkBox_rcampon;
private: System::Windows::Forms::TextBox^  textBox_tunk46;
private: System::Windows::Forms::Label^  label216;
private: System::Windows::Forms::TextBox^  textBox_rspecialflg;
private: System::Windows::Forms::CheckBox^  checkBox_rspecialaward;
private: System::Windows::Forms::CheckBox^  checkBox_rview2;
private: System::Windows::Forms::CheckBox^  checkBox_rview1;
private: System::Windows::Forms::CheckBox^  checkBox_rpanshi;
private: System::Windows::Forms::CheckBox^  checkBox_rchushi;
private: System::Windows::Forms::TextBox^  textBox_runk43;
private: System::Windows::Forms::TextBox^  textBox_runk42;
private: System::Windows::Forms::Label^  label215;
private: System::Windows::Forms::Label^  label214;
private: System::Windows::Forms::TextBox^  textBox_rdbltime;
private: System::Windows::Forms::Label^  label213;
private: System::Windows::Forms::TextBox^  textBox_rremtask;
private: System::Windows::Forms::Label^  label212;
private: System::Windows::Forms::TextBox^  textBox_rnotch;
private: System::Windows::Forms::CheckBox^  checkBox_rnotice;
private: System::Windows::Forms::TextBox^  textBox_rfacunk;
private: System::Windows::Forms::Label^  label211;
private: System::Windows::Forms::TextBox^  textBox_runk40;
private: System::Windows::Forms::Label^  label210;
private: System::Windows::Forms::TextBox^  textBox_rclanunk;
private: System::Windows::Forms::Label^  label209;
private: System::Windows::Forms::TextBox^  textBox_rnval2len;
private: System::Windows::Forms::TextBox^  textBox_rnval1len;
private: System::Windows::Forms::TextBox^  textBox_rnval4h;
private: System::Windows::Forms::TextBox^  textBox_rnval3h;
private: System::Windows::Forms::TextBox^  textBox_rnval2h;
private: System::Windows::Forms::TextBox^  textBox_rnval1h;
private: System::Windows::Forms::CheckBox^  checkBox_rnval;
private: System::Windows::Forms::TextBox^  textBox_runkn4;
private: System::Windows::Forms::Label^  label274;
private: System::Windows::Forms::TextBox^  textBox_runkn3;
private: System::Windows::Forms::Label^  label273;
private: System::Windows::Forms::TextBox^  textBox_runkn2;
private: System::Windows::Forms::Label^  label272;
private: System::Windows::Forms::TextBox^  textBox_runkn1;
private: System::Windows::Forms::Label^  label271;
private: System::Windows::Forms::TextBox^  textBox_runk49;
private: System::Windows::Forms::Label^  label220;
private: System::Windows::Forms::TextBox^  textBox_rvalc;
private: System::Windows::Forms::TextBox^  textBox_rval3;
private: System::Windows::Forms::TextBox^  textBox_rval2;
private: System::Windows::Forms::TextBox^  textBox_val1;
private: System::Windows::Forms::Label^  label219;
private: System::Windows::Forms::Label^  label218;
private: System::Windows::Forms::TextBox^  textBox_runk48;
private: System::Windows::Forms::TextBox^  textBox_noticemess;
private: System::Windows::Forms::TextBox^  textBox_rjobup;
private: System::Windows::Forms::Label^  label275;



private: System::Windows::Forms::TextBox^  textBox_expfname;
private: System::Windows::Forms::Label^  label276;
private: System::Windows::Forms::CheckBox^  checkBox_runk52;
private: System::Windows::Forms::Label^  label278;
private: System::Windows::Forms::Label^  label277;
private: System::Windows::Forms::TextBox^  textBox_newtxt;
private: System::Windows::Forms::TextBox^  textBox_xanfumess;
private: System::Windows::Forms::Label^  label266;
private: System::Windows::Forms::TextBox^  textBox_agernmess;
private: System::Windows::Forms::TextBox^  textBox_type;
private: System::Windows::Forms::Label^  label285;
private: System::Windows::Forms::TextBox^  textBox_rew1330_8;
private: System::Windows::Forms::Label^  label284;
private: System::Windows::Forms::TextBox^  textBox_rew1330_7;
private: System::Windows::Forms::Label^  label283;
private: System::Windows::Forms::TextBox^  textBox_rew1330_6;
private: System::Windows::Forms::Label^  label282;


private: System::Windows::Forms::TextBox^  textBox_un13304;
private: System::Windows::Forms::TextBox^  textBox_un13303;
private: System::Windows::Forms::Label^  label280;
private: System::Windows::Forms::Label^  label279;
private: System::Windows::Forms::CheckBox^  checkBox_un13302;
private: System::Windows::Forms::TextBox^  textBox_un13301;
private: System::Windows::Forms::Label^  label38;
private: System::Windows::Forms::CheckBox^  checkBox_rew1330_10;
private: System::Windows::Forms::TextBox^  textBox_rew1330_9;











private: System::Windows::Forms::RadioButton^  radioButton_teacher;
private: System::Windows::Forms::RadioButton^  radioButton_team_lead;
private: System::Windows::Forms::RadioButton^  radioButton_teammem;
private: System::Windows::Forms::Label^  label281;
private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
private: System::Windows::Forms::GroupBox^  groupBox_fail_location;
private: System::Windows::Forms::CheckBox^  checkBox_fail_locations_has_spans;
private: System::Windows::Forms::Label^  label15;
private: System::Windows::Forms::DataGridView^  dataGridView_fail_location_spans;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn43;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn44;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn45;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn46;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn47;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn48;
private: System::Windows::Forms::TextBox^  textBox_fail_locations_map_id;
private: System::Windows::Forms::Label^  label11;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn24;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  dataGridViewTextBoxColumn5;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn26;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn27;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column3;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn4;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column37;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn6;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn7;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column19;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column20;
private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem50;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem51;











private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn8;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn9;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn10;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn11;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn12;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn13;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn14;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn15;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column18;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column32;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn20;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn21;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn22;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn23;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column14;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column15;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column16;











private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn25;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn29;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn32;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn33;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn34;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn35;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  dataGridViewTextBoxColumn36;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  dataGridViewTextBoxColumn55;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  dataGridViewTextBoxColumn56;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column21;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column22;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem52;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem53;
private: System::Windows::Forms::DataGridViewComboBoxColumn^  Column_reward_item_groups;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn28;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column29;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn30;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn31;
private: System::Windows::Forms::DataGridViewCheckBoxColumn^  Column30;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column23;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column31;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column24;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column25;
private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column26;










































































































































	private: System::ComponentModel::IContainer^  components;

	private:
	/// <summary>
	/// Required designer variable.
	/// </summary>


#pragma region Windows Form Designer generated code
	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	void InitializeComponent(void)
	{
		this->components = (gcnew System::ComponentModel::Container());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle14 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle9 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle10 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle11 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle12 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle13 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle20 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle15 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle16 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle17 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle18 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle19 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle27 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle21 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle22 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle23 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle24 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle25 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle26 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle28 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle35 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle29 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle30 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle31 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle32 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle33 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle34 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle42 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle36 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle37 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle38 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle39 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle40 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle41 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle49 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle43 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle44 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle45 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle46 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle47 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle48 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle58 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle50 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle51 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle52 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle53 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle54 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle55 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle56 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle57 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle63 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle59 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle60 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle61 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle62 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle68 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle64 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle65 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle66 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle67 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle73 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle69 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle70 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle71 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle72 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle78 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle74 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle75 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle76 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle77 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle79 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
		this->menuStrip_mainMenu = (gcnew System::Windows::Forms::MenuStrip());
		this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
		this->exportToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->taskSplitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->creatureBuilderListToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->developerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->changeConfirmationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->developerSearchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripComboBox_developer_search = (gcnew System::Windows::Forms::ToolStripComboBox());
		this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
		this->toolStripMenuItem17 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_date_spans = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->addRowToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->deleteRowToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->progressBar_progress = (gcnew System::Windows::Forms::ProgressBar());
		this->textBox_search = (gcnew System::Windows::Forms::TextBox());
		this->button_search = (gcnew System::Windows::Forms::Button());
		this->treeView_tasks = (gcnew System::Windows::Forms::TreeView());
		this->contextMenuStrip_task = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->cloneSelectedToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->deleteSelectedToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
		this->exportSelectedItemToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->replaceSelectedItemToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
		this->toolStripMenuItem52 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem53 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->label55 = (gcnew System::Windows::Forms::Label());
		this->checkBox_author_mode = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_time_limit = (gcnew System::Windows::Forms::TextBox());
		this->label32 = (gcnew System::Windows::Forms::Label());
		this->textBox_author_text = (gcnew System::Windows::Forms::TextBox());
		this->textBox_name = (gcnew System::Windows::Forms::TextBox());
		this->label29 = (gcnew System::Windows::Forms::Label());
		this->textBox_id = (gcnew System::Windows::Forms::TextBox());
		this->label28 = (gcnew System::Windows::Forms::Label());
		this->textBox_unknown_07 = (gcnew System::Windows::Forms::TextBox());
		this->label76 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_date_spans = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column6 = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
		this->Column7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column8 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column9 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column10 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column11 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column12 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column13 = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
		this->dataGridView_reward_item_group_items = (gcnew System::Windows::Forms::DataGridView());
		this->Column_reward_item_groups = (gcnew System::Windows::Forms::DataGridViewComboBoxColumn());
		this->dataGridViewTextBoxColumn28 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column29 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn30 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn31 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column30 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->Column23 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column31 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column24 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column25 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column26 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->contextMenuStrip_reward_items = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem18 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem19 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_reward_selector = (gcnew System::Windows::Forms::GroupBox());
		this->radioButton_teacher = (gcnew System::Windows::Forms::RadioButton());
		this->radioButton_team_lead = (gcnew System::Windows::Forms::RadioButton());
		this->radioButton_teammem = (gcnew System::Windows::Forms::RadioButton());
		this->checkedListBox_reward_item_groups_flag = (gcnew System::Windows::Forms::CheckedListBox());
		this->textBox_rewardtype2 = (gcnew System::Windows::Forms::TextBox());
		this->label261 = (gcnew System::Windows::Forms::Label());
		this->label56 = (gcnew System::Windows::Forms::Label());
		this->numericUpDown_reward_item_groups_count = (gcnew System::Windows::Forms::NumericUpDown());
		this->textBox_rewardtype = (gcnew System::Windows::Forms::TextBox());
		this->label260 = (gcnew System::Windows::Forms::Label());
		this->label57 = (gcnew System::Windows::Forms::Label());
		this->numericUpDown_time_factor = (gcnew System::Windows::Forms::NumericUpDown());
		this->label37 = (gcnew System::Windows::Forms::Label());
		this->label3 = (gcnew System::Windows::Forms::Label());
		this->radioButton_timed = (gcnew System::Windows::Forms::RadioButton());
		this->radioButton_failed = (gcnew System::Windows::Forms::RadioButton());
		this->radioButton_success = (gcnew System::Windows::Forms::RadioButton());
		this->listBox_reward_timed = (gcnew System::Windows::Forms::ListBox());
		this->contextMenuStrip_reward_timed = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem38 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem39 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_reward = (gcnew System::Windows::Forms::GroupBox());
		this->label281 = (gcnew System::Windows::Forms::Label());
		this->checkBox_rew1330_10 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rew1330_9 = (gcnew System::Windows::Forms::TextBox());
		this->label285 = (gcnew System::Windows::Forms::Label());
		this->textBox_rew1330_8 = (gcnew System::Windows::Forms::TextBox());
		this->label284 = (gcnew System::Windows::Forms::Label());
		this->textBox_rew1330_7 = (gcnew System::Windows::Forms::TextBox());
		this->label283 = (gcnew System::Windows::Forms::Label());
		this->textBox_rew1330_6 = (gcnew System::Windows::Forms::TextBox());
		this->label282 = (gcnew System::Windows::Forms::Label());
		this->checkBox_runk52 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_noticemess = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rjobup = (gcnew System::Windows::Forms::TextBox());
		this->label275 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk56 = (gcnew System::Windows::Forms::TextBox());
		this->label270 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk55 = (gcnew System::Windows::Forms::TextBox());
		this->label269 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk53 = (gcnew System::Windows::Forms::TextBox());
		this->label268 = (gcnew System::Windows::Forms::Label());
		this->textBox_rhuli = (gcnew System::Windows::Forms::TextBox());
		this->label267 = (gcnew System::Windows::Forms::Label());
		this->checkBox_openreason = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rsound = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rsound = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk51 = (gcnew System::Windows::Forms::TextBox());
		this->label265 = (gcnew System::Windows::Forms::Label());
		this->textBox_run50 = (gcnew System::Windows::Forms::TextBox());
		this->label264 = (gcnew System::Windows::Forms::Label());
		this->textBox_transftime = (gcnew System::Windows::Forms::TextBox());
		this->label263 = (gcnew System::Windows::Forms::Label());
		this->textBox_rtransf = (gcnew System::Windows::Forms::TextBox());
		this->label221 = (gcnew System::Windows::Forms::Label());
		this->textBox_risexpf = (gcnew System::Windows::Forms::TextBox());
		this->label217 = (gcnew System::Windows::Forms::Label());
		this->checkBox_rtomeon = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rskillcnt = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_rskillon = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rcampv = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_rcampon = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_tunk46 = (gcnew System::Windows::Forms::TextBox());
		this->label216 = (gcnew System::Windows::Forms::Label());
		this->textBox_rspecialflg = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_rspecialaward = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rview2 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rview1 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rpanshi = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rchushi = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_runk43 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk42 = (gcnew System::Windows::Forms::TextBox());
		this->label215 = (gcnew System::Windows::Forms::Label());
		this->label214 = (gcnew System::Windows::Forms::Label());
		this->textBox_rdbltime = (gcnew System::Windows::Forms::TextBox());
		this->label213 = (gcnew System::Windows::Forms::Label());
		this->textBox_rremtask = (gcnew System::Windows::Forms::TextBox());
		this->label212 = (gcnew System::Windows::Forms::Label());
		this->textBox_rnotch = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_rnotice = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rfacunk = (gcnew System::Windows::Forms::TextBox());
		this->label211 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk40 = (gcnew System::Windows::Forms::TextBox());
		this->label210 = (gcnew System::Windows::Forms::Label());
		this->textBox_rclanunk = (gcnew System::Windows::Forms::TextBox());
		this->label209 = (gcnew System::Windows::Forms::Label());
		this->textBox_rclanskillid = (gcnew System::Windows::Forms::TextBox());
		this->label208 = (gcnew System::Windows::Forms::Label());
		this->textBox_rskill1 = (gcnew System::Windows::Forms::TextBox());
		this->label207 = (gcnew System::Windows::Forms::Label());
		this->textBox_proff = (gcnew System::Windows::Forms::TextBox());
		this->label206 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk36 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk35 = (gcnew System::Windows::Forms::TextBox());
		this->label205 = (gcnew System::Windows::Forms::Label());
		this->label63 = (gcnew System::Windows::Forms::Label());
		this->checkBox_triggeron = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rsetjob = (gcnew System::Windows::Forms::TextBox());
		this->label204 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk33 = (gcnew System::Windows::Forms::TextBox());
		this->label59 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk32 = (gcnew System::Windows::Forms::TextBox());
		this->label262 = (gcnew System::Windows::Forms::Label());
		this->checkBox_craftup = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_runk301 = (gcnew System::Windows::Forms::TextBox());
		this->label237 = (gcnew System::Windows::Forms::Label());
		this->textBox_mountbag = (gcnew System::Windows::Forms::TextBox());
		this->label236 = (gcnew System::Windows::Forms::Label());
		this->textBox_petslot = (gcnew System::Windows::Forms::TextBox());
		this->label234 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewpetbag = (gcnew System::Windows::Forms::TextBox());
		this->label230 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewbag = (gcnew System::Windows::Forms::TextBox());
		this->label229 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewstash = (gcnew System::Windows::Forms::TextBox());
		this->label228 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk30 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk29 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk28 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk27 = (gcnew System::Windows::Forms::TextBox());
		this->label227 = (gcnew System::Windows::Forms::Label());
		this->label226 = (gcnew System::Windows::Forms::Label());
		this->label225 = (gcnew System::Windows::Forms::Label());
		this->label224 = (gcnew System::Windows::Forms::Label());
		this->textBox_run31 = (gcnew System::Windows::Forms::TextBox());
		this->label152 = (gcnew System::Windows::Forms::Label());
		this->textBox_run23 = (gcnew System::Windows::Forms::TextBox());
		this->label128 = (gcnew System::Windows::Forms::Label());
		this->textBox_run22 = (gcnew System::Windows::Forms::TextBox());
		this->label127 = (gcnew System::Windows::Forms::Label());
		this->textBox_run21 = (gcnew System::Windows::Forms::TextBox());
		this->label24 = (gcnew System::Windows::Forms::Label());
		this->textBox_run20 = (gcnew System::Windows::Forms::TextBox());
		this->label23 = (gcnew System::Windows::Forms::Label());
		this->checkBox_rewdivorce = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rewremallinf = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_clearpk = (gcnew System::Windows::Forms::TextBox());
		this->label126 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewardtitle = (gcnew System::Windows::Forms::TextBox());
		this->label125 = (gcnew System::Windows::Forms::Label());
		this->textBox_run13 = (gcnew System::Windows::Forms::TextBox());
		this->label124 = (gcnew System::Windows::Forms::Label());
		this->textBox_clancontribut = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rewclancont = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run121 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_grint = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run12 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run10 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run09 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run08 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run07 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run06 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run05 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_run04 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_un03 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_un02 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_un01 = (gcnew System::Windows::Forms::TextBox());
		this->label105 = (gcnew System::Windows::Forms::Label());
		this->label104 = (gcnew System::Windows::Forms::Label());
		this->label103 = (gcnew System::Windows::Forms::Label());
		this->label67 = (gcnew System::Windows::Forms::Label());
		this->label66 = (gcnew System::Windows::Forms::Label());
		this->label61 = (gcnew System::Windows::Forms::Label());
		this->label60 = (gcnew System::Windows::Forms::Label());
		this->label53 = (gcnew System::Windows::Forms::Label());
		this->label39 = (gcnew System::Windows::Forms::Label());
		this->label27 = (gcnew System::Windows::Forms::Label());
		this->label13 = (gcnew System::Windows::Forms::Label());
		this->label12 = (gcnew System::Windows::Forms::Label());
		this->label198 = (gcnew System::Windows::Forms::Label());
		this->label197 = (gcnew System::Windows::Forms::Label());
		this->label196 = (gcnew System::Windows::Forms::Label());
		this->label192 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_ai_trigger = (gcnew System::Windows::Forms::TextBox());
		this->textBox_reward_teleport_map_id = (gcnew System::Windows::Forms::TextBox());
		this->textBox_reward_teleport_z = (gcnew System::Windows::Forms::TextBox());
		this->textBox_reward_teleport_x = (gcnew System::Windows::Forms::TextBox());
		this->textBox_reward_teleport_altitude = (gcnew System::Windows::Forms::TextBox());
		this->label26 = (gcnew System::Windows::Forms::Label());
		this->label25 = (gcnew System::Windows::Forms::Label());
		this->label22 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_newtask = (gcnew System::Windows::Forms::TextBox());
		this->label21 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_APLv = (gcnew System::Windows::Forms::TextBox());
		this->label20 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_rclvl = (gcnew System::Windows::Forms::TextBox());
		this->label19 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewardExpLv = (gcnew System::Windows::Forms::TextBox());
		this->label18 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_experience = (gcnew System::Windows::Forms::TextBox());
		this->label17 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_coins = (gcnew System::Windows::Forms::TextBox());
		this->label16 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk26 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_runk25 = (gcnew System::Windows::Forms::TextBox());
		this->label223 = (gcnew System::Windows::Forms::Label());
		this->label193 = (gcnew System::Windows::Forms::Label());
		this->groupBox_conversation = (gcnew System::Windows::Forms::GroupBox());
		this->label278 = (gcnew System::Windows::Forms::Label());
		this->label277 = (gcnew System::Windows::Forms::Label());
		this->textBox_newtxt = (gcnew System::Windows::Forms::TextBox());
		this->textBox_xanfumess = (gcnew System::Windows::Forms::TextBox());
		this->label266 = (gcnew System::Windows::Forms::Label());
		this->textBox_agernmess = (gcnew System::Windows::Forms::TextBox());
		this->textBox_conversation_general_text = (gcnew System::Windows::Forms::TextBox());
		this->label42 = (gcnew System::Windows::Forms::Label());
		this->label40 = (gcnew System::Windows::Forms::Label());
		this->textBox_conversation_prompt_text = (gcnew System::Windows::Forms::TextBox());
		this->groupBox_answers = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_conversation_answer_text = (gcnew System::Windows::Forms::TextBox());
		this->textBox_conversation_answer_task_link = (gcnew System::Windows::Forms::TextBox());
		this->label46 = (gcnew System::Windows::Forms::Label());
		this->textBox_conversation_answer_question_link = (gcnew System::Windows::Forms::TextBox());
		this->label47 = (gcnew System::Windows::Forms::Label());
		this->listBox_conversation_answers = (gcnew System::Windows::Forms::ListBox());
		this->contextMenuStrip_conversation_answer = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem46 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem47 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_conversation_question = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->addToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->removeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_questions = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_conversation_question_text = (gcnew System::Windows::Forms::TextBox());
		this->textBox_conversation_question_previous = (gcnew System::Windows::Forms::TextBox());
		this->label45 = (gcnew System::Windows::Forms::Label());
		this->textBox_conversation_question_id = (gcnew System::Windows::Forms::TextBox());
		this->label44 = (gcnew System::Windows::Forms::Label());
		this->listBox_conversation_questions = (gcnew System::Windows::Forms::ListBox());
		this->groupBox_dialogs = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_conversation_dialog_text = (gcnew System::Windows::Forms::TextBox());
		this->textBox_conversation_dialog_unknown = (gcnew System::Windows::Forms::TextBox());
		this->label43 = (gcnew System::Windows::Forms::Label());
		this->listBox_conversation_dialogs = (gcnew System::Windows::Forms::ListBox());
		this->toolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
		this->label106 = (gcnew System::Windows::Forms::Label());
		this->label107 = (gcnew System::Windows::Forms::Label());
		this->label4 = (gcnew System::Windows::Forms::Label());
		this->label6 = (gcnew System::Windows::Forms::Label());
		this->label41 = (gcnew System::Windows::Forms::Label());
		this->label48 = (gcnew System::Windows::Forms::Label());
		this->label7 = (gcnew System::Windows::Forms::Label());
		this->label8 = (gcnew System::Windows::Forms::Label());
		this->label9 = (gcnew System::Windows::Forms::Label());
		this->label185 = (gcnew System::Windows::Forms::Label());
		this->label11 = (gcnew System::Windows::Forms::Label());
		this->label15 = (gcnew System::Windows::Forms::Label());
		this->comboBox_search = (gcnew System::Windows::Forms::ComboBox());
		this->groupBox_basic_2 = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_parent_quest = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sub_quest_first = (gcnew System::Windows::Forms::TextBox());
		this->label52 = (gcnew System::Windows::Forms::Label());
		this->label51 = (gcnew System::Windows::Forms::Label());
		this->textBox_previous_quest = (gcnew System::Windows::Forms::TextBox());
		this->label50 = (gcnew System::Windows::Forms::Label());
		this->label49 = (gcnew System::Windows::Forms::Label());
		this->textBox_next_quest = (gcnew System::Windows::Forms::TextBox());
		this->contextMenuStrip_pq_messages = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem36 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem37 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_pq_special_scripts = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem34 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem35 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_pq_chases = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem24 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem25 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_pq_script_infos = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem30 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem31 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_pq_location = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem28 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem29 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_basic_1 = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_un13304 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_un13303 = (gcnew System::Windows::Forms::TextBox());
		this->label280 = (gcnew System::Windows::Forms::Label());
		this->label279 = (gcnew System::Windows::Forms::Label());
		this->textBox_bldbmax = (gcnew System::Windows::Forms::TextBox());
		this->textBox_bldbmin = (gcnew System::Windows::Forms::TextBox());
		this->label242 = (gcnew System::Windows::Forms::Label());
		this->checkBox_showlvl = (gcnew System::Windows::Forms::CheckBox());
		this->label62 = (gcnew System::Windows::Forms::Label());
		this->textBox_newtype2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_newtype = (gcnew System::Windows::Forms::TextBox());
		this->label116 = (gcnew System::Windows::Forms::Label());
		this->label115 = (gcnew System::Windows::Forms::Label());
		this->checkBox_clantask = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_classify = (gcnew System::Windows::Forms::TextBox());
		this->label110 = (gcnew System::Windows::Forms::Label());
		this->label108 = (gcnew System::Windows::Forms::Label());
		this->textBox_component = (gcnew System::Windows::Forms::TextBox());
		this->textBox_cleartask = (gcnew System::Windows::Forms::TextBox());
		this->textBox_autounk = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_trigger_on = (gcnew System::Windows::Forms::CheckBox());
		this->label35 = (gcnew System::Windows::Forms::Label());
		this->textBox_level_min = (gcnew System::Windows::Forms::TextBox());
		this->label36 = (gcnew System::Windows::Forms::Label());
		this->textBox_level_max = (gcnew System::Windows::Forms::TextBox());
		this->label33 = (gcnew System::Windows::Forms::Label());
		this->textBox_quest_npc = (gcnew System::Windows::Forms::TextBox());
		this->label34 = (gcnew System::Windows::Forms::Label());
		this->textBox_reward_npc = (gcnew System::Windows::Forms::TextBox());
		this->label123 = (gcnew System::Windows::Forms::Label());
		this->checkBox_mark_available_point = (gcnew System::Windows::Forms::CheckBox());
		this->label122 = (gcnew System::Windows::Forms::Label());
		this->checkBox_mark_available_icon = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_instant_teleport_location_z = (gcnew System::Windows::Forms::TextBox());
		this->label121 = (gcnew System::Windows::Forms::Label());
		this->textBox_instant_teleport_location_alt = (gcnew System::Windows::Forms::TextBox());
		this->label120 = (gcnew System::Windows::Forms::Label());
		this->textBox_instant_teleport_location_x = (gcnew System::Windows::Forms::TextBox());
		this->label119 = (gcnew System::Windows::Forms::Label());
		this->textBox_instant_teleport_location_map_id = (gcnew System::Windows::Forms::TextBox());
		this->label118 = (gcnew System::Windows::Forms::Label());
		this->textBox_unknown_level = (gcnew System::Windows::Forms::TextBox());
		this->label117 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_23 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_unknown_17 = (gcnew System::Windows::Forms::CheckBox());
		this->label109 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_22 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_has_instant_teleport = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_unknown_21 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_unknown_18 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_ai_trigger = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_unknown_19 = (gcnew System::Windows::Forms::CheckBox());
		this->label113 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_20 = (gcnew System::Windows::Forms::CheckBox());
		this->contextMenuStrip_valid_location = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem5 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem6 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_fail_location = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem3 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem4 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_trigger_location = (gcnew System::Windows::Forms::GroupBox());
		this->dataGridView_trigger_location_spans = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn37 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn38 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn39 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn40 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn41 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn42 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->checkBox_trigger_locations_has_spans = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_trigger_locations_map_id = (gcnew System::Windows::Forms::TextBox());
		this->contextMenuStrip_trigger_location = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->addToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->removeToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_flags = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_plimitnum = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_haslimit = (gcnew System::Windows::Forms::CheckBox());
		this->label114 = (gcnew System::Windows::Forms::Label());
		this->textBox_allkillnum = (gcnew System::Windows::Forms::TextBox());
		this->label112 = (gcnew System::Windows::Forms::Label());
		this->label111 = (gcnew System::Windows::Forms::Label());
		this->textBox_playerpenalty = (gcnew System::Windows::Forms::TextBox());
		this->textBox_unkpenalty = (gcnew System::Windows::Forms::TextBox());
		this->textBox_allzonenum = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_allzone = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_plimit = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_player_limit = (gcnew System::Windows::Forms::TextBox());
		this->label102 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_globals = (gcnew System::Windows::Forms::DataGridView());
		this->Column33 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column34 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column35 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column36 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->label101 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_10 = (gcnew System::Windows::Forms::CheckBox());
		this->label100 = (gcnew System::Windows::Forms::Label());
		this->checkBox_on_fail_parent_fail = (gcnew System::Windows::Forms::CheckBox());
		this->label99 = (gcnew System::Windows::Forms::Label());
		this->checkBox_fail_on_death = (gcnew System::Windows::Forms::CheckBox());
		this->label98 = (gcnew System::Windows::Forms::Label());
		this->checkBox_repeatable_after_failure = (gcnew System::Windows::Forms::CheckBox());
		this->label97 = (gcnew System::Windows::Forms::Label());
		this->checkBox_repeatable = (gcnew System::Windows::Forms::CheckBox());
		this->label96 = (gcnew System::Windows::Forms::Label());
		this->checkBox_can_give_up = (gcnew System::Windows::Forms::CheckBox());
		this->label95 = (gcnew System::Windows::Forms::Label());
		this->checkBox_on_success_parent_success = (gcnew System::Windows::Forms::CheckBox());
		this->label94 = (gcnew System::Windows::Forms::Label());
		this->checkBox_on_give_up_parent_fails = (gcnew System::Windows::Forms::CheckBox());
		this->label93 = (gcnew System::Windows::Forms::Label());
		this->checkBox_activate_next_subquest = (gcnew System::Windows::Forms::CheckBox());
		this->label92 = (gcnew System::Windows::Forms::Label());
		this->checkBox_activate_random_subquest = (gcnew System::Windows::Forms::CheckBox());
		this->label91 = (gcnew System::Windows::Forms::Label());
		this->checkBox_activate_first_subquest = (gcnew System::Windows::Forms::CheckBox());
		this->contextMenuStrip_morai_pk = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem48 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem49 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_team_members = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem13 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem14 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_given_items = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem9 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem10 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_required_items = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem7 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem8 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_required_get_items = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem11 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem12 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_chases = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem26 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem27 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->groupBox_general = (gcnew System::Windows::Forms::GroupBox());
		this->checkBox_un13302 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_un13301 = (gcnew System::Windows::Forms::TextBox());
		this->label38 = (gcnew System::Windows::Forms::Label());
		this->textBox_type = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_valunk3 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_resetcycle = (gcnew System::Windows::Forms::TextBox());
		this->textBox_resettype = (gcnew System::Windows::Forms::TextBox());
		this->label241 = (gcnew System::Windows::Forms::Label());
		this->label240 = (gcnew System::Windows::Forms::Label());
		this->textBox_cooldown = (gcnew System::Windows::Forms::TextBox());
		this->label239 = (gcnew System::Windows::Forms::Label());
		this->label238 = (gcnew System::Windows::Forms::Label());
		this->textBox_xunhan = (gcnew System::Windows::Forms::TextBox());
		this->textBox_unknown_09 = (gcnew System::Windows::Forms::TextBox());
		this->label90 = (gcnew System::Windows::Forms::Label());
		this->textBox_unknown_08 = (gcnew System::Windows::Forms::TextBox());
		this->label89 = (gcnew System::Windows::Forms::Label());
		this->label88 = (gcnew System::Windows::Forms::Label());
		this->label82 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_03 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_unknown_02 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unknown_06 = (gcnew System::Windows::Forms::TextBox());
		this->label85 = (gcnew System::Windows::Forms::Label());
		this->textBox_unknown_05 = (gcnew System::Windows::Forms::TextBox());
		this->label84 = (gcnew System::Windows::Forms::Label());
		this->textBox_unknown_04 = (gcnew System::Windows::Forms::TextBox());
		this->label31 = (gcnew System::Windows::Forms::Label());
		this->checkBox_has_date_spans = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_has_date_fail = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unknown_01 = (gcnew System::Windows::Forms::TextBox());
		this->label81 = (gcnew System::Windows::Forms::Label());
		this->groupBox_reach_location = (gcnew System::Windows::Forms::GroupBox());
		this->checkBox_reach_locations_has_spans = (gcnew System::Windows::Forms::CheckBox());
		this->dataGridView_reach_location_spans = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn60 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn61 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn62 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn63 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn64 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn65 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->textBox_reach_locations_map_id = (gcnew System::Windows::Forms::TextBox());
		this->contextMenuStrip_reach_location = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem15 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem16 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
		this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
		this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
		this->groupBox_fail_location = (gcnew System::Windows::Forms::GroupBox());
		this->checkBox_fail_locations_has_spans = (gcnew System::Windows::Forms::CheckBox());
		this->dataGridView_fail_location_spans = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn43 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn44 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn45 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn46 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn47 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn48 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->textBox_fail_locations_map_id = (gcnew System::Windows::Forms::TextBox());
		this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
		this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
		this->groupBox_requirements = (gcnew System::Windows::Forms::GroupBox());
		this->label151 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_dynamics = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn25 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn29 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn32 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn33 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn34 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn35 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn36 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn55 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn56 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->Column21 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->Column22 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->textBox3 = (gcnew System::Windows::Forms::TextBox());
		this->label235 = (gcnew System::Windows::Forms::Label());
		this->listBox_tops = (gcnew System::Windows::Forms::ListBox());
		this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem50 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem51 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->textBox_needlvl = (gcnew System::Windows::Forms::TextBox());
		this->label186 = (gcnew System::Windows::Forms::Label());
		this->textBox_titlechange = (gcnew System::Windows::Forms::TextBox());
		this->listBox_titles = (gcnew System::Windows::Forms::ListBox());
		this->textBox_waittime = (gcnew System::Windows::Forms::TextBox());
		this->label183 = (gcnew System::Windows::Forms::Label());
		this->textBox_npctime = (gcnew System::Windows::Forms::TextBox());
		this->label180 = (gcnew System::Windows::Forms::Label());
		this->textBox_safenpc = (gcnew System::Windows::Forms::TextBox());
		this->label179 = (gcnew System::Windows::Forms::Label());
		this->textBox_noncash = (gcnew System::Windows::Forms::TextBox());
		this->label178 = (gcnew System::Windows::Forms::Label());
		this->textBox_completetype = (gcnew System::Windows::Forms::TextBox());
		this->label173 = (gcnew System::Windows::Forms::Label());
		this->textBox_method = (gcnew System::Windows::Forms::TextBox());
		this->label171 = (gcnew System::Windows::Forms::Label());
		this->textBox_soulmax = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_soulmaxon = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_soulminom = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_soulmin = (gcnew System::Windows::Forms::TextBox());
		this->label149 = (gcnew System::Windows::Forms::Label());
		this->textBox_soulunkn1 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_ingotmax = (gcnew System::Windows::Forms::TextBox());
		this->textBox_ingotmin = (gcnew System::Windows::Forms::TextBox());
		this->label131 = (gcnew System::Windows::Forms::Label());
		this->label130 = (gcnew System::Windows::Forms::Label());
		this->label73 = (gcnew System::Windows::Forms::Label());
		this->textBox_zhenying = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_frongfang03 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_frontfeng02 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_frontfeng01 = (gcnew System::Windows::Forms::CheckBox());
		this->label143 = (gcnew System::Windows::Forms::Label());
		this->textBox_blamon = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_blded = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_clanchmax = (gcnew System::Windows::Forms::TextBox());
		this->textBox_clanchmin = (gcnew System::Windows::Forms::TextBox());
		this->label140 = (gcnew System::Windows::Forms::Label());
		this->label139 = (gcnew System::Windows::Forms::Label());
		this->textBox_clanmap = (gcnew System::Windows::Forms::TextBox());
		this->label138 = (gcnew System::Windows::Forms::Label());
		this->textBox_clanskillid = (gcnew System::Windows::Forms::TextBox());
		this->label137 = (gcnew System::Windows::Forms::Label());
		this->textBox_clanskillexpmax = (gcnew System::Windows::Forms::TextBox());
		this->textBox_clanskillexpmin = (gcnew System::Windows::Forms::TextBox());
		this->label136 = (gcnew System::Windows::Forms::Label());
		this->label135 = (gcnew System::Windows::Forms::Label());
		this->textBox_clanskillmax = (gcnew System::Windows::Forms::TextBox());
		this->textBox_clanskillmin = (gcnew System::Windows::Forms::TextBox());
		this->label133 = (gcnew System::Windows::Forms::Label());
		this->label80 = (gcnew System::Windows::Forms::Label());
		this->checkBox_clanlead = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_clanc = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_vtask = (gcnew System::Windows::Forms::TextBox());
		this->label77 = (gcnew System::Windows::Forms::Label());
		this->label30 = (gcnew System::Windows::Forms::Label());
		this->textBox_viewlvl = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_vunkn2 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_vunkn1 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_vpeach = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_vchef = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_view = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_viewreq = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_leavefail = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_tamaccfail = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_leaderfail = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_teamchecknum = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_teamshare2 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_teamunk = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_teamshare = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_teamask = (gcnew System::Windows::Forms::CheckBox());
		this->label166 = (gcnew System::Windows::Forms::Label());
		this->label164 = (gcnew System::Windows::Forms::Label());
		this->textBox_maxpk = (gcnew System::Windows::Forms::TextBox());
		this->textBox_minpk = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_frontmarried = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_frontunkn01 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_licreq = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_frontgen = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unknfront = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_frontalliance = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_frontgender = (gcnew System::Windows::Forms::TextBox());
		this->textBox_zhuanchonglv = (gcnew System::Windows::Forms::TextBox());
		this->textBox_renwu = (gcnew System::Windows::Forms::TextBox());
		this->textBox_yaoqiu = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_tasknum = (gcnew System::Windows::Forms::CheckBox());
		this->textBox1 = (gcnew System::Windows::Forms::TextBox());
		this->listBox_jobs = (gcnew System::Windows::Forms::ListBox());
		this->label257 = (gcnew System::Windows::Forms::Label());
		this->label256 = (gcnew System::Windows::Forms::Label());
		this->label255 = (gcnew System::Windows::Forms::Label());
		this->label254 = (gcnew System::Windows::Forms::Label());
		this->label253 = (gcnew System::Windows::Forms::Label());
		this->label252 = (gcnew System::Windows::Forms::Label());
		this->label251 = (gcnew System::Windows::Forms::Label());
		this->label250 = (gcnew System::Windows::Forms::Label());
		this->label249 = (gcnew System::Windows::Forms::Label());
		this->label248 = (gcnew System::Windows::Forms::Label());
		this->label247 = (gcnew System::Windows::Forms::Label());
		this->label246 = (gcnew System::Windows::Forms::Label());
		this->label245 = (gcnew System::Windows::Forms::Label());
		this->label244 = (gcnew System::Windows::Forms::Label());
		this->label243 = (gcnew System::Windows::Forms::Label());
		this->label87 = (gcnew System::Windows::Forms::Label());
		this->label86 = (gcnew System::Windows::Forms::Label());
		this->label83 = (gcnew System::Windows::Forms::Label());
		this->label69 = (gcnew System::Windows::Forms::Label());
		this->label58 = (gcnew System::Windows::Forms::Label());
		this->textBox_rep20 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep19 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep13 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep14 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep15 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep17 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep16 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep18 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_hassheng = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_rep7 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep8 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep12 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep11 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep10 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep9 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep6 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep5 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep3 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rep1 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_needgender = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_autoval = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_hasauto = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_workval = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_haswork = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_contribu = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_needclan = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_hasnextval = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_hasnext = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_rineedbag = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_iunk11 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_iunk10 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_iunk09 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_itunk07 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_itunk06 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_iunk4 = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_iunk5 = (gcnew System::Windows::Forms::CheckBox());
		this->label222 = (gcnew System::Windows::Forms::Label());
		this->label74 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_team_members = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn8 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn9 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn10 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn11 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn12 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn13 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn14 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn15 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column18 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column32 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->label68 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_given_items = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn24 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn5 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn26 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn27 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridView_required_items = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column37 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->label2 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_required_get_items = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn16 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn17 = (gcnew System::Windows::Forms::DataGridViewCheckBoxColumn());
		this->dataGridViewTextBoxColumn18 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn19 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column17 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_required_chases = (gcnew System::Windows::Forms::DataGridView());
		this->dataGridViewTextBoxColumn20 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn21 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn22 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->dataGridViewTextBoxColumn23 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column14 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column15 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column16 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->textBox_required_quests_undone_1 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_undone_2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_undone_3 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_undone_5 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_undone_4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_unknown_44 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_required_be_gm = (gcnew System::Windows::Forms::CheckBox());
		this->checkBox_required_be_married = (gcnew System::Windows::Forms::CheckBox());
		this->comboBox_required_gender = (gcnew System::Windows::Forms::ComboBox());
		this->textBox_required_quests_done_1 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_done_2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_done_3 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_done_4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_quests_done_5 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_required_items_unknown = (gcnew System::Windows::Forms::TextBox());
		this->label70 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown_27 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_instant_pay_coins = (gcnew System::Windows::Forms::TextBox());
		this->label65 = (gcnew System::Windows::Forms::Label());
		this->dataGridView_tasknum = (gcnew System::Windows::Forms::DataGridView());
		this->Column19 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->Column20 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
		this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
		this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_rewRenZong = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rewDiZong = (gcnew System::Windows::Forms::TextBox());
		this->textBox_TianZong = (gcnew System::Windows::Forms::TextBox());
		this->textBox_ZhanXun = (gcnew System::Windows::Forms::TextBox());
		this->textBox_ZhanJi = (gcnew System::Windows::Forms::TextBox());
		this->textBox_FenXiang = (gcnew System::Windows::Forms::TextBox());
		this->textBox_TianHua = (gcnew System::Windows::Forms::TextBox());
		this->textBox_HuaiGuang = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rewreptaihao = (gcnew System::Windows::Forms::TextBox());
		this->textBox_ChenHuang = (gcnew System::Windows::Forms::TextBox());
		this->textBox_LieShan = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rewJiuLi = (gcnew System::Windows::Forms::TextBox());
		this->label203 = (gcnew System::Windows::Forms::Label());
		this->label202 = (gcnew System::Windows::Forms::Label());
		this->label201 = (gcnew System::Windows::Forms::Label());
		this->label200 = (gcnew System::Windows::Forms::Label());
		this->label199 = (gcnew System::Windows::Forms::Label());
		this->label195 = (gcnew System::Windows::Forms::Label());
		this->label194 = (gcnew System::Windows::Forms::Label());
		this->label191 = (gcnew System::Windows::Forms::Label());
		this->label190 = (gcnew System::Windows::Forms::Label());
		this->label189 = (gcnew System::Windows::Forms::Label());
		this->label176 = (gcnew System::Windows::Forms::Label());
		this->label175 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk15 = (gcnew System::Windows::Forms::TextBox());
		this->label172 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepfoyuan = (gcnew System::Windows::Forms::TextBox());
		this->label165 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewmoxing = (gcnew System::Windows::Forms::TextBox());
		this->label163 = (gcnew System::Windows::Forms::Label());
		this->textBox_DaoXin = (gcnew System::Windows::Forms::TextBox());
		this->label162 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewshide = (gcnew System::Windows::Forms::TextBox());
		this->label161 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewwencai = (gcnew System::Windows::Forms::TextBox());
		this->label160 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewQingyuan = (gcnew System::Windows::Forms::TextBox());
		this->label159 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepzhongyi = (gcnew System::Windows::Forms::TextBox());
		this->label158 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewreplupi = (gcnew System::Windows::Forms::TextBox());
		this->label157 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepvim = (gcnew System::Windows::Forms::TextBox());
		this->label156 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepsky = (gcnew System::Windows::Forms::TextBox());
		this->label155 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepjad = (gcnew System::Windows::Forms::TextBox());
		this->label154 = (gcnew System::Windows::Forms::Label());
		this->textBox_rewrepmodo = (gcnew System::Windows::Forms::TextBox());
		this->label153 = (gcnew System::Windows::Forms::Label());
		this->groupBox_reward_items = (gcnew System::Windows::Forms::GroupBox());
		this->textBox_expfname = (gcnew System::Windows::Forms::TextBox());
		this->label276 = (gcnew System::Windows::Forms::Label());
		this->textBox_rnval2len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rnval1len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rnval4h = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rnval3h = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rnval2h = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rnval1h = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_rnval = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_runkn4 = (gcnew System::Windows::Forms::TextBox());
		this->label274 = (gcnew System::Windows::Forms::Label());
		this->textBox_runkn3 = (gcnew System::Windows::Forms::TextBox());
		this->label273 = (gcnew System::Windows::Forms::Label());
		this->textBox_runkn2 = (gcnew System::Windows::Forms::TextBox());
		this->label272 = (gcnew System::Windows::Forms::Label());
		this->textBox_runkn1 = (gcnew System::Windows::Forms::TextBox());
		this->label271 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk49 = (gcnew System::Windows::Forms::TextBox());
		this->label220 = (gcnew System::Windows::Forms::Label());
		this->textBox_rvalc = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rval3 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_rval2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_val1 = (gcnew System::Windows::Forms::TextBox());
		this->label219 = (gcnew System::Windows::Forms::Label());
		this->label218 = (gcnew System::Windows::Forms::Label());
		this->textBox_runk48 = (gcnew System::Windows::Forms::TextBox());
		this->tabPage7 = (gcnew System::Windows::Forms::TabPage());
		this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
		this->textBox_unkn134 = (gcnew System::Windows::Forms::TextBox());
		this->label259 = (gcnew System::Windows::Forms::Label());
		this->textBox_sval5 = (gcnew System::Windows::Forms::TextBox());
		this->label258 = (gcnew System::Windows::Forms::Label());
		this->textBox_sval4len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sval3len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_svallen2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sva1len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sval4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sval3 = (gcnew System::Windows::Forms::TextBox());
		this->label188 = (gcnew System::Windows::Forms::Label());
		this->label231 = (gcnew System::Windows::Forms::Label());
		this->label232 = (gcnew System::Windows::Forms::Label());
		this->label233 = (gcnew System::Windows::Forms::Label());
		this->textBox_sval2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_sval1 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_conval2 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unkn133 = (gcnew System::Windows::Forms::TextBox());
		this->label187 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn132 = (gcnew System::Windows::Forms::TextBox());
		this->label184 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn131 = (gcnew System::Windows::Forms::TextBox());
		this->label182 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn130 = (gcnew System::Windows::Forms::TextBox());
		this->label181 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn23456 = (gcnew System::Windows::Forms::TextBox());
		this->label177 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn129 = (gcnew System::Windows::Forms::TextBox());
		this->label174 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn1628 = (gcnew System::Windows::Forms::TextBox());
		this->label150 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn128 = (gcnew System::Windows::Forms::TextBox());
		this->label148 = (gcnew System::Windows::Forms::Label());
		this->textBox_val4len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_val3len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_val2len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_val1len = (gcnew System::Windows::Forms::TextBox());
		this->textBox_nval4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_nval3 = (gcnew System::Windows::Forms::TextBox());
		this->label147 = (gcnew System::Windows::Forms::Label());
		this->label146 = (gcnew System::Windows::Forms::Label());
		this->label145 = (gcnew System::Windows::Forms::Label());
		this->label144 = (gcnew System::Windows::Forms::Label());
		this->textBox_nval2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox_nval1 = (gcnew System::Windows::Forms::TextBox());
		this->checkBox_nval = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unkn127 = (gcnew System::Windows::Forms::TextBox());
		this->label72 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn126 = (gcnew System::Windows::Forms::TextBox());
		this->label142 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn125 = (gcnew System::Windows::Forms::TextBox());
		this->label141 = (gcnew System::Windows::Forms::Label());
		this->textBox_ubkb124 = (gcnew System::Windows::Forms::TextBox());
		this->label78 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn123 = (gcnew System::Windows::Forms::TextBox());
		this->label170 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn122 = (gcnew System::Windows::Forms::TextBox());
		this->label169 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn121 = (gcnew System::Windows::Forms::TextBox());
		this->label168 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn120 = (gcnew System::Windows::Forms::TextBox());
		this->label134 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn119 = (gcnew System::Windows::Forms::TextBox());
		this->label132 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn118 = (gcnew System::Windows::Forms::TextBox());
		this->label167 = (gcnew System::Windows::Forms::Label());
		this->checkBox_unknown31 = (gcnew System::Windows::Forms::CheckBox());
		this->textBox_unkn116 = (gcnew System::Windows::Forms::TextBox());
		this->label75 = (gcnew System::Windows::Forms::Label());
		this->label54 = (gcnew System::Windows::Forms::Label());
		this->textBox_unkn115 = (gcnew System::Windows::Forms::TextBox());
		this->contextMenuStrip_reward_pq_specials = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem42 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem43 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_reward_pq_items = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem22 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem23 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_reward_pq_chases = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem20 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem21 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_reward_pq_messages = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem44 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem45 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_reward_pq_scripts = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem40 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem41 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->contextMenuStrip_pq_scripts = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
		this->toolStripMenuItem32 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem33 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->menuStrip_mainMenu->SuspendLayout();
		this->contextMenuStrip_date_spans->SuspendLayout();
		this->contextMenuStrip_task->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_date_spans))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_reward_item_group_items))->BeginInit();
		this->contextMenuStrip_reward_items->SuspendLayout();
		this->groupBox_reward_selector->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown_reward_item_groups_count))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown_time_factor))->BeginInit();
		this->contextMenuStrip_reward_timed->SuspendLayout();
		this->groupBox_reward->SuspendLayout();
		this->groupBox_conversation->SuspendLayout();
		this->groupBox_answers->SuspendLayout();
		this->contextMenuStrip_conversation_answer->SuspendLayout();
		this->contextMenuStrip_conversation_question->SuspendLayout();
		this->groupBox_questions->SuspendLayout();
		this->groupBox_dialogs->SuspendLayout();
		this->groupBox_basic_2->SuspendLayout();
		this->contextMenuStrip_pq_messages->SuspendLayout();
		this->contextMenuStrip_pq_special_scripts->SuspendLayout();
		this->contextMenuStrip_pq_chases->SuspendLayout();
		this->contextMenuStrip_pq_script_infos->SuspendLayout();
		this->contextMenuStrip_pq_location->SuspendLayout();
		this->groupBox_basic_1->SuspendLayout();
		this->contextMenuStrip_valid_location->SuspendLayout();
		this->contextMenuStrip_fail_location->SuspendLayout();
		this->groupBox_trigger_location->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_trigger_location_spans))->BeginInit();
		this->contextMenuStrip_trigger_location->SuspendLayout();
		this->groupBox_flags->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_globals))->BeginInit();
		this->contextMenuStrip_morai_pk->SuspendLayout();
		this->contextMenuStrip_team_members->SuspendLayout();
		this->contextMenuStrip_given_items->SuspendLayout();
		this->contextMenuStrip_required_items->SuspendLayout();
		this->contextMenuStrip_required_get_items->SuspendLayout();
		this->contextMenuStrip_chases->SuspendLayout();
		this->groupBox_general->SuspendLayout();
		this->groupBox_reach_location->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_reach_location_spans))->BeginInit();
		this->contextMenuStrip_reach_location->SuspendLayout();
		this->tabControl1->SuspendLayout();
		this->tabPage1->SuspendLayout();
		this->tabPage2->SuspendLayout();
		this->groupBox_fail_location->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_fail_location_spans))->BeginInit();
		this->tabPage3->SuspendLayout();
		this->tabPage4->SuspendLayout();
		this->groupBox_requirements->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_dynamics))->BeginInit();
		this->contextMenuStrip1->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_team_members))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_given_items))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_items))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_get_items))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_chases))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_tasknum))->BeginInit();
		this->tabPage6->SuspendLayout();
		this->groupBox1->SuspendLayout();
		this->groupBox_reward_items->SuspendLayout();
		this->tabPage7->SuspendLayout();
		this->tabPage5->SuspendLayout();
		this->contextMenuStrip_reward_pq_specials->SuspendLayout();
		this->contextMenuStrip_reward_pq_items->SuspendLayout();
		this->contextMenuStrip_reward_pq_chases->SuspendLayout();
		this->contextMenuStrip_reward_pq_messages->SuspendLayout();
		this->contextMenuStrip_reward_pq_scripts->SuspendLayout();
		this->contextMenuStrip_pq_scripts->SuspendLayout();
		this->SuspendLayout();
		// 
		// menuStrip_mainMenu
		// 
		this->menuStrip_mainMenu->BackColor = System::Drawing::SystemColors::Control;
		this->menuStrip_mainMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->toolStripMenuItem1, 
			this->toolStripMenuItem2, this->developerToolStripMenuItem});
		this->menuStrip_mainMenu->Location = System::Drawing::Point(0, 0);
		this->menuStrip_mainMenu->Name = L"menuStrip_mainMenu";
		this->menuStrip_mainMenu->Padding = System::Windows::Forms::Padding(0, 2, 2, 2);
		this->menuStrip_mainMenu->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
		this->menuStrip_mainMenu->Size = System::Drawing::Size(931, 24);
		this->menuStrip_mainMenu->TabIndex = 0;
		this->menuStrip_mainMenu->Text = L"menuStrip1";
		// 
		// toolStripMenuItem1
		// 
		this->toolStripMenuItem1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
		this->toolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->loadToolStripMenuItem, 
			this->saveToolStripMenuItem, this->toolStripSeparator3, this->exportToolStripMenuItem});
		this->toolStripMenuItem1->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
		this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
		this->toolStripMenuItem1->Padding = System::Windows::Forms::Padding(0);
		this->toolStripMenuItem1->Size = System::Drawing::Size(27, 20);
		this->toolStripMenuItem1->Text = L"File";
		this->toolStripMenuItem1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
		// 
		// loadToolStripMenuItem
		// 
		this->loadToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
		this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
		this->loadToolStripMenuItem->Size = System::Drawing::Size(109, 22);
		this->loadToolStripMenuItem->Text = L"Load...";
		this->loadToolStripMenuItem->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
		this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_load);
		// 
		// saveToolStripMenuItem
		// 
		this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
		this->saveToolStripMenuItem->Size = System::Drawing::Size(109, 22);
		this->saveToolStripMenuItem->Text = L"Save";
		this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_save);
		// 
		// toolStripSeparator3
		// 
		this->toolStripSeparator3->Name = L"toolStripSeparator3";
		this->toolStripSeparator3->Size = System::Drawing::Size(106, 6);
		// 
		// exportToolStripMenuItem
		// 
		this->exportToolStripMenuItem->Name = L"exportToolStripMenuItem";
		this->exportToolStripMenuItem->Size = System::Drawing::Size(109, 22);
		this->exportToolStripMenuItem->Text = L"Export";
		// 
		// toolStripMenuItem2
		// 
		this->toolStripMenuItem2->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
		this->toolStripMenuItem2->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->taskSplitToolStripMenuItem, 
			this->creatureBuilderListToolStripMenuItem});
		this->toolStripMenuItem2->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
		this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
		this->toolStripMenuItem2->Padding = System::Windows::Forms::Padding(0);
		this->toolStripMenuItem2->Size = System::Drawing::Size(36, 20);
		this->toolStripMenuItem2->Text = L"Tools";
		this->toolStripMenuItem2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
		// 
		// taskSplitToolStripMenuItem
		// 
		this->taskSplitToolStripMenuItem->Name = L"taskSplitToolStripMenuItem";
		this->taskSplitToolStripMenuItem->Size = System::Drawing::Size(183, 22);
		this->taskSplitToolStripMenuItem->Text = L"Task Split...";
		this->taskSplitToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_split);
		// 
		// creatureBuilderListToolStripMenuItem
		// 
		this->creatureBuilderListToolStripMenuItem->Name = L"creatureBuilderListToolStripMenuItem";
		this->creatureBuilderListToolStripMenuItem->Size = System::Drawing::Size(183, 22);
		this->creatureBuilderListToolStripMenuItem->Text = L"Creature Builder List...";
		this->creatureBuilderListToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_creatureBuilder);
		// 
		// developerToolStripMenuItem
		// 
		this->developerToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->changeConfirmationToolStripMenuItem, 
			this->developerSearchToolStripMenuItem});
		this->developerToolStripMenuItem->Name = L"developerToolStripMenuItem";
		this->developerToolStripMenuItem->Size = System::Drawing::Size(68, 20);
		this->developerToolStripMenuItem->Text = L"Developer";
		// 
		// changeConfirmationToolStripMenuItem
		// 
		this->changeConfirmationToolStripMenuItem->CheckOnClick = true;
		this->changeConfirmationToolStripMenuItem->Name = L"changeConfirmationToolStripMenuItem";
		this->changeConfirmationToolStripMenuItem->Size = System::Drawing::Size(175, 22);
		this->changeConfirmationToolStripMenuItem->Text = L"Change Confirmation";
		// 
		// developerSearchToolStripMenuItem
		// 
		this->developerSearchToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->toolStripComboBox_developer_search, 
			this->toolStripSeparator2, this->toolStripMenuItem17});
		this->developerSearchToolStripMenuItem->Name = L"developerSearchToolStripMenuItem";
		this->developerSearchToolStripMenuItem->Size = System::Drawing::Size(175, 22);
		this->developerSearchToolStripMenuItem->Text = L"Search !NULL Fields";
		// 
		// toolStripComboBox_developer_search
		// 
		this->toolStripComboBox_developer_search->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
		this->toolStripComboBox_developer_search->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->toolStripComboBox_developer_search->Items->AddRange(gcnew cli::array< System::Object^  >(12) {L"Required be GM", L"UNKNOWN_43", 
			L"Prestige", L"Influence", L"Event Gold", L"Reward PQ Chases", L"Reward PQ Items", L"Reward PQ Scripts", L"Reward PQ Messages", 
			L"Reward Unknown 5", L"Reward Unknown 6", L"Reward Timed"});
		this->toolStripComboBox_developer_search->Name = L"toolStripComboBox_developer_search";
		this->toolStripComboBox_developer_search->Size = System::Drawing::Size(121, 21);
		// 
		// toolStripSeparator2
		// 
		this->toolStripSeparator2->Name = L"toolStripSeparator2";
		this->toolStripSeparator2->Size = System::Drawing::Size(178, 6);
		// 
		// toolStripMenuItem17
		// 
		this->toolStripMenuItem17->Name = L"toolStripMenuItem17";
		this->toolStripMenuItem17->Size = System::Drawing::Size(181, 22);
		this->toolStripMenuItem17->Text = L"Start Search";
		this->toolStripMenuItem17->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_developer_search);
		// 
		// contextMenuStrip_date_spans
		// 
		this->contextMenuStrip_date_spans->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->addRowToolStripMenuItem, 
			this->deleteRowToolStripMenuItem});
		this->contextMenuStrip_date_spans->Name = L"contextMenuStrip_dataGrid";
		this->contextMenuStrip_date_spans->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
		this->contextMenuStrip_date_spans->ShowImageMargin = false;
		this->contextMenuStrip_date_spans->Size = System::Drawing::Size(134, 48);
		// 
		// addRowToolStripMenuItem
		// 
		this->addRowToolStripMenuItem->Name = L"addRowToolStripMenuItem";
		this->addRowToolStripMenuItem->Size = System::Drawing::Size(133, 22);
		this->addRowToolStripMenuItem->Text = L"Add Date Span";
		this->addRowToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_date_span);
		// 
		// deleteRowToolStripMenuItem
		// 
		this->deleteRowToolStripMenuItem->Name = L"deleteRowToolStripMenuItem";
		this->deleteRowToolStripMenuItem->Size = System::Drawing::Size(133, 22);
		this->deleteRowToolStripMenuItem->Text = L"Delete Date Span";
		this->deleteRowToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_date_span);
		// 
		// progressBar_progress
		// 
		this->progressBar_progress->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->progressBar_progress->Location = System::Drawing::Point(0, 637);
		this->progressBar_progress->Name = L"progressBar_progress";
		this->progressBar_progress->Size = System::Drawing::Size(931, 16);
		this->progressBar_progress->TabIndex = 7;
		// 
		// textBox_search
		// 
		this->textBox_search->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_search->Location = System::Drawing::Point(0, 584);
		this->textBox_search->Name = L"textBox_search";
		this->textBox_search->Size = System::Drawing::Size(222, 20);
		this->textBox_search->TabIndex = 2;
		this->textBox_search->Text = L"ID or NAME";
		this->textBox_search->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_search->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &zxTASKeditWindow::keyPress_search);
		// 
		// button_search
		// 
		this->button_search->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->button_search->Location = System::Drawing::Point(143, 610);
		this->button_search->Name = L"button_search";
		this->button_search->Size = System::Drawing::Size(79, 21);
		this->button_search->TabIndex = 4;
		this->button_search->Text = L"Find Next";
		this->button_search->UseVisualStyleBackColor = true;
		this->button_search->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_search);
		// 
		// treeView_tasks
		// 
		this->treeView_tasks->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->treeView_tasks->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(32)), static_cast<System::Int32>(static_cast<System::Byte>(32)), 
			static_cast<System::Int32>(static_cast<System::Byte>(32)));
		this->treeView_tasks->ContextMenuStrip = this->contextMenuStrip_task;
		this->treeView_tasks->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
			static_cast<System::Byte>(0)));
		this->treeView_tasks->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)), 
			static_cast<System::Int32>(static_cast<System::Byte>(64)));
		this->treeView_tasks->HideSelection = false;
		this->treeView_tasks->Indent = 16;
		this->treeView_tasks->LineColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)), 
			static_cast<System::Int32>(static_cast<System::Byte>(64)));
		this->treeView_tasks->Location = System::Drawing::Point(0, 27);
		this->treeView_tasks->Name = L"treeView_tasks";
		this->treeView_tasks->ShowLines = false;
		this->treeView_tasks->Size = System::Drawing::Size(222, 551);
		this->treeView_tasks->TabIndex = 1;
		this->treeView_tasks->AfterSelect += gcnew System::Windows::Forms::TreeViewEventHandler(this, &zxTASKeditWindow::select_task);
		// 
		// contextMenuStrip_task
		// 
		this->contextMenuStrip_task->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {this->cloneSelectedToolStripMenuItem, 
			this->deleteSelectedToolStripMenuItem, this->toolStripSeparator4, this->exportSelectedItemToolStripMenuItem, this->replaceSelectedItemToolStripMenuItem, 
			this->toolStripSeparator1, this->toolStripMenuItem52, this->toolStripMenuItem53});
		this->contextMenuStrip_task->Name = L"contextMenuStrip_task";
		this->contextMenuStrip_task->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
		this->contextMenuStrip_task->ShowImageMargin = false;
		this->contextMenuStrip_task->Size = System::Drawing::Size(163, 148);
		// 
		// cloneSelectedToolStripMenuItem
		// 
		this->cloneSelectedToolStripMenuItem->Name = L"cloneSelectedToolStripMenuItem";
		this->cloneSelectedToolStripMenuItem->Size = System::Drawing::Size(162, 22);
		this->cloneSelectedToolStripMenuItem->Text = L"Clone Selected";
		this->cloneSelectedToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_cloneTask);
		// 
		// deleteSelectedToolStripMenuItem
		// 
		this->deleteSelectedToolStripMenuItem->Name = L"deleteSelectedToolStripMenuItem";
		this->deleteSelectedToolStripMenuItem->Size = System::Drawing::Size(162, 22);
		this->deleteSelectedToolStripMenuItem->Text = L"Delete Selected";
		this->deleteSelectedToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_deleteTask);
		// 
		// toolStripSeparator4
		// 
		this->toolStripSeparator4->Name = L"toolStripSeparator4";
		this->toolStripSeparator4->Size = System::Drawing::Size(159, 6);
		// 
		// exportSelectedItemToolStripMenuItem
		// 
		this->exportSelectedItemToolStripMenuItem->Name = L"exportSelectedItemToolStripMenuItem";
		this->exportSelectedItemToolStripMenuItem->Size = System::Drawing::Size(162, 22);
		this->exportSelectedItemToolStripMenuItem->Text = L"Export Selected Task...";
		this->exportSelectedItemToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_ExportTask);
		// 
		// replaceSelectedItemToolStripMenuItem
		// 
		this->replaceSelectedItemToolStripMenuItem->Name = L"replaceSelectedItemToolStripMenuItem";
		this->replaceSelectedItemToolStripMenuItem->Size = System::Drawing::Size(162, 22);
		this->replaceSelectedItemToolStripMenuItem->Text = L"Import New Task...";
		this->replaceSelectedItemToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_ImportTask);
		// 
		// toolStripSeparator1
		// 
		this->toolStripSeparator1->Name = L"toolStripSeparator1";
		this->toolStripSeparator1->Size = System::Drawing::Size(159, 6);
		// 
		// toolStripMenuItem52
		// 
		this->toolStripMenuItem52->Name = L"toolStripMenuItem52";
		this->toolStripMenuItem52->Size = System::Drawing::Size(162, 22);
		this->toolStripMenuItem52->Text = L"Import Task Texts...";
		this->toolStripMenuItem52->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_import_text);
		// 
		// toolStripMenuItem53
		// 
		this->toolStripMenuItem53->Name = L"toolStripMenuItem53";
		this->toolStripMenuItem53->Size = System::Drawing::Size(162, 22);
		this->toolStripMenuItem53->Text = L"Export Task Texts...";
		this->toolStripMenuItem53->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::click_export_text);
		// 
		// label55
		// 
		this->label55->AutoSize = true;
		this->label55->Location = System::Drawing::Point(216, 22);
		this->label55->Name = L"label55";
		this->label55->Size = System::Drawing::Size(34, 13);
		this->label55->TabIndex = 2;
		this->label55->Text = L"Type:";
		// 
		// checkBox_author_mode
		// 
		this->checkBox_author_mode->AutoSize = true;
		this->checkBox_author_mode->Enabled = false;
		this->checkBox_author_mode->Location = System::Drawing::Point(95, 74);
		this->checkBox_author_mode->Name = L"checkBox_author_mode";
		this->checkBox_author_mode->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_author_mode->Size = System::Drawing::Size(15, 14);
		this->checkBox_author_mode->TabIndex = 8;
		this->checkBox_author_mode->UseVisualStyleBackColor = true;
		// 
		// textBox_time_limit
		// 
		this->textBox_time_limit->Location = System::Drawing::Point(466, 19);
		this->textBox_time_limit->Name = L"textBox_time_limit";
		this->textBox_time_limit->Size = System::Drawing::Size(50, 20);
		this->textBox_time_limit->TabIndex = 10;
		this->textBox_time_limit->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_time_limit);
		// 
		// label32
		// 
		this->label32->AutoSize = true;
		this->label32->Location = System::Drawing::Point(377, 22);
		this->label32->Name = L"label32";
		this->label32->Size = System::Drawing::Size(83, 13);
		this->label32->TabIndex = 9;
		this->label32->Text = L"Time Limit [sec]:";
		// 
		// textBox_author_text
		// 
		this->textBox_author_text->Location = System::Drawing::Point(116, 71);
		this->textBox_author_text->MaxLength = 28;
		this->textBox_author_text->Name = L"textBox_author_text";
		this->textBox_author_text->Size = System::Drawing::Size(400, 20);
		this->textBox_author_text->TabIndex = 7;
		this->textBox_author_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_author_text);
		// 
		// textBox_name
		// 
		this->textBox_name->Location = System::Drawing::Point(95, 45);
		this->textBox_name->MaxLength = 28;
		this->textBox_name->Name = L"textBox_name";
		this->textBox_name->Size = System::Drawing::Size(421, 20);
		this->textBox_name->TabIndex = 5;
		this->textBox_name->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_name);
		// 
		// label29
		// 
		this->label29->AutoSize = true;
		this->label29->Location = System::Drawing::Point(6, 48);
		this->label29->Name = L"label29";
		this->label29->Size = System::Drawing::Size(38, 13);
		this->label29->TabIndex = 4;
		this->label29->Text = L"Name:";
		// 
		// textBox_id
		// 
		this->textBox_id->Location = System::Drawing::Point(95, 19);
		this->textBox_id->Name = L"textBox_id";
		this->textBox_id->Size = System::Drawing::Size(115, 20);
		this->textBox_id->TabIndex = 1;
		this->textBox_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_id);
		// 
		// label28
		// 
		this->label28->AutoSize = true;
		this->label28->Location = System::Drawing::Point(6, 22);
		this->label28->Name = L"label28";
		this->label28->Size = System::Drawing::Size(21, 13);
		this->label28->TabIndex = 0;
		this->label28->Text = L"ID:";
		// 
		// textBox_unknown_07
		// 
		this->textBox_unknown_07->Location = System::Drawing::Point(446, 260);
		this->textBox_unknown_07->Name = L"textBox_unknown_07";
		this->textBox_unknown_07->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_07->TabIndex = 25;
		this->textBox_unknown_07->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_07);
		// 
		// label76
		// 
		this->label76->AutoSize = true;
		this->label76->Location = System::Drawing::Point(358, 263);
		this->label76->Name = L"label76";
		this->label76->Size = System::Drawing::Size(71, 13);
		this->label76->TabIndex = 24;
		this->label76->Text = L"Unknown 07:";
		// 
		// dataGridView_date_spans
		// 
		this->dataGridView_date_spans->AllowUserToAddRows = false;
		this->dataGridView_date_spans->AllowUserToDeleteRows = false;
		this->dataGridView_date_spans->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_date_spans->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_date_spans->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_date_spans->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(13) {this->dataGridViewTextBoxColumn1, 
			this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3, this->Column4, this->Column5, this->Column6, this->Column7, 
			this->Column8, this->Column9, this->Column10, this->Column11, this->Column12, this->Column13});
		this->dataGridView_date_spans->ContextMenuStrip = this->contextMenuStrip_date_spans;
		this->dataGridView_date_spans->Location = System::Drawing::Point(116, 172);
		this->dataGridView_date_spans->MultiSelect = false;
		this->dataGridView_date_spans->Name = L"dataGridView_date_spans";
		this->dataGridView_date_spans->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle14->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle14->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle14->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle14->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle14->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle14->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_date_spans->RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
		this->dataGridView_date_spans->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_date_spans->RowTemplate->Height = 18;
		this->dataGridView_date_spans->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_date_spans->Size = System::Drawing::Size(549, 82);
		this->dataGridView_date_spans->TabIndex = 0;
		this->dataGridView_date_spans->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_date_span);
		// 
		// dataGridViewTextBoxColumn1
		// 
		this->dataGridViewTextBoxColumn1->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn1->DefaultCellStyle = dataGridViewCellStyle1;
		this->dataGridViewTextBoxColumn1->HeaderText = L"Y";
		this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
		this->dataGridViewTextBoxColumn1->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn1->Width = 19;
		// 
		// dataGridViewTextBoxColumn2
		// 
		this->dataGridViewTextBoxColumn2->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn2->DefaultCellStyle = dataGridViewCellStyle2;
		this->dataGridViewTextBoxColumn2->HeaderText = L"M";
		this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
		this->dataGridViewTextBoxColumn2->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn2->Width = 21;
		// 
		// dataGridViewTextBoxColumn3
		// 
		this->dataGridViewTextBoxColumn3->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn3->DefaultCellStyle = dataGridViewCellStyle3;
		this->dataGridViewTextBoxColumn3->HeaderText = L"D";
		this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
		this->dataGridViewTextBoxColumn3->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn3->Width = 20;
		// 
		// Column4
		// 
		this->Column4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column4->DefaultCellStyle = dataGridViewCellStyle4;
		this->Column4->HeaderText = L"H";
		this->Column4->Name = L"Column4";
		this->Column4->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column4->Width = 20;
		// 
		// Column5
		// 
		this->Column5->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column5->DefaultCellStyle = dataGridViewCellStyle5;
		this->Column5->HeaderText = L"M";
		this->Column5->Name = L"Column5";
		this->Column5->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column5->Width = 21;
		// 
		// Column6
		// 
		this->Column6->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column6->DefaultCellStyle = dataGridViewCellStyle6;
		this->Column6->HeaderText = L"d";
		this->Column6->Items->AddRange(gcnew cli::array< System::Object^  >(8) {L"ALL", L"Mon", L"Tue", L"Wed", L"Thu", L"Fri", L"Sat", 
			L"Sun"});
		this->Column6->Name = L"Column6";
		this->Column6->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column6->Width = 18;
		// 
		// Column7
		// 
		this->Column7->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		dataGridViewCellStyle7->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
		this->Column7->DefaultCellStyle = dataGridViewCellStyle7;
		this->Column7->HeaderText = L"";
		this->Column7->Name = L"Column7";
		this->Column7->ReadOnly = true;
		this->Column7->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// Column8
		// 
		this->Column8->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle8->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column8->DefaultCellStyle = dataGridViewCellStyle8;
		this->Column8->HeaderText = L"Y";
		this->Column8->Name = L"Column8";
		this->Column8->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column8->Width = 19;
		// 
		// Column9
		// 
		this->Column9->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle9->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column9->DefaultCellStyle = dataGridViewCellStyle9;
		this->Column9->HeaderText = L"M";
		this->Column9->Name = L"Column9";
		this->Column9->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column9->Width = 21;
		// 
		// Column10
		// 
		this->Column10->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle10->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column10->DefaultCellStyle = dataGridViewCellStyle10;
		this->Column10->HeaderText = L"D";
		this->Column10->Name = L"Column10";
		this->Column10->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column10->Width = 20;
		// 
		// Column11
		// 
		this->Column11->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle11->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column11->DefaultCellStyle = dataGridViewCellStyle11;
		this->Column11->HeaderText = L"H";
		this->Column11->Name = L"Column11";
		this->Column11->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column11->Width = 20;
		// 
		// Column12
		// 
		this->Column12->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle12->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column12->DefaultCellStyle = dataGridViewCellStyle12;
		this->Column12->HeaderText = L"M";
		this->Column12->Name = L"Column12";
		this->Column12->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column12->Width = 21;
		// 
		// Column13
		// 
		this->Column13->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle13->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column13->DefaultCellStyle = dataGridViewCellStyle13;
		this->Column13->HeaderText = L"d";
		this->Column13->Items->AddRange(gcnew cli::array< System::Object^  >(8) {L"ALL", L"Mon", L"Tue", L"Wed", L"Thu", L"Fri", 
			L"Sat", L"Sun"});
		this->Column13->Name = L"Column13";
		this->Column13->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column13->Width = 18;
		// 
		// dataGridView_reward_item_group_items
		// 
		this->dataGridView_reward_item_group_items->AllowUserToAddRows = false;
		this->dataGridView_reward_item_group_items->AllowUserToDeleteRows = false;
		this->dataGridView_reward_item_group_items->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->dataGridView_reward_item_group_items->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_reward_item_group_items->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_reward_item_group_items->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_reward_item_group_items->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(11) {this->Column_reward_item_groups, 
			this->dataGridViewTextBoxColumn28, this->Column29, this->dataGridViewTextBoxColumn30, this->dataGridViewTextBoxColumn31, this->Column30, 
			this->Column23, this->Column31, this->Column24, this->Column25, this->Column26});
		this->dataGridView_reward_item_group_items->ContextMenuStrip = this->contextMenuStrip_reward_items;
		this->dataGridView_reward_item_group_items->Location = System::Drawing::Point(187, 27);
		this->dataGridView_reward_item_group_items->MultiSelect = false;
		this->dataGridView_reward_item_group_items->Name = L"dataGridView_reward_item_group_items";
		this->dataGridView_reward_item_group_items->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle20->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle20->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle20->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle20->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle20->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle20->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle20->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_reward_item_group_items->RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
		this->dataGridView_reward_item_group_items->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_reward_item_group_items->RowTemplate->Height = 18;
		this->dataGridView_reward_item_group_items->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_reward_item_group_items->Size = System::Drawing::Size(464, 110);
		this->dataGridView_reward_item_group_items->TabIndex = 38;
		this->dataGridView_reward_item_group_items->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_reward_items);
		// 
		// Column_reward_item_groups
		// 
		this->Column_reward_item_groups->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle15->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->Column_reward_item_groups->DefaultCellStyle = dataGridViewCellStyle15;
		this->Column_reward_item_groups->DisplayStyle = System::Windows::Forms::DataGridViewComboBoxDisplayStyle::ComboBox;
		this->Column_reward_item_groups->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->Column_reward_item_groups->HeaderText = L"Group";
		this->Column_reward_item_groups->Name = L"Column_reward_item_groups";
		this->Column_reward_item_groups->Width = 41;
		// 
		// dataGridViewTextBoxColumn28
		// 
		this->dataGridViewTextBoxColumn28->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle16->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn28->DefaultCellStyle = dataGridViewCellStyle16;
		this->dataGridViewTextBoxColumn28->HeaderText = L"ID";
		this->dataGridViewTextBoxColumn28->Name = L"dataGridViewTextBoxColumn28";
		this->dataGridViewTextBoxColumn28->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn28->Width = 23;
		// 
		// Column29
		// 
		this->Column29->HeaderText = L"Activ.";
		this->Column29->Name = L"Column29";
		this->Column29->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column29->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->Column29->Width = 40;
		// 
		// dataGridViewTextBoxColumn30
		// 
		this->dataGridViewTextBoxColumn30->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle17->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn30->DefaultCellStyle = dataGridViewCellStyle17;
		this->dataGridViewTextBoxColumn30->HeaderText = L"Amount";
		this->dataGridViewTextBoxColumn30->Name = L"dataGridViewTextBoxColumn30";
		this->dataGridViewTextBoxColumn30->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn30->Width = 48;
		// 
		// dataGridViewTextBoxColumn31
		// 
		this->dataGridViewTextBoxColumn31->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle18->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn31->DefaultCellStyle = dataGridViewCellStyle18;
		this->dataGridViewTextBoxColumn31->HeaderText = L"%";
		this->dataGridViewTextBoxColumn31->Name = L"dataGridViewTextBoxColumn31";
		this->dataGridViewTextBoxColumn31->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn31->Width = 20;
		// 
		// Column30
		// 
		this->Column30->HeaderText = L"Flag";
		this->Column30->Name = L"Column30";
		this->Column30->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column30->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->Column30->Width = 30;
		// 
		// Column23
		// 
		dataGridViewCellStyle19->ForeColor = System::Drawing::Color::Gray;
		this->Column23->DefaultCellStyle = dataGridViewCellStyle19;
		this->Column23->HeaderText = L"Exp time";
		this->Column23->Name = L"Column23";
		this->Column23->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->Column23->Width = 60;
		// 
		// Column31
		// 
		this->Column31->HeaderText = L"Refinery";
		this->Column31->Name = L"Column31";
		this->Column31->Width = 50;
		// 
		// Column24
		// 
		this->Column24->HeaderText = L"Un1";
		this->Column24->Name = L"Column24";
		this->Column24->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column24->Width = 30;
		// 
		// Column25
		// 
		this->Column25->HeaderText = L"Un2";
		this->Column25->Name = L"Column25";
		this->Column25->Width = 30;
		// 
		// Column26
		// 
		this->Column26->HeaderText = L"Un3";
		this->Column26->Name = L"Column26";
		this->Column26->Width = 30;
		// 
		// contextMenuStrip_reward_items
		// 
		this->contextMenuStrip_reward_items->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem18, 
			this->toolStripMenuItem19});
		this->contextMenuStrip_reward_items->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_items->Size = System::Drawing::Size(139, 48);
		// 
		// toolStripMenuItem18
		// 
		this->toolStripMenuItem18->Name = L"toolStripMenuItem18";
		this->toolStripMenuItem18->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem18->Text = L"Add Item";
		this->toolStripMenuItem18->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_reward_item);
		// 
		// toolStripMenuItem19
		// 
		this->toolStripMenuItem19->Name = L"toolStripMenuItem19";
		this->toolStripMenuItem19->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem19->Text = L"Remove Item";
		this->toolStripMenuItem19->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_reward_item);
		// 
		// groupBox_reward_selector
		// 
		this->groupBox_reward_selector->Controls->Add(this->radioButton_teacher);
		this->groupBox_reward_selector->Controls->Add(this->radioButton_team_lead);
		this->groupBox_reward_selector->Controls->Add(this->radioButton_teammem);
		this->groupBox_reward_selector->Controls->Add(this->checkedListBox_reward_item_groups_flag);
		this->groupBox_reward_selector->Controls->Add(this->textBox_rewardtype2);
		this->groupBox_reward_selector->Controls->Add(this->label261);
		this->groupBox_reward_selector->Controls->Add(this->label56);
		this->groupBox_reward_selector->Controls->Add(this->numericUpDown_reward_item_groups_count);
		this->groupBox_reward_selector->Controls->Add(this->textBox_rewardtype);
		this->groupBox_reward_selector->Controls->Add(this->label260);
		this->groupBox_reward_selector->Controls->Add(this->label57);
		this->groupBox_reward_selector->Controls->Add(this->dataGridView_reward_item_group_items);
		this->groupBox_reward_selector->Controls->Add(this->numericUpDown_time_factor);
		this->groupBox_reward_selector->Controls->Add(this->label37);
		this->groupBox_reward_selector->Controls->Add(this->label3);
		this->groupBox_reward_selector->Controls->Add(this->radioButton_timed);
		this->groupBox_reward_selector->Controls->Add(this->radioButton_failed);
		this->groupBox_reward_selector->Controls->Add(this->radioButton_success);
		this->groupBox_reward_selector->Location = System::Drawing::Point(3, 3);
		this->groupBox_reward_selector->Name = L"groupBox_reward_selector";
		this->groupBox_reward_selector->Size = System::Drawing::Size(655, 169);
		this->groupBox_reward_selector->TabIndex = 0;
		this->groupBox_reward_selector->TabStop = false;
		this->groupBox_reward_selector->Text = L"REWARD SELECTOR";
		// 
		// radioButton_teacher
		// 
		this->radioButton_teacher->AutoSize = true;
		this->radioButton_teacher->Location = System::Drawing::Point(6, 111);
		this->radioButton_teacher->Name = L"radioButton_teacher";
		this->radioButton_teacher->Size = System::Drawing::Size(57, 17);
		this->radioButton_teacher->TabIndex = 60;
		this->radioButton_teacher->Text = L"Master";
		this->radioButton_teacher->UseVisualStyleBackColor = true;
		// 
		// radioButton_team_lead
		// 
		this->radioButton_team_lead->AutoSize = true;
		this->radioButton_team_lead->Location = System::Drawing::Point(6, 88);
		this->radioButton_team_lead->Name = L"radioButton_team_lead";
		this->radioButton_team_lead->Size = System::Drawing::Size(88, 17);
		this->radioButton_team_lead->TabIndex = 59;
		this->radioButton_team_lead->Text = L"Team Leader";
		this->radioButton_team_lead->UseVisualStyleBackColor = true;
		// 
		// radioButton_teammem
		// 
		this->radioButton_teammem->AutoSize = true;
		this->radioButton_teammem->Location = System::Drawing::Point(6, 65);
		this->radioButton_teammem->Name = L"radioButton_teammem";
		this->radioButton_teammem->Size = System::Drawing::Size(93, 17);
		this->radioButton_teammem->TabIndex = 58;
		this->radioButton_teammem->Text = L"Team Member";
		this->radioButton_teammem->UseVisualStyleBackColor = true;
		// 
		// checkedListBox_reward_item_groups_flag
		// 
		this->checkedListBox_reward_item_groups_flag->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->checkedListBox_reward_item_groups_flag->FormattingEnabled = true;
		this->checkedListBox_reward_item_groups_flag->Location = System::Drawing::Point(110, 68);
		this->checkedListBox_reward_item_groups_flag->Name = L"checkedListBox_reward_item_groups_flag";
		this->checkedListBox_reward_item_groups_flag->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkedListBox_reward_item_groups_flag->Size = System::Drawing::Size(65, 94);
		this->checkedListBox_reward_item_groups_flag->TabIndex = 41;
		this->checkedListBox_reward_item_groups_flag->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &zxTASKeditWindow::change_reward_item_group_flag);
		// 
		// textBox_rewardtype2
		// 
		this->textBox_rewardtype2->Enabled = false;
		this->textBox_rewardtype2->Location = System::Drawing::Point(447, 143);
		this->textBox_rewardtype2->Name = L"textBox_rewardtype2";
		this->textBox_rewardtype2->Size = System::Drawing::Size(60, 20);
		this->textBox_rewardtype2->TabIndex = 57;
		// 
		// label261
		// 
		this->label261->AutoSize = true;
		this->label261->Location = System::Drawing::Point(367, 147);
		this->label261->Name = L"label261";
		this->label261->Size = System::Drawing::Size(80, 13);
		this->label261->TabIndex = 57;
		this->label261->Text = L"Reward Type2:";
		// 
		// label56
		// 
		this->label56->AutoSize = true;
		this->label56->Location = System::Drawing::Point(104, 16);
		this->label56->Name = L"label56";
		this->label56->Size = System::Drawing::Size(77, 13);
		this->label56->TabIndex = 33;
		this->label56->Text = L"# Item Groups:";
		// 
		// numericUpDown_reward_item_groups_count
		// 
		this->numericUpDown_reward_item_groups_count->Location = System::Drawing::Point(109, 29);
		this->numericUpDown_reward_item_groups_count->Name = L"numericUpDown_reward_item_groups_count";
		this->numericUpDown_reward_item_groups_count->Size = System::Drawing::Size(66, 20);
		this->numericUpDown_reward_item_groups_count->TabIndex = 34;
		this->numericUpDown_reward_item_groups_count->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->numericUpDown_reward_item_groups_count->ValueChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_item_groups_count);
		// 
		// textBox_rewardtype
		// 
		this->textBox_rewardtype->Enabled = false;
		this->textBox_rewardtype->Location = System::Drawing::Point(301, 143);
		this->textBox_rewardtype->Name = L"textBox_rewardtype";
		this->textBox_rewardtype->Size = System::Drawing::Size(60, 20);
		this->textBox_rewardtype->TabIndex = 56;
		// 
		// label260
		// 
		this->label260->AutoSize = true;
		this->label260->Location = System::Drawing::Point(221, 147);
		this->label260->Name = L"label260";
		this->label260->Size = System::Drawing::Size(74, 13);
		this->label260->TabIndex = 56;
		this->label260->Text = L"Reward Type:";
		// 
		// label57
		// 
		this->label57->AutoSize = true;
		this->label57->Location = System::Drawing::Point(106, 52);
		this->label57->Name = L"label57";
		this->label57->Size = System::Drawing::Size(67, 13);
		this->label57->TabIndex = 35;
		this->label57->Text = L"Group Flags:";
		// 
		// numericUpDown_time_factor
		// 
		this->numericUpDown_time_factor->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
		this->numericUpDown_time_factor->DecimalPlaces = 3;
		this->numericUpDown_time_factor->Enabled = false;
		this->numericUpDown_time_factor->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
		this->numericUpDown_time_factor->Location = System::Drawing::Point(576, 147);
		this->numericUpDown_time_factor->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 0});
		this->numericUpDown_time_factor->Name = L"numericUpDown_time_factor";
		this->numericUpDown_time_factor->Size = System::Drawing::Size(73, 20);
		this->numericUpDown_time_factor->TabIndex = 5;
		this->numericUpDown_time_factor->ValueChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_time_factor);
		// 
		// label37
		// 
		this->label37->AutoSize = true;
		this->label37->Location = System::Drawing::Point(214, 13);
		this->label37->Name = L"label37";
		this->label37->Size = System::Drawing::Size(35, 13);
		this->label37->TabIndex = 37;
		this->label37->Text = L"Items:";
		// 
		// label3
		// 
		this->label3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
		this->label3->AutoSize = true;
		this->label3->Location = System::Drawing::Point(513, 150);
		this->label3->Name = L"label3";
		this->label3->Size = System::Drawing::Size(66, 13);
		this->label3->TabIndex = 4;
		this->label3->Text = L"Time Factor:";
		// 
		// radioButton_timed
		// 
		this->radioButton_timed->AutoSize = true;
		this->radioButton_timed->Location = System::Drawing::Point(6, 134);
		this->radioButton_timed->Name = L"radioButton_timed";
		this->radioButton_timed->Size = System::Drawing::Size(81, 17);
		this->radioButton_timed->TabIndex = 2;
		this->radioButton_timed->Text = L"Time Based";
		this->radioButton_timed->UseVisualStyleBackColor = true;
		this->radioButton_timed->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_reward);
		// 
		// radioButton_failed
		// 
		this->radioButton_failed->AutoSize = true;
		this->radioButton_failed->Location = System::Drawing::Point(6, 42);
		this->radioButton_failed->Name = L"radioButton_failed";
		this->radioButton_failed->Size = System::Drawing::Size(53, 17);
		this->radioButton_failed->TabIndex = 1;
		this->radioButton_failed->Text = L"Failed";
		this->radioButton_failed->UseVisualStyleBackColor = true;
		this->radioButton_failed->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_reward);
		// 
		// radioButton_success
		// 
		this->radioButton_success->AutoSize = true;
		this->radioButton_success->Checked = true;
		this->radioButton_success->Location = System::Drawing::Point(6, 19);
		this->radioButton_success->Name = L"radioButton_success";
		this->radioButton_success->Size = System::Drawing::Size(66, 17);
		this->radioButton_success->TabIndex = 0;
		this->radioButton_success->TabStop = true;
		this->radioButton_success->Text = L"Success";
		this->radioButton_success->UseVisualStyleBackColor = true;
		this->radioButton_success->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_reward);
		// 
		// listBox_reward_timed
		// 
		this->listBox_reward_timed->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->listBox_reward_timed->ContextMenuStrip = this->contextMenuStrip_reward_timed;
		this->listBox_reward_timed->Enabled = false;
		this->listBox_reward_timed->FormattingEnabled = true;
		this->listBox_reward_timed->Location = System::Drawing::Point(48, 279);
		this->listBox_reward_timed->Name = L"listBox_reward_timed";
		this->listBox_reward_timed->Size = System::Drawing::Size(556, 43);
		this->listBox_reward_timed->TabIndex = 3;
		this->listBox_reward_timed->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_reward);
		// 
		// contextMenuStrip_reward_timed
		// 
		this->contextMenuStrip_reward_timed->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem38, 
			this->toolStripMenuItem39});
		this->contextMenuStrip_reward_timed->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_timed->Size = System::Drawing::Size(154, 48);
		// 
		// toolStripMenuItem38
		// 
		this->toolStripMenuItem38->Name = L"toolStripMenuItem38";
		this->toolStripMenuItem38->Size = System::Drawing::Size(153, 22);
		this->toolStripMenuItem38->Text = L"Add Reward";
		this->toolStripMenuItem38->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_reward_timed);
		// 
		// toolStripMenuItem39
		// 
		this->toolStripMenuItem39->Name = L"toolStripMenuItem39";
		this->toolStripMenuItem39->Size = System::Drawing::Size(153, 22);
		this->toolStripMenuItem39->Text = L"Remove Reward";
		this->toolStripMenuItem39->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_reward_timed);
		// 
		// groupBox_reward
		// 
		this->groupBox_reward->Controls->Add(this->label281);
		this->groupBox_reward->Controls->Add(this->checkBox_rew1330_10);
		this->groupBox_reward->Controls->Add(this->textBox_rew1330_9);
		this->groupBox_reward->Controls->Add(this->label285);
		this->groupBox_reward->Controls->Add(this->textBox_rew1330_8);
		this->groupBox_reward->Controls->Add(this->label284);
		this->groupBox_reward->Controls->Add(this->textBox_rew1330_7);
		this->groupBox_reward->Controls->Add(this->label283);
		this->groupBox_reward->Controls->Add(this->textBox_rew1330_6);
		this->groupBox_reward->Controls->Add(this->label282);
		this->groupBox_reward->Controls->Add(this->checkBox_runk52);
		this->groupBox_reward->Controls->Add(this->textBox_noticemess);
		this->groupBox_reward->Controls->Add(this->textBox_rjobup);
		this->groupBox_reward->Controls->Add(this->label275);
		this->groupBox_reward->Controls->Add(this->textBox_runk56);
		this->groupBox_reward->Controls->Add(this->label270);
		this->groupBox_reward->Controls->Add(this->textBox_runk55);
		this->groupBox_reward->Controls->Add(this->label269);
		this->groupBox_reward->Controls->Add(this->textBox_runk53);
		this->groupBox_reward->Controls->Add(this->label268);
		this->groupBox_reward->Controls->Add(this->textBox_rhuli);
		this->groupBox_reward->Controls->Add(this->label267);
		this->groupBox_reward->Controls->Add(this->checkBox_openreason);
		this->groupBox_reward->Controls->Add(this->checkBox_rsound);
		this->groupBox_reward->Controls->Add(this->textBox_rsound);
		this->groupBox_reward->Controls->Add(this->textBox_runk51);
		this->groupBox_reward->Controls->Add(this->label265);
		this->groupBox_reward->Controls->Add(this->textBox_run50);
		this->groupBox_reward->Controls->Add(this->label264);
		this->groupBox_reward->Controls->Add(this->textBox_transftime);
		this->groupBox_reward->Controls->Add(this->label263);
		this->groupBox_reward->Controls->Add(this->textBox_rtransf);
		this->groupBox_reward->Controls->Add(this->label221);
		this->groupBox_reward->Controls->Add(this->textBox_risexpf);
		this->groupBox_reward->Controls->Add(this->label217);
		this->groupBox_reward->Controls->Add(this->checkBox_rtomeon);
		this->groupBox_reward->Controls->Add(this->textBox_rskillcnt);
		this->groupBox_reward->Controls->Add(this->checkBox_rskillon);
		this->groupBox_reward->Controls->Add(this->textBox_rcampv);
		this->groupBox_reward->Controls->Add(this->checkBox_rcampon);
		this->groupBox_reward->Controls->Add(this->textBox_tunk46);
		this->groupBox_reward->Controls->Add(this->label216);
		this->groupBox_reward->Controls->Add(this->textBox_rspecialflg);
		this->groupBox_reward->Controls->Add(this->checkBox_rspecialaward);
		this->groupBox_reward->Controls->Add(this->checkBox_rview2);
		this->groupBox_reward->Controls->Add(this->checkBox_rview1);
		this->groupBox_reward->Controls->Add(this->checkBox_rpanshi);
		this->groupBox_reward->Controls->Add(this->checkBox_rchushi);
		this->groupBox_reward->Controls->Add(this->textBox_runk43);
		this->groupBox_reward->Controls->Add(this->textBox_runk42);
		this->groupBox_reward->Controls->Add(this->label215);
		this->groupBox_reward->Controls->Add(this->label214);
		this->groupBox_reward->Controls->Add(this->textBox_rdbltime);
		this->groupBox_reward->Controls->Add(this->label213);
		this->groupBox_reward->Controls->Add(this->textBox_rremtask);
		this->groupBox_reward->Controls->Add(this->label212);
		this->groupBox_reward->Controls->Add(this->textBox_rnotch);
		this->groupBox_reward->Controls->Add(this->checkBox_rnotice);
		this->groupBox_reward->Controls->Add(this->textBox_rfacunk);
		this->groupBox_reward->Controls->Add(this->label211);
		this->groupBox_reward->Controls->Add(this->textBox_runk40);
		this->groupBox_reward->Controls->Add(this->label210);
		this->groupBox_reward->Controls->Add(this->textBox_rclanunk);
		this->groupBox_reward->Controls->Add(this->label209);
		this->groupBox_reward->Controls->Add(this->textBox_rclanskillid);
		this->groupBox_reward->Controls->Add(this->label208);
		this->groupBox_reward->Controls->Add(this->textBox_rskill1);
		this->groupBox_reward->Controls->Add(this->label207);
		this->groupBox_reward->Controls->Add(this->textBox_proff);
		this->groupBox_reward->Controls->Add(this->label206);
		this->groupBox_reward->Controls->Add(this->textBox_runk36);
		this->groupBox_reward->Controls->Add(this->textBox_runk35);
		this->groupBox_reward->Controls->Add(this->label205);
		this->groupBox_reward->Controls->Add(this->label63);
		this->groupBox_reward->Controls->Add(this->checkBox_triggeron);
		this->groupBox_reward->Controls->Add(this->textBox_rsetjob);
		this->groupBox_reward->Controls->Add(this->label204);
		this->groupBox_reward->Controls->Add(this->textBox_runk33);
		this->groupBox_reward->Controls->Add(this->label59);
		this->groupBox_reward->Controls->Add(this->textBox_runk32);
		this->groupBox_reward->Controls->Add(this->label262);
		this->groupBox_reward->Controls->Add(this->checkBox_craftup);
		this->groupBox_reward->Controls->Add(this->textBox_runk301);
		this->groupBox_reward->Controls->Add(this->label237);
		this->groupBox_reward->Controls->Add(this->textBox_mountbag);
		this->groupBox_reward->Controls->Add(this->label236);
		this->groupBox_reward->Controls->Add(this->textBox_petslot);
		this->groupBox_reward->Controls->Add(this->label234);
		this->groupBox_reward->Controls->Add(this->textBox_rewpetbag);
		this->groupBox_reward->Controls->Add(this->label230);
		this->groupBox_reward->Controls->Add(this->textBox_rewbag);
		this->groupBox_reward->Controls->Add(this->label229);
		this->groupBox_reward->Controls->Add(this->textBox_rewstash);
		this->groupBox_reward->Controls->Add(this->label228);
		this->groupBox_reward->Controls->Add(this->textBox_runk30);
		this->groupBox_reward->Controls->Add(this->textBox_runk29);
		this->groupBox_reward->Controls->Add(this->textBox_runk28);
		this->groupBox_reward->Controls->Add(this->textBox_runk27);
		this->groupBox_reward->Controls->Add(this->label227);
		this->groupBox_reward->Controls->Add(this->label226);
		this->groupBox_reward->Controls->Add(this->label225);
		this->groupBox_reward->Controls->Add(this->label224);
		this->groupBox_reward->Controls->Add(this->textBox_run31);
		this->groupBox_reward->Controls->Add(this->label152);
		this->groupBox_reward->Controls->Add(this->textBox_run23);
		this->groupBox_reward->Controls->Add(this->label128);
		this->groupBox_reward->Controls->Add(this->textBox_run22);
		this->groupBox_reward->Controls->Add(this->label127);
		this->groupBox_reward->Controls->Add(this->textBox_run21);
		this->groupBox_reward->Controls->Add(this->label24);
		this->groupBox_reward->Controls->Add(this->textBox_run20);
		this->groupBox_reward->Controls->Add(this->label23);
		this->groupBox_reward->Controls->Add(this->checkBox_rewdivorce);
		this->groupBox_reward->Controls->Add(this->checkBox_rewremallinf);
		this->groupBox_reward->Controls->Add(this->textBox_clearpk);
		this->groupBox_reward->Controls->Add(this->label126);
		this->groupBox_reward->Controls->Add(this->textBox_rewardtitle);
		this->groupBox_reward->Controls->Add(this->label125);
		this->groupBox_reward->Controls->Add(this->textBox_run13);
		this->groupBox_reward->Controls->Add(this->label124);
		this->groupBox_reward->Controls->Add(this->textBox_clancontribut);
		this->groupBox_reward->Controls->Add(this->textBox_rewclancont);
		this->groupBox_reward->Controls->Add(this->textBox_run121);
		this->groupBox_reward->Controls->Add(this->textBox_grint);
		this->groupBox_reward->Controls->Add(this->textBox_run12);
		this->groupBox_reward->Controls->Add(this->textBox_run10);
		this->groupBox_reward->Controls->Add(this->textBox_run09);
		this->groupBox_reward->Controls->Add(this->textBox_run08);
		this->groupBox_reward->Controls->Add(this->textBox_run07);
		this->groupBox_reward->Controls->Add(this->textBox_run06);
		this->groupBox_reward->Controls->Add(this->textBox_run05);
		this->groupBox_reward->Controls->Add(this->textBox_run04);
		this->groupBox_reward->Controls->Add(this->textBox_un03);
		this->groupBox_reward->Controls->Add(this->textBox_un02);
		this->groupBox_reward->Controls->Add(this->textBox_un01);
		this->groupBox_reward->Controls->Add(this->label105);
		this->groupBox_reward->Controls->Add(this->label104);
		this->groupBox_reward->Controls->Add(this->label103);
		this->groupBox_reward->Controls->Add(this->label67);
		this->groupBox_reward->Controls->Add(this->label66);
		this->groupBox_reward->Controls->Add(this->label61);
		this->groupBox_reward->Controls->Add(this->label60);
		this->groupBox_reward->Controls->Add(this->label53);
		this->groupBox_reward->Controls->Add(this->label39);
		this->groupBox_reward->Controls->Add(this->label27);
		this->groupBox_reward->Controls->Add(this->label13);
		this->groupBox_reward->Controls->Add(this->label12);
		this->groupBox_reward->Controls->Add(this->label198);
		this->groupBox_reward->Controls->Add(this->label197);
		this->groupBox_reward->Controls->Add(this->label196);
		this->groupBox_reward->Controls->Add(this->label192);
		this->groupBox_reward->Controls->Add(this->textBox_reward_ai_trigger);
		this->groupBox_reward->Controls->Add(this->textBox_reward_teleport_map_id);
		this->groupBox_reward->Controls->Add(this->textBox_reward_teleport_z);
		this->groupBox_reward->Controls->Add(this->textBox_reward_teleport_x);
		this->groupBox_reward->Controls->Add(this->textBox_reward_teleport_altitude);
		this->groupBox_reward->Controls->Add(this->label26);
		this->groupBox_reward->Controls->Add(this->label25);
		this->groupBox_reward->Controls->Add(this->label22);
		this->groupBox_reward->Controls->Add(this->textBox_reward_newtask);
		this->groupBox_reward->Controls->Add(this->label21);
		this->groupBox_reward->Controls->Add(this->textBox_reward_APLv);
		this->groupBox_reward->Controls->Add(this->label20);
		this->groupBox_reward->Controls->Add(this->textBox_reward_rclvl);
		this->groupBox_reward->Controls->Add(this->label19);
		this->groupBox_reward->Controls->Add(this->textBox_rewardExpLv);
		this->groupBox_reward->Controls->Add(this->label18);
		this->groupBox_reward->Controls->Add(this->textBox_reward_experience);
		this->groupBox_reward->Controls->Add(this->label17);
		this->groupBox_reward->Controls->Add(this->textBox_reward_coins);
		this->groupBox_reward->Controls->Add(this->label16);
		this->groupBox_reward->Location = System::Drawing::Point(3, 178);
		this->groupBox_reward->Name = L"groupBox_reward";
		this->groupBox_reward->Size = System::Drawing::Size(655, 630);
		this->groupBox_reward->TabIndex = 2;
		this->groupBox_reward->TabStop = false;
		this->groupBox_reward->Text = L"GENERAL";
		// 
		// label281
		// 
		this->label281->AutoSize = true;
		this->label281->Location = System::Drawing::Point(540, 605);
		this->label281->Name = L"label281";
		this->label281->Size = System::Drawing::Size(46, 13);
		this->label281->TabIndex = 435;
		this->label281->Text = L"Chroma:";
		// 
		// checkBox_rew1330_10
		// 
		this->checkBox_rew1330_10->AutoSize = true;
		this->checkBox_rew1330_10->Location = System::Drawing::Point(142, 599);
		this->checkBox_rew1330_10->Name = L"checkBox_rew1330_10";
		this->checkBox_rew1330_10->Size = System::Drawing::Size(85, 17);
		this->checkBox_rew1330_10->TabIndex = 434;
		this->checkBox_rew1330_10->Text = L"Un 1330_10";
		this->checkBox_rew1330_10->UseVisualStyleBackColor = true;
		this->checkBox_rew1330_10->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rew1330_10);
		// 
		// textBox_rew1330_9
		// 
		this->textBox_rew1330_9->Location = System::Drawing::Point(70, 599);
		this->textBox_rew1330_9->Name = L"textBox_rew1330_9";
		this->textBox_rew1330_9->Size = System::Drawing::Size(60, 20);
		this->textBox_rew1330_9->TabIndex = 433;
		this->textBox_rew1330_9->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rew1330_9);
		// 
		// label285
		// 
		this->label285->AutoSize = true;
		this->label285->Location = System::Drawing::Point(4, 602);
		this->label285->Name = L"label285";
		this->label285->Size = System::Drawing::Size(63, 13);
		this->label285->TabIndex = 432;
		this->label285->Text = L"Un 1330_9:";
		// 
		// textBox_rew1330_8
		// 
		this->textBox_rew1330_8->Location = System::Drawing::Point(474, 566);
		this->textBox_rew1330_8->Name = L"textBox_rew1330_8";
		this->textBox_rew1330_8->Size = System::Drawing::Size(60, 20);
		this->textBox_rew1330_8->TabIndex = 431;
		this->textBox_rew1330_8->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rew1330_8);
		// 
		// label284
		// 
		this->label284->AutoSize = true;
		this->label284->Location = System::Drawing::Point(405, 569);
		this->label284->Name = L"label284";
		this->label284->Size = System::Drawing::Size(63, 13);
		this->label284->TabIndex = 430;
		this->label284->Text = L"Un 1330_8:";
		// 
		// textBox_rew1330_7
		// 
		this->textBox_rew1330_7->Location = System::Drawing::Point(340, 566);
		this->textBox_rew1330_7->Name = L"textBox_rew1330_7";
		this->textBox_rew1330_7->Size = System::Drawing::Size(60, 20);
		this->textBox_rew1330_7->TabIndex = 429;
		this->textBox_rew1330_7->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rew1330_7);
		// 
		// label283
		// 
		this->label283->AutoSize = true;
		this->label283->Location = System::Drawing::Point(274, 569);
		this->label283->Name = L"label283";
		this->label283->Size = System::Drawing::Size(63, 13);
		this->label283->TabIndex = 428;
		this->label283->Text = L"Un 1330_7:";
		// 
		// textBox_rew1330_6
		// 
		this->textBox_rew1330_6->Location = System::Drawing::Point(208, 566);
		this->textBox_rew1330_6->Name = L"textBox_rew1330_6";
		this->textBox_rew1330_6->Size = System::Drawing::Size(60, 20);
		this->textBox_rew1330_6->TabIndex = 427;
		this->textBox_rew1330_6->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rew1330_6);
		// 
		// label282
		// 
		this->label282->AutoSize = true;
		this->label282->Location = System::Drawing::Point(139, 569);
		this->label282->Name = L"label282";
		this->label282->Size = System::Drawing::Size(63, 13);
		this->label282->TabIndex = 426;
		this->label282->Text = L"Un 1330_6:";
		// 
		// checkBox_runk52
		// 
		this->checkBox_runk52->AutoSize = true;
		this->checkBox_runk52->Location = System::Drawing::Point(544, 476);
		this->checkBox_runk52->Name = L"checkBox_runk52";
		this->checkBox_runk52->Size = System::Drawing::Size(55, 17);
		this->checkBox_runk52->TabIndex = 423;
		this->checkBox_runk52->Text = L"Un 52";
		this->checkBox_runk52->UseVisualStyleBackColor = true;
		this->checkBox_runk52->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk52);
		// 
		// textBox_noticemess
		// 
		this->textBox_noticemess->Enabled = false;
		this->textBox_noticemess->Location = System::Drawing::Point(88, 357);
		this->textBox_noticemess->Name = L"textBox_noticemess";
		this->textBox_noticemess->Size = System::Drawing::Size(141, 20);
		this->textBox_noticemess->TabIndex = 422;
		// 
		// textBox_rjobup
		// 
		this->textBox_rjobup->Location = System::Drawing::Point(367, 500);
		this->textBox_rjobup->Name = L"textBox_rjobup";
		this->textBox_rjobup->Size = System::Drawing::Size(60, 20);
		this->textBox_rjobup->TabIndex = 421;
		this->textBox_rjobup->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rjobup);
		// 
		// label275
		// 
		this->label275->AutoSize = true;
		this->label275->Location = System::Drawing::Point(313, 501);
		this->label275->Name = L"label275";
		this->label275->Size = System::Drawing::Size(45, 13);
		this->label275->TabIndex = 420;
		this->label275->Text = L"Job UP:";
		// 
		// textBox_runk56
		// 
		this->textBox_runk56->Location = System::Drawing::Point(580, 520);
		this->textBox_runk56->Name = L"textBox_runk56";
		this->textBox_runk56->Size = System::Drawing::Size(51, 20);
		this->textBox_runk56->TabIndex = 419;
		this->textBox_runk56->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk56);
		// 
		// label270
		// 
		this->label270->AutoSize = true;
		this->label270->Location = System::Drawing::Point(541, 524);
		this->label270->Name = L"label270";
		this->label270->Size = System::Drawing::Size(39, 13);
		this->label270->TabIndex = 418;
		this->label270->Text = L"Un 56:";
		// 
		// textBox_runk55
		// 
		this->textBox_runk55->Location = System::Drawing::Point(478, 522);
		this->textBox_runk55->Name = L"textBox_runk55";
		this->textBox_runk55->Size = System::Drawing::Size(60, 20);
		this->textBox_runk55->TabIndex = 417;
		this->textBox_runk55->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk55);
		// 
		// label269
		// 
		this->label269->AutoSize = true;
		this->label269->Location = System::Drawing::Point(433, 525);
		this->label269->Name = L"label269";
		this->label269->Size = System::Drawing::Size(39, 13);
		this->label269->TabIndex = 416;
		this->label269->Text = L"Un 55:";
		// 
		// textBox_runk53
		// 
		this->textBox_runk53->Location = System::Drawing::Point(580, 500);
		this->textBox_runk53->Name = L"textBox_runk53";
		this->textBox_runk53->Size = System::Drawing::Size(51, 20);
		this->textBox_runk53->TabIndex = 415;
		this->textBox_runk53->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk53);
		// 
		// label268
		// 
		this->label268->AutoSize = true;
		this->label268->Location = System::Drawing::Point(541, 501);
		this->label268->Name = L"label268";
		this->label268->Size = System::Drawing::Size(39, 13);
		this->label268->TabIndex = 414;
		this->label268->Text = L"Un 53:";
		// 
		// textBox_rhuli
		// 
		this->textBox_rhuli->Location = System::Drawing::Point(478, 498);
		this->textBox_rhuli->Name = L"textBox_rhuli";
		this->textBox_rhuli->Size = System::Drawing::Size(60, 20);
		this->textBox_rhuli->TabIndex = 413;
		this->textBox_rhuli->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rhuli);
		// 
		// label267
		// 
		this->label267->AutoSize = true;
		this->label267->Location = System::Drawing::Point(433, 501);
		this->label267->Name = L"label267";
		this->label267->Size = System::Drawing::Size(35, 13);
		this->label267->TabIndex = 412;
		this->label267->Text = L"HULI:";
		// 
		// checkBox_openreason
		// 
		this->checkBox_openreason->AutoSize = true;
		this->checkBox_openreason->Location = System::Drawing::Point(198, 327);
		this->checkBox_openreason->Name = L"checkBox_openreason";
		this->checkBox_openreason->Size = System::Drawing::Size(92, 17);
		this->checkBox_openreason->TabIndex = 411;
		this->checkBox_openreason->Text = L"Open Reason";
		this->checkBox_openreason->UseVisualStyleBackColor = true;
		this->checkBox_openreason->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_openreason);
		// 
		// checkBox_rsound
		// 
		this->checkBox_rsound->AutoSize = true;
		this->checkBox_rsound->Location = System::Drawing::Point(397, 604);
		this->checkBox_rsound->Name = L"checkBox_rsound";
		this->checkBox_rsound->Size = System::Drawing::Size(101, 17);
		this->checkBox_rsound->TabIndex = 410;
		this->checkBox_rsound->Text = L"Chroma Tier UP";
		this->checkBox_rsound->UseVisualStyleBackColor = true;
		this->checkBox_rsound->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rsound);
		// 
		// textBox_rsound
		// 
		this->textBox_rsound->Location = System::Drawing::Point(589, 602);
		this->textBox_rsound->Name = L"textBox_rsound";
		this->textBox_rsound->Size = System::Drawing::Size(60, 20);
		this->textBox_rsound->TabIndex = 409;
		this->textBox_rsound->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rsoundt);
		// 
		// textBox_runk51
		// 
		this->textBox_runk51->Location = System::Drawing::Point(478, 475);
		this->textBox_runk51->Name = L"textBox_runk51";
		this->textBox_runk51->Size = System::Drawing::Size(60, 20);
		this->textBox_runk51->TabIndex = 405;
		this->textBox_runk51->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk51);
		// 
		// label265
		// 
		this->label265->AutoSize = true;
		this->label265->Location = System::Drawing::Point(433, 477);
		this->label265->Name = L"label265";
		this->label265->Size = System::Drawing::Size(39, 13);
		this->label265->TabIndex = 404;
		this->label265->Text = L"Un 51:";
		// 
		// textBox_run50
		// 
		this->textBox_run50->Location = System::Drawing::Point(580, 447);
		this->textBox_run50->Name = L"textBox_run50";
		this->textBox_run50->Size = System::Drawing::Size(51, 20);
		this->textBox_run50->TabIndex = 403;
		this->textBox_run50->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run50);
		// 
		// label264
		// 
		this->label264->AutoSize = true;
		this->label264->Location = System::Drawing::Point(541, 451);
		this->label264->Name = L"label264";
		this->label264->Size = System::Drawing::Size(39, 13);
		this->label264->TabIndex = 402;
		this->label264->Text = L"Un 50:";
		// 
		// textBox_transftime
		// 
		this->textBox_transftime->Location = System::Drawing::Point(274, 537);
		this->textBox_transftime->Name = L"textBox_transftime";
		this->textBox_transftime->Size = System::Drawing::Size(89, 20);
		this->textBox_transftime->TabIndex = 401;
		this->textBox_transftime->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_transftime);
		// 
		// label263
		// 
		this->label263->AutoSize = true;
		this->label263->Location = System::Drawing::Point(235, 540);
		this->label263->Name = L"label263";
		this->label263->Size = System::Drawing::Size(33, 13);
		this->label263->TabIndex = 400;
		this->label263->Text = L"Time:";
		// 
		// textBox_rtransf
		// 
		this->textBox_rtransf->Location = System::Drawing::Point(129, 540);
		this->textBox_rtransf->Name = L"textBox_rtransf";
		this->textBox_rtransf->Size = System::Drawing::Size(100, 20);
		this->textBox_rtransf->TabIndex = 399;
		this->textBox_rtransf->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rtransf);
		// 
		// label221
		// 
		this->label221->AutoSize = true;
		this->label221->Location = System::Drawing::Point(5, 543);
		this->label221->Name = L"label221";
		this->label221->Size = System::Drawing::Size(57, 13);
		this->label221->TabIndex = 398;
		this->label221->Text = L"Transform:";
		// 
		// textBox_risexpf
		// 
		this->textBox_risexpf->Enabled = false;
		this->textBox_risexpf->Location = System::Drawing::Point(367, 451);
		this->textBox_risexpf->Name = L"textBox_risexpf";
		this->textBox_risexpf->Size = System::Drawing::Size(60, 20);
		this->textBox_risexpf->TabIndex = 397;
		// 
		// label217
		// 
		this->label217->AutoSize = true;
		this->label217->Location = System::Drawing::Point(290, 454);
		this->label217->Name = L"label217";
		this->label217->Size = System::Drawing::Size(68, 13);
		this->label217->TabIndex = 396;
		this->label217->Text = L"EXP formula:";
		// 
		// checkBox_rtomeon
		// 
		this->checkBox_rtomeon->AutoSize = true;
		this->checkBox_rtomeon->Location = System::Drawing::Point(129, 440);
		this->checkBox_rtomeon->Name = L"checkBox_rtomeon";
		this->checkBox_rtomeon->Size = System::Drawing::Size(53, 17);
		this->checkBox_rtomeon->TabIndex = 395;
		this->checkBox_rtomeon->Text = L"Tome";
		this->checkBox_rtomeon->UseVisualStyleBackColor = true;
		this->checkBox_rtomeon->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rtomeon);
		// 
		// textBox_rskillcnt
		// 
		this->textBox_rskillcnt->Location = System::Drawing::Point(129, 518);
		this->textBox_rskillcnt->Name = L"textBox_rskillcnt";
		this->textBox_rskillcnt->Size = System::Drawing::Size(100, 20);
		this->textBox_rskillcnt->TabIndex = 394;
		this->textBox_rskillcnt->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rskillcnt);
		// 
		// checkBox_rskillon
		// 
		this->checkBox_rskillon->AutoSize = true;
		this->checkBox_rskillon->Location = System::Drawing::Point(6, 520);
		this->checkBox_rskillon->Name = L"checkBox_rskillon";
		this->checkBox_rskillon->Size = System::Drawing::Size(45, 17);
		this->checkBox_rskillon->TabIndex = 393;
		this->checkBox_rskillon->Text = L"Skill";
		this->checkBox_rskillon->UseVisualStyleBackColor = true;
		this->checkBox_rskillon->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rskillon);
		// 
		// textBox_rcampv
		// 
		this->textBox_rcampv->Location = System::Drawing::Point(129, 494);
		this->textBox_rcampv->Name = L"textBox_rcampv";
		this->textBox_rcampv->Size = System::Drawing::Size(100, 20);
		this->textBox_rcampv->TabIndex = 392;
		this->textBox_rcampv->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rcampv);
		// 
		// checkBox_rcampon
		// 
		this->checkBox_rcampon->AutoSize = true;
		this->checkBox_rcampon->Location = System::Drawing::Point(6, 497);
		this->checkBox_rcampon->Name = L"checkBox_rcampon";
		this->checkBox_rcampon->Size = System::Drawing::Size(53, 17);
		this->checkBox_rcampon->TabIndex = 391;
		this->checkBox_rcampon->Text = L"Camp";
		this->checkBox_rcampon->UseVisualStyleBackColor = true;
		this->checkBox_rcampon->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rcampon);
		// 
		// textBox_tunk46
		// 
		this->textBox_tunk46->Location = System::Drawing::Point(478, 447);
		this->textBox_tunk46->Name = L"textBox_tunk46";
		this->textBox_tunk46->Size = System::Drawing::Size(60, 20);
		this->textBox_tunk46->TabIndex = 390;
		this->textBox_tunk46->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_tunk46);
		// 
		// label216
		// 
		this->label216->AutoSize = true;
		this->label216->Location = System::Drawing::Point(433, 454);
		this->label216->Name = L"label216";
		this->label216->Size = System::Drawing::Size(39, 13);
		this->label216->TabIndex = 389;
		this->label216->Text = L"Un 46:";
		// 
		// textBox_rspecialflg
		// 
		this->textBox_rspecialflg->Enabled = false;
		this->textBox_rspecialflg->Location = System::Drawing::Point(129, 471);
		this->textBox_rspecialflg->Name = L"textBox_rspecialflg";
		this->textBox_rspecialflg->Size = System::Drawing::Size(100, 20);
		this->textBox_rspecialflg->TabIndex = 388;
		// 
		// checkBox_rspecialaward
		// 
		this->checkBox_rspecialaward->AutoSize = true;
		this->checkBox_rspecialaward->Enabled = false;
		this->checkBox_rspecialaward->Location = System::Drawing::Point(7, 474);
		this->checkBox_rspecialaward->Name = L"checkBox_rspecialaward";
		this->checkBox_rspecialaward->Size = System::Drawing::Size(94, 17);
		this->checkBox_rspecialaward->TabIndex = 387;
		this->checkBox_rspecialaward->Text = L"Special Award";
		this->checkBox_rspecialaward->UseVisualStyleBackColor = true;
		// 
		// checkBox_rview2
		// 
		this->checkBox_rview2->AutoSize = true;
		this->checkBox_rview2->Location = System::Drawing::Point(70, 440);
		this->checkBox_rview2->Name = L"checkBox_rview2";
		this->checkBox_rview2->Size = System::Drawing::Size(58, 17);
		this->checkBox_rview2->TabIndex = 386;
		this->checkBox_rview2->Text = L"View 2";
		this->checkBox_rview2->UseVisualStyleBackColor = true;
		this->checkBox_rview2->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rview2);
		// 
		// checkBox_rview1
		// 
		this->checkBox_rview1->AutoSize = true;
		this->checkBox_rview1->Location = System::Drawing::Point(6, 440);
		this->checkBox_rview1->Name = L"checkBox_rview1";
		this->checkBox_rview1->Size = System::Drawing::Size(58, 17);
		this->checkBox_rview1->TabIndex = 385;
		this->checkBox_rview1->Text = L"View 1";
		this->checkBox_rview1->UseVisualStyleBackColor = true;
		this->checkBox_rview1->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rview1);
		// 
		// checkBox_rpanshi
		// 
		this->checkBox_rpanshi->AutoSize = true;
		this->checkBox_rpanshi->Location = System::Drawing::Point(129, 330);
		this->checkBox_rpanshi->Name = L"checkBox_rpanshi";
		this->checkBox_rpanshi->Size = System::Drawing::Size(60, 17);
		this->checkBox_rpanshi->TabIndex = 384;
		this->checkBox_rpanshi->Text = L"PanShi";
		this->checkBox_rpanshi->UseVisualStyleBackColor = true;
		this->checkBox_rpanshi->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rpanshi);
		// 
		// checkBox_rchushi
		// 
		this->checkBox_rchushi->AutoSize = true;
		this->checkBox_rchushi->Location = System::Drawing::Point(198, 304);
		this->checkBox_rchushi->Name = L"checkBox_rchushi";
		this->checkBox_rchushi->Size = System::Drawing::Size(60, 17);
		this->checkBox_rchushi->TabIndex = 383;
		this->checkBox_rchushi->Text = L"ChuShi";
		this->checkBox_rchushi->UseVisualStyleBackColor = true;
		this->checkBox_rchushi->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rchushi);
		// 
		// textBox_runk43
		// 
		this->textBox_runk43->Location = System::Drawing::Point(580, 413);
		this->textBox_runk43->Name = L"textBox_runk43";
		this->textBox_runk43->Size = System::Drawing::Size(51, 20);
		this->textBox_runk43->TabIndex = 382;
		this->textBox_runk43->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk43);
		// 
		// textBox_runk42
		// 
		this->textBox_runk42->Location = System::Drawing::Point(478, 416);
		this->textBox_runk42->Name = L"textBox_runk42";
		this->textBox_runk42->Size = System::Drawing::Size(60, 20);
		this->textBox_runk42->TabIndex = 381;
		this->textBox_runk42->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk42);
		// 
		// label215
		// 
		this->label215->AutoSize = true;
		this->label215->Location = System::Drawing::Point(541, 420);
		this->label215->Name = L"label215";
		this->label215->Size = System::Drawing::Size(39, 13);
		this->label215->TabIndex = 380;
		this->label215->Text = L"Un 43:";
		// 
		// label214
		// 
		this->label214->AutoSize = true;
		this->label214->Location = System::Drawing::Point(433, 423);
		this->label214->Name = L"label214";
		this->label214->Size = System::Drawing::Size(39, 13);
		this->label214->TabIndex = 379;
		this->label214->Text = L"Un 42:";
		// 
		// textBox_rdbltime
		// 
		this->textBox_rdbltime->Location = System::Drawing::Point(129, 408);
		this->textBox_rdbltime->Name = L"textBox_rdbltime";
		this->textBox_rdbltime->Size = System::Drawing::Size(100, 20);
		this->textBox_rdbltime->TabIndex = 378;
		this->textBox_rdbltime->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rdbltime);
		// 
		// label213
		// 
		this->label213->AutoSize = true;
		this->label213->Location = System::Drawing::Point(5, 412);
		this->label213->Name = L"label213";
		this->label213->Size = System::Drawing::Size(70, 13);
		this->label213->TabIndex = 377;
		this->label213->Text = L"Double Time:";
		// 
		// textBox_rremtask
		// 
		this->textBox_rremtask->Location = System::Drawing::Point(129, 382);
		this->textBox_rremtask->Name = L"textBox_rremtask";
		this->textBox_rremtask->Size = System::Drawing::Size(100, 20);
		this->textBox_rremtask->TabIndex = 376;
		this->textBox_rremtask->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rremtask);
		// 
		// label212
		// 
		this->label212->AutoSize = true;
		this->label212->Location = System::Drawing::Point(3, 385);
		this->label212->Name = L"label212";
		this->label212->Size = System::Drawing::Size(77, 13);
		this->label212->TabIndex = 375;
		this->label212->Text = L"Remove Task:";
		// 
		// textBox_rnotch
		// 
		this->textBox_rnotch->Location = System::Drawing::Point(64, 357);
		this->textBox_rnotch->Name = L"textBox_rnotch";
		this->textBox_rnotch->Size = System::Drawing::Size(23, 20);
		this->textBox_rnotch->TabIndex = 374;
		this->textBox_rnotch->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rnotch);
		// 
		// checkBox_rnotice
		// 
		this->checkBox_rnotice->AutoSize = true;
		this->checkBox_rnotice->Location = System::Drawing::Point(6, 360);
		this->checkBox_rnotice->Name = L"checkBox_rnotice";
		this->checkBox_rnotice->Size = System::Drawing::Size(60, 17);
		this->checkBox_rnotice->TabIndex = 373;
		this->checkBox_rnotice->Text = L"Notice:";
		this->checkBox_rnotice->UseVisualStyleBackColor = true;
		this->checkBox_rnotice->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rnotice);
		// 
		// textBox_rfacunk
		// 
		this->textBox_rfacunk->Location = System::Drawing::Point(367, 420);
		this->textBox_rfacunk->Name = L"textBox_rfacunk";
		this->textBox_rfacunk->Size = System::Drawing::Size(60, 20);
		this->textBox_rfacunk->TabIndex = 372;
		this->textBox_rfacunk->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rfacunk);
		// 
		// label211
		// 
		this->label211->AutoSize = true;
		this->label211->Location = System::Drawing::Point(284, 423);
		this->label211->Name = L"label211";
		this->label211->Size = System::Drawing::Size(74, 13);
		this->label211->TabIndex = 371;
		this->label211->Text = L"Faction Unkn:";
		// 
		// textBox_runk40
		// 
		this->textBox_runk40->Location = System::Drawing::Point(580, 386);
		this->textBox_runk40->Name = L"textBox_runk40";
		this->textBox_runk40->Size = System::Drawing::Size(51, 20);
		this->textBox_runk40->TabIndex = 370;
		this->textBox_runk40->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk40);
		// 
		// label210
		// 
		this->label210->AutoSize = true;
		this->label210->Location = System::Drawing::Point(540, 389);
		this->label210->Name = L"label210";
		this->label210->Size = System::Drawing::Size(39, 13);
		this->label210->TabIndex = 369;
		this->label210->Text = L"Un 40:";
		// 
		// textBox_rclanunk
		// 
		this->textBox_rclanunk->Location = System::Drawing::Point(367, 390);
		this->textBox_rclanunk->Name = L"textBox_rclanunk";
		this->textBox_rclanunk->Size = System::Drawing::Size(60, 20);
		this->textBox_rclanunk->TabIndex = 368;
		this->textBox_rclanunk->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rclanunk);
		// 
		// label209
		// 
		this->label209->AutoSize = true;
		this->label209->Location = System::Drawing::Point(298, 396);
		this->label209->Name = L"label209";
		this->label209->Size = System::Drawing::Size(60, 13);
		this->label209->TabIndex = 367;
		this->label209->Text = L"Clan Unkn:";
		// 
		// textBox_rclanskillid
		// 
		this->textBox_rclanskillid->Location = System::Drawing::Point(367, 364);
		this->textBox_rclanskillid->Name = L"textBox_rclanskillid";
		this->textBox_rclanskillid->Size = System::Drawing::Size(60, 20);
		this->textBox_rclanskillid->TabIndex = 366;
		this->textBox_rclanskillid->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rclanskillid);
		// 
		// label208
		// 
		this->label208->AutoSize = true;
		this->label208->Location = System::Drawing::Point(292, 367);
		this->label208->Name = L"label208";
		this->label208->Size = System::Drawing::Size(67, 13);
		this->label208->TabIndex = 365;
		this->label208->Text = L"Clan Skill ID:";
		// 
		// textBox_rskill1
		// 
		this->textBox_rskill1->Location = System::Drawing::Point(367, 337);
		this->textBox_rskill1->Name = L"textBox_rskill1";
		this->textBox_rskill1->Size = System::Drawing::Size(60, 20);
		this->textBox_rskill1->TabIndex = 364;
		this->textBox_rskill1->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rskill1);
		// 
		// label207
		// 
		this->label207->AutoSize = true;
		this->label207->Location = System::Drawing::Point(313, 341);
		this->label207->Name = L"label207";
		this->label207->Size = System::Drawing::Size(45, 13);
		this->label207->TabIndex = 363;
		this->label207->Text = L"Skill LV:";
		// 
		// textBox_proff
		// 
		this->textBox_proff->Location = System::Drawing::Point(367, 309);
		this->textBox_proff->Name = L"textBox_proff";
		this->textBox_proff->Size = System::Drawing::Size(60, 20);
		this->textBox_proff->TabIndex = 362;
		this->textBox_proff->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_proff);
		// 
		// label206
		// 
		this->label206->AutoSize = true;
		this->label206->Location = System::Drawing::Point(299, 312);
		this->label206->Name = L"label206";
		this->label206->Size = System::Drawing::Size(62, 13);
		this->label206->TabIndex = 361;
		this->label206->Text = L"Proficiency:";
		// 
		// textBox_runk36
		// 
		this->textBox_runk36->Location = System::Drawing::Point(478, 386);
		this->textBox_runk36->Name = L"textBox_runk36";
		this->textBox_runk36->Size = System::Drawing::Size(60, 20);
		this->textBox_runk36->TabIndex = 360;
		this->textBox_runk36->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk36);
		// 
		// textBox_runk35
		// 
		this->textBox_runk35->Location = System::Drawing::Point(580, 356);
		this->textBox_runk35->Name = L"textBox_runk35";
		this->textBox_runk35->Size = System::Drawing::Size(51, 20);
		this->textBox_runk35->TabIndex = 359;
		this->textBox_runk35->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk35);
		// 
		// label205
		// 
		this->label205->AutoSize = true;
		this->label205->Location = System::Drawing::Point(433, 393);
		this->label205->Name = L"label205";
		this->label205->Size = System::Drawing::Size(39, 13);
		this->label205->TabIndex = 358;
		this->label205->Text = L"Un 36:";
		// 
		// label63
		// 
		this->label63->AutoSize = true;
		this->label63->Location = System::Drawing::Point(541, 360);
		this->label63->Name = L"label63";
		this->label63->Size = System::Drawing::Size(39, 13);
		this->label63->TabIndex = 357;
		this->label63->Text = L"Un 35:";
		// 
		// checkBox_triggeron
		// 
		this->checkBox_triggeron->AutoSize = true;
		this->checkBox_triggeron->Location = System::Drawing::Point(301, 128);
		this->checkBox_triggeron->Name = L"checkBox_triggeron";
		this->checkBox_triggeron->Size = System::Drawing::Size(62, 17);
		this->checkBox_triggeron->TabIndex = 356;
		this->checkBox_triggeron->Text = L"Trigger:";
		this->checkBox_triggeron->UseVisualStyleBackColor = true;
		this->checkBox_triggeron->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_triggeron);
		// 
		// textBox_rsetjob
		// 
		this->textBox_rsetjob->Location = System::Drawing::Point(367, 283);
		this->textBox_rsetjob->Name = L"textBox_rsetjob";
		this->textBox_rsetjob->Size = System::Drawing::Size(60, 20);
		this->textBox_rsetjob->TabIndex = 355;
		this->textBox_rsetjob->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rsetjob);
		// 
		// label204
		// 
		this->label204->AutoSize = true;
		this->label204->Location = System::Drawing::Point(313, 286);
		this->label204->Name = L"label204";
		this->label204->Size = System::Drawing::Size(46, 13);
		this->label204->TabIndex = 354;
		this->label204->Text = L"Set Job:";
		// 
		// textBox_runk33
		// 
		this->textBox_runk33->Location = System::Drawing::Point(478, 357);
		this->textBox_runk33->Name = L"textBox_runk33";
		this->textBox_runk33->Size = System::Drawing::Size(60, 20);
		this->textBox_runk33->TabIndex = 353;
		this->textBox_runk33->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk33);
		// 
		// label59
		// 
		this->label59->AutoSize = true;
		this->label59->Location = System::Drawing::Point(433, 360);
		this->label59->Name = L"label59";
		this->label59->Size = System::Drawing::Size(39, 13);
		this->label59->TabIndex = 352;
		this->label59->Text = L"Un 33:";
		// 
		// textBox_runk32
		// 
		this->textBox_runk32->Location = System::Drawing::Point(580, 330);
		this->textBox_runk32->Name = L"textBox_runk32";
		this->textBox_runk32->Size = System::Drawing::Size(51, 20);
		this->textBox_runk32->TabIndex = 350;
		// 
		// label262
		// 
		this->label262->AutoSize = true;
		this->label262->Location = System::Drawing::Point(541, 334);
		this->label262->Name = L"label262";
		this->label262->Size = System::Drawing::Size(39, 13);
		this->label262->TabIndex = 349;
		this->label262->Text = L"Un 32:";
		// 
		// checkBox_craftup
		// 
		this->checkBox_craftup->AutoSize = true;
		this->checkBox_craftup->Location = System::Drawing::Point(6, 330);
		this->checkBox_craftup->Name = L"checkBox_craftup";
		this->checkBox_craftup->Size = System::Drawing::Size(70, 17);
		this->checkBox_craftup->TabIndex = 348;
		this->checkBox_craftup->Text = L"Craft LVL";
		this->checkBox_craftup->UseVisualStyleBackColor = true;
		this->checkBox_craftup->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_craftup);
		// 
		// textBox_runk301
		// 
		this->textBox_runk301->Location = System::Drawing::Point(478, 331);
		this->textBox_runk301->Name = L"textBox_runk301";
		this->textBox_runk301->Size = System::Drawing::Size(60, 20);
		this->textBox_runk301->TabIndex = 347;
		// 
		// label237
		// 
		this->label237->AutoSize = true;
		this->label237->Location = System::Drawing::Point(433, 337);
		this->label237->Name = L"label237";
		this->label237->Size = System::Drawing::Size(51, 13);
		this->label237->TabIndex = 346;
		this->label237->Text = L"Un 30_1:";
		// 
		// textBox_mountbag
		// 
		this->textBox_mountbag->Location = System::Drawing::Point(367, 257);
		this->textBox_mountbag->Name = L"textBox_mountbag";
		this->textBox_mountbag->Size = System::Drawing::Size(60, 20);
		this->textBox_mountbag->TabIndex = 345;
		this->textBox_mountbag->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_mountbag);
		// 
		// label236
		// 
		this->label236->AutoSize = true;
		this->label236->Location = System::Drawing::Point(298, 260);
		this->label236->Name = L"label236";
		this->label236->Size = System::Drawing::Size(62, 13);
		this->label236->TabIndex = 344;
		this->label236->Text = L"Mount Bag:";
		// 
		// textBox_petslot
		// 
		this->textBox_petslot->Location = System::Drawing::Point(367, 231);
		this->textBox_petslot->Name = L"textBox_petslot";
		this->textBox_petslot->Size = System::Drawing::Size(60, 20);
		this->textBox_petslot->TabIndex = 343;
		this->textBox_petslot->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_petslot);
		// 
		// label234
		// 
		this->label234->AutoSize = true;
		this->label234->Location = System::Drawing::Point(313, 234);
		this->label234->Name = L"label234";
		this->label234->Size = System::Drawing::Size(47, 13);
		this->label234->TabIndex = 342;
		this->label234->Text = L"Pet Slot:";
		// 
		// textBox_rewpetbag
		// 
		this->textBox_rewpetbag->Location = System::Drawing::Point(367, 204);
		this->textBox_rewpetbag->Name = L"textBox_rewpetbag";
		this->textBox_rewpetbag->Size = System::Drawing::Size(60, 20);
		this->textBox_rewpetbag->TabIndex = 341;
		this->textBox_rewpetbag->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewpetbag);
		// 
		// label230
		// 
		this->label230->AutoSize = true;
		this->label230->Location = System::Drawing::Point(313, 208);
		this->label230->Name = L"label230";
		this->label230->Size = System::Drawing::Size(48, 13);
		this->label230->TabIndex = 340;
		this->label230->Text = L"Pet Bag:";
		// 
		// textBox_rewbag
		// 
		this->textBox_rewbag->Location = System::Drawing::Point(367, 179);
		this->textBox_rewbag->Name = L"textBox_rewbag";
		this->textBox_rewbag->Size = System::Drawing::Size(60, 20);
		this->textBox_rewbag->TabIndex = 339;
		this->textBox_rewbag->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewbag);
		// 
		// label229
		// 
		this->label229->AutoSize = true;
		this->label229->Location = System::Drawing::Point(332, 182);
		this->label229->Name = L"label229";
		this->label229->Size = System::Drawing::Size(29, 13);
		this->label229->TabIndex = 338;
		this->label229->Text = L"Bag:";
		// 
		// textBox_rewstash
		// 
		this->textBox_rewstash->Location = System::Drawing::Point(367, 153);
		this->textBox_rewstash->Name = L"textBox_rewstash";
		this->textBox_rewstash->Size = System::Drawing::Size(60, 20);
		this->textBox_rewstash->TabIndex = 337;
		this->textBox_rewstash->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewstash);
		// 
		// label228
		// 
		this->label228->AutoSize = true;
		this->label228->Location = System::Drawing::Point(324, 156);
		this->label228->Name = L"label228";
		this->label228->Size = System::Drawing::Size(37, 13);
		this->label228->TabIndex = 336;
		this->label228->Text = L"Stash:";
		// 
		// textBox_runk30
		// 
		this->textBox_runk30->Location = System::Drawing::Point(580, 305);
		this->textBox_runk30->Name = L"textBox_runk30";
		this->textBox_runk30->Size = System::Drawing::Size(51, 20);
		this->textBox_runk30->TabIndex = 335;
		// 
		// textBox_runk29
		// 
		this->textBox_runk29->Location = System::Drawing::Point(580, 279);
		this->textBox_runk29->Name = L"textBox_runk29";
		this->textBox_runk29->Size = System::Drawing::Size(51, 20);
		this->textBox_runk29->TabIndex = 334;
		// 
		// textBox_runk28
		// 
		this->textBox_runk28->Location = System::Drawing::Point(580, 253);
		this->textBox_runk28->Name = L"textBox_runk28";
		this->textBox_runk28->Size = System::Drawing::Size(51, 20);
		this->textBox_runk28->TabIndex = 333;
		// 
		// textBox_runk27
		// 
		this->textBox_runk27->Location = System::Drawing::Point(580, 227);
		this->textBox_runk27->Name = L"textBox_runk27";
		this->textBox_runk27->Size = System::Drawing::Size(51, 20);
		this->textBox_runk27->TabIndex = 332;
		// 
		// label227
		// 
		this->label227->AutoSize = true;
		this->label227->Location = System::Drawing::Point(541, 308);
		this->label227->Name = L"label227";
		this->label227->Size = System::Drawing::Size(39, 13);
		this->label227->TabIndex = 329;
		this->label227->Text = L"Un 30:";
		// 
		// label226
		// 
		this->label226->AutoSize = true;
		this->label226->Location = System::Drawing::Point(540, 282);
		this->label226->Name = L"label226";
		this->label226->Size = System::Drawing::Size(39, 13);
		this->label226->TabIndex = 328;
		this->label226->Text = L"Un 29:";
		// 
		// label225
		// 
		this->label225->AutoSize = true;
		this->label225->Location = System::Drawing::Point(540, 256);
		this->label225->Name = L"label225";
		this->label225->Size = System::Drawing::Size(39, 13);
		this->label225->TabIndex = 327;
		this->label225->Text = L"Un 28:";
		// 
		// label224
		// 
		this->label224->AutoSize = true;
		this->label224->Location = System::Drawing::Point(540, 230);
		this->label224->Name = L"label224";
		this->label224->Size = System::Drawing::Size(39, 13);
		this->label224->TabIndex = 326;
		this->label224->Text = L"Un 27:";
		// 
		// textBox_run31
		// 
		this->textBox_run31->Location = System::Drawing::Point(580, 149);
		this->textBox_run31->Name = L"textBox_run31";
		this->textBox_run31->Size = System::Drawing::Size(51, 20);
		this->textBox_run31->TabIndex = 323;
		this->textBox_run31->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run31);
		// 
		// label152
		// 
		this->label152->AutoSize = true;
		this->label152->Location = System::Drawing::Point(541, 152);
		this->label152->Name = L"label152";
		this->label152->Size = System::Drawing::Size(39, 13);
		this->label152->TabIndex = 322;
		this->label152->Text = L"Un 31:";
		// 
		// textBox_run23
		// 
		this->textBox_run23->Location = System::Drawing::Point(580, 123);
		this->textBox_run23->Name = L"textBox_run23";
		this->textBox_run23->Size = System::Drawing::Size(51, 20);
		this->textBox_run23->TabIndex = 321;
		this->textBox_run23->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run23);
		// 
		// label128
		// 
		this->label128->AutoSize = true;
		this->label128->Location = System::Drawing::Point(540, 126);
		this->label128->Name = L"label128";
		this->label128->Size = System::Drawing::Size(39, 13);
		this->label128->TabIndex = 320;
		this->label128->Text = L"Un 23:";
		// 
		// textBox_run22
		// 
		this->textBox_run22->Location = System::Drawing::Point(580, 97);
		this->textBox_run22->Name = L"textBox_run22";
		this->textBox_run22->Size = System::Drawing::Size(51, 20);
		this->textBox_run22->TabIndex = 319;
		this->textBox_run22->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run22);
		// 
		// label127
		// 
		this->label127->AutoSize = true;
		this->label127->Location = System::Drawing::Point(540, 100);
		this->label127->Name = L"label127";
		this->label127->Size = System::Drawing::Size(39, 13);
		this->label127->TabIndex = 318;
		this->label127->Text = L"Un 22:";
		// 
		// textBox_run21
		// 
		this->textBox_run21->Location = System::Drawing::Point(580, 71);
		this->textBox_run21->Name = L"textBox_run21";
		this->textBox_run21->Size = System::Drawing::Size(51, 20);
		this->textBox_run21->TabIndex = 317;
		this->textBox_run21->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run21);
		// 
		// label24
		// 
		this->label24->AutoSize = true;
		this->label24->Location = System::Drawing::Point(541, 74);
		this->label24->Name = L"label24";
		this->label24->Size = System::Drawing::Size(39, 13);
		this->label24->TabIndex = 316;
		this->label24->Text = L"Un 21:";
		// 
		// textBox_run20
		// 
		this->textBox_run20->Location = System::Drawing::Point(580, 45);
		this->textBox_run20->Name = L"textBox_run20";
		this->textBox_run20->Size = System::Drawing::Size(51, 20);
		this->textBox_run20->TabIndex = 315;
		this->textBox_run20->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run20);
		// 
		// label23
		// 
		this->label23->AutoSize = true;
		this->label23->Location = System::Drawing::Point(541, 48);
		this->label23->Name = L"label23";
		this->label23->Size = System::Drawing::Size(39, 13);
		this->label23->TabIndex = 314;
		this->label23->Text = L"Un 20:";
		// 
		// checkBox_rewdivorce
		// 
		this->checkBox_rewdivorce->AutoSize = true;
		this->checkBox_rewdivorce->Location = System::Drawing::Point(129, 304);
		this->checkBox_rewdivorce->Name = L"checkBox_rewdivorce";
		this->checkBox_rewdivorce->Size = System::Drawing::Size(63, 17);
		this->checkBox_rewdivorce->TabIndex = 313;
		this->checkBox_rewdivorce->Text = L"Divorce";
		this->checkBox_rewdivorce->UseVisualStyleBackColor = true;
		this->checkBox_rewdivorce->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewdivorce);
		// 
		// checkBox_rewremallinf
		// 
		this->checkBox_rewremallinf->AutoSize = true;
		this->checkBox_rewremallinf->Location = System::Drawing::Point(6, 307);
		this->checkBox_rewremallinf->Name = L"checkBox_rewremallinf";
		this->checkBox_rewremallinf->Size = System::Drawing::Size(112, 17);
		this->checkBox_rewremallinf->TabIndex = 312;
		this->checkBox_rewremallinf->Text = L"Remove all infamy";
		this->checkBox_rewremallinf->UseVisualStyleBackColor = true;
		this->checkBox_rewremallinf->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewremallinf);
		// 
		// textBox_clearpk
		// 
		this->textBox_clearpk->Location = System::Drawing::Point(129, 279);
		this->textBox_clearpk->Name = L"textBox_clearpk";
		this->textBox_clearpk->Size = System::Drawing::Size(100, 20);
		this->textBox_clearpk->TabIndex = 88;
		this->textBox_clearpk->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clearpk);
		// 
		// label126
		// 
		this->label126->AutoSize = true;
		this->label126->Location = System::Drawing::Point(6, 282);
		this->label126->Name = L"label126";
		this->label126->Size = System::Drawing::Size(84, 13);
		this->label126->TabIndex = 87;
		this->label126->Text = L"Remove Infamy:";
		// 
		// textBox_rewardtitle
		// 
		this->textBox_rewardtitle->Location = System::Drawing::Point(129, 253);
		this->textBox_rewardtitle->Name = L"textBox_rewardtitle";
		this->textBox_rewardtitle->Size = System::Drawing::Size(100, 20);
		this->textBox_rewardtitle->TabIndex = 86;
		this->textBox_rewardtitle->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewardtitle);
		// 
		// label125
		// 
		this->label125->AutoSize = true;
		this->label125->Location = System::Drawing::Point(6, 256);
		this->label125->Name = L"label125";
		this->label125->Size = System::Drawing::Size(30, 13);
		this->label125->TabIndex = 85;
		this->label125->Text = L"Title:";
		// 
		// textBox_run13
		// 
		this->textBox_run13->Location = System::Drawing::Point(580, 19);
		this->textBox_run13->Name = L"textBox_run13";
		this->textBox_run13->Size = System::Drawing::Size(51, 20);
		this->textBox_run13->TabIndex = 84;
		this->textBox_run13->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run13);
		// 
		// label124
		// 
		this->label124->AutoSize = true;
		this->label124->Location = System::Drawing::Point(541, 22);
		this->label124->Name = L"label124";
		this->label124->Size = System::Drawing::Size(39, 13);
		this->label124->TabIndex = 83;
		this->label124->Text = L"Un 13:";
		// 
		// textBox_clancontribut
		// 
		this->textBox_clancontribut->Location = System::Drawing::Point(129, 227);
		this->textBox_clancontribut->Name = L"textBox_clancontribut";
		this->textBox_clancontribut->Size = System::Drawing::Size(100, 20);
		this->textBox_clancontribut->TabIndex = 82;
		this->textBox_clancontribut->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clancontribut);
		// 
		// textBox_rewclancont
		// 
		this->textBox_rewclancont->Location = System::Drawing::Point(129, 201);
		this->textBox_rewclancont->Name = L"textBox_rewclancont";
		this->textBox_rewclancont->Size = System::Drawing::Size(100, 20);
		this->textBox_rewclancont->TabIndex = 81;
		this->textBox_rewclancont->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewclancont);
		// 
		// textBox_run121
		// 
		this->textBox_run121->Location = System::Drawing::Point(478, 305);
		this->textBox_run121->Name = L"textBox_run121";
		this->textBox_run121->Size = System::Drawing::Size(60, 20);
		this->textBox_run121->TabIndex = 80;
		this->textBox_run121->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run121);
		// 
		// textBox_grint
		// 
		this->textBox_grint->Location = System::Drawing::Point(129, 175);
		this->textBox_grint->Name = L"textBox_grint";
		this->textBox_grint->Size = System::Drawing::Size(100, 20);
		this->textBox_grint->TabIndex = 79;
		this->textBox_grint->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_grint);
		// 
		// textBox_run12
		// 
		this->textBox_run12->Location = System::Drawing::Point(478, 279);
		this->textBox_run12->Name = L"textBox_run12";
		this->textBox_run12->Size = System::Drawing::Size(60, 20);
		this->textBox_run12->TabIndex = 78;
		this->textBox_run12->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run12);
		// 
		// textBox_run10
		// 
		this->textBox_run10->Location = System::Drawing::Point(478, 253);
		this->textBox_run10->Name = L"textBox_run10";
		this->textBox_run10->Size = System::Drawing::Size(60, 20);
		this->textBox_run10->TabIndex = 77;
		this->textBox_run10->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run10);
		// 
		// textBox_run09
		// 
		this->textBox_run09->Location = System::Drawing::Point(478, 227);
		this->textBox_run09->Name = L"textBox_run09";
		this->textBox_run09->Size = System::Drawing::Size(60, 20);
		this->textBox_run09->TabIndex = 76;
		this->textBox_run09->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run09);
		// 
		// textBox_run08
		// 
		this->textBox_run08->Location = System::Drawing::Point(478, 201);
		this->textBox_run08->Name = L"textBox_run08";
		this->textBox_run08->Size = System::Drawing::Size(60, 20);
		this->textBox_run08->TabIndex = 75;
		this->textBox_run08->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run08);
		// 
		// textBox_run07
		// 
		this->textBox_run07->Location = System::Drawing::Point(478, 175);
		this->textBox_run07->Name = L"textBox_run07";
		this->textBox_run07->Size = System::Drawing::Size(60, 20);
		this->textBox_run07->TabIndex = 74;
		this->textBox_run07->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run07);
		// 
		// textBox_run06
		// 
		this->textBox_run06->Location = System::Drawing::Point(478, 149);
		this->textBox_run06->Name = L"textBox_run06";
		this->textBox_run06->Size = System::Drawing::Size(60, 20);
		this->textBox_run06->TabIndex = 73;
		this->textBox_run06->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run06);
		// 
		// textBox_run05
		// 
		this->textBox_run05->Location = System::Drawing::Point(478, 123);
		this->textBox_run05->Name = L"textBox_run05";
		this->textBox_run05->Size = System::Drawing::Size(60, 20);
		this->textBox_run05->TabIndex = 72;
		this->textBox_run05->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run05);
		// 
		// textBox_run04
		// 
		this->textBox_run04->Location = System::Drawing::Point(478, 97);
		this->textBox_run04->Name = L"textBox_run04";
		this->textBox_run04->Size = System::Drawing::Size(60, 20);
		this->textBox_run04->TabIndex = 71;
		this->textBox_run04->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_run04);
		// 
		// textBox_un03
		// 
		this->textBox_un03->Location = System::Drawing::Point(478, 71);
		this->textBox_un03->Name = L"textBox_un03";
		this->textBox_un03->Size = System::Drawing::Size(60, 20);
		this->textBox_un03->TabIndex = 70;
		this->textBox_un03->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un03);
		// 
		// textBox_un02
		// 
		this->textBox_un02->Location = System::Drawing::Point(478, 45);
		this->textBox_un02->Name = L"textBox_un02";
		this->textBox_un02->Size = System::Drawing::Size(60, 20);
		this->textBox_un02->TabIndex = 69;
		this->textBox_un02->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un02);
		// 
		// textBox_un01
		// 
		this->textBox_un01->Location = System::Drawing::Point(478, 19);
		this->textBox_un01->Name = L"textBox_un01";
		this->textBox_un01->Size = System::Drawing::Size(60, 20);
		this->textBox_un01->TabIndex = 68;
		this->textBox_un01->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un01);
		// 
		// label105
		// 
		this->label105->AutoSize = true;
		this->label105->Location = System::Drawing::Point(433, 308);
		this->label105->Name = L"label105";
		this->label105->Size = System::Drawing::Size(51, 13);
		this->label105->TabIndex = 67;
		this->label105->Text = L"Un 12_1:";
		// 
		// label104
		// 
		this->label104->AutoSize = true;
		this->label104->Location = System::Drawing::Point(433, 282);
		this->label104->Name = L"label104";
		this->label104->Size = System::Drawing::Size(39, 13);
		this->label104->TabIndex = 66;
		this->label104->Text = L"Un 12:";
		// 
		// label103
		// 
		this->label103->AutoSize = true;
		this->label103->Location = System::Drawing::Point(433, 256);
		this->label103->Name = L"label103";
		this->label103->Size = System::Drawing::Size(39, 13);
		this->label103->TabIndex = 65;
		this->label103->Text = L"Un 10:";
		// 
		// label67
		// 
		this->label67->AutoSize = true;
		this->label67->Location = System::Drawing::Point(433, 230);
		this->label67->Name = L"label67";
		this->label67->Size = System::Drawing::Size(39, 13);
		this->label67->TabIndex = 64;
		this->label67->Text = L"Un 09:";
		// 
		// label66
		// 
		this->label66->AutoSize = true;
		this->label66->Location = System::Drawing::Point(433, 204);
		this->label66->Name = L"label66";
		this->label66->Size = System::Drawing::Size(39, 13);
		this->label66->TabIndex = 63;
		this->label66->Text = L"Un 08:";
		// 
		// label61
		// 
		this->label61->AutoSize = true;
		this->label61->Location = System::Drawing::Point(433, 178);
		this->label61->Name = L"label61";
		this->label61->Size = System::Drawing::Size(39, 13);
		this->label61->TabIndex = 62;
		this->label61->Text = L"Un 07:";
		// 
		// label60
		// 
		this->label60->AutoSize = true;
		this->label60->Location = System::Drawing::Point(433, 152);
		this->label60->Name = L"label60";
		this->label60->Size = System::Drawing::Size(39, 13);
		this->label60->TabIndex = 61;
		this->label60->Text = L"Un 06:";
		// 
		// label53
		// 
		this->label53->AutoSize = true;
		this->label53->Location = System::Drawing::Point(433, 126);
		this->label53->Name = L"label53";
		this->label53->Size = System::Drawing::Size(39, 13);
		this->label53->TabIndex = 60;
		this->label53->Text = L"Un 05:";
		// 
		// label39
		// 
		this->label39->AutoSize = true;
		this->label39->Location = System::Drawing::Point(433, 100);
		this->label39->Name = L"label39";
		this->label39->Size = System::Drawing::Size(39, 13);
		this->label39->TabIndex = 59;
		this->label39->Text = L"Un 04:";
		// 
		// label27
		// 
		this->label27->AutoSize = true;
		this->label27->Location = System::Drawing::Point(433, 74);
		this->label27->Name = L"label27";
		this->label27->Size = System::Drawing::Size(39, 13);
		this->label27->TabIndex = 58;
		this->label27->Text = L"Un 03:";
		// 
		// label13
		// 
		this->label13->AutoSize = true;
		this->label13->Location = System::Drawing::Point(433, 48);
		this->label13->Name = L"label13";
		this->label13->Size = System::Drawing::Size(39, 13);
		this->label13->TabIndex = 57;
		this->label13->Text = L"Un 02:";
		// 
		// label12
		// 
		this->label12->AutoSize = true;
		this->label12->Location = System::Drawing::Point(433, 22);
		this->label12->Name = L"label12";
		this->label12->Size = System::Drawing::Size(39, 13);
		this->label12->TabIndex = 56;
		this->label12->Text = L"Un 01:";
		// 
		// label198
		// 
		this->label198->AutoSize = true;
		this->label198->Location = System::Drawing::Point(344, 104);
		this->label198->Name = L"label198";
		this->label198->Size = System::Drawing::Size(17, 13);
		this->label198->TabIndex = 43;
		this->label198->Text = L"Z:";
		// 
		// label197
		// 
		this->label197->AutoSize = true;
		this->label197->Location = System::Drawing::Point(316, 74);
		this->label197->Name = L"label197";
		this->label197->Size = System::Drawing::Size(45, 13);
		this->label197->TabIndex = 42;
		this->label197->Text = L"Altitude:";
		// 
		// label196
		// 
		this->label196->AutoSize = true;
		this->label196->Location = System::Drawing::Point(344, 48);
		this->label196->Name = L"label196";
		this->label196->Size = System::Drawing::Size(17, 13);
		this->label196->TabIndex = 41;
		this->label196->Text = L"X:";
		// 
		// label192
		// 
		this->label192->AutoSize = true;
		this->label192->Location = System::Drawing::Point(6, 178);
		this->label192->Name = L"label192";
		this->label192->Size = System::Drawing::Size(77, 13);
		this->label192->TabIndex = 33;
		this->label192->Text = L"Group Integral:";
		// 
		// textBox_reward_ai_trigger
		// 
		this->textBox_reward_ai_trigger->Location = System::Drawing::Point(367, 126);
		this->textBox_reward_ai_trigger->Name = L"textBox_reward_ai_trigger";
		this->textBox_reward_ai_trigger->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_ai_trigger->TabIndex = 23;
		this->textBox_reward_ai_trigger->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_ai_trigger);
		// 
		// textBox_reward_teleport_map_id
		// 
		this->textBox_reward_teleport_map_id->Location = System::Drawing::Point(367, 15);
		this->textBox_reward_teleport_map_id->Name = L"textBox_reward_teleport_map_id";
		this->textBox_reward_teleport_map_id->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_teleport_map_id->TabIndex = 29;
		this->textBox_reward_teleport_map_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_teleport_map_id);
		// 
		// textBox_reward_teleport_z
		// 
		this->textBox_reward_teleport_z->Location = System::Drawing::Point(367, 97);
		this->textBox_reward_teleport_z->Name = L"textBox_reward_teleport_z";
		this->textBox_reward_teleport_z->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_teleport_z->TabIndex = 32;
		this->textBox_reward_teleport_z->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_teleport_z);
		// 
		// textBox_reward_teleport_x
		// 
		this->textBox_reward_teleport_x->Location = System::Drawing::Point(367, 45);
		this->textBox_reward_teleport_x->Name = L"textBox_reward_teleport_x";
		this->textBox_reward_teleport_x->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_teleport_x->TabIndex = 30;
		this->textBox_reward_teleport_x->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_teleport_x);
		// 
		// textBox_reward_teleport_altitude
		// 
		this->textBox_reward_teleport_altitude->Location = System::Drawing::Point(367, 71);
		this->textBox_reward_teleport_altitude->Name = L"textBox_reward_teleport_altitude";
		this->textBox_reward_teleport_altitude->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_teleport_altitude->TabIndex = 31;
		this->textBox_reward_teleport_altitude->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_teleport_altitude);
		// 
		// label26
		// 
		this->label26->AutoSize = true;
		this->label26->Location = System::Drawing::Point(274, 22);
		this->label26->Name = L"label26";
		this->label26->Size = System::Drawing::Size(87, 13);
		this->label26->TabIndex = 28;
		this->label26->Text = L"Teleport Map ID:";
		// 
		// label25
		// 
		this->label25->AutoSize = true;
		this->label25->Location = System::Drawing::Point(6, 230);
		this->label25->Name = L"label25";
		this->label25->Size = System::Drawing::Size(89, 13);
		this->label25->TabIndex = 14;
		this->label25->Text = L"Clan contribution:";
		// 
		// label22
		// 
		this->label22->AutoSize = true;
		this->label22->Location = System::Drawing::Point(6, 204);
		this->label22->Name = L"label22";
		this->label22->Size = System::Drawing::Size(104, 13);
		this->label22->TabIndex = 20;
		this->label22->Text = L"Faction Contribution:";
		// 
		// textBox_reward_newtask
		// 
		this->textBox_reward_newtask->Location = System::Drawing::Point(129, 149);
		this->textBox_reward_newtask->Name = L"textBox_reward_newtask";
		this->textBox_reward_newtask->Size = System::Drawing::Size(100, 20);
		this->textBox_reward_newtask->TabIndex = 13;
		this->textBox_reward_newtask->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_newtask);
		// 
		// label21
		// 
		this->label21->AutoSize = true;
		this->label21->Location = System::Drawing::Point(6, 152);
		this->label21->Name = L"label21";
		this->label21->Size = System::Drawing::Size(78, 13);
		this->label21->TabIndex = 12;
		this->label21->Text = L"Give new task:";
		// 
		// textBox_reward_APLv
		// 
		this->textBox_reward_APLv->Location = System::Drawing::Point(129, 123);
		this->textBox_reward_APLv->Name = L"textBox_reward_APLv";
		this->textBox_reward_APLv->Size = System::Drawing::Size(100, 20);
		this->textBox_reward_APLv->TabIndex = 11;
		this->textBox_reward_APLv->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_APLv);
		// 
		// label20
		// 
		this->label20->AutoSize = true;
		this->label20->Location = System::Drawing::Point(6, 126);
		this->label20->Name = L"label20";
		this->label20->Size = System::Drawing::Size(36, 13);
		this->label20->TabIndex = 10;
		this->label20->Text = L"APLv:";
		// 
		// textBox_reward_rclvl
		// 
		this->textBox_reward_rclvl->Location = System::Drawing::Point(129, 97);
		this->textBox_reward_rclvl->Name = L"textBox_reward_rclvl";
		this->textBox_reward_rclvl->Size = System::Drawing::Size(100, 20);
		this->textBox_reward_rclvl->TabIndex = 7;
		this->textBox_reward_rclvl->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_rclvl);
		// 
		// label19
		// 
		this->label19->AutoSize = true;
		this->label19->Location = System::Drawing::Point(6, 100);
		this->label19->Name = L"label19";
		this->label19->Size = System::Drawing::Size(37, 13);
		this->label19->TabIndex = 6;
		this->label19->Text = L"RCLv:";
		// 
		// textBox_rewardExpLv
		// 
		this->textBox_rewardExpLv->Location = System::Drawing::Point(129, 71);
		this->textBox_rewardExpLv->Name = L"textBox_rewardExpLv";
		this->textBox_rewardExpLv->Size = System::Drawing::Size(100, 20);
		this->textBox_rewardExpLv->TabIndex = 5;
		this->textBox_rewardExpLv->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewardExpLv);
		// 
		// label18
		// 
		this->label18->AutoSize = true;
		this->label18->Location = System::Drawing::Point(6, 74);
		this->label18->Name = L"label18";
		this->label18->Size = System::Drawing::Size(40, 13);
		this->label18->TabIndex = 4;
		this->label18->Text = L"ExpLv:";
		// 
		// textBox_reward_experience
		// 
		this->textBox_reward_experience->Location = System::Drawing::Point(129, 45);
		this->textBox_reward_experience->Name = L"textBox_reward_experience";
		this->textBox_reward_experience->Size = System::Drawing::Size(100, 20);
		this->textBox_reward_experience->TabIndex = 3;
		this->textBox_reward_experience->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_experience);
		// 
		// label17
		// 
		this->label17->AutoSize = true;
		this->label17->Location = System::Drawing::Point(6, 48);
		this->label17->Name = L"label17";
		this->label17->Size = System::Drawing::Size(63, 13);
		this->label17->TabIndex = 2;
		this->label17->Text = L"Experience:";
		// 
		// textBox_reward_coins
		// 
		this->textBox_reward_coins->Location = System::Drawing::Point(129, 19);
		this->textBox_reward_coins->Name = L"textBox_reward_coins";
		this->textBox_reward_coins->Size = System::Drawing::Size(100, 20);
		this->textBox_reward_coins->TabIndex = 1;
		this->textBox_reward_coins->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_coins);
		// 
		// label16
		// 
		this->label16->AutoSize = true;
		this->label16->Location = System::Drawing::Point(6, 22);
		this->label16->Name = L"label16";
		this->label16->Size = System::Drawing::Size(36, 13);
		this->label16->TabIndex = 0;
		this->label16->Text = L"Coins:";
		// 
		// textBox_runk26
		// 
		this->textBox_runk26->Location = System::Drawing::Point(235, 192);
		this->textBox_runk26->Name = L"textBox_runk26";
		this->textBox_runk26->Size = System::Drawing::Size(112, 20);
		this->textBox_runk26->TabIndex = 331;
		// 
		// textBox_runk25
		// 
		this->textBox_runk25->Location = System::Drawing::Point(68, 195);
		this->textBox_runk25->Name = L"textBox_runk25";
		this->textBox_runk25->Size = System::Drawing::Size(100, 20);
		this->textBox_runk25->TabIndex = 330;
		this->textBox_runk25->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk25);
		// 
		// label223
		// 
		this->label223->AutoSize = true;
		this->label223->Location = System::Drawing::Point(174, 199);
		this->label223->Name = L"label223";
		this->label223->Size = System::Drawing::Size(36, 13);
		this->label223->TabIndex = 325;
		this->label223->Text = L"Kytos:";
		// 
		// label193
		// 
		this->label193->AutoSize = true;
		this->label193->Location = System::Drawing::Point(10, 199);
		this->label193->Name = L"label193";
		this->label193->Size = System::Drawing::Size(51, 13);
		this->label193->TabIndex = 324;
		this->label193->Text = L"Psychea:";
		// 
		// groupBox_conversation
		// 
		this->groupBox_conversation->Controls->Add(this->label278);
		this->groupBox_conversation->Controls->Add(this->label277);
		this->groupBox_conversation->Controls->Add(this->textBox_newtxt);
		this->groupBox_conversation->Controls->Add(this->textBox_xanfumess);
		this->groupBox_conversation->Controls->Add(this->label266);
		this->groupBox_conversation->Controls->Add(this->textBox_agernmess);
		this->groupBox_conversation->Controls->Add(this->textBox_conversation_general_text);
		this->groupBox_conversation->Controls->Add(this->label42);
		this->groupBox_conversation->Controls->Add(this->label40);
		this->groupBox_conversation->Controls->Add(this->textBox_conversation_prompt_text);
		this->groupBox_conversation->Location = System::Drawing::Point(3, 3);
		this->groupBox_conversation->Name = L"groupBox_conversation";
		this->groupBox_conversation->Size = System::Drawing::Size(671, 214);
		this->groupBox_conversation->TabIndex = 0;
		this->groupBox_conversation->TabStop = false;
		this->groupBox_conversation->Text = L"CONVERSATION";
		// 
		// label278
		// 
		this->label278->AutoSize = true;
		this->label278->Location = System::Drawing::Point(317, 136);
		this->label278->Name = L"label278";
		this->label278->Size = System::Drawing::Size(32, 13);
		this->label278->TabIndex = 9;
		this->label278->Text = L"New:";
		// 
		// label277
		// 
		this->label277->AutoSize = true;
		this->label277->Location = System::Drawing::Point(317, 76);
		this->label277->Name = L"label277";
		this->label277->Size = System::Drawing::Size(64, 13);
		this->label277->TabIndex = 8;
		this->label277->Text = L"Suspended:";
		// 
		// textBox_newtxt
		// 
		this->textBox_newtxt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_newtxt->Location = System::Drawing::Point(317, 152);
		this->textBox_newtxt->Multiline = true;
		this->textBox_newtxt->Name = L"textBox_newtxt";
		this->textBox_newtxt->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_newtxt->Size = System::Drawing::Size(348, 56);
		this->textBox_newtxt->TabIndex = 7;
		this->textBox_newtxt->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_new);
		// 
		// textBox_xanfumess
		// 
		this->textBox_xanfumess->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_xanfumess->Location = System::Drawing::Point(317, 90);
		this->textBox_xanfumess->Multiline = true;
		this->textBox_xanfumess->Name = L"textBox_xanfumess";
		this->textBox_xanfumess->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_xanfumess->Size = System::Drawing::Size(348, 46);
		this->textBox_xanfumess->TabIndex = 6;
		this->textBox_xanfumess->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_xanfu);
		// 
		// label266
		// 
		this->label266->AutoSize = true;
		this->label266->Location = System::Drawing::Point(6, 127);
		this->label266->Name = L"label266";
		this->label266->Size = System::Drawing::Size(38, 13);
		this->label266->TabIndex = 5;
		this->label266->Text = L"Agern:";
		// 
		// textBox_agernmess
		// 
		this->textBox_agernmess->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_agernmess->Location = System::Drawing::Point(6, 143);
		this->textBox_agernmess->Multiline = true;
		this->textBox_agernmess->Name = L"textBox_agernmess";
		this->textBox_agernmess->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_agernmess->Size = System::Drawing::Size(305, 65);
		this->textBox_agernmess->TabIndex = 4;
		this->textBox_agernmess->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_agern);
		// 
		// textBox_conversation_general_text
		// 
		this->textBox_conversation_general_text->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_conversation_general_text->Location = System::Drawing::Point(317, 35);
		this->textBox_conversation_general_text->Multiline = true;
		this->textBox_conversation_general_text->Name = L"textBox_conversation_general_text";
		this->textBox_conversation_general_text->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_conversation_general_text->Size = System::Drawing::Size(348, 38);
		this->textBox_conversation_general_text->TabIndex = 3;
		this->textBox_conversation_general_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_general_text);
		// 
		// label42
		// 
		this->label42->AutoSize = true;
		this->label42->Location = System::Drawing::Point(314, 19);
		this->label42->Name = L"label42";
		this->label42->Size = System::Drawing::Size(109, 13);
		this->label42->TabIndex = 2;
		this->label42->Text = L"World Chat Message:";
		// 
		// label40
		// 
		this->label40->AutoSize = true;
		this->label40->Location = System::Drawing::Point(3, 19);
		this->label40->Name = L"label40";
		this->label40->Size = System::Drawing::Size(43, 13);
		this->label40->TabIndex = 0;
		this->label40->Text = L"Prompt:";
		// 
		// textBox_conversation_prompt_text
		// 
		this->textBox_conversation_prompt_text->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_prompt_text->Location = System::Drawing::Point(6, 35);
		this->textBox_conversation_prompt_text->Multiline = true;
		this->textBox_conversation_prompt_text->Name = L"textBox_conversation_prompt_text";
		this->textBox_conversation_prompt_text->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_conversation_prompt_text->Size = System::Drawing::Size(305, 89);
		this->textBox_conversation_prompt_text->TabIndex = 1;
		this->textBox_conversation_prompt_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_prompt_text);
		// 
		// groupBox_answers
		// 
		this->groupBox_answers->Controls->Add(this->textBox_conversation_answer_text);
		this->groupBox_answers->Controls->Add(this->textBox_conversation_answer_task_link);
		this->groupBox_answers->Controls->Add(this->label46);
		this->groupBox_answers->Controls->Add(this->textBox_conversation_answer_question_link);
		this->groupBox_answers->Controls->Add(this->label47);
		this->groupBox_answers->Controls->Add(this->listBox_conversation_answers);
		this->groupBox_answers->Location = System::Drawing::Point(4, 467);
		this->groupBox_answers->Name = L"groupBox_answers";
		this->groupBox_answers->Size = System::Drawing::Size(671, 132);
		this->groupBox_answers->TabIndex = 3;
		this->groupBox_answers->TabStop = false;
		this->groupBox_answers->Text = L"ANSWERS";
		// 
		// textBox_conversation_answer_text
		// 
		this->textBox_conversation_answer_text->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_conversation_answer_text->Location = System::Drawing::Point(178, 19);
		this->textBox_conversation_answer_text->MaxLength = 63;
		this->textBox_conversation_answer_text->Multiline = true;
		this->textBox_conversation_answer_text->Name = L"textBox_conversation_answer_text";
		this->textBox_conversation_answer_text->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_conversation_answer_text->Size = System::Drawing::Size(487, 107);
		this->textBox_conversation_answer_text->TabIndex = 5;
		this->textBox_conversation_answer_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_answer_text);
		// 
		// textBox_conversation_answer_task_link
		// 
		this->textBox_conversation_answer_task_link->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_answer_task_link->Location = System::Drawing::Point(84, 106);
		this->textBox_conversation_answer_task_link->Name = L"textBox_conversation_answer_task_link";
		this->textBox_conversation_answer_task_link->Size = System::Drawing::Size(88, 20);
		this->textBox_conversation_answer_task_link->TabIndex = 4;
		this->textBox_conversation_answer_task_link->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_answer_task_link);
		// 
		// label46
		// 
		this->label46->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->label46->AutoSize = true;
		this->label46->Location = System::Drawing::Point(3, 109);
		this->label46->Name = L"label46";
		this->label46->Size = System::Drawing::Size(57, 13);
		this->label46->TabIndex = 3;
		this->label46->Text = L"Task Link:";
		// 
		// textBox_conversation_answer_question_link
		// 
		this->textBox_conversation_answer_question_link->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_answer_question_link->Location = System::Drawing::Point(84, 80);
		this->textBox_conversation_answer_question_link->Name = L"textBox_conversation_answer_question_link";
		this->textBox_conversation_answer_question_link->Size = System::Drawing::Size(88, 20);
		this->textBox_conversation_answer_question_link->TabIndex = 2;
		this->textBox_conversation_answer_question_link->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_answer_question_link);
		// 
		// label47
		// 
		this->label47->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->label47->AutoSize = true;
		this->label47->Location = System::Drawing::Point(3, 83);
		this->label47->Name = L"label47";
		this->label47->Size = System::Drawing::Size(75, 13);
		this->label47->TabIndex = 1;
		this->label47->Text = L"Question Link:";
		// 
		// listBox_conversation_answers
		// 
		this->listBox_conversation_answers->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->listBox_conversation_answers->ContextMenuStrip = this->contextMenuStrip_conversation_answer;
		this->listBox_conversation_answers->FormattingEnabled = true;
		this->listBox_conversation_answers->Location = System::Drawing::Point(6, 19);
		this->listBox_conversation_answers->Name = L"listBox_conversation_answers";
		this->listBox_conversation_answers->Size = System::Drawing::Size(166, 56);
		this->listBox_conversation_answers->TabIndex = 0;
		this->listBox_conversation_answers->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_answer);
		// 
		// contextMenuStrip_conversation_answer
		// 
		this->contextMenuStrip_conversation_answer->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem46, 
			this->toolStripMenuItem47});
		this->contextMenuStrip_conversation_answer->Name = L"contextMenuStrip_conversation";
		this->contextMenuStrip_conversation_answer->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
		this->contextMenuStrip_conversation_answer->ShowImageMargin = false;
		this->contextMenuStrip_conversation_answer->Size = System::Drawing::Size(120, 48);
		// 
		// toolStripMenuItem46
		// 
		this->toolStripMenuItem46->Name = L"toolStripMenuItem46";
		this->toolStripMenuItem46->Size = System::Drawing::Size(119, 22);
		this->toolStripMenuItem46->Text = L"Add Answer";
		this->toolStripMenuItem46->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_answer);
		// 
		// toolStripMenuItem47
		// 
		this->toolStripMenuItem47->Name = L"toolStripMenuItem47";
		this->toolStripMenuItem47->Size = System::Drawing::Size(119, 22);
		this->toolStripMenuItem47->Text = L"Delete Answer";
		this->toolStripMenuItem47->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_answer);
		// 
		// contextMenuStrip_conversation_question
		// 
		this->contextMenuStrip_conversation_question->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->addToolStripMenuItem, 
			this->removeToolStripMenuItem});
		this->contextMenuStrip_conversation_question->Name = L"contextMenuStrip_conversation";
		this->contextMenuStrip_conversation_question->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
		this->contextMenuStrip_conversation_question->ShowImageMargin = false;
		this->contextMenuStrip_conversation_question->Size = System::Drawing::Size(127, 48);
		// 
		// addToolStripMenuItem
		// 
		this->addToolStripMenuItem->Name = L"addToolStripMenuItem";
		this->addToolStripMenuItem->Size = System::Drawing::Size(126, 22);
		this->addToolStripMenuItem->Text = L"Add Question";
		this->addToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_question);
		// 
		// removeToolStripMenuItem
		// 
		this->removeToolStripMenuItem->Name = L"removeToolStripMenuItem";
		this->removeToolStripMenuItem->Size = System::Drawing::Size(126, 22);
		this->removeToolStripMenuItem->Text = L"Delete Question";
		this->removeToolStripMenuItem->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_question);
		// 
		// groupBox_questions
		// 
		this->groupBox_questions->Controls->Add(this->textBox_conversation_question_text);
		this->groupBox_questions->Controls->Add(this->textBox_conversation_question_previous);
		this->groupBox_questions->Controls->Add(this->label45);
		this->groupBox_questions->Controls->Add(this->textBox_conversation_question_id);
		this->groupBox_questions->Controls->Add(this->label44);
		this->groupBox_questions->Controls->Add(this->listBox_conversation_questions);
		this->groupBox_questions->Location = System::Drawing::Point(3, 329);
		this->groupBox_questions->Name = L"groupBox_questions";
		this->groupBox_questions->Size = System::Drawing::Size(671, 132);
		this->groupBox_questions->TabIndex = 2;
		this->groupBox_questions->TabStop = false;
		this->groupBox_questions->Text = L"QUESTIONS";
		// 
		// textBox_conversation_question_text
		// 
		this->textBox_conversation_question_text->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_conversation_question_text->Location = System::Drawing::Point(178, 19);
		this->textBox_conversation_question_text->Multiline = true;
		this->textBox_conversation_question_text->Name = L"textBox_conversation_question_text";
		this->textBox_conversation_question_text->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_conversation_question_text->Size = System::Drawing::Size(487, 107);
		this->textBox_conversation_question_text->TabIndex = 5;
		this->textBox_conversation_question_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_question_text);
		// 
		// textBox_conversation_question_previous
		// 
		this->textBox_conversation_question_previous->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_question_previous->Location = System::Drawing::Point(84, 106);
		this->textBox_conversation_question_previous->Name = L"textBox_conversation_question_previous";
		this->textBox_conversation_question_previous->Size = System::Drawing::Size(88, 20);
		this->textBox_conversation_question_previous->TabIndex = 4;
		this->textBox_conversation_question_previous->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_question_previous);
		// 
		// label45
		// 
		this->label45->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->label45->AutoSize = true;
		this->label45->Location = System::Drawing::Point(3, 109);
		this->label45->Name = L"label45";
		this->label45->Size = System::Drawing::Size(51, 13);
		this->label45->TabIndex = 3;
		this->label45->Text = L"Previous:";
		// 
		// textBox_conversation_question_id
		// 
		this->textBox_conversation_question_id->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_question_id->Location = System::Drawing::Point(84, 80);
		this->textBox_conversation_question_id->Name = L"textBox_conversation_question_id";
		this->textBox_conversation_question_id->Size = System::Drawing::Size(88, 20);
		this->textBox_conversation_question_id->TabIndex = 2;
		this->textBox_conversation_question_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_question_id);
		// 
		// label44
		// 
		this->label44->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->label44->AutoSize = true;
		this->label44->Location = System::Drawing::Point(3, 83);
		this->label44->Name = L"label44";
		this->label44->Size = System::Drawing::Size(21, 13);
		this->label44->TabIndex = 1;
		this->label44->Text = L"ID:";
		// 
		// listBox_conversation_questions
		// 
		this->listBox_conversation_questions->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->listBox_conversation_questions->ContextMenuStrip = this->contextMenuStrip_conversation_question;
		this->listBox_conversation_questions->FormattingEnabled = true;
		this->listBox_conversation_questions->Location = System::Drawing::Point(6, 19);
		this->listBox_conversation_questions->Name = L"listBox_conversation_questions";
		this->listBox_conversation_questions->Size = System::Drawing::Size(166, 56);
		this->listBox_conversation_questions->TabIndex = 0;
		this->listBox_conversation_questions->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_question);
		// 
		// groupBox_dialogs
		// 
		this->groupBox_dialogs->Controls->Add(this->textBox_conversation_dialog_text);
		this->groupBox_dialogs->Controls->Add(this->textBox_conversation_dialog_unknown);
		this->groupBox_dialogs->Controls->Add(this->label43);
		this->groupBox_dialogs->Controls->Add(this->listBox_conversation_dialogs);
		this->groupBox_dialogs->Location = System::Drawing::Point(1, 204);
		this->groupBox_dialogs->Name = L"groupBox_dialogs";
		this->groupBox_dialogs->Size = System::Drawing::Size(671, 119);
		this->groupBox_dialogs->TabIndex = 1;
		this->groupBox_dialogs->TabStop = false;
		this->groupBox_dialogs->Text = L"DIALOGS";
		// 
		// textBox_conversation_dialog_text
		// 
		this->textBox_conversation_dialog_text->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->textBox_conversation_dialog_text->Location = System::Drawing::Point(178, 19);
		this->textBox_conversation_dialog_text->MaxLength = 63;
		this->textBox_conversation_dialog_text->Multiline = true;
		this->textBox_conversation_dialog_text->Name = L"textBox_conversation_dialog_text";
		this->textBox_conversation_dialog_text->ScrollBars = System::Windows::Forms::ScrollBars::Both;
		this->textBox_conversation_dialog_text->Size = System::Drawing::Size(487, 94);
		this->textBox_conversation_dialog_text->TabIndex = 3;
		this->textBox_conversation_dialog_text->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_dialog_text);
		// 
		// textBox_conversation_dialog_unknown
		// 
		this->textBox_conversation_dialog_unknown->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->textBox_conversation_dialog_unknown->Location = System::Drawing::Point(84, 93);
		this->textBox_conversation_dialog_unknown->Name = L"textBox_conversation_dialog_unknown";
		this->textBox_conversation_dialog_unknown->Size = System::Drawing::Size(88, 20);
		this->textBox_conversation_dialog_unknown->TabIndex = 2;
		this->textBox_conversation_dialog_unknown->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_conversation_dialog_unknown);
		// 
		// label43
		// 
		this->label43->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->label43->AutoSize = true;
		this->label43->Location = System::Drawing::Point(3, 96);
		this->label43->Name = L"label43";
		this->label43->Size = System::Drawing::Size(56, 13);
		this->label43->TabIndex = 1;
		this->label43->Text = L"Unknown:";
		// 
		// listBox_conversation_dialogs
		// 
		this->listBox_conversation_dialogs->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left));
		this->listBox_conversation_dialogs->FormattingEnabled = true;
		this->listBox_conversation_dialogs->Items->AddRange(gcnew cli::array< System::Object^  >(5) {L"[1] Dialog", L"[2] Dialog", 
			L"[3] Dialog", L"[4] Dialog", L"[5] Dialog"});
		this->listBox_conversation_dialogs->Location = System::Drawing::Point(6, 19);
		this->listBox_conversation_dialogs->Name = L"listBox_conversation_dialogs";
		this->listBox_conversation_dialogs->Size = System::Drawing::Size(166, 69);
		this->listBox_conversation_dialogs->TabIndex = 0;
		this->listBox_conversation_dialogs->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::select_dialog);
		// 
		// toolTip
		// 
		this->toolTip->AutomaticDelay = 0;
		this->toolTip->AutoPopDelay = 10000;
		this->toolTip->InitialDelay = 0;
		this->toolTip->ReshowDelay = 0;
		// 
		// label106
		// 
		this->label106->AutoSize = true;
		this->label106->Location = System::Drawing::Point(6, 19);
		this->label106->Name = L"label106";
		this->label106->Size = System::Drawing::Size(62, 13);
		this->label106->TabIndex = 89;
		this->label106->Text = L"Has Spans:";
		this->toolTip->SetToolTip(this->label106, L"If version < 89 there must be exactly one location span!");
		// 
		// label107
		// 
		this->label107->AutoSize = true;
		this->label107->Location = System::Drawing::Point(6, 42);
		this->label107->Name = L"label107";
		this->label107->Size = System::Drawing::Size(45, 13);
		this->label107->TabIndex = 93;
		this->label107->Text = L"Map ID:";
		this->toolTip->SetToolTip(this->label107, L"If version < 89 there must be exactly one location span!");
		// 
		// label4
		// 
		this->label4->AutoSize = true;
		this->label4->Location = System::Drawing::Point(6, 19);
		this->label4->Name = L"label4";
		this->label4->Size = System::Drawing::Size(62, 13);
		this->label4->TabIndex = 95;
		this->label4->Text = L"Has Spans:";
		this->toolTip->SetToolTip(this->label4, L"If version < 89 there must be exactly one location span!");
		// 
		// label6
		// 
		this->label6->AutoSize = true;
		this->label6->Location = System::Drawing::Point(6, 42);
		this->label6->Name = L"label6";
		this->label6->Size = System::Drawing::Size(45, 13);
		this->label6->TabIndex = 93;
		this->label6->Text = L"Map ID:";
		this->toolTip->SetToolTip(this->label6, L"If version < 89 there must be exactly one location span!");
		// 
		// label41
		// 
		this->label41->AutoSize = true;
		this->label41->Cursor = System::Windows::Forms::Cursors::Help;
		this->label41->Location = System::Drawing::Point(7, 258);
		this->label41->Name = L"label41";
		this->label41->Size = System::Drawing::Size(85, 13);
		this->label41->TabIndex = 164;
		this->label41->Text = L"Finished Quests:";
		this->toolTip->SetToolTip(this->label41, L"The Quest can only be activated if the following quests are completed");
		// 
		// label48
		// 
		this->label48->AutoSize = true;
		this->label48->Cursor = System::Windows::Forms::Cursors::Help;
		this->label48->Location = System::Drawing::Point(98, 258);
		this->label48->Name = L"label48";
		this->label48->Size = System::Drawing::Size(104, 13);
		this->label48->TabIndex = 10;
		this->label48->Text = L"Unactivated Quests:";
		this->toolTip->SetToolTip(this->label48, L"The Quest can only be activated if the following Quests are not in your Quest Log" 
			L"");
		// 
		// label7
		// 
		this->label7->AutoSize = true;
		this->label7->Cursor = System::Windows::Forms::Cursors::Help;
		this->label7->Location = System::Drawing::Point(586, 152);
		this->label7->Name = L"label7";
		this->label7->Size = System::Drawing::Size(66, 13);
		this->label7->TabIndex = 368;
		this->label7->Text = L"Req unk 01:";
		this->toolTip->SetToolTip(this->label7, L"The Quest can only be activated if the following Quests are not in your Quest Log" 
			L"");
		// 
		// label8
		// 
		this->label8->AutoSize = true;
		this->label8->Cursor = System::Windows::Forms::Cursors::Help;
		this->label8->Location = System::Drawing::Point(586, 191);
		this->label8->Name = L"label8";
		this->label8->Size = System::Drawing::Size(66, 13);
		this->label8->TabIndex = 369;
		this->label8->Text = L"Req unk 02:";
		this->toolTip->SetToolTip(this->label8, L"The Quest can only be activated if the following Quests are not in your Quest Log" 
			L"");
		// 
		// label9
		// 
		this->label9->AutoSize = true;
		this->label9->Cursor = System::Windows::Forms::Cursors::Help;
		this->label9->Location = System::Drawing::Point(586, 230);
		this->label9->Name = L"label9";
		this->label9->Size = System::Drawing::Size(43, 13);
		this->label9->TabIndex = 371;
		this->label9->Text = L"Pet Lvl:";
		this->toolTip->SetToolTip(this->label9, L"The Quest can only be activated if the following Quests are not in your Quest Log" 
			L"");
		// 
		// label185
		// 
		this->label185->AutoSize = true;
		this->label185->Cursor = System::Windows::Forms::Cursors::Help;
		this->label185->Location = System::Drawing::Point(214, 271);
		this->label185->Name = L"label185";
		this->label185->Size = System::Drawing::Size(35, 13);
		this->label185->TabIndex = 453;
		this->label185->Text = L"Titles:";
		this->toolTip->SetToolTip(this->label185, L"The Quest can only be activated if the following quests are completed");
		// 
		// label11
		// 
		this->label11->AutoSize = true;
		this->label11->Location = System::Drawing::Point(6, 42);
		this->label11->Name = L"label11";
		this->label11->Size = System::Drawing::Size(45, 13);
		this->label11->TabIndex = 93;
		this->label11->Text = L"Map ID:";
		this->toolTip->SetToolTip(this->label11, L"If version < 89 there must be exactly one location span!");
		// 
		// label15
		// 
		this->label15->AutoSize = true;
		this->label15->Location = System::Drawing::Point(6, 19);
		this->label15->Name = L"label15";
		this->label15->Size = System::Drawing::Size(62, 13);
		this->label15->TabIndex = 95;
		this->label15->Text = L"Has Spans:";
		this->toolTip->SetToolTip(this->label15, L"If version < 89 there must be exactly one location span!");
		// 
		// comboBox_search
		// 
		this->comboBox_search->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
		this->comboBox_search->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
		this->comboBox_search->FormattingEnabled = true;
		this->comboBox_search->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"ID", L"Name", L"Creature Builder ID"});
		this->comboBox_search->Location = System::Drawing::Point(0, 610);
		this->comboBox_search->Name = L"comboBox_search";
		this->comboBox_search->Size = System::Drawing::Size(137, 21);
		this->comboBox_search->TabIndex = 3;
		// 
		// groupBox_basic_2
		// 
		this->groupBox_basic_2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_basic_2->Controls->Add(this->textBox_parent_quest);
		this->groupBox_basic_2->Controls->Add(this->textBox_sub_quest_first);
		this->groupBox_basic_2->Controls->Add(this->label52);
		this->groupBox_basic_2->Controls->Add(this->label51);
		this->groupBox_basic_2->Controls->Add(this->textBox_previous_quest);
		this->groupBox_basic_2->Controls->Add(this->label50);
		this->groupBox_basic_2->Controls->Add(this->label49);
		this->groupBox_basic_2->Controls->Add(this->textBox_next_quest);
		this->groupBox_basic_2->Location = System::Drawing::Point(3, 281);
		this->groupBox_basic_2->Name = L"groupBox_basic_2";
		this->groupBox_basic_2->Size = System::Drawing::Size(671, 93);
		this->groupBox_basic_2->TabIndex = 135;
		this->groupBox_basic_2->TabStop = false;
		this->groupBox_basic_2->Text = L"BASIC PROPERTIES II";
		// 
		// textBox_parent_quest
		// 
		this->textBox_parent_quest->Location = System::Drawing::Point(86, 29);
		this->textBox_parent_quest->Name = L"textBox_parent_quest";
		this->textBox_parent_quest->Size = System::Drawing::Size(60, 20);
		this->textBox_parent_quest->TabIndex = 60;
		this->textBox_parent_quest->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_parent_quest);
		// 
		// textBox_sub_quest_first
		// 
		this->textBox_sub_quest_first->Location = System::Drawing::Point(553, 29);
		this->textBox_sub_quest_first->Name = L"textBox_sub_quest_first";
		this->textBox_sub_quest_first->Size = System::Drawing::Size(60, 20);
		this->textBox_sub_quest_first->TabIndex = 64;
		this->textBox_sub_quest_first->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_sub_quest_first);
		// 
		// label52
		// 
		this->label52->AutoSize = true;
		this->label52->Location = System::Drawing::Point(462, 32);
		this->label52->Name = L"label52";
		this->label52->Size = System::Drawing::Size(85, 13);
		this->label52->TabIndex = 63;
		this->label52->Text = L"SubQuest (First):";
		// 
		// label51
		// 
		this->label51->AutoSize = true;
		this->label51->Location = System::Drawing::Point(327, 32);
		this->label51->Name = L"label51";
		this->label51->Size = System::Drawing::Size(63, 13);
		this->label51->TabIndex = 65;
		this->label51->Text = L"Next Quest:";
		// 
		// textBox_previous_quest
		// 
		this->textBox_previous_quest->Location = System::Drawing::Point(244, 29);
		this->textBox_previous_quest->Name = L"textBox_previous_quest";
		this->textBox_previous_quest->Size = System::Drawing::Size(60, 20);
		this->textBox_previous_quest->TabIndex = 62;
		this->textBox_previous_quest->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_previous_quest);
		// 
		// label50
		// 
		this->label50->AutoSize = true;
		this->label50->Location = System::Drawing::Point(157, 32);
		this->label50->Name = L"label50";
		this->label50->Size = System::Drawing::Size(82, 13);
		this->label50->TabIndex = 61;
		this->label50->Text = L"Previous Quest:";
		// 
		// label49
		// 
		this->label49->AutoSize = true;
		this->label49->Location = System::Drawing::Point(8, 32);
		this->label49->Name = L"label49";
		this->label49->Size = System::Drawing::Size(72, 13);
		this->label49->TabIndex = 59;
		this->label49->Text = L"Parent Quest:";
		// 
		// textBox_next_quest
		// 
		this->textBox_next_quest->Location = System::Drawing::Point(396, 29);
		this->textBox_next_quest->Name = L"textBox_next_quest";
		this->textBox_next_quest->Size = System::Drawing::Size(60, 20);
		this->textBox_next_quest->TabIndex = 66;
		this->textBox_next_quest->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_next_quest);
		// 
		// contextMenuStrip_pq_messages
		// 
		this->contextMenuStrip_pq_messages->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem36, 
			this->toolStripMenuItem37});
		this->contextMenuStrip_pq_messages->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_messages->Size = System::Drawing::Size(159, 48);
		// 
		// toolStripMenuItem36
		// 
		this->toolStripMenuItem36->Name = L"toolStripMenuItem36";
		this->toolStripMenuItem36->Size = System::Drawing::Size(158, 22);
		this->toolStripMenuItem36->Text = L"Add Message";
		// 
		// toolStripMenuItem37
		// 
		this->toolStripMenuItem37->Name = L"toolStripMenuItem37";
		this->toolStripMenuItem37->Size = System::Drawing::Size(158, 22);
		this->toolStripMenuItem37->Text = L"Remove Message";
		// 
		// contextMenuStrip_pq_special_scripts
		// 
		this->contextMenuStrip_pq_special_scripts->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem34, 
			this->toolStripMenuItem35});
		this->contextMenuStrip_pq_special_scripts->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_special_scripts->Size = System::Drawing::Size(144, 48);
		// 
		// toolStripMenuItem34
		// 
		this->toolStripMenuItem34->Name = L"toolStripMenuItem34";
		this->toolStripMenuItem34->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem34->Text = L"Add Script";
		// 
		// toolStripMenuItem35
		// 
		this->toolStripMenuItem35->Name = L"toolStripMenuItem35";
		this->toolStripMenuItem35->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem35->Text = L"Remove Script";
		// 
		// contextMenuStrip_pq_chases
		// 
		this->contextMenuStrip_pq_chases->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem24, 
			this->toolStripMenuItem25});
		this->contextMenuStrip_pq_chases->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_chases->Size = System::Drawing::Size(147, 48);
		// 
		// toolStripMenuItem24
		// 
		this->toolStripMenuItem24->Name = L"toolStripMenuItem24";
		this->toolStripMenuItem24->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem24->Text = L"Add Chase";
		// 
		// toolStripMenuItem25
		// 
		this->toolStripMenuItem25->Name = L"toolStripMenuItem25";
		this->toolStripMenuItem25->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem25->Text = L"Remove Chase";
		// 
		// contextMenuStrip_pq_script_infos
		// 
		this->contextMenuStrip_pq_script_infos->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem30, 
			this->toolStripMenuItem31});
		this->contextMenuStrip_pq_script_infos->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_script_infos->Size = System::Drawing::Size(167, 48);
		// 
		// toolStripMenuItem30
		// 
		this->toolStripMenuItem30->Name = L"toolStripMenuItem30";
		this->toolStripMenuItem30->Size = System::Drawing::Size(166, 22);
		this->toolStripMenuItem30->Text = L"Add Script Info";
		// 
		// toolStripMenuItem31
		// 
		this->toolStripMenuItem31->Name = L"toolStripMenuItem31";
		this->toolStripMenuItem31->Size = System::Drawing::Size(166, 22);
		this->toolStripMenuItem31->Text = L"Remove Script Info";
		// 
		// contextMenuStrip_pq_location
		// 
		this->contextMenuStrip_pq_location->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem28, 
			this->toolStripMenuItem29});
		this->contextMenuStrip_pq_location->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_location->Size = System::Drawing::Size(184, 48);
		// 
		// toolStripMenuItem28
		// 
		this->toolStripMenuItem28->Name = L"toolStripMenuItem28";
		this->toolStripMenuItem28->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem28->Text = L"Add Location Span";
		// 
		// toolStripMenuItem29
		// 
		this->toolStripMenuItem29->Name = L"toolStripMenuItem29";
		this->toolStripMenuItem29->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem29->Text = L"Remove Location Span";
		// 
		// groupBox_basic_1
		// 
		this->groupBox_basic_1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_basic_1->Controls->Add(this->textBox_un13304);
		this->groupBox_basic_1->Controls->Add(this->textBox_un13303);
		this->groupBox_basic_1->Controls->Add(this->label280);
		this->groupBox_basic_1->Controls->Add(this->label279);
		this->groupBox_basic_1->Controls->Add(this->textBox_bldbmax);
		this->groupBox_basic_1->Controls->Add(this->textBox_bldbmin);
		this->groupBox_basic_1->Controls->Add(this->label242);
		this->groupBox_basic_1->Controls->Add(this->checkBox_showlvl);
		this->groupBox_basic_1->Controls->Add(this->label62);
		this->groupBox_basic_1->Controls->Add(this->textBox_newtype2);
		this->groupBox_basic_1->Controls->Add(this->textBox_newtype);
		this->groupBox_basic_1->Controls->Add(this->label116);
		this->groupBox_basic_1->Controls->Add(this->label115);
		this->groupBox_basic_1->Controls->Add(this->checkBox_clantask);
		this->groupBox_basic_1->Controls->Add(this->textBox_classify);
		this->groupBox_basic_1->Controls->Add(this->label110);
		this->groupBox_basic_1->Controls->Add(this->label108);
		this->groupBox_basic_1->Controls->Add(this->textBox_component);
		this->groupBox_basic_1->Controls->Add(this->textBox_cleartask);
		this->groupBox_basic_1->Controls->Add(this->textBox_autounk);
		this->groupBox_basic_1->Controls->Add(this->checkBox_trigger_on);
		this->groupBox_basic_1->Controls->Add(this->label35);
		this->groupBox_basic_1->Controls->Add(this->textBox_level_min);
		this->groupBox_basic_1->Controls->Add(this->label36);
		this->groupBox_basic_1->Controls->Add(this->textBox_level_max);
		this->groupBox_basic_1->Controls->Add(this->label33);
		this->groupBox_basic_1->Controls->Add(this->textBox_quest_npc);
		this->groupBox_basic_1->Controls->Add(this->label34);
		this->groupBox_basic_1->Controls->Add(this->textBox_reward_npc);
		this->groupBox_basic_1->Controls->Add(this->label123);
		this->groupBox_basic_1->Controls->Add(this->checkBox_mark_available_point);
		this->groupBox_basic_1->Controls->Add(this->label122);
		this->groupBox_basic_1->Controls->Add(this->checkBox_mark_available_icon);
		this->groupBox_basic_1->Controls->Add(this->textBox_instant_teleport_location_z);
		this->groupBox_basic_1->Controls->Add(this->label121);
		this->groupBox_basic_1->Controls->Add(this->textBox_instant_teleport_location_alt);
		this->groupBox_basic_1->Controls->Add(this->label120);
		this->groupBox_basic_1->Controls->Add(this->textBox_instant_teleport_location_x);
		this->groupBox_basic_1->Controls->Add(this->label119);
		this->groupBox_basic_1->Controls->Add(this->textBox_instant_teleport_location_map_id);
		this->groupBox_basic_1->Controls->Add(this->label118);
		this->groupBox_basic_1->Controls->Add(this->textBox_unknown_level);
		this->groupBox_basic_1->Controls->Add(this->label117);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_23);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_17);
		this->groupBox_basic_1->Controls->Add(this->label109);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_22);
		this->groupBox_basic_1->Controls->Add(this->checkBox_has_instant_teleport);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_21);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_18);
		this->groupBox_basic_1->Controls->Add(this->textBox_ai_trigger);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_19);
		this->groupBox_basic_1->Controls->Add(this->label113);
		this->groupBox_basic_1->Controls->Add(this->checkBox_unknown_20);
		this->groupBox_basic_1->Location = System::Drawing::Point(3, 3);
		this->groupBox_basic_1->Name = L"groupBox_basic_1";
		this->groupBox_basic_1->Size = System::Drawing::Size(671, 272);
		this->groupBox_basic_1->TabIndex = 119;
		this->groupBox_basic_1->TabStop = false;
		this->groupBox_basic_1->Text = L"BASIC PROPERTIES I";
		// 
		// textBox_un13304
		// 
		this->textBox_un13304->Location = System::Drawing::Point(582, 187);
		this->textBox_un13304->Name = L"textBox_un13304";
		this->textBox_un13304->Size = System::Drawing::Size(60, 20);
		this->textBox_un13304->TabIndex = 168;
		this->textBox_un13304->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un13304);
		// 
		// textBox_un13303
		// 
		this->textBox_un13303->Location = System::Drawing::Point(582, 157);
		this->textBox_un13303->Name = L"textBox_un13303";
		this->textBox_un13303->Size = System::Drawing::Size(60, 20);
		this->textBox_un13303->TabIndex = 167;
		this->textBox_un13303->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un13303);
		// 
		// label280
		// 
		this->label280->AutoSize = true;
		this->label280->Location = System::Drawing::Point(516, 190);
		this->label280->Name = L"label280";
		this->label280->Size = System::Drawing::Size(60, 13);
		this->label280->TabIndex = 166;
		this->label280->Text = L"Un 1330 4:";
		// 
		// label279
		// 
		this->label279->AutoSize = true;
		this->label279->Location = System::Drawing::Point(516, 161);
		this->label279->Name = L"label279";
		this->label279->Size = System::Drawing::Size(60, 13);
		this->label279->TabIndex = 165;
		this->label279->Text = L"Un 1330 3:";
		// 
		// textBox_bldbmax
		// 
		this->textBox_bldbmax->Location = System::Drawing::Point(435, 244);
		this->textBox_bldbmax->Name = L"textBox_bldbmax";
		this->textBox_bldbmax->Size = System::Drawing::Size(60, 20);
		this->textBox_bldbmax->TabIndex = 164;
		this->textBox_bldbmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_bldbmin);
		// 
		// textBox_bldbmin
		// 
		this->textBox_bldbmin->Location = System::Drawing::Point(435, 223);
		this->textBox_bldbmin->Name = L"textBox_bldbmin";
		this->textBox_bldbmin->Size = System::Drawing::Size(60, 20);
		this->textBox_bldbmin->TabIndex = 163;
		// 
		// label242
		// 
		this->label242->AutoSize = true;
		this->label242->Location = System::Drawing::Point(336, 248);
		this->label242->Name = L"label242";
		this->label242->Size = System::Drawing::Size(93, 13);
		this->label242->TabIndex = 162;
		this->label242->Text = L"Max. Bloodbound:";
		// 
		// checkBox_showlvl
		// 
		this->checkBox_showlvl->AutoSize = true;
		this->checkBox_showlvl->Location = System::Drawing::Point(9, 200);
		this->checkBox_showlvl->Name = L"checkBox_showlvl";
		this->checkBox_showlvl->Size = System::Drawing::Size(70, 17);
		this->checkBox_showlvl->TabIndex = 161;
		this->checkBox_showlvl->Text = L"Has level";
		this->checkBox_showlvl->UseVisualStyleBackColor = true;
		this->checkBox_showlvl->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_showlvl);
		// 
		// label62
		// 
		this->label62->AutoSize = true;
		this->label62->Location = System::Drawing::Point(339, 225);
		this->label62->Name = L"label62";
		this->label62->Size = System::Drawing::Size(90, 13);
		this->label62->TabIndex = 160;
		this->label62->Text = L"Min. Bloodbound:";
		// 
		// textBox_newtype2
		// 
		this->textBox_newtype2->Location = System::Drawing::Point(260, 244);
		this->textBox_newtype2->Name = L"textBox_newtype2";
		this->textBox_newtype2->Size = System::Drawing::Size(60, 20);
		this->textBox_newtype2->TabIndex = 159;
		this->textBox_newtype2->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_newtype2);
		// 
		// textBox_newtype
		// 
		this->textBox_newtype->Location = System::Drawing::Point(260, 223);
		this->textBox_newtype->Name = L"textBox_newtype";
		this->textBox_newtype->Size = System::Drawing::Size(60, 20);
		this->textBox_newtype->TabIndex = 158;
		this->textBox_newtype->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_newtype);
		// 
		// label116
		// 
		this->label116->AutoSize = true;
		this->label116->Location = System::Drawing::Point(193, 248);
		this->label116->Name = L"label116";
		this->label116->Size = System::Drawing::Size(61, 13);
		this->label116->TabIndex = 157;
		this->label116->Text = L"New type2:";
		// 
		// label115
		// 
		this->label115->AutoSize = true;
		this->label115->Location = System::Drawing::Point(193, 225);
		this->label115->Name = L"label115";
		this->label115->Size = System::Drawing::Size(55, 13);
		this->label115->TabIndex = 156;
		this->label115->Text = L"New type:";
		// 
		// checkBox_clantask
		// 
		this->checkBox_clantask->AutoSize = true;
		this->checkBox_clantask->Location = System::Drawing::Point(559, 111);
		this->checkBox_clantask->Name = L"checkBox_clantask";
		this->checkBox_clantask->Size = System::Drawing::Size(70, 17);
		this->checkBox_clantask->TabIndex = 155;
		this->checkBox_clantask->Text = L"Clan task";
		this->checkBox_clantask->UseVisualStyleBackColor = true;
		this->checkBox_clantask->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clantask);
		// 
		// textBox_classify
		// 
		this->textBox_classify->Location = System::Drawing::Point(355, 108);
		this->textBox_classify->Name = L"textBox_classify";
		this->textBox_classify->Size = System::Drawing::Size(51, 20);
		this->textBox_classify->TabIndex = 154;
		this->textBox_classify->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_classify);
		// 
		// label110
		// 
		this->label110->AutoSize = true;
		this->label110->Location = System::Drawing::Point(311, 111);
		this->label110->Name = L"label110";
		this->label110->Size = System::Drawing::Size(45, 13);
		this->label110->TabIndex = 153;
		this->label110->Text = L"Classify:";
		// 
		// label108
		// 
		this->label108->AutoSize = true;
		this->label108->Location = System::Drawing::Point(184, 111);
		this->label108->Name = L"label108";
		this->label108->Size = System::Drawing::Size(64, 13);
		this->label108->TabIndex = 152;
		this->label108->Text = L"Component:";
		// 
		// textBox_component
		// 
		this->textBox_component->Location = System::Drawing::Point(252, 108);
		this->textBox_component->Name = L"textBox_component";
		this->textBox_component->Size = System::Drawing::Size(51, 20);
		this->textBox_component->TabIndex = 151;
		this->textBox_component->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_component);
		// 
		// textBox_cleartask
		// 
		this->textBox_cleartask->Location = System::Drawing::Point(314, 83);
		this->textBox_cleartask->Name = L"textBox_cleartask";
		this->textBox_cleartask->Size = System::Drawing::Size(51, 20);
		this->textBox_cleartask->TabIndex = 150;
		this->textBox_cleartask->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_cleartask);
		// 
		// textBox_autounk
		// 
		this->textBox_autounk->Location = System::Drawing::Point(187, 86);
		this->textBox_autounk->Name = L"textBox_autounk";
		this->textBox_autounk->Size = System::Drawing::Size(51, 20);
		this->textBox_autounk->TabIndex = 148;
		this->textBox_autounk->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_autounk);
		// 
		// checkBox_trigger_on
		// 
		this->checkBox_trigger_on->AutoSize = true;
		this->checkBox_trigger_on->Location = System::Drawing::Point(169, 65);
		this->checkBox_trigger_on->Name = L"checkBox_trigger_on";
		this->checkBox_trigger_on->Size = System::Drawing::Size(98, 17);
		this->checkBox_trigger_on->TabIndex = 147;
		this->checkBox_trigger_on->Text = L"Has trigger on\?";
		this->checkBox_trigger_on->UseVisualStyleBackColor = true;
		this->checkBox_trigger_on->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::checkBox_trigger_on_CheckedChanged);
		// 
		// label35
		// 
		this->label35->AutoSize = true;
		this->label35->Location = System::Drawing::Point(6, 226);
		this->label35->Name = L"label35";
		this->label35->Size = System::Drawing::Size(59, 13);
		this->label35->TabIndex = 82;
		this->label35->Text = L"Min. Level:";
		// 
		// textBox_level_min
		// 
		this->textBox_level_min->Location = System::Drawing::Point(118, 222);
		this->textBox_level_min->Name = L"textBox_level_min";
		this->textBox_level_min->Size = System::Drawing::Size(60, 20);
		this->textBox_level_min->TabIndex = 83;
		this->textBox_level_min->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_level_min);
		// 
		// label36
		// 
		this->label36->AutoSize = true;
		this->label36->Location = System::Drawing::Point(6, 247);
		this->label36->Name = L"label36";
		this->label36->Size = System::Drawing::Size(62, 13);
		this->label36->TabIndex = 86;
		this->label36->Text = L"Max. Level:";
		// 
		// textBox_level_max
		// 
		this->textBox_level_max->Location = System::Drawing::Point(118, 244);
		this->textBox_level_max->Name = L"textBox_level_max";
		this->textBox_level_max->Size = System::Drawing::Size(60, 20);
		this->textBox_level_max->TabIndex = 87;
		this->textBox_level_max->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_level_max);
		// 
		// label33
		// 
		this->label33->AutoSize = true;
		this->label33->Location = System::Drawing::Point(6, 154);
		this->label33->Name = L"label33";
		this->label33->Size = System::Drawing::Size(63, 13);
		this->label33->TabIndex = 133;
		this->label33->Text = L"Quest NPC:";
		// 
		// textBox_quest_npc
		// 
		this->textBox_quest_npc->Location = System::Drawing::Point(118, 154);
		this->textBox_quest_npc->Name = L"textBox_quest_npc";
		this->textBox_quest_npc->Size = System::Drawing::Size(60, 20);
		this->textBox_quest_npc->TabIndex = 134;
		this->textBox_quest_npc->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_quest_npc);
		// 
		// label34
		// 
		this->label34->AutoSize = true;
		this->label34->Location = System::Drawing::Point(184, 157);
		this->label34->Name = L"label34";
		this->label34->Size = System::Drawing::Size(72, 13);
		this->label34->TabIndex = 135;
		this->label34->Text = L"Reward NPC:";
		// 
		// textBox_reward_npc
		// 
		this->textBox_reward_npc->Location = System::Drawing::Point(262, 154);
		this->textBox_reward_npc->Name = L"textBox_reward_npc";
		this->textBox_reward_npc->Size = System::Drawing::Size(60, 20);
		this->textBox_reward_npc->TabIndex = 136;
		this->textBox_reward_npc->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reward_npc);
		// 
		// label123
		// 
		this->label123->AutoSize = true;
		this->label123->Location = System::Drawing::Point(139, 131);
		this->label123->Name = L"label123";
		this->label123->Size = System::Drawing::Size(107, 13);
		this->label123->TabIndex = 132;
		this->label123->Text = L"Mark Available Point:";
		// 
		// checkBox_mark_available_point
		// 
		this->checkBox_mark_available_point->AutoSize = true;
		this->checkBox_mark_available_point->Location = System::Drawing::Point(252, 131);
		this->checkBox_mark_available_point->Name = L"checkBox_mark_available_point";
		this->checkBox_mark_available_point->Size = System::Drawing::Size(15, 14);
		this->checkBox_mark_available_point->TabIndex = 131;
		this->checkBox_mark_available_point->UseVisualStyleBackColor = true;
		this->checkBox_mark_available_point->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_mark_available_point);
		// 
		// label122
		// 
		this->label122->AutoSize = true;
		this->label122->Location = System::Drawing::Point(6, 134);
		this->label122->Name = L"label122";
		this->label122->Size = System::Drawing::Size(104, 13);
		this->label122->TabIndex = 130;
		this->label122->Text = L"Mark Available Icon:";
		// 
		// checkBox_mark_available_icon
		// 
		this->checkBox_mark_available_icon->AutoSize = true;
		this->checkBox_mark_available_icon->Location = System::Drawing::Point(118, 134);
		this->checkBox_mark_available_icon->Name = L"checkBox_mark_available_icon";
		this->checkBox_mark_available_icon->Size = System::Drawing::Size(15, 14);
		this->checkBox_mark_available_icon->TabIndex = 129;
		this->checkBox_mark_available_icon->UseVisualStyleBackColor = true;
		this->checkBox_mark_available_icon->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_mark_available_icon);
		// 
		// textBox_instant_teleport_location_z
		// 
		this->textBox_instant_teleport_location_z->Location = System::Drawing::Point(466, 39);
		this->textBox_instant_teleport_location_z->Name = L"textBox_instant_teleport_location_z";
		this->textBox_instant_teleport_location_z->Size = System::Drawing::Size(70, 20);
		this->textBox_instant_teleport_location_z->TabIndex = 128;
		this->textBox_instant_teleport_location_z->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_instant_teleport_location_z);
		// 
		// label121
		// 
		this->label121->AutoSize = true;
		this->label121->Location = System::Drawing::Point(443, 42);
		this->label121->Name = L"label121";
		this->label121->Size = System::Drawing::Size(17, 13);
		this->label121->TabIndex = 127;
		this->label121->Text = L"Z:";
		// 
		// textBox_instant_teleport_location_alt
		// 
		this->textBox_instant_teleport_location_alt->Location = System::Drawing::Point(367, 39);
		this->textBox_instant_teleport_location_alt->Name = L"textBox_instant_teleport_location_alt";
		this->textBox_instant_teleport_location_alt->Size = System::Drawing::Size(70, 20);
		this->textBox_instant_teleport_location_alt->TabIndex = 126;
		this->textBox_instant_teleport_location_alt->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_instant_teleport_location_alt);
		// 
		// label120
		// 
		this->label120->AutoSize = true;
		this->label120->Location = System::Drawing::Point(339, 42);
		this->label120->Name = L"label120";
		this->label120->Size = System::Drawing::Size(22, 13);
		this->label120->TabIndex = 125;
		this->label120->Text = L"Alt:";
		// 
		// textBox_instant_teleport_location_x
		// 
		this->textBox_instant_teleport_location_x->Location = System::Drawing::Point(263, 39);
		this->textBox_instant_teleport_location_x->Name = L"textBox_instant_teleport_location_x";
		this->textBox_instant_teleport_location_x->Size = System::Drawing::Size(70, 20);
		this->textBox_instant_teleport_location_x->TabIndex = 124;
		this->textBox_instant_teleport_location_x->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_instant_teleport_location_x);
		// 
		// label119
		// 
		this->label119->AutoSize = true;
		this->label119->Location = System::Drawing::Point(240, 42);
		this->label119->Name = L"label119";
		this->label119->Size = System::Drawing::Size(17, 13);
		this->label119->TabIndex = 123;
		this->label119->Text = L"X:";
		// 
		// textBox_instant_teleport_location_map_id
		// 
		this->textBox_instant_teleport_location_map_id->Location = System::Drawing::Point(190, 39);
		this->textBox_instant_teleport_location_map_id->Name = L"textBox_instant_teleport_location_map_id";
		this->textBox_instant_teleport_location_map_id->Size = System::Drawing::Size(44, 20);
		this->textBox_instant_teleport_location_map_id->TabIndex = 122;
		this->textBox_instant_teleport_location_map_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_instant_teleport_location_map_id);
		// 
		// label118
		// 
		this->label118->AutoSize = true;
		this->label118->Location = System::Drawing::Point(139, 42);
		this->label118->Name = L"label118";
		this->label118->Size = System::Drawing::Size(45, 13);
		this->label118->TabIndex = 121;
		this->label118->Text = L"Map ID:";
		// 
		// textBox_unknown_level
		// 
		this->textBox_unknown_level->Location = System::Drawing::Point(118, 108);
		this->textBox_unknown_level->Name = L"textBox_unknown_level";
		this->textBox_unknown_level->Size = System::Drawing::Size(44, 20);
		this->textBox_unknown_level->TabIndex = 120;
		this->textBox_unknown_level->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_level);
		// 
		// label117
		// 
		this->label117->AutoSize = true;
		this->label117->Location = System::Drawing::Point(6, 111);
		this->label117->Name = L"label117";
		this->label117->Size = System::Drawing::Size(85, 13);
		this->label117->TabIndex = 119;
		this->label117->Text = L"Unknown Level:";
		// 
		// checkBox_unknown_23
		// 
		this->checkBox_unknown_23->AutoSize = true;
		this->checkBox_unknown_23->Location = System::Drawing::Point(559, 89);
		this->checkBox_unknown_23->Name = L"checkBox_unknown_23";
		this->checkBox_unknown_23->Size = System::Drawing::Size(67, 17);
		this->checkBox_unknown_23->TabIndex = 117;
		this->checkBox_unknown_23->Text = L"Marriage";
		this->checkBox_unknown_23->UseVisualStyleBackColor = true;
		this->checkBox_unknown_23->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_23);
		// 
		// checkBox_unknown_17
		// 
		this->checkBox_unknown_17->AutoSize = true;
		this->checkBox_unknown_17->Location = System::Drawing::Point(559, 19);
		this->checkBox_unknown_17->Name = L"checkBox_unknown_17";
		this->checkBox_unknown_17->Size = System::Drawing::Size(88, 17);
		this->checkBox_unknown_17->TabIndex = 97;
		this->checkBox_unknown_17->Text = L"Mission Task";
		this->checkBox_unknown_17->UseVisualStyleBackColor = true;
		this->checkBox_unknown_17->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_17);
		// 
		// label109
		// 
		this->label109->AutoSize = true;
		this->label109->Location = System::Drawing::Point(6, 42);
		this->label109->Name = L"label109";
		this->label109->Size = System::Drawing::Size(106, 13);
		this->label109->TabIndex = 104;
		this->label109->Text = L"Has Instant Teleport:";
		// 
		// checkBox_unknown_22
		// 
		this->checkBox_unknown_22->AutoSize = true;
		this->checkBox_unknown_22->Location = System::Drawing::Point(559, 65);
		this->checkBox_unknown_22->Name = L"checkBox_unknown_22";
		this->checkBox_unknown_22->Size = System::Drawing::Size(70, 17);
		this->checkBox_unknown_22->TabIndex = 115;
		this->checkBox_unknown_22->Text = L"Map task";
		this->checkBox_unknown_22->UseVisualStyleBackColor = true;
		this->checkBox_unknown_22->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_22);
		// 
		// checkBox_has_instant_teleport
		// 
		this->checkBox_has_instant_teleport->AutoSize = true;
		this->checkBox_has_instant_teleport->Location = System::Drawing::Point(118, 42);
		this->checkBox_has_instant_teleport->Name = L"checkBox_has_instant_teleport";
		this->checkBox_has_instant_teleport->Size = System::Drawing::Size(15, 14);
		this->checkBox_has_instant_teleport->TabIndex = 103;
		this->checkBox_has_instant_teleport->UseVisualStyleBackColor = true;
		this->checkBox_has_instant_teleport->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_has_instant_teleport);
		// 
		// checkBox_unknown_21
		// 
		this->checkBox_unknown_21->AutoSize = true;
		this->checkBox_unknown_21->Location = System::Drawing::Point(559, 42);
		this->checkBox_unknown_21->Name = L"checkBox_unknown_21";
		this->checkBox_unknown_21->Size = System::Drawing::Size(79, 17);
		this->checkBox_unknown_21->TabIndex = 113;
		this->checkBox_unknown_21->Text = L"Travel task";
		this->checkBox_unknown_21->UseVisualStyleBackColor = true;
		this->checkBox_unknown_21->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_21);
		// 
		// checkBox_unknown_18
		// 
		this->checkBox_unknown_18->AutoSize = true;
		this->checkBox_unknown_18->Location = System::Drawing::Point(9, 88);
		this->checkBox_unknown_18->Name = L"checkBox_unknown_18";
		this->checkBox_unknown_18->Size = System::Drawing::Size(87, 17);
		this->checkBox_unknown_18->TabIndex = 105;
		this->checkBox_unknown_18->Text = L"Unknown 18";
		this->checkBox_unknown_18->UseVisualStyleBackColor = true;
		this->checkBox_unknown_18->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_18);
		// 
		// textBox_ai_trigger
		// 
		this->textBox_ai_trigger->Location = System::Drawing::Point(118, 62);
		this->textBox_ai_trigger->Name = L"textBox_ai_trigger";
		this->textBox_ai_trigger->Size = System::Drawing::Size(44, 20);
		this->textBox_ai_trigger->TabIndex = 112;
		this->textBox_ai_trigger->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ai_trigger);
		// 
		// checkBox_unknown_19
		// 
		this->checkBox_unknown_19->AutoSize = true;
		this->checkBox_unknown_19->Location = System::Drawing::Point(118, 88);
		this->checkBox_unknown_19->Name = L"checkBox_unknown_19";
		this->checkBox_unknown_19->Size = System::Drawing::Size(75, 17);
		this->checkBox_unknown_19->TabIndex = 107;
		this->checkBox_unknown_19->Text = L"Auto Task";
		this->checkBox_unknown_19->UseVisualStyleBackColor = true;
		this->checkBox_unknown_19->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_19);
		// 
		// label113
		// 
		this->label113->AutoSize = true;
		this->label113->Location = System::Drawing::Point(6, 65);
		this->label113->Name = L"label113";
		this->label113->Size = System::Drawing::Size(99, 13);
		this->label113->TabIndex = 111;
		this->label113->Text = L"Creature Builder ID:";
		// 
		// checkBox_unknown_20
		// 
		this->checkBox_unknown_20->AutoSize = true;
		this->checkBox_unknown_20->Location = System::Drawing::Point(244, 86);
		this->checkBox_unknown_20->Name = L"checkBox_unknown_20";
		this->checkBox_unknown_20->Size = System::Drawing::Size(77, 17);
		this->checkBox_unknown_20->TabIndex = 109;
		this->checkBox_unknown_20->Text = L"Clear Task";
		this->checkBox_unknown_20->UseVisualStyleBackColor = true;
		this->checkBox_unknown_20->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_20);
		// 
		// contextMenuStrip_valid_location
		// 
		this->contextMenuStrip_valid_location->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem5, 
			this->toolStripMenuItem6});
		this->contextMenuStrip_valid_location->Name = L"contextMenuStrip1";
		this->contextMenuStrip_valid_location->Size = System::Drawing::Size(184, 48);
		// 
		// toolStripMenuItem5
		// 
		this->toolStripMenuItem5->Name = L"toolStripMenuItem5";
		this->toolStripMenuItem5->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem5->Text = L"Add Location Span";
		// 
		// toolStripMenuItem6
		// 
		this->toolStripMenuItem6->Name = L"toolStripMenuItem6";
		this->toolStripMenuItem6->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem6->Text = L"Remove Location Span";
		// 
		// contextMenuStrip_fail_location
		// 
		this->contextMenuStrip_fail_location->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem3, 
			this->toolStripMenuItem4});
		this->contextMenuStrip_fail_location->Name = L"contextMenuStrip1";
		this->contextMenuStrip_fail_location->Size = System::Drawing::Size(184, 48);
		// 
		// toolStripMenuItem3
		// 
		this->toolStripMenuItem3->Name = L"toolStripMenuItem3";
		this->toolStripMenuItem3->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem3->Text = L"Add Location Span";
		this->toolStripMenuItem3->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_fail_locations_span);
		// 
		// toolStripMenuItem4
		// 
		this->toolStripMenuItem4->Name = L"toolStripMenuItem4";
		this->toolStripMenuItem4->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem4->Text = L"Remove Location Span";
		this->toolStripMenuItem4->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_fail_locations_span);
		// 
		// groupBox_trigger_location
		// 
		this->groupBox_trigger_location->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_trigger_location->Controls->Add(this->dataGridView_trigger_location_spans);
		this->groupBox_trigger_location->Controls->Add(this->checkBox_trigger_locations_has_spans);
		this->groupBox_trigger_location->Controls->Add(this->textBox_trigger_locations_map_id);
		this->groupBox_trigger_location->Controls->Add(this->label106);
		this->groupBox_trigger_location->Controls->Add(this->label107);
		this->groupBox_trigger_location->Location = System::Drawing::Point(3, 3);
		this->groupBox_trigger_location->Name = L"groupBox_trigger_location";
		this->groupBox_trigger_location->Size = System::Drawing::Size(671, 91);
		this->groupBox_trigger_location->TabIndex = 95;
		this->groupBox_trigger_location->TabStop = false;
		this->groupBox_trigger_location->Text = L"TRIGGER ON ENTER";
		// 
		// dataGridView_trigger_location_spans
		// 
		this->dataGridView_trigger_location_spans->AllowUserToAddRows = false;
		this->dataGridView_trigger_location_spans->AllowUserToDeleteRows = false;
		this->dataGridView_trigger_location_spans->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->dataGridView_trigger_location_spans->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_trigger_location_spans->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_trigger_location_spans->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_trigger_location_spans->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {this->dataGridViewTextBoxColumn37, 
			this->dataGridViewTextBoxColumn38, this->dataGridViewTextBoxColumn39, this->dataGridViewTextBoxColumn40, this->dataGridViewTextBoxColumn41, 
			this->dataGridViewTextBoxColumn42});
		this->dataGridView_trigger_location_spans->Location = System::Drawing::Point(205, 10);
		this->dataGridView_trigger_location_spans->MultiSelect = false;
		this->dataGridView_trigger_location_spans->Name = L"dataGridView_trigger_location_spans";
		this->dataGridView_trigger_location_spans->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle27->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle27->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle27->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle27->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle27->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle27->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle27->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_trigger_location_spans->RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
		this->dataGridView_trigger_location_spans->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_trigger_location_spans->RowTemplate->Height = 18;
		this->dataGridView_trigger_location_spans->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_trigger_location_spans->Size = System::Drawing::Size(460, 75);
		this->dataGridView_trigger_location_spans->TabIndex = 91;
		this->dataGridView_trigger_location_spans->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_trigger_locations_span);
		// 
		// dataGridViewTextBoxColumn37
		// 
		this->dataGridViewTextBoxColumn37->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle21->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn37->DefaultCellStyle = dataGridViewCellStyle21;
		this->dataGridViewTextBoxColumn37->HeaderText = L"North";
		this->dataGridViewTextBoxColumn37->Name = L"dataGridViewTextBoxColumn37";
		this->dataGridViewTextBoxColumn37->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn38
		// 
		this->dataGridViewTextBoxColumn38->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle22->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn38->DefaultCellStyle = dataGridViewCellStyle22;
		this->dataGridViewTextBoxColumn38->HeaderText = L"South";
		this->dataGridViewTextBoxColumn38->Name = L"dataGridViewTextBoxColumn38";
		this->dataGridViewTextBoxColumn38->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn39
		// 
		this->dataGridViewTextBoxColumn39->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle23->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn39->DefaultCellStyle = dataGridViewCellStyle23;
		this->dataGridViewTextBoxColumn39->HeaderText = L"Weast";
		this->dataGridViewTextBoxColumn39->Name = L"dataGridViewTextBoxColumn39";
		this->dataGridViewTextBoxColumn39->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn40
		// 
		this->dataGridViewTextBoxColumn40->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle24->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn40->DefaultCellStyle = dataGridViewCellStyle24;
		this->dataGridViewTextBoxColumn40->HeaderText = L"East";
		this->dataGridViewTextBoxColumn40->Name = L"dataGridViewTextBoxColumn40";
		this->dataGridViewTextBoxColumn40->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn41
		// 
		this->dataGridViewTextBoxColumn41->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle25->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn41->DefaultCellStyle = dataGridViewCellStyle25;
		this->dataGridViewTextBoxColumn41->HeaderText = L"Low";
		this->dataGridViewTextBoxColumn41->Name = L"dataGridViewTextBoxColumn41";
		this->dataGridViewTextBoxColumn41->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn42
		// 
		this->dataGridViewTextBoxColumn42->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle26->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn42->DefaultCellStyle = dataGridViewCellStyle26;
		this->dataGridViewTextBoxColumn42->HeaderText = L"High";
		this->dataGridViewTextBoxColumn42->Name = L"dataGridViewTextBoxColumn42";
		this->dataGridViewTextBoxColumn42->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// checkBox_trigger_locations_has_spans
		// 
		this->checkBox_trigger_locations_has_spans->AutoSize = true;
		this->checkBox_trigger_locations_has_spans->Location = System::Drawing::Point(77, 19);
		this->checkBox_trigger_locations_has_spans->Name = L"checkBox_trigger_locations_has_spans";
		this->checkBox_trigger_locations_has_spans->Size = System::Drawing::Size(15, 14);
		this->checkBox_trigger_locations_has_spans->TabIndex = 90;
		this->checkBox_trigger_locations_has_spans->UseVisualStyleBackColor = true;
		this->checkBox_trigger_locations_has_spans->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_trigger_locations_has_spans);
		// 
		// textBox_trigger_locations_map_id
		// 
		this->textBox_trigger_locations_map_id->Location = System::Drawing::Point(77, 39);
		this->textBox_trigger_locations_map_id->Name = L"textBox_trigger_locations_map_id";
		this->textBox_trigger_locations_map_id->Size = System::Drawing::Size(51, 20);
		this->textBox_trigger_locations_map_id->TabIndex = 92;
		this->textBox_trigger_locations_map_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_trigger_locations_map_id);
		// 
		// contextMenuStrip_trigger_location
		// 
		this->contextMenuStrip_trigger_location->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->addToolStripMenuItem1, 
			this->removeToolStripMenuItem1});
		this->contextMenuStrip_trigger_location->Name = L"contextMenuStrip1";
		this->contextMenuStrip_trigger_location->Size = System::Drawing::Size(184, 48);
		// 
		// addToolStripMenuItem1
		// 
		this->addToolStripMenuItem1->Name = L"addToolStripMenuItem1";
		this->addToolStripMenuItem1->Size = System::Drawing::Size(183, 22);
		this->addToolStripMenuItem1->Text = L"Add Location Span";
		this->addToolStripMenuItem1->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_trigger_locations_span);
		// 
		// removeToolStripMenuItem1
		// 
		this->removeToolStripMenuItem1->Name = L"removeToolStripMenuItem1";
		this->removeToolStripMenuItem1->Size = System::Drawing::Size(183, 22);
		this->removeToolStripMenuItem1->Text = L"Remove Location Span";
		this->removeToolStripMenuItem1->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_trigger_locations_span);
		// 
		// groupBox_flags
		// 
		this->groupBox_flags->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_flags->Controls->Add(this->textBox_plimitnum);
		this->groupBox_flags->Controls->Add(this->checkBox_haslimit);
		this->groupBox_flags->Controls->Add(this->label114);
		this->groupBox_flags->Controls->Add(this->textBox_allkillnum);
		this->groupBox_flags->Controls->Add(this->label112);
		this->groupBox_flags->Controls->Add(this->label111);
		this->groupBox_flags->Controls->Add(this->textBox_playerpenalty);
		this->groupBox_flags->Controls->Add(this->textBox_unkpenalty);
		this->groupBox_flags->Controls->Add(this->textBox_allzonenum);
		this->groupBox_flags->Controls->Add(this->checkBox_allzone);
		this->groupBox_flags->Controls->Add(this->checkBox_plimit);
		this->groupBox_flags->Controls->Add(this->textBox_player_limit);
		this->groupBox_flags->Controls->Add(this->label102);
		this->groupBox_flags->Controls->Add(this->dataGridView_globals);
		this->groupBox_flags->Controls->Add(this->label101);
		this->groupBox_flags->Controls->Add(this->checkBox_unknown_10);
		this->groupBox_flags->Controls->Add(this->label100);
		this->groupBox_flags->Controls->Add(this->checkBox_on_fail_parent_fail);
		this->groupBox_flags->Controls->Add(this->label99);
		this->groupBox_flags->Controls->Add(this->checkBox_fail_on_death);
		this->groupBox_flags->Controls->Add(this->label98);
		this->groupBox_flags->Controls->Add(this->checkBox_repeatable_after_failure);
		this->groupBox_flags->Controls->Add(this->label97);
		this->groupBox_flags->Controls->Add(this->checkBox_repeatable);
		this->groupBox_flags->Controls->Add(this->label96);
		this->groupBox_flags->Controls->Add(this->checkBox_can_give_up);
		this->groupBox_flags->Controls->Add(this->label95);
		this->groupBox_flags->Controls->Add(this->checkBox_on_success_parent_success);
		this->groupBox_flags->Controls->Add(this->label94);
		this->groupBox_flags->Controls->Add(this->checkBox_on_give_up_parent_fails);
		this->groupBox_flags->Controls->Add(this->label93);
		this->groupBox_flags->Controls->Add(this->checkBox_activate_next_subquest);
		this->groupBox_flags->Controls->Add(this->label92);
		this->groupBox_flags->Controls->Add(this->checkBox_activate_random_subquest);
		this->groupBox_flags->Controls->Add(this->label91);
		this->groupBox_flags->Controls->Add(this->checkBox_activate_first_subquest);
		this->groupBox_flags->Location = System::Drawing::Point(3, 347);
		this->groupBox_flags->Name = L"groupBox_flags";
		this->groupBox_flags->Size = System::Drawing::Size(671, 225);
		this->groupBox_flags->TabIndex = 4;
		this->groupBox_flags->TabStop = false;
		this->groupBox_flags->Text = L"FLAGS";
		// 
		// textBox_plimitnum
		// 
		this->textBox_plimitnum->Location = System::Drawing::Point(498, 145);
		this->textBox_plimitnum->Name = L"textBox_plimitnum";
		this->textBox_plimitnum->Size = System::Drawing::Size(71, 20);
		this->textBox_plimitnum->TabIndex = 108;
		this->textBox_plimitnum->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_plimitnum);
		// 
		// checkBox_haslimit
		// 
		this->checkBox_haslimit->AutoSize = true;
		this->checkBox_haslimit->Location = System::Drawing::Point(579, 15);
		this->checkBox_haslimit->Name = L"checkBox_haslimit";
		this->checkBox_haslimit->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_haslimit->Size = System::Drawing::Size(65, 17);
		this->checkBox_haslimit->TabIndex = 107;
		this->checkBox_haslimit->Text = L"Has limit";
		this->checkBox_haslimit->UseVisualStyleBackColor = true;
		this->checkBox_haslimit->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_haslimit);
		// 
		// label114
		// 
		this->label114->AutoSize = true;
		this->label114->Location = System::Drawing::Point(398, 125);
		this->label114->Name = L"label114";
		this->label114->Size = System::Drawing::Size(74, 13);
		this->label114->TabIndex = 106;
		this->label114->Text = L"All kill number:";
		// 
		// textBox_allkillnum
		// 
		this->textBox_allkillnum->Location = System::Drawing::Point(498, 122);
		this->textBox_allkillnum->Name = L"textBox_allkillnum";
		this->textBox_allkillnum->Size = System::Drawing::Size(71, 20);
		this->textBox_allkillnum->TabIndex = 105;
		this->textBox_allkillnum->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_allkillnum);
		// 
		// label112
		// 
		this->label112->AutoSize = true;
		this->label112->Location = System::Drawing::Point(399, 102);
		this->label112->Name = L"label112";
		this->label112->Size = System::Drawing::Size(77, 13);
		this->label112->TabIndex = 104;
		this->label112->Text = L"Player Penalty:";
		// 
		// label111
		// 
		this->label111->AutoSize = true;
		this->label111->Location = System::Drawing::Point(399, 80);
		this->label111->Name = L"label111";
		this->label111->Size = System::Drawing::Size(68, 13);
		this->label111->TabIndex = 103;
		this->label111->Text = L"Unk Penalty:";
		// 
		// textBox_playerpenalty
		// 
		this->textBox_playerpenalty->Location = System::Drawing::Point(498, 99);
		this->textBox_playerpenalty->Name = L"textBox_playerpenalty";
		this->textBox_playerpenalty->Size = System::Drawing::Size(71, 20);
		this->textBox_playerpenalty->TabIndex = 102;
		this->textBox_playerpenalty->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_playerpenalty);
		// 
		// textBox_unkpenalty
		// 
		this->textBox_unkpenalty->Location = System::Drawing::Point(498, 77);
		this->textBox_unkpenalty->Name = L"textBox_unkpenalty";
		this->textBox_unkpenalty->Size = System::Drawing::Size(71, 20);
		this->textBox_unkpenalty->TabIndex = 101;
		this->textBox_unkpenalty->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unkpenalty);
		// 
		// textBox_allzonenum
		// 
		this->textBox_allzonenum->Location = System::Drawing::Point(498, 56);
		this->textBox_allzonenum->Name = L"textBox_allzonenum";
		this->textBox_allzonenum->Size = System::Drawing::Size(71, 20);
		this->textBox_allzonenum->TabIndex = 100;
		this->textBox_allzonenum->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_allzonenum);
		// 
		// checkBox_allzone
		// 
		this->checkBox_allzone->AutoSize = true;
		this->checkBox_allzone->Location = System::Drawing::Point(401, 59);
		this->checkBox_allzone->Name = L"checkBox_allzone";
		this->checkBox_allzone->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_allzone->Size = System::Drawing::Size(68, 17);
		this->checkBox_allzone->TabIndex = 99;
		this->checkBox_allzone->Text = L"All zones";
		this->checkBox_allzone->UseVisualStyleBackColor = true;
		this->checkBox_allzone->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_allzone);
		// 
		// checkBox_plimit
		// 
		this->checkBox_plimit->AutoSize = true;
		this->checkBox_plimit->Location = System::Drawing::Point(399, 148);
		this->checkBox_plimit->Name = L"checkBox_plimit";
		this->checkBox_plimit->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_plimit->Size = System::Drawing::Size(92, 17);
		this->checkBox_plimit->TabIndex = 98;
		this->checkBox_plimit->Text = L"Times per day";
		this->checkBox_plimit->UseVisualStyleBackColor = true;
		this->checkBox_plimit->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_plimit);
		// 
		// textBox_player_limit
		// 
		this->textBox_player_limit->Location = System::Drawing::Point(498, 35);
		this->textBox_player_limit->Name = L"textBox_player_limit";
		this->textBox_player_limit->Size = System::Drawing::Size(71, 20);
		this->textBox_player_limit->TabIndex = 97;
		this->textBox_player_limit->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_player_limit);
		// 
		// label102
		// 
		this->label102->AutoSize = true;
		this->label102->Location = System::Drawing::Point(398, 39);
		this->label102->Name = L"label102";
		this->label102->Size = System::Drawing::Size(69, 13);
		this->label102->TabIndex = 85;
		this->label102->Text = L"Player Limit\?:";
		// 
		// dataGridView_globals
		// 
		this->dataGridView_globals->AllowUserToAddRows = false;
		this->dataGridView_globals->AllowUserToDeleteRows = false;
		this->dataGridView_globals->BackgroundColor = System::Drawing::SystemColors::Control;
		this->dataGridView_globals->BorderStyle = System::Windows::Forms::BorderStyle::None;
		this->dataGridView_globals->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_globals->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_globals->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {this->Column33, 
			this->Column34, this->Column35, this->Column36});
		this->dataGridView_globals->Location = System::Drawing::Point(10, 122);
		this->dataGridView_globals->MultiSelect = false;
		this->dataGridView_globals->Name = L"dataGridView_globals";
		this->dataGridView_globals->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle28->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle28->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle28->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle28->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle28->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle28->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle28->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_globals->RowHeadersDefaultCellStyle = dataGridViewCellStyle28;
		this->dataGridView_globals->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_globals->RowTemplate->Height = 18;
		this->dataGridView_globals->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_globals->Size = System::Drawing::Size(304, 97);
		this->dataGridView_globals->TabIndex = 66;
		// 
		// Column33
		// 
		this->Column33->HeaderText = L"Val_1";
		this->Column33->Name = L"Column33";
		this->Column33->Width = 50;
		// 
		// Column34
		// 
		this->Column34->HeaderText = L"Val_2";
		this->Column34->Name = L"Column34";
		this->Column34->Width = 50;
		// 
		// Column35
		// 
		this->Column35->HeaderText = L"Val_3";
		this->Column35->Name = L"Column35";
		this->Column35->Width = 50;
		// 
		// Column36
		// 
		this->Column36->HeaderText = L"Count";
		this->Column36->Name = L"Column36";
		this->Column36->Width = 50;
		// 
		// label101
		// 
		this->label101->AutoSize = true;
		this->label101->Location = System::Drawing::Point(396, 19);
		this->label101->Name = L"label101";
		this->label101->Size = System::Drawing::Size(71, 13);
		this->label101->TabIndex = 83;
		this->label101->Text = L"Unknown 10:";
		// 
		// checkBox_unknown_10
		// 
		this->checkBox_unknown_10->AutoSize = true;
		this->checkBox_unknown_10->Location = System::Drawing::Point(475, 19);
		this->checkBox_unknown_10->Name = L"checkBox_unknown_10";
		this->checkBox_unknown_10->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_unknown_10->Size = System::Drawing::Size(15, 14);
		this->checkBox_unknown_10->TabIndex = 82;
		this->checkBox_unknown_10->UseVisualStyleBackColor = true;
		this->checkBox_unknown_10->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_10);
		// 
		// label100
		// 
		this->label100->AutoSize = true;
		this->label100->Location = System::Drawing::Point(209, 97);
		this->label100->Name = L"label100";
		this->label100->Size = System::Drawing::Size(96, 13);
		this->label100->TabIndex = 81;
		this->label100->Text = L"On Fail Parent Fail:";
		// 
		// checkBox_on_fail_parent_fail
		// 
		this->checkBox_on_fail_parent_fail->AutoSize = true;
		this->checkBox_on_fail_parent_fail->Location = System::Drawing::Point(339, 97);
		this->checkBox_on_fail_parent_fail->Name = L"checkBox_on_fail_parent_fail";
		this->checkBox_on_fail_parent_fail->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_on_fail_parent_fail->Size = System::Drawing::Size(15, 14);
		this->checkBox_on_fail_parent_fail->TabIndex = 80;
		this->checkBox_on_fail_parent_fail->UseVisualStyleBackColor = true;
		this->checkBox_on_fail_parent_fail->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_on_fail_parent_fail);
		// 
		// label99
		// 
		this->label99->AutoSize = true;
		this->label99->Location = System::Drawing::Point(209, 77);
		this->label99->Name = L"label99";
		this->label99->Size = System::Drawing::Size(75, 13);
		this->label99->TabIndex = 79;
		this->label99->Text = L"Fail On Death:";
		// 
		// checkBox_fail_on_death
		// 
		this->checkBox_fail_on_death->AutoSize = true;
		this->checkBox_fail_on_death->Location = System::Drawing::Point(339, 77);
		this->checkBox_fail_on_death->Name = L"checkBox_fail_on_death";
		this->checkBox_fail_on_death->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_fail_on_death->Size = System::Drawing::Size(15, 14);
		this->checkBox_fail_on_death->TabIndex = 78;
		this->checkBox_fail_on_death->UseVisualStyleBackColor = true;
		this->checkBox_fail_on_death->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_fail_on_death);
		// 
		// label98
		// 
		this->label98->AutoSize = true;
		this->label98->Location = System::Drawing::Point(209, 59);
		this->label98->Name = L"label98";
		this->label98->Size = System::Drawing::Size(124, 13);
		this->label98->TabIndex = 77;
		this->label98->Text = L"Repeatable After Failure:";
		// 
		// checkBox_repeatable_after_failure
		// 
		this->checkBox_repeatable_after_failure->AutoSize = true;
		this->checkBox_repeatable_after_failure->Location = System::Drawing::Point(339, 59);
		this->checkBox_repeatable_after_failure->Name = L"checkBox_repeatable_after_failure";
		this->checkBox_repeatable_after_failure->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_repeatable_after_failure->Size = System::Drawing::Size(15, 14);
		this->checkBox_repeatable_after_failure->TabIndex = 76;
		this->checkBox_repeatable_after_failure->UseVisualStyleBackColor = true;
		this->checkBox_repeatable_after_failure->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_repeatable_after_failure);
		// 
		// label97
		// 
		this->label97->AutoSize = true;
		this->label97->Location = System::Drawing::Point(209, 39);
		this->label97->Name = L"label97";
		this->label97->Size = System::Drawing::Size(65, 13);
		this->label97->TabIndex = 75;
		this->label97->Text = L"Repeatable:";
		// 
		// checkBox_repeatable
		// 
		this->checkBox_repeatable->AutoSize = true;
		this->checkBox_repeatable->Location = System::Drawing::Point(339, 39);
		this->checkBox_repeatable->Name = L"checkBox_repeatable";
		this->checkBox_repeatable->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_repeatable->Size = System::Drawing::Size(15, 14);
		this->checkBox_repeatable->TabIndex = 74;
		this->checkBox_repeatable->UseVisualStyleBackColor = true;
		this->checkBox_repeatable->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_repeatable);
		// 
		// label96
		// 
		this->label96->AutoSize = true;
		this->label96->Location = System::Drawing::Point(209, 20);
		this->label96->Name = L"label96";
		this->label96->Size = System::Drawing::Size(71, 13);
		this->label96->TabIndex = 73;
		this->label96->Text = L"Can Give Up:";
		// 
		// checkBox_can_give_up
		// 
		this->checkBox_can_give_up->AutoSize = true;
		this->checkBox_can_give_up->Location = System::Drawing::Point(339, 19);
		this->checkBox_can_give_up->Name = L"checkBox_can_give_up";
		this->checkBox_can_give_up->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_can_give_up->Size = System::Drawing::Size(15, 14);
		this->checkBox_can_give_up->TabIndex = 72;
		this->checkBox_can_give_up->UseVisualStyleBackColor = true;
		this->checkBox_can_give_up->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_can_give_up);
		// 
		// label95
		// 
		this->label95->AutoSize = true;
		this->label95->Location = System::Drawing::Point(6, 99);
		this->label95->Name = L"label95";
		this->label95->Size = System::Drawing::Size(146, 13);
		this->label95->TabIndex = 71;
		this->label95->Text = L"On Success Parent Success:";
		// 
		// checkBox_on_success_parent_success
		// 
		this->checkBox_on_success_parent_success->AutoSize = true;
		this->checkBox_on_success_parent_success->Location = System::Drawing::Point(159, 99);
		this->checkBox_on_success_parent_success->Name = L"checkBox_on_success_parent_success";
		this->checkBox_on_success_parent_success->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_on_success_parent_success->Size = System::Drawing::Size(15, 14);
		this->checkBox_on_success_parent_success->TabIndex = 70;
		this->checkBox_on_success_parent_success->UseVisualStyleBackColor = true;
		this->checkBox_on_success_parent_success->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_on_success_parent_success);
		// 
		// label94
		// 
		this->label94->AutoSize = true;
		this->label94->Location = System::Drawing::Point(6, 79);
		this->label94->Name = L"label94";
		this->label94->Size = System::Drawing::Size(124, 13);
		this->label94->TabIndex = 69;
		this->label94->Text = L"On Give Up Parent Fails:";
		// 
		// checkBox_on_give_up_parent_fails
		// 
		this->checkBox_on_give_up_parent_fails->AutoSize = true;
		this->checkBox_on_give_up_parent_fails->Location = System::Drawing::Point(159, 79);
		this->checkBox_on_give_up_parent_fails->Name = L"checkBox_on_give_up_parent_fails";
		this->checkBox_on_give_up_parent_fails->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_on_give_up_parent_fails->Size = System::Drawing::Size(15, 14);
		this->checkBox_on_give_up_parent_fails->TabIndex = 68;
		this->checkBox_on_give_up_parent_fails->UseVisualStyleBackColor = true;
		this->checkBox_on_give_up_parent_fails->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_on_give_up_parent_fails);
		// 
		// label93
		// 
		this->label93->AutoSize = true;
		this->label93->Location = System::Drawing::Point(6, 59);
		this->label93->Name = L"label93";
		this->label93->Size = System::Drawing::Size(122, 13);
		this->label93->TabIndex = 67;
		this->label93->Text = L"Activate Next Subquest:";
		// 
		// checkBox_activate_next_subquest
		// 
		this->checkBox_activate_next_subquest->AutoSize = true;
		this->checkBox_activate_next_subquest->Location = System::Drawing::Point(159, 59);
		this->checkBox_activate_next_subquest->Name = L"checkBox_activate_next_subquest";
		this->checkBox_activate_next_subquest->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_activate_next_subquest->Size = System::Drawing::Size(15, 14);
		this->checkBox_activate_next_subquest->TabIndex = 66;
		this->checkBox_activate_next_subquest->UseVisualStyleBackColor = true;
		this->checkBox_activate_next_subquest->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_activate_next_subquest);
		// 
		// label92
		// 
		this->label92->AutoSize = true;
		this->label92->Location = System::Drawing::Point(6, 39);
		this->label92->Name = L"label92";
		this->label92->Size = System::Drawing::Size(140, 13);
		this->label92->TabIndex = 65;
		this->label92->Text = L"Activate Random Subquest:";
		// 
		// checkBox_activate_random_subquest
		// 
		this->checkBox_activate_random_subquest->AutoSize = true;
		this->checkBox_activate_random_subquest->Location = System::Drawing::Point(159, 39);
		this->checkBox_activate_random_subquest->Name = L"checkBox_activate_random_subquest";
		this->checkBox_activate_random_subquest->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_activate_random_subquest->Size = System::Drawing::Size(15, 14);
		this->checkBox_activate_random_subquest->TabIndex = 64;
		this->checkBox_activate_random_subquest->UseVisualStyleBackColor = true;
		this->checkBox_activate_random_subquest->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_activate_random_subquest);
		// 
		// label91
		// 
		this->label91->AutoSize = true;
		this->label91->Location = System::Drawing::Point(6, 19);
		this->label91->Name = L"label91";
		this->label91->Size = System::Drawing::Size(119, 13);
		this->label91->TabIndex = 63;
		this->label91->Text = L"Activate First Subquest:";
		// 
		// checkBox_activate_first_subquest
		// 
		this->checkBox_activate_first_subquest->AutoSize = true;
		this->checkBox_activate_first_subquest->Location = System::Drawing::Point(159, 19);
		this->checkBox_activate_first_subquest->Name = L"checkBox_activate_first_subquest";
		this->checkBox_activate_first_subquest->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_activate_first_subquest->Size = System::Drawing::Size(15, 14);
		this->checkBox_activate_first_subquest->TabIndex = 62;
		this->checkBox_activate_first_subquest->UseVisualStyleBackColor = true;
		this->checkBox_activate_first_subquest->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_activate_first_subquest);
		// 
		// contextMenuStrip_morai_pk
		// 
		this->contextMenuStrip_morai_pk->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem48, 
			this->toolStripMenuItem49});
		this->contextMenuStrip_morai_pk->Name = L"contextMenuStrip1";
		this->contextMenuStrip_morai_pk->Size = System::Drawing::Size(158, 48);
		// 
		// toolStripMenuItem48
		// 
		this->toolStripMenuItem48->Name = L"toolStripMenuItem48";
		this->toolStripMenuItem48->Size = System::Drawing::Size(157, 22);
		this->toolStripMenuItem48->Text = L"Add Morai PK";
		// 
		// toolStripMenuItem49
		// 
		this->toolStripMenuItem49->Name = L"toolStripMenuItem49";
		this->toolStripMenuItem49->Size = System::Drawing::Size(157, 22);
		this->toolStripMenuItem49->Text = L"Remove Morai PK";
		// 
		// contextMenuStrip_team_members
		// 
		this->contextMenuStrip_team_members->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem13, 
			this->toolStripMenuItem14});
		this->contextMenuStrip_team_members->Name = L"contextMenuStrip1";
		this->contextMenuStrip_team_members->Size = System::Drawing::Size(155, 48);
		// 
		// toolStripMenuItem13
		// 
		this->toolStripMenuItem13->Name = L"toolStripMenuItem13";
		this->toolStripMenuItem13->Size = System::Drawing::Size(154, 22);
		this->toolStripMenuItem13->Text = L"Add Member";
		this->toolStripMenuItem13->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_required_team_member_group);
		// 
		// toolStripMenuItem14
		// 
		this->toolStripMenuItem14->Name = L"toolStripMenuItem14";
		this->toolStripMenuItem14->Size = System::Drawing::Size(154, 22);
		this->toolStripMenuItem14->Text = L"Remove Member";
		this->toolStripMenuItem14->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_required_team_member_group);
		// 
		// contextMenuStrip_given_items
		// 
		this->contextMenuStrip_given_items->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem9, 
			this->toolStripMenuItem10});
		this->contextMenuStrip_given_items->Name = L"contextMenuStrip1";
		this->contextMenuStrip_given_items->Size = System::Drawing::Size(139, 48);
		// 
		// toolStripMenuItem9
		// 
		this->toolStripMenuItem9->Name = L"toolStripMenuItem9";
		this->toolStripMenuItem9->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem9->Text = L"Add Item";
		this->toolStripMenuItem9->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_given_item);
		// 
		// toolStripMenuItem10
		// 
		this->toolStripMenuItem10->Name = L"toolStripMenuItem10";
		this->toolStripMenuItem10->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem10->Text = L"Remove Item";
		this->toolStripMenuItem10->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_given_item);
		// 
		// contextMenuStrip_required_items
		// 
		this->contextMenuStrip_required_items->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem7, 
			this->toolStripMenuItem8});
		this->contextMenuStrip_required_items->Name = L"contextMenuStrip1";
		this->contextMenuStrip_required_items->Size = System::Drawing::Size(139, 48);
		// 
		// toolStripMenuItem7
		// 
		this->toolStripMenuItem7->Name = L"toolStripMenuItem7";
		this->toolStripMenuItem7->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem7->Text = L"Add Item";
		this->toolStripMenuItem7->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_required_item);
		// 
		// toolStripMenuItem8
		// 
		this->toolStripMenuItem8->Name = L"toolStripMenuItem8";
		this->toolStripMenuItem8->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem8->Text = L"Remove Item";
		this->toolStripMenuItem8->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_required_item);
		// 
		// contextMenuStrip_required_get_items
		// 
		this->contextMenuStrip_required_get_items->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem11, 
			this->toolStripMenuItem12});
		this->contextMenuStrip_required_get_items->Name = L"contextMenuStrip1";
		this->contextMenuStrip_required_get_items->Size = System::Drawing::Size(139, 48);
		// 
		// toolStripMenuItem11
		// 
		this->toolStripMenuItem11->Name = L"toolStripMenuItem11";
		this->toolStripMenuItem11->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem11->Text = L"Add Item";
		this->toolStripMenuItem11->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_required_get_item);
		// 
		// toolStripMenuItem12
		// 
		this->toolStripMenuItem12->Name = L"toolStripMenuItem12";
		this->toolStripMenuItem12->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem12->Text = L"Remove Item";
		this->toolStripMenuItem12->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_required_get_item);
		// 
		// contextMenuStrip_chases
		// 
		this->contextMenuStrip_chases->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem26, 
			this->toolStripMenuItem27});
		this->contextMenuStrip_chases->Name = L"contextMenuStrip1";
		this->contextMenuStrip_chases->Size = System::Drawing::Size(147, 48);
		// 
		// toolStripMenuItem26
		// 
		this->toolStripMenuItem26->Name = L"toolStripMenuItem26";
		this->toolStripMenuItem26->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem26->Text = L"Add Chase";
		this->toolStripMenuItem26->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_required_chase);
		// 
		// toolStripMenuItem27
		// 
		this->toolStripMenuItem27->Name = L"toolStripMenuItem27";
		this->toolStripMenuItem27->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem27->Text = L"Remove Chase";
		this->toolStripMenuItem27->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_required_chases);
		// 
		// groupBox_general
		// 
		this->groupBox_general->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_general->Controls->Add(this->checkBox_un13302);
		this->groupBox_general->Controls->Add(this->textBox_un13301);
		this->groupBox_general->Controls->Add(this->label38);
		this->groupBox_general->Controls->Add(this->textBox_type);
		this->groupBox_general->Controls->Add(this->checkBox_valunk3);
		this->groupBox_general->Controls->Add(this->textBox_resetcycle);
		this->groupBox_general->Controls->Add(this->textBox_resettype);
		this->groupBox_general->Controls->Add(this->label241);
		this->groupBox_general->Controls->Add(this->label240);
		this->groupBox_general->Controls->Add(this->textBox_cooldown);
		this->groupBox_general->Controls->Add(this->label239);
		this->groupBox_general->Controls->Add(this->label238);
		this->groupBox_general->Controls->Add(this->textBox_xunhan);
		this->groupBox_general->Controls->Add(this->textBox_unknown_09);
		this->groupBox_general->Controls->Add(this->label90);
		this->groupBox_general->Controls->Add(this->textBox_unknown_08);
		this->groupBox_general->Controls->Add(this->label89);
		this->groupBox_general->Controls->Add(this->label88);
		this->groupBox_general->Controls->Add(this->label82);
		this->groupBox_general->Controls->Add(this->checkBox_unknown_03);
		this->groupBox_general->Controls->Add(this->checkBox_unknown_02);
		this->groupBox_general->Controls->Add(this->textBox_unknown_06);
		this->groupBox_general->Controls->Add(this->label85);
		this->groupBox_general->Controls->Add(this->textBox_unknown_07);
		this->groupBox_general->Controls->Add(this->textBox_unknown_05);
		this->groupBox_general->Controls->Add(this->label76);
		this->groupBox_general->Controls->Add(this->label84);
		this->groupBox_general->Controls->Add(this->textBox_unknown_04);
		this->groupBox_general->Controls->Add(this->label31);
		this->groupBox_general->Controls->Add(this->checkBox_has_date_spans);
		this->groupBox_general->Controls->Add(this->dataGridView_date_spans);
		this->groupBox_general->Controls->Add(this->checkBox_has_date_fail);
		this->groupBox_general->Controls->Add(this->textBox_unknown_01);
		this->groupBox_general->Controls->Add(this->label81);
		this->groupBox_general->Controls->Add(this->label28);
		this->groupBox_general->Controls->Add(this->textBox_id);
		this->groupBox_general->Controls->Add(this->label55);
		this->groupBox_general->Controls->Add(this->label29);
		this->groupBox_general->Controls->Add(this->textBox_name);
		this->groupBox_general->Controls->Add(this->textBox_author_text);
		this->groupBox_general->Controls->Add(this->checkBox_author_mode);
		this->groupBox_general->Controls->Add(this->label32);
		this->groupBox_general->Controls->Add(this->textBox_time_limit);
		this->groupBox_general->Location = System::Drawing::Point(3, 3);
		this->groupBox_general->Name = L"groupBox_general";
		this->groupBox_general->Size = System::Drawing::Size(671, 338);
		this->groupBox_general->TabIndex = 0;
		this->groupBox_general->TabStop = false;
		this->groupBox_general->Text = L"GENERAL";
		// 
		// checkBox_un13302
		// 
		this->checkBox_un13302->AutoSize = true;
		this->checkBox_un13302->Enabled = false;
		this->checkBox_un13302->Location = System::Drawing::Point(551, 145);
		this->checkBox_un13302->Name = L"checkBox_un13302";
		this->checkBox_un13302->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->checkBox_un13302->Size = System::Drawing::Size(76, 17);
		this->checkBox_un13302->TabIndex = 83;
		this->checkBox_un13302->Text = L"Un 1330 2";
		this->checkBox_un13302->UseVisualStyleBackColor = true;
		this->checkBox_un13302->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un13302);
		// 
		// textBox_un13301
		// 
		this->textBox_un13301->Location = System::Drawing::Point(430, 143);
		this->textBox_un13301->Name = L"textBox_un13301";
		this->textBox_un13301->Size = System::Drawing::Size(115, 20);
		this->textBox_un13301->TabIndex = 82;
		this->textBox_un13301->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_un13301);
		// 
		// label38
		// 
		this->label38->AutoSize = true;
		this->label38->Location = System::Drawing::Point(358, 145);
		this->label38->Name = L"label38";
		this->label38->Size = System::Drawing::Size(66, 13);
		this->label38->TabIndex = 81;
		this->label38->Text = L"Unk 1330 1:";
		// 
		// textBox_type
		// 
		this->textBox_type->Location = System::Drawing::Point(256, 19);
		this->textBox_type->Name = L"textBox_type";
		this->textBox_type->Size = System::Drawing::Size(115, 20);
		this->textBox_type->TabIndex = 80;
		this->textBox_type->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_type);
		// 
		// checkBox_valunk3
		// 
		this->checkBox_valunk3->AutoSize = true;
		this->checkBox_valunk3->ForeColor = System::Drawing::Color::Gray;
		this->checkBox_valunk3->Location = System::Drawing::Point(95, 145);
		this->checkBox_valunk3->Name = L"checkBox_valunk3";
		this->checkBox_valunk3->Size = System::Drawing::Size(73, 17);
		this->checkBox_valunk3->TabIndex = 79;
		this->checkBox_valunk3->Text = L"Val Unk 3";
		this->checkBox_valunk3->UseVisualStyleBackColor = true;
		this->checkBox_valunk3->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_valunknown3);
		// 
		// textBox_resetcycle
		// 
		this->textBox_resetcycle->Location = System::Drawing::Point(611, 97);
		this->textBox_resetcycle->Name = L"textBox_resetcycle";
		this->textBox_resetcycle->Size = System::Drawing::Size(50, 20);
		this->textBox_resetcycle->TabIndex = 78;
		this->textBox_resetcycle->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_resetcycle);
		// 
		// textBox_resettype
		// 
		this->textBox_resettype->Location = System::Drawing::Point(611, 71);
		this->textBox_resettype->Name = L"textBox_resettype";
		this->textBox_resettype->Size = System::Drawing::Size(50, 20);
		this->textBox_resettype->TabIndex = 77;
		this->textBox_resettype->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_resettype);
		// 
		// label241
		// 
		this->label241->AutoSize = true;
		this->label241->Location = System::Drawing::Point(522, 97);
		this->label241->Name = L"label241";
		this->label241->Size = System::Drawing::Size(67, 13);
		this->label241->TabIndex = 76;
		this->label241->Text = L"Reset Cycle:";
		// 
		// label240
		// 
		this->label240->AutoSize = true;
		this->label240->Location = System::Drawing::Point(522, 71);
		this->label240->Name = L"label240";
		this->label240->Size = System::Drawing::Size(65, 13);
		this->label240->TabIndex = 75;
		this->label240->Text = L"Reset Type:";
		// 
		// textBox_cooldown
		// 
		this->textBox_cooldown->Location = System::Drawing::Point(611, 45);
		this->textBox_cooldown->Name = L"textBox_cooldown";
		this->textBox_cooldown->Size = System::Drawing::Size(50, 20);
		this->textBox_cooldown->TabIndex = 74;
		this->textBox_cooldown->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_cooldown);
		// 
		// label239
		// 
		this->label239->AutoSize = true;
		this->label239->Location = System::Drawing::Point(522, 48);
		this->label239->Name = L"label239";
		this->label239->Size = System::Drawing::Size(57, 13);
		this->label239->TabIndex = 73;
		this->label239->Text = L"Cooldown:";
		// 
		// label238
		// 
		this->label238->AutoSize = true;
		this->label238->Location = System::Drawing::Point(522, 22);
		this->label238->Name = L"label238";
		this->label238->Size = System::Drawing::Size(83, 13);
		this->label238->TabIndex = 72;
		this->label238->Text = L"Unknown Type:";
		// 
		// textBox_xunhan
		// 
		this->textBox_xunhan->Location = System::Drawing::Point(611, 18);
		this->textBox_xunhan->Name = L"textBox_xunhan";
		this->textBox_xunhan->Size = System::Drawing::Size(50, 20);
		this->textBox_xunhan->TabIndex = 71;
		this->textBox_xunhan->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_xunhan_type);
		// 
		// textBox_unknown_09
		// 
		this->textBox_unknown_09->Location = System::Drawing::Point(446, 312);
		this->textBox_unknown_09->Name = L"textBox_unknown_09";
		this->textBox_unknown_09->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_09->TabIndex = 70;
		this->textBox_unknown_09->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_09);
		// 
		// label90
		// 
		this->label90->AutoSize = true;
		this->label90->ForeColor = System::Drawing::Color::Black;
		this->label90->Location = System::Drawing::Point(358, 315);
		this->label90->Name = L"label90";
		this->label90->Size = System::Drawing::Size(71, 13);
		this->label90->TabIndex = 69;
		this->label90->Text = L"Unknown 09:";
		// 
		// textBox_unknown_08
		// 
		this->textBox_unknown_08->Location = System::Drawing::Point(446, 286);
		this->textBox_unknown_08->Name = L"textBox_unknown_08";
		this->textBox_unknown_08->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_08->TabIndex = 68;
		this->textBox_unknown_08->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_08);
		// 
		// label89
		// 
		this->label89->AutoSize = true;
		this->label89->Location = System::Drawing::Point(358, 289);
		this->label89->Name = L"label89";
		this->label89->Size = System::Drawing::Size(71, 13);
		this->label89->TabIndex = 67;
		this->label89->Text = L"Unknown 08:";
		// 
		// label88
		// 
		this->label88->AutoSize = true;
		this->label88->Location = System::Drawing::Point(6, 172);
		this->label88->Name = L"label88";
		this->label88->Size = System::Drawing::Size(59, 13);
		this->label88->TabIndex = 65;
		this->label88->Text = L"Date Valid:";
		// 
		// label82
		// 
		this->label82->AutoSize = true;
		this->label82->Location = System::Drawing::Point(6, 74);
		this->label82->Name = L"label82";
		this->label82->Size = System::Drawing::Size(65, 13);
		this->label82->TabIndex = 61;
		this->label82->Text = L"Author Text:";
		// 
		// checkBox_unknown_03
		// 
		this->checkBox_unknown_03->AutoSize = true;
		this->checkBox_unknown_03->ForeColor = System::Drawing::Color::Gray;
		this->checkBox_unknown_03->Location = System::Drawing::Point(6, 122);
		this->checkBox_unknown_03->Name = L"checkBox_unknown_03";
		this->checkBox_unknown_03->Size = System::Drawing::Size(73, 17);
		this->checkBox_unknown_03->TabIndex = 60;
		this->checkBox_unknown_03->Text = L"Val Unk 1";
		this->checkBox_unknown_03->UseVisualStyleBackColor = true;
		this->checkBox_unknown_03->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_03);
		// 
		// checkBox_unknown_02
		// 
		this->checkBox_unknown_02->AutoSize = true;
		this->checkBox_unknown_02->ForeColor = System::Drawing::Color::Gray;
		this->checkBox_unknown_02->Location = System::Drawing::Point(6, 145);
		this->checkBox_unknown_02->Name = L"checkBox_unknown_02";
		this->checkBox_unknown_02->Size = System::Drawing::Size(61, 17);
		this->checkBox_unknown_02->TabIndex = 59;
		this->checkBox_unknown_02->Text = L"Unk 02";
		this->checkBox_unknown_02->UseVisualStyleBackColor = true;
		this->checkBox_unknown_02->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_02);
		// 
		// textBox_unknown_06
		// 
		this->textBox_unknown_06->Location = System::Drawing::Point(95, 312);
		this->textBox_unknown_06->Name = L"textBox_unknown_06";
		this->textBox_unknown_06->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_06->TabIndex = 58;
		this->textBox_unknown_06->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_06);
		// 
		// label85
		// 
		this->label85->AutoSize = true;
		this->label85->ForeColor = System::Drawing::Color::Black;
		this->label85->Location = System::Drawing::Point(6, 315);
		this->label85->Name = L"label85";
		this->label85->Size = System::Drawing::Size(71, 13);
		this->label85->TabIndex = 57;
		this->label85->Text = L"Unknown 06:";
		// 
		// textBox_unknown_05
		// 
		this->textBox_unknown_05->Location = System::Drawing::Point(95, 286);
		this->textBox_unknown_05->Name = L"textBox_unknown_05";
		this->textBox_unknown_05->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_05->TabIndex = 56;
		this->textBox_unknown_05->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_05);
		// 
		// label84
		// 
		this->label84->AutoSize = true;
		this->label84->Location = System::Drawing::Point(6, 289);
		this->label84->Name = L"label84";
		this->label84->Size = System::Drawing::Size(71, 13);
		this->label84->TabIndex = 55;
		this->label84->Text = L"Unknown 05:";
		// 
		// textBox_unknown_04
		// 
		this->textBox_unknown_04->Location = System::Drawing::Point(95, 260);
		this->textBox_unknown_04->Name = L"textBox_unknown_04";
		this->textBox_unknown_04->Size = System::Drawing::Size(219, 20);
		this->textBox_unknown_04->TabIndex = 54;
		this->textBox_unknown_04->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_04);
		// 
		// label31
		// 
		this->label31->AutoSize = true;
		this->label31->Location = System::Drawing::Point(6, 263);
		this->label31->Name = L"label31";
		this->label31->Size = System::Drawing::Size(71, 13);
		this->label31->TabIndex = 53;
		this->label31->Text = L"Unknown 04:";
		// 
		// checkBox_has_date_spans
		// 
		this->checkBox_has_date_spans->AutoSize = true;
		this->checkBox_has_date_spans->Location = System::Drawing::Point(95, 172);
		this->checkBox_has_date_spans->Name = L"checkBox_has_date_spans";
		this->checkBox_has_date_spans->Size = System::Drawing::Size(15, 14);
		this->checkBox_has_date_spans->TabIndex = 48;
		this->checkBox_has_date_spans->UseVisualStyleBackColor = true;
		this->checkBox_has_date_spans->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_has_date_spans);
		// 
		// checkBox_has_date_fail
		// 
		this->checkBox_has_date_fail->AutoSize = true;
		this->checkBox_has_date_fail->ForeColor = System::Drawing::Color::Gray;
		this->checkBox_has_date_fail->Location = System::Drawing::Point(95, 123);
		this->checkBox_has_date_fail->Name = L"checkBox_has_date_fail";
		this->checkBox_has_date_fail->Size = System::Drawing::Size(73, 17);
		this->checkBox_has_date_fail->TabIndex = 47;
		this->checkBox_has_date_fail->Text = L"Val Unk 2";
		this->checkBox_has_date_fail->UseVisualStyleBackColor = true;
		this->checkBox_has_date_fail->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_has_date_fail);
		// 
		// textBox_unknown_01
		// 
		this->textBox_unknown_01->Location = System::Drawing::Point(95, 97);
		this->textBox_unknown_01->Name = L"textBox_unknown_01";
		this->textBox_unknown_01->Size = System::Drawing::Size(115, 20);
		this->textBox_unknown_01->TabIndex = 41;
		this->textBox_unknown_01->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_01);
		// 
		// label81
		// 
		this->label81->AutoSize = true;
		this->label81->Location = System::Drawing::Point(6, 100);
		this->label81->Name = L"label81";
		this->label81->Size = System::Drawing::Size(71, 13);
		this->label81->TabIndex = 40;
		this->label81->Text = L"Unknown 01:";
		// 
		// groupBox_reach_location
		// 
		this->groupBox_reach_location->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_reach_location->Controls->Add(this->checkBox_reach_locations_has_spans);
		this->groupBox_reach_location->Controls->Add(this->label4);
		this->groupBox_reach_location->Controls->Add(this->dataGridView_reach_location_spans);
		this->groupBox_reach_location->Controls->Add(this->textBox_reach_locations_map_id);
		this->groupBox_reach_location->Controls->Add(this->label6);
		this->groupBox_reach_location->Location = System::Drawing::Point(3, 197);
		this->groupBox_reach_location->Name = L"groupBox_reach_location";
		this->groupBox_reach_location->Size = System::Drawing::Size(671, 91);
		this->groupBox_reach_location->TabIndex = 98;
		this->groupBox_reach_location->TabStop = false;
		this->groupBox_reach_location->Text = L"SUCCESS ON ENTER";
		// 
		// checkBox_reach_locations_has_spans
		// 
		this->checkBox_reach_locations_has_spans->AutoSize = true;
		this->checkBox_reach_locations_has_spans->Location = System::Drawing::Point(77, 19);
		this->checkBox_reach_locations_has_spans->Name = L"checkBox_reach_locations_has_spans";
		this->checkBox_reach_locations_has_spans->Size = System::Drawing::Size(15, 14);
		this->checkBox_reach_locations_has_spans->TabIndex = 96;
		this->checkBox_reach_locations_has_spans->UseVisualStyleBackColor = true;
		this->checkBox_reach_locations_has_spans->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reach_locations_has_spans);
		// 
		// dataGridView_reach_location_spans
		// 
		this->dataGridView_reach_location_spans->AllowUserToAddRows = false;
		this->dataGridView_reach_location_spans->AllowUserToDeleteRows = false;
		this->dataGridView_reach_location_spans->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->dataGridView_reach_location_spans->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_reach_location_spans->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_reach_location_spans->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_reach_location_spans->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {this->dataGridViewTextBoxColumn60, 
			this->dataGridViewTextBoxColumn61, this->dataGridViewTextBoxColumn62, this->dataGridViewTextBoxColumn63, this->dataGridViewTextBoxColumn64, 
			this->dataGridViewTextBoxColumn65});
		this->dataGridView_reach_location_spans->Location = System::Drawing::Point(205, 10);
		this->dataGridView_reach_location_spans->MultiSelect = false;
		this->dataGridView_reach_location_spans->Name = L"dataGridView_reach_location_spans";
		this->dataGridView_reach_location_spans->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle35->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle35->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle35->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle35->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle35->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle35->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle35->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_reach_location_spans->RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
		this->dataGridView_reach_location_spans->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_reach_location_spans->RowTemplate->Height = 18;
		this->dataGridView_reach_location_spans->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_reach_location_spans->Size = System::Drawing::Size(460, 75);
		this->dataGridView_reach_location_spans->TabIndex = 91;
		this->dataGridView_reach_location_spans->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_reach_locations_span);
		// 
		// dataGridViewTextBoxColumn60
		// 
		this->dataGridViewTextBoxColumn60->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle29->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn60->DefaultCellStyle = dataGridViewCellStyle29;
		this->dataGridViewTextBoxColumn60->HeaderText = L"North";
		this->dataGridViewTextBoxColumn60->Name = L"dataGridViewTextBoxColumn60";
		this->dataGridViewTextBoxColumn60->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn61
		// 
		this->dataGridViewTextBoxColumn61->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle30->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn61->DefaultCellStyle = dataGridViewCellStyle30;
		this->dataGridViewTextBoxColumn61->HeaderText = L"South";
		this->dataGridViewTextBoxColumn61->Name = L"dataGridViewTextBoxColumn61";
		this->dataGridViewTextBoxColumn61->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn62
		// 
		this->dataGridViewTextBoxColumn62->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle31->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn62->DefaultCellStyle = dataGridViewCellStyle31;
		this->dataGridViewTextBoxColumn62->HeaderText = L"Weast";
		this->dataGridViewTextBoxColumn62->Name = L"dataGridViewTextBoxColumn62";
		this->dataGridViewTextBoxColumn62->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn63
		// 
		this->dataGridViewTextBoxColumn63->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle32->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn63->DefaultCellStyle = dataGridViewCellStyle32;
		this->dataGridViewTextBoxColumn63->HeaderText = L"East";
		this->dataGridViewTextBoxColumn63->Name = L"dataGridViewTextBoxColumn63";
		this->dataGridViewTextBoxColumn63->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn64
		// 
		this->dataGridViewTextBoxColumn64->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle33->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn64->DefaultCellStyle = dataGridViewCellStyle33;
		this->dataGridViewTextBoxColumn64->HeaderText = L"Low";
		this->dataGridViewTextBoxColumn64->Name = L"dataGridViewTextBoxColumn64";
		this->dataGridViewTextBoxColumn64->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn65
		// 
		this->dataGridViewTextBoxColumn65->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle34->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn65->DefaultCellStyle = dataGridViewCellStyle34;
		this->dataGridViewTextBoxColumn65->HeaderText = L"High";
		this->dataGridViewTextBoxColumn65->Name = L"dataGridViewTextBoxColumn65";
		this->dataGridViewTextBoxColumn65->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// textBox_reach_locations_map_id
		// 
		this->textBox_reach_locations_map_id->Location = System::Drawing::Point(77, 39);
		this->textBox_reach_locations_map_id->Name = L"textBox_reach_locations_map_id";
		this->textBox_reach_locations_map_id->Size = System::Drawing::Size(51, 20);
		this->textBox_reach_locations_map_id->TabIndex = 92;
		this->textBox_reach_locations_map_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_reach_locations_map_id);
		// 
		// contextMenuStrip_reach_location
		// 
		this->contextMenuStrip_reach_location->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem15, 
			this->toolStripMenuItem16});
		this->contextMenuStrip_reach_location->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reach_location->Size = System::Drawing::Size(184, 48);
		// 
		// toolStripMenuItem15
		// 
		this->toolStripMenuItem15->Name = L"toolStripMenuItem15";
		this->toolStripMenuItem15->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem15->Text = L"Add Location Span";
		this->toolStripMenuItem15->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_reach_locations_span);
		// 
		// toolStripMenuItem16
		// 
		this->toolStripMenuItem16->Name = L"toolStripMenuItem16";
		this->toolStripMenuItem16->Size = System::Drawing::Size(183, 22);
		this->toolStripMenuItem16->Text = L"Remove Location Span";
		this->toolStripMenuItem16->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_reach_locations_span);
		// 
		// tabControl1
		// 
		this->tabControl1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->tabControl1->Appearance = System::Windows::Forms::TabAppearance::FlatButtons;
		this->tabControl1->Controls->Add(this->tabPage1);
		this->tabControl1->Controls->Add(this->tabPage2);
		this->tabControl1->Controls->Add(this->tabPage3);
		this->tabControl1->Controls->Add(this->tabPage4);
		this->tabControl1->Controls->Add(this->tabPage6);
		this->tabControl1->Controls->Add(this->tabPage7);
		this->tabControl1->Controls->Add(this->tabPage5);
		this->tabControl1->Location = System::Drawing::Point(225, 27);
		this->tabControl1->Margin = System::Windows::Forms::Padding(0);
		this->tabControl1->Name = L"tabControl1";
		this->tabControl1->SelectedIndex = 0;
		this->tabControl1->Size = System::Drawing::Size(706, 607);
		this->tabControl1->TabIndex = 11;
		// 
		// tabPage1
		// 
		this->tabPage1->AutoScroll = true;
		this->tabPage1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage1->Controls->Add(this->groupBox_general);
		this->tabPage1->Controls->Add(this->groupBox_flags);
		this->tabPage1->Location = System::Drawing::Point(4, 25);
		this->tabPage1->Margin = System::Windows::Forms::Padding(0);
		this->tabPage1->Name = L"tabPage1";
		this->tabPage1->Size = System::Drawing::Size(698, 578);
		this->tabPage1->TabIndex = 0;
		this->tabPage1->Text = L"General";
		this->tabPage1->UseVisualStyleBackColor = true;
		// 
		// tabPage2
		// 
		this->tabPage2->AutoScroll = true;
		this->tabPage2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage2->Controls->Add(this->groupBox_reach_location);
		this->tabPage2->Controls->Add(this->groupBox_fail_location);
		this->tabPage2->Controls->Add(this->groupBox_trigger_location);
		this->tabPage2->Location = System::Drawing::Point(4, 25);
		this->tabPage2->Margin = System::Windows::Forms::Padding(0);
		this->tabPage2->Name = L"tabPage2";
		this->tabPage2->Size = System::Drawing::Size(698, 578);
		this->tabPage2->TabIndex = 1;
		this->tabPage2->Text = L"Locations";
		this->tabPage2->UseVisualStyleBackColor = true;
		// 
		// groupBox_fail_location
		// 
		this->groupBox_fail_location->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->groupBox_fail_location->Controls->Add(this->checkBox_fail_locations_has_spans);
		this->groupBox_fail_location->Controls->Add(this->label15);
		this->groupBox_fail_location->Controls->Add(this->dataGridView_fail_location_spans);
		this->groupBox_fail_location->Controls->Add(this->textBox_fail_locations_map_id);
		this->groupBox_fail_location->Controls->Add(this->label11);
		this->groupBox_fail_location->ForeColor = System::Drawing::Color::Black;
		this->groupBox_fail_location->Location = System::Drawing::Point(3, 100);
		this->groupBox_fail_location->Name = L"groupBox_fail_location";
		this->groupBox_fail_location->Size = System::Drawing::Size(671, 91);
		this->groupBox_fail_location->TabIndex = 96;
		this->groupBox_fail_location->TabStop = false;
		this->groupBox_fail_location->Text = L"FAIL ON ENTER";
		// 
		// checkBox_fail_locations_has_spans
		// 
		this->checkBox_fail_locations_has_spans->AutoSize = true;
		this->checkBox_fail_locations_has_spans->Location = System::Drawing::Point(77, 19);
		this->checkBox_fail_locations_has_spans->Name = L"checkBox_fail_locations_has_spans";
		this->checkBox_fail_locations_has_spans->Size = System::Drawing::Size(15, 14);
		this->checkBox_fail_locations_has_spans->TabIndex = 96;
		this->checkBox_fail_locations_has_spans->UseVisualStyleBackColor = true;
		this->checkBox_fail_locations_has_spans->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_fail_locations_has_spans);
		// 
		// dataGridView_fail_location_spans
		// 
		this->dataGridView_fail_location_spans->AllowUserToAddRows = false;
		this->dataGridView_fail_location_spans->AllowUserToDeleteRows = false;
		this->dataGridView_fail_location_spans->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
			| System::Windows::Forms::AnchorStyles::Left) 
			| System::Windows::Forms::AnchorStyles::Right));
		this->dataGridView_fail_location_spans->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_fail_location_spans->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_fail_location_spans->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_fail_location_spans->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {this->dataGridViewTextBoxColumn43, 
			this->dataGridViewTextBoxColumn44, this->dataGridViewTextBoxColumn45, this->dataGridViewTextBoxColumn46, this->dataGridViewTextBoxColumn47, 
			this->dataGridViewTextBoxColumn48});
		this->dataGridView_fail_location_spans->Location = System::Drawing::Point(205, 10);
		this->dataGridView_fail_location_spans->MultiSelect = false;
		this->dataGridView_fail_location_spans->Name = L"dataGridView_fail_location_spans";
		this->dataGridView_fail_location_spans->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle42->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle42->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle42->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle42->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle42->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle42->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle42->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_fail_location_spans->RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
		this->dataGridView_fail_location_spans->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_fail_location_spans->RowTemplate->Height = 18;
		this->dataGridView_fail_location_spans->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_fail_location_spans->Size = System::Drawing::Size(460, 75);
		this->dataGridView_fail_location_spans->TabIndex = 91;
		this->dataGridView_fail_location_spans->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_fail_locations_span);
		// 
		// dataGridViewTextBoxColumn43
		// 
		this->dataGridViewTextBoxColumn43->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle36->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn43->DefaultCellStyle = dataGridViewCellStyle36;
		this->dataGridViewTextBoxColumn43->HeaderText = L"North";
		this->dataGridViewTextBoxColumn43->Name = L"dataGridViewTextBoxColumn43";
		this->dataGridViewTextBoxColumn43->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn44
		// 
		this->dataGridViewTextBoxColumn44->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle37->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn44->DefaultCellStyle = dataGridViewCellStyle37;
		this->dataGridViewTextBoxColumn44->HeaderText = L"South";
		this->dataGridViewTextBoxColumn44->Name = L"dataGridViewTextBoxColumn44";
		this->dataGridViewTextBoxColumn44->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn45
		// 
		this->dataGridViewTextBoxColumn45->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle38->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn45->DefaultCellStyle = dataGridViewCellStyle38;
		this->dataGridViewTextBoxColumn45->HeaderText = L"Weast";
		this->dataGridViewTextBoxColumn45->Name = L"dataGridViewTextBoxColumn45";
		this->dataGridViewTextBoxColumn45->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn46
		// 
		this->dataGridViewTextBoxColumn46->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle39->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn46->DefaultCellStyle = dataGridViewCellStyle39;
		this->dataGridViewTextBoxColumn46->HeaderText = L"East";
		this->dataGridViewTextBoxColumn46->Name = L"dataGridViewTextBoxColumn46";
		this->dataGridViewTextBoxColumn46->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn47
		// 
		this->dataGridViewTextBoxColumn47->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle40->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn47->DefaultCellStyle = dataGridViewCellStyle40;
		this->dataGridViewTextBoxColumn47->HeaderText = L"Low";
		this->dataGridViewTextBoxColumn47->Name = L"dataGridViewTextBoxColumn47";
		this->dataGridViewTextBoxColumn47->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridViewTextBoxColumn48
		// 
		this->dataGridViewTextBoxColumn48->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle41->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn48->DefaultCellStyle = dataGridViewCellStyle41;
		this->dataGridViewTextBoxColumn48->HeaderText = L"High";
		this->dataGridViewTextBoxColumn48->Name = L"dataGridViewTextBoxColumn48";
		this->dataGridViewTextBoxColumn48->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// textBox_fail_locations_map_id
		// 
		this->textBox_fail_locations_map_id->Location = System::Drawing::Point(77, 39);
		this->textBox_fail_locations_map_id->Name = L"textBox_fail_locations_map_id";
		this->textBox_fail_locations_map_id->Size = System::Drawing::Size(51, 20);
		this->textBox_fail_locations_map_id->TabIndex = 92;
		this->textBox_fail_locations_map_id->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_fail_locations_map_id);
		// 
		// tabPage3
		// 
		this->tabPage3->AutoScroll = true;
		this->tabPage3->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage3->Controls->Add(this->groupBox_basic_1);
		this->tabPage3->Controls->Add(this->groupBox_basic_2);
		this->tabPage3->Location = System::Drawing::Point(4, 25);
		this->tabPage3->Margin = System::Windows::Forms::Padding(0);
		this->tabPage3->Name = L"tabPage3";
		this->tabPage3->Size = System::Drawing::Size(698, 578);
		this->tabPage3->TabIndex = 2;
		this->tabPage3->Text = L"Basic I+II";
		this->tabPage3->UseVisualStyleBackColor = true;
		// 
		// tabPage4
		// 
		this->tabPage4->AutoScroll = true;
		this->tabPage4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage4->Controls->Add(this->groupBox_requirements);
		this->tabPage4->Location = System::Drawing::Point(4, 25);
		this->tabPage4->Margin = System::Windows::Forms::Padding(0);
		this->tabPage4->Name = L"tabPage4";
		this->tabPage4->Size = System::Drawing::Size(698, 578);
		this->tabPage4->TabIndex = 3;
		this->tabPage4->Text = L"Requirements";
		this->tabPage4->UseVisualStyleBackColor = true;
		// 
		// groupBox_requirements
		// 
		this->groupBox_requirements->Controls->Add(this->label151);
		this->groupBox_requirements->Controls->Add(this->dataGridView_dynamics);
		this->groupBox_requirements->Controls->Add(this->textBox3);
		this->groupBox_requirements->Controls->Add(this->label235);
		this->groupBox_requirements->Controls->Add(this->listBox_tops);
		this->groupBox_requirements->Controls->Add(this->textBox_needlvl);
		this->groupBox_requirements->Controls->Add(this->label186);
		this->groupBox_requirements->Controls->Add(this->textBox_titlechange);
		this->groupBox_requirements->Controls->Add(this->label185);
		this->groupBox_requirements->Controls->Add(this->listBox_titles);
		this->groupBox_requirements->Controls->Add(this->textBox_waittime);
		this->groupBox_requirements->Controls->Add(this->label183);
		this->groupBox_requirements->Controls->Add(this->textBox_npctime);
		this->groupBox_requirements->Controls->Add(this->label180);
		this->groupBox_requirements->Controls->Add(this->textBox_safenpc);
		this->groupBox_requirements->Controls->Add(this->label179);
		this->groupBox_requirements->Controls->Add(this->textBox_noncash);
		this->groupBox_requirements->Controls->Add(this->label178);
		this->groupBox_requirements->Controls->Add(this->textBox_completetype);
		this->groupBox_requirements->Controls->Add(this->label173);
		this->groupBox_requirements->Controls->Add(this->textBox_method);
		this->groupBox_requirements->Controls->Add(this->label171);
		this->groupBox_requirements->Controls->Add(this->textBox_soulmax);
		this->groupBox_requirements->Controls->Add(this->checkBox_soulmaxon);
		this->groupBox_requirements->Controls->Add(this->checkBox_soulminom);
		this->groupBox_requirements->Controls->Add(this->textBox_soulmin);
		this->groupBox_requirements->Controls->Add(this->label149);
		this->groupBox_requirements->Controls->Add(this->textBox_soulunkn1);
		this->groupBox_requirements->Controls->Add(this->textBox_ingotmax);
		this->groupBox_requirements->Controls->Add(this->textBox_ingotmin);
		this->groupBox_requirements->Controls->Add(this->label131);
		this->groupBox_requirements->Controls->Add(this->label130);
		this->groupBox_requirements->Controls->Add(this->label73);
		this->groupBox_requirements->Controls->Add(this->textBox_zhenying);
		this->groupBox_requirements->Controls->Add(this->checkBox_frongfang03);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontfeng02);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontfeng01);
		this->groupBox_requirements->Controls->Add(this->label143);
		this->groupBox_requirements->Controls->Add(this->textBox_blamon);
		this->groupBox_requirements->Controls->Add(this->checkBox_blded);
		this->groupBox_requirements->Controls->Add(this->textBox_clanchmax);
		this->groupBox_requirements->Controls->Add(this->textBox_clanchmin);
		this->groupBox_requirements->Controls->Add(this->label140);
		this->groupBox_requirements->Controls->Add(this->label139);
		this->groupBox_requirements->Controls->Add(this->textBox_clanmap);
		this->groupBox_requirements->Controls->Add(this->label138);
		this->groupBox_requirements->Controls->Add(this->textBox_clanskillid);
		this->groupBox_requirements->Controls->Add(this->label137);
		this->groupBox_requirements->Controls->Add(this->textBox_clanskillexpmax);
		this->groupBox_requirements->Controls->Add(this->textBox_clanskillexpmin);
		this->groupBox_requirements->Controls->Add(this->label136);
		this->groupBox_requirements->Controls->Add(this->label135);
		this->groupBox_requirements->Controls->Add(this->textBox_clanskillmax);
		this->groupBox_requirements->Controls->Add(this->textBox_clanskillmin);
		this->groupBox_requirements->Controls->Add(this->label133);
		this->groupBox_requirements->Controls->Add(this->label80);
		this->groupBox_requirements->Controls->Add(this->checkBox_clanlead);
		this->groupBox_requirements->Controls->Add(this->checkBox_clanc);
		this->groupBox_requirements->Controls->Add(this->textBox_vtask);
		this->groupBox_requirements->Controls->Add(this->label77);
		this->groupBox_requirements->Controls->Add(this->label30);
		this->groupBox_requirements->Controls->Add(this->textBox_viewlvl);
		this->groupBox_requirements->Controls->Add(this->checkBox_vunkn2);
		this->groupBox_requirements->Controls->Add(this->checkBox_vunkn1);
		this->groupBox_requirements->Controls->Add(this->checkBox_vpeach);
		this->groupBox_requirements->Controls->Add(this->checkBox_vchef);
		this->groupBox_requirements->Controls->Add(this->checkBox_view);
		this->groupBox_requirements->Controls->Add(this->checkBox_viewreq);
		this->groupBox_requirements->Controls->Add(this->checkBox_leavefail);
		this->groupBox_requirements->Controls->Add(this->checkBox_tamaccfail);
		this->groupBox_requirements->Controls->Add(this->checkBox_leaderfail);
		this->groupBox_requirements->Controls->Add(this->checkBox_teamchecknum);
		this->groupBox_requirements->Controls->Add(this->checkBox_teamshare2);
		this->groupBox_requirements->Controls->Add(this->checkBox_teamunk);
		this->groupBox_requirements->Controls->Add(this->checkBox_teamshare);
		this->groupBox_requirements->Controls->Add(this->checkBox_teamask);
		this->groupBox_requirements->Controls->Add(this->label166);
		this->groupBox_requirements->Controls->Add(this->label164);
		this->groupBox_requirements->Controls->Add(this->textBox_maxpk);
		this->groupBox_requirements->Controls->Add(this->textBox_minpk);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontmarried);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontunkn01);
		this->groupBox_requirements->Controls->Add(this->checkBox_licreq);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontgen);
		this->groupBox_requirements->Controls->Add(this->textBox_unknfront);
		this->groupBox_requirements->Controls->Add(this->checkBox_frontalliance);
		this->groupBox_requirements->Controls->Add(this->textBox_frontgender);
		this->groupBox_requirements->Controls->Add(this->textBox_zhuanchonglv);
		this->groupBox_requirements->Controls->Add(this->label9);
		this->groupBox_requirements->Controls->Add(this->textBox_renwu);
		this->groupBox_requirements->Controls->Add(this->label8);
		this->groupBox_requirements->Controls->Add(this->label7);
		this->groupBox_requirements->Controls->Add(this->textBox_yaoqiu);
		this->groupBox_requirements->Controls->Add(this->checkBox_tasknum);
		this->groupBox_requirements->Controls->Add(this->textBox1);
		this->groupBox_requirements->Controls->Add(this->listBox_jobs);
		this->groupBox_requirements->Controls->Add(this->label257);
		this->groupBox_requirements->Controls->Add(this->label256);
		this->groupBox_requirements->Controls->Add(this->label255);
		this->groupBox_requirements->Controls->Add(this->label254);
		this->groupBox_requirements->Controls->Add(this->label253);
		this->groupBox_requirements->Controls->Add(this->label252);
		this->groupBox_requirements->Controls->Add(this->label251);
		this->groupBox_requirements->Controls->Add(this->label250);
		this->groupBox_requirements->Controls->Add(this->label249);
		this->groupBox_requirements->Controls->Add(this->label248);
		this->groupBox_requirements->Controls->Add(this->label247);
		this->groupBox_requirements->Controls->Add(this->label246);
		this->groupBox_requirements->Controls->Add(this->label245);
		this->groupBox_requirements->Controls->Add(this->label244);
		this->groupBox_requirements->Controls->Add(this->label243);
		this->groupBox_requirements->Controls->Add(this->label87);
		this->groupBox_requirements->Controls->Add(this->label86);
		this->groupBox_requirements->Controls->Add(this->label83);
		this->groupBox_requirements->Controls->Add(this->label69);
		this->groupBox_requirements->Controls->Add(this->label58);
		this->groupBox_requirements->Controls->Add(this->textBox_rep20);
		this->groupBox_requirements->Controls->Add(this->textBox_rep19);
		this->groupBox_requirements->Controls->Add(this->textBox_rep13);
		this->groupBox_requirements->Controls->Add(this->textBox_rep14);
		this->groupBox_requirements->Controls->Add(this->textBox_rep15);
		this->groupBox_requirements->Controls->Add(this->textBox_rep17);
		this->groupBox_requirements->Controls->Add(this->textBox_rep16);
		this->groupBox_requirements->Controls->Add(this->textBox_rep18);
		this->groupBox_requirements->Controls->Add(this->checkBox_hassheng);
		this->groupBox_requirements->Controls->Add(this->textBox_rep7);
		this->groupBox_requirements->Controls->Add(this->textBox_rep8);
		this->groupBox_requirements->Controls->Add(this->textBox_rep12);
		this->groupBox_requirements->Controls->Add(this->textBox_rep11);
		this->groupBox_requirements->Controls->Add(this->textBox_rep10);
		this->groupBox_requirements->Controls->Add(this->textBox_rep9);
		this->groupBox_requirements->Controls->Add(this->textBox_rep6);
		this->groupBox_requirements->Controls->Add(this->textBox_rep5);
		this->groupBox_requirements->Controls->Add(this->textBox_rep4);
		this->groupBox_requirements->Controls->Add(this->textBox_rep3);
		this->groupBox_requirements->Controls->Add(this->textBox_rep2);
		this->groupBox_requirements->Controls->Add(this->textBox_rep1);
		this->groupBox_requirements->Controls->Add(this->checkBox_needgender);
		this->groupBox_requirements->Controls->Add(this->textBox_autoval);
		this->groupBox_requirements->Controls->Add(this->checkBox_hasauto);
		this->groupBox_requirements->Controls->Add(this->textBox_workval);
		this->groupBox_requirements->Controls->Add(this->checkBox_haswork);
		this->groupBox_requirements->Controls->Add(this->checkBox_contribu);
		this->groupBox_requirements->Controls->Add(this->checkBox_needclan);
		this->groupBox_requirements->Controls->Add(this->textBox_hasnextval);
		this->groupBox_requirements->Controls->Add(this->checkBox_hasnext);
		this->groupBox_requirements->Controls->Add(this->checkBox_rineedbag);
		this->groupBox_requirements->Controls->Add(this->textBox_iunk11);
		this->groupBox_requirements->Controls->Add(this->textBox_iunk10);
		this->groupBox_requirements->Controls->Add(this->textBox_iunk09);
		this->groupBox_requirements->Controls->Add(this->textBox_itunk07);
		this->groupBox_requirements->Controls->Add(this->textBox_itunk06);
		this->groupBox_requirements->Controls->Add(this->checkBox_iunk4);
		this->groupBox_requirements->Controls->Add(this->checkBox_iunk5);
		this->groupBox_requirements->Controls->Add(this->label222);
		this->groupBox_requirements->Controls->Add(this->label74);
		this->groupBox_requirements->Controls->Add(this->dataGridView_team_members);
		this->groupBox_requirements->Controls->Add(this->label68);
		this->groupBox_requirements->Controls->Add(this->dataGridView_given_items);
		this->groupBox_requirements->Controls->Add(this->dataGridView_required_items);
		this->groupBox_requirements->Controls->Add(this->label2);
		this->groupBox_requirements->Controls->Add(this->dataGridView_required_get_items);
		this->groupBox_requirements->Controls->Add(this->label1);
		this->groupBox_requirements->Controls->Add(this->dataGridView_required_chases);
		this->groupBox_requirements->Controls->Add(this->label48);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_undone_1);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_undone_2);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_undone_3);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_undone_5);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_undone_4);
		this->groupBox_requirements->Controls->Add(this->textBox_unknown_44);
		this->groupBox_requirements->Controls->Add(this->checkBox_required_be_gm);
		this->groupBox_requirements->Controls->Add(this->checkBox_required_be_married);
		this->groupBox_requirements->Controls->Add(this->comboBox_required_gender);
		this->groupBox_requirements->Controls->Add(this->label41);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_done_1);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_done_2);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_done_3);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_done_4);
		this->groupBox_requirements->Controls->Add(this->textBox_required_quests_done_5);
		this->groupBox_requirements->Controls->Add(this->textBox_required_items_unknown);
		this->groupBox_requirements->Controls->Add(this->label70);
		this->groupBox_requirements->Controls->Add(this->checkBox_unknown_27);
		this->groupBox_requirements->Controls->Add(this->textBox_instant_pay_coins);
		this->groupBox_requirements->Controls->Add(this->label65);
		this->groupBox_requirements->Controls->Add(this->dataGridView_tasknum);
		this->groupBox_requirements->Location = System::Drawing::Point(3, 3);
		this->groupBox_requirements->Name = L"groupBox_requirements";
		this->groupBox_requirements->Size = System::Drawing::Size(655, 1869);
		this->groupBox_requirements->TabIndex = 2;
		this->groupBox_requirements->TabStop = false;
		this->groupBox_requirements->Text = L"REQUIREMENTS";
		// 
		// label151
		// 
		this->label151->AutoSize = true;
		this->label151->Location = System::Drawing::Point(137, 992);
		this->label151->Name = L"label151";
		this->label151->Size = System::Drawing::Size(56, 13);
		this->label151->TabIndex = 461;
		this->label151->Text = L"Dynamics:";
		// 
		// dataGridView_dynamics
		// 
		this->dataGridView_dynamics->AllowUserToAddRows = false;
		this->dataGridView_dynamics->AllowUserToDeleteRows = false;
		this->dataGridView_dynamics->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_dynamics->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_dynamics->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_dynamics->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(11) {this->dataGridViewTextBoxColumn25, 
			this->dataGridViewTextBoxColumn29, this->dataGridViewTextBoxColumn32, this->dataGridViewTextBoxColumn33, this->dataGridViewTextBoxColumn34, 
			this->dataGridViewTextBoxColumn35, this->dataGridViewTextBoxColumn36, this->dataGridViewTextBoxColumn55, this->dataGridViewTextBoxColumn56, 
			this->Column21, this->Column22});
		this->dataGridView_dynamics->ContextMenuStrip = this->contextMenuStrip_team_members;
		this->dataGridView_dynamics->Location = System::Drawing::Point(135, 1012);
		this->dataGridView_dynamics->MultiSelect = false;
		this->dataGridView_dynamics->Name = L"dataGridView_dynamics";
		this->dataGridView_dynamics->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle49->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle49->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle49->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle49->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle49->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle49->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle49->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_dynamics->RowHeadersDefaultCellStyle = dataGridViewCellStyle49;
		this->dataGridView_dynamics->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_dynamics->RowTemplate->Height = 18;
		this->dataGridView_dynamics->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_dynamics->Size = System::Drawing::Size(512, 81);
		this->dataGridView_dynamics->TabIndex = 460;
		this->dataGridView_dynamics->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_dynamicsvals);
		// 
		// dataGridViewTextBoxColumn25
		// 
		this->dataGridViewTextBoxColumn25->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle43->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn25->DefaultCellStyle = dataGridViewCellStyle43;
		this->dataGridViewTextBoxColumn25->HeaderText = L"ID";
		this->dataGridViewTextBoxColumn25->Name = L"dataGridViewTextBoxColumn25";
		this->dataGridViewTextBoxColumn25->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn25->Width = 23;
		// 
		// dataGridViewTextBoxColumn29
		// 
		this->dataGridViewTextBoxColumn29->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle44->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn29->DefaultCellStyle = dataGridViewCellStyle44;
		this->dataGridViewTextBoxColumn29->HeaderText = L"map_id";
		this->dataGridViewTextBoxColumn29->Name = L"dataGridViewTextBoxColumn29";
		this->dataGridViewTextBoxColumn29->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn29->Width = 46;
		// 
		// dataGridViewTextBoxColumn32
		// 
		dataGridViewCellStyle45->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn32->DefaultCellStyle = dataGridViewCellStyle45;
		this->dataGridViewTextBoxColumn32->HeaderText = L"x";
		this->dataGridViewTextBoxColumn32->Name = L"dataGridViewTextBoxColumn32";
		this->dataGridViewTextBoxColumn32->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn32->Width = 50;
		// 
		// dataGridViewTextBoxColumn33
		// 
		dataGridViewCellStyle46->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn33->DefaultCellStyle = dataGridViewCellStyle46;
		this->dataGridViewTextBoxColumn33->HeaderText = L"y";
		this->dataGridViewTextBoxColumn33->Name = L"dataGridViewTextBoxColumn33";
		this->dataGridViewTextBoxColumn33->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn33->Width = 50;
		// 
		// dataGridViewTextBoxColumn34
		// 
		dataGridViewCellStyle47->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn34->DefaultCellStyle = dataGridViewCellStyle47;
		this->dataGridViewTextBoxColumn34->HeaderText = L"z";
		this->dataGridViewTextBoxColumn34->Name = L"dataGridViewTextBoxColumn34";
		this->dataGridViewTextBoxColumn34->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn34->Width = 50;
		// 
		// dataGridViewTextBoxColumn35
		// 
		this->dataGridViewTextBoxColumn35->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle48->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn35->DefaultCellStyle = dataGridViewCellStyle48;
		this->dataGridViewTextBoxColumn35->HeaderText = L"times";
		this->dataGridViewTextBoxColumn35->Name = L"dataGridViewTextBoxColumn35";
		this->dataGridViewTextBoxColumn35->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn35->Width = 36;
		// 
		// dataGridViewTextBoxColumn36
		// 
		this->dataGridViewTextBoxColumn36->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->dataGridViewTextBoxColumn36->HeaderText = L"unk 1";
		this->dataGridViewTextBoxColumn36->Name = L"dataGridViewTextBoxColumn36";
		this->dataGridViewTextBoxColumn36->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->dataGridViewTextBoxColumn36->Width = 39;
		// 
		// dataGridViewTextBoxColumn55
		// 
		this->dataGridViewTextBoxColumn55->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->dataGridViewTextBoxColumn55->HeaderText = L"unk 2";
		this->dataGridViewTextBoxColumn55->Name = L"dataGridViewTextBoxColumn55";
		this->dataGridViewTextBoxColumn55->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->dataGridViewTextBoxColumn55->Width = 39;
		// 
		// dataGridViewTextBoxColumn56
		// 
		this->dataGridViewTextBoxColumn56->HeaderText = L"unk 3";
		this->dataGridViewTextBoxColumn56->Name = L"dataGridViewTextBoxColumn56";
		this->dataGridViewTextBoxColumn56->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->dataGridViewTextBoxColumn56->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->dataGridViewTextBoxColumn56->Width = 40;
		// 
		// Column21
		// 
		this->Column21->HeaderText = L"unk 4";
		this->Column21->Name = L"Column21";
		this->Column21->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column21->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->Column21->Width = 40;
		// 
		// Column22
		// 
		this->Column22->HeaderText = L"unk 5";
		this->Column22->Name = L"Column22";
		this->Column22->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column22->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->Column22->Width = 40;
		// 
		// textBox3
		// 
		this->textBox3->Location = System::Drawing::Point(438, 383);
		this->textBox3->Name = L"textBox3";
		this->textBox3->Size = System::Drawing::Size(113, 20);
		this->textBox3->TabIndex = 459;
		this->textBox3->TextChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::rename_tops);
		// 
		// label235
		// 
		this->label235->AutoSize = true;
		this->label235->Location = System::Drawing::Point(440, 271);
		this->label235->Name = L"label235";
		this->label235->Size = System::Drawing::Size(57, 13);
		this->label235->TabIndex = 458;
		this->label235->Text = L"Toplist ids:";
		// 
		// listBox_tops
		// 
		this->listBox_tops->ContextMenuStrip = this->contextMenuStrip1;
		this->listBox_tops->FormattingEnabled = true;
		this->listBox_tops->Location = System::Drawing::Point(438, 287);
		this->listBox_tops->Name = L"listBox_tops";
		this->listBox_tops->Size = System::Drawing::Size(113, 95);
		this->listBox_tops->TabIndex = 457;
		this->listBox_tops->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_tops);
		// 
		// contextMenuStrip1
		// 
		this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem50, 
			this->toolStripMenuItem51});
		this->contextMenuStrip1->Name = L"contextMenuStrip1";
		this->contextMenuStrip1->Size = System::Drawing::Size(114, 48);
		// 
		// toolStripMenuItem50
		// 
		this->toolStripMenuItem50->Name = L"toolStripMenuItem50";
		this->toolStripMenuItem50->Size = System::Drawing::Size(113, 22);
		this->toolStripMenuItem50->Text = L"Add";
		this->toolStripMenuItem50->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::add_tops);
		// 
		// toolStripMenuItem51
		// 
		this->toolStripMenuItem51->Name = L"toolStripMenuItem51";
		this->toolStripMenuItem51->Size = System::Drawing::Size(113, 22);
		this->toolStripMenuItem51->Text = L"Remove";
		this->toolStripMenuItem51->Click += gcnew System::EventHandler(this, &zxTASKeditWindow::remove_tops);
		// 
		// textBox_needlvl
		// 
		this->textBox_needlvl->Location = System::Drawing::Point(237, 855);
		this->textBox_needlvl->Name = L"textBox_needlvl";
		this->textBox_needlvl->Size = System::Drawing::Size(46, 20);
		this->textBox_needlvl->TabIndex = 456;
		this->textBox_needlvl->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_needlvl);
		// 
		// label186
		// 
		this->label186->AutoSize = true;
		this->label186->Location = System::Drawing::Point(157, 861);
		this->label186->Name = L"label186";
		this->label186->Size = System::Drawing::Size(49, 13);
		this->label186->TabIndex = 455;
		this->label186->Text = L"Need lvl:";
		// 
		// textBox_titlechange
		// 
		this->textBox_titlechange->Location = System::Drawing::Point(214, 383);
		this->textBox_titlechange->Name = L"textBox_titlechange";
		this->textBox_titlechange->Size = System::Drawing::Size(106, 20);
		this->textBox_titlechange->TabIndex = 454;
		this->textBox_titlechange->TextChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::rename_title);
		// 
		// listBox_titles
		// 
		this->listBox_titles->FormattingEnabled = true;
		this->listBox_titles->Location = System::Drawing::Point(214, 287);
		this->listBox_titles->Name = L"listBox_titles";
		this->listBox_titles->Size = System::Drawing::Size(106, 95);
		this->listBox_titles->TabIndex = 452;
		this->listBox_titles->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_title);
		// 
		// textBox_waittime
		// 
		this->textBox_waittime->Location = System::Drawing::Point(206, 13);
		this->textBox_waittime->Name = L"textBox_waittime";
		this->textBox_waittime->Size = System::Drawing::Size(59, 20);
		this->textBox_waittime->TabIndex = 451;
		this->textBox_waittime->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_waittime);
		// 
		// label183
		// 
		this->label183->AutoSize = true;
		this->label183->Location = System::Drawing::Point(146, 16);
		this->label183->Name = L"label183";
		this->label183->Size = System::Drawing::Size(58, 13);
		this->label183->TabIndex = 450;
		this->label183->Text = L"Wait Time:";
		// 
		// textBox_npctime
		// 
		this->textBox_npctime->Location = System::Drawing::Point(237, 823);
		this->textBox_npctime->Name = L"textBox_npctime";
		this->textBox_npctime->Size = System::Drawing::Size(46, 20);
		this->textBox_npctime->TabIndex = 449;
		this->textBox_npctime->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_npctime);
		// 
		// label180
		// 
		this->label180->AutoSize = true;
		this->label180->Location = System::Drawing::Point(157, 828);
		this->label180->Name = L"label180";
		this->label180->Size = System::Drawing::Size(79, 13);
		this->label180->TabIndex = 448;
		this->label180->Text = L"Safe NPC time:";
		// 
		// textBox_safenpc
		// 
		this->textBox_safenpc->Location = System::Drawing::Point(237, 795);
		this->textBox_safenpc->Name = L"textBox_safenpc";
		this->textBox_safenpc->Size = System::Drawing::Size(46, 20);
		this->textBox_safenpc->TabIndex = 447;
		this->textBox_safenpc->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_safenpc);
		// 
		// label179
		// 
		this->label179->AutoSize = true;
		this->label179->Location = System::Drawing::Point(158, 800);
		this->label179->Name = L"label179";
		this->label179->Size = System::Drawing::Size(57, 13);
		this->label179->TabIndex = 446;
		this->label179->Text = L"Safe NPC:";
		// 
		// textBox_noncash
		// 
		this->textBox_noncash->Location = System::Drawing::Point(103, 883);
		this->textBox_noncash->Name = L"textBox_noncash";
		this->textBox_noncash->Size = System::Drawing::Size(48, 20);
		this->textBox_noncash->TabIndex = 445;
		// 
		// label178
		// 
		this->label178->AutoSize = true;
		this->label178->Location = System::Drawing::Point(5, 888);
		this->label178->Name = L"label178";
		this->label178->Size = System::Drawing::Size(56, 13);
		this->label178->TabIndex = 444;
		this->label178->Text = L"Non cash:";
		// 
		// textBox_completetype
		// 
		this->textBox_completetype->Location = System::Drawing::Point(103, 857);
		this->textBox_completetype->Name = L"textBox_completetype";
		this->textBox_completetype->Size = System::Drawing::Size(48, 20);
		this->textBox_completetype->TabIndex = 443;
		this->textBox_completetype->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_completetype);
		// 
		// label173
		// 
		this->label173->AutoSize = true;
		this->label173->Location = System::Drawing::Point(6, 856);
		this->label173->Name = L"label173";
		this->label173->Size = System::Drawing::Size(77, 13);
		this->label173->TabIndex = 442;
		this->label173->Text = L"Complete type:";
		// 
		// textBox_method
		// 
		this->textBox_method->Location = System::Drawing::Point(103, 828);
		this->textBox_method->Name = L"textBox_method";
		this->textBox_method->Size = System::Drawing::Size(49, 20);
		this->textBox_method->TabIndex = 441;
		this->textBox_method->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_method);
		// 
		// label171
		// 
		this->label171->AutoSize = true;
		this->label171->Location = System::Drawing::Point(6, 833);
		this->label171->Name = L"label171";
		this->label171->Size = System::Drawing::Size(46, 13);
		this->label171->TabIndex = 440;
		this->label171->Text = L"Method:";
		// 
		// textBox_soulmax
		// 
		this->textBox_soulmax->Location = System::Drawing::Point(114, 790);
		this->textBox_soulmax->Name = L"textBox_soulmax";
		this->textBox_soulmax->Size = System::Drawing::Size(46, 20);
		this->textBox_soulmax->TabIndex = 439;
		this->textBox_soulmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_soulmax);
		// 
		// checkBox_soulmaxon
		// 
		this->checkBox_soulmaxon->AutoSize = true;
		this->checkBox_soulmaxon->Location = System::Drawing::Point(9, 793);
		this->checkBox_soulmaxon->Name = L"checkBox_soulmaxon";
		this->checkBox_soulmaxon->Size = System::Drawing::Size(103, 17);
		this->checkBox_soulmaxon->TabIndex = 438;
		this->checkBox_soulmaxon->Text = L"Chroma LV max:";
		this->checkBox_soulmaxon->UseVisualStyleBackColor = true;
		this->checkBox_soulmaxon->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_soulmaxon);
		// 
		// checkBox_soulminom
		// 
		this->checkBox_soulminom->AutoSize = true;
		this->checkBox_soulminom->Location = System::Drawing::Point(9, 754);
		this->checkBox_soulminom->Name = L"checkBox_soulminom";
		this->checkBox_soulminom->Size = System::Drawing::Size(100, 17);
		this->checkBox_soulminom->TabIndex = 437;
		this->checkBox_soulminom->Text = L"Chroma LV min:";
		this->checkBox_soulminom->UseVisualStyleBackColor = true;
		this->checkBox_soulminom->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_soulminom);
		// 
		// textBox_soulmin
		// 
		this->textBox_soulmin->Location = System::Drawing::Point(114, 753);
		this->textBox_soulmin->Name = L"textBox_soulmin";
		this->textBox_soulmin->Size = System::Drawing::Size(46, 20);
		this->textBox_soulmin->TabIndex = 436;
		this->textBox_soulmin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_soulmin);
		// 
		// label149
		// 
		this->label149->AutoSize = true;
		this->label149->Location = System::Drawing::Point(6, 725);
		this->label149->Name = L"label149";
		this->label149->Size = System::Drawing::Size(64, 13);
		this->label149->TabIndex = 434;
		this->label149->Text = L"Chroma req:";
		// 
		// textBox_soulunkn1
		// 
		this->textBox_soulunkn1->Location = System::Drawing::Point(75, 724);
		this->textBox_soulunkn1->Name = L"textBox_soulunkn1";
		this->textBox_soulunkn1->Size = System::Drawing::Size(46, 20);
		this->textBox_soulunkn1->TabIndex = 433;
		this->textBox_soulunkn1->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_soulunkn1);
		// 
		// textBox_ingotmax
		// 
		this->textBox_ingotmax->Location = System::Drawing::Point(60, 679);
		this->textBox_ingotmax->Name = L"textBox_ingotmax";
		this->textBox_ingotmax->Size = System::Drawing::Size(46, 20);
		this->textBox_ingotmax->TabIndex = 432;
		this->textBox_ingotmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ingotmax);
		// 
		// textBox_ingotmin
		// 
		this->textBox_ingotmin->Location = System::Drawing::Point(60, 652);
		this->textBox_ingotmin->Name = L"textBox_ingotmin";
		this->textBox_ingotmin->Size = System::Drawing::Size(46, 20);
		this->textBox_ingotmin->TabIndex = 431;
		this->textBox_ingotmin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ingotmin);
		// 
		// label131
		// 
		this->label131->AutoSize = true;
		this->label131->Location = System::Drawing::Point(7, 682);
		this->label131->Name = L"label131";
		this->label131->Size = System::Drawing::Size(56, 13);
		this->label131->TabIndex = 430;
		this->label131->Text = L"Ingot max:";
		// 
		// label130
		// 
		this->label130->AutoSize = true;
		this->label130->Location = System::Drawing::Point(7, 654);
		this->label130->Name = L"label130";
		this->label130->Size = System::Drawing::Size(53, 13);
		this->label130->TabIndex = 429;
		this->label130->Text = L"Ingot min:";
		// 
		// label73
		// 
		this->label73->AutoSize = true;
		this->label73->Location = System::Drawing::Point(3, 630);
		this->label73->Name = L"label73";
		this->label73->Size = System::Drawing::Size(54, 13);
		this->label73->TabIndex = 428;
		this->label73->Text = L"Zhenying:";
		// 
		// textBox_zhenying
		// 
		this->textBox_zhenying->Location = System::Drawing::Point(60, 626);
		this->textBox_zhenying->Name = L"textBox_zhenying";
		this->textBox_zhenying->Size = System::Drawing::Size(46, 20);
		this->textBox_zhenying->TabIndex = 427;
		this->textBox_zhenying->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_zhenying);
		// 
		// checkBox_frongfang03
		// 
		this->checkBox_frongfang03->AutoSize = true;
		this->checkBox_frongfang03->Location = System::Drawing::Point(229, 220);
		this->checkBox_frongfang03->Name = L"checkBox_frongfang03";
		this->checkBox_frongfang03->Size = System::Drawing::Size(103, 17);
		this->checkBox_frongfang03->TabIndex = 426;
		this->checkBox_frongfang03->Text = L"Finished task 03";
		this->checkBox_frongfang03->UseVisualStyleBackColor = true;
		// 
		// checkBox_frontfeng02
		// 
		this->checkBox_frontfeng02->AutoSize = true;
		this->checkBox_frontfeng02->Location = System::Drawing::Point(119, 219);
		this->checkBox_frontfeng02->Name = L"checkBox_frontfeng02";
		this->checkBox_frontfeng02->Size = System::Drawing::Size(103, 17);
		this->checkBox_frontfeng02->TabIndex = 425;
		this->checkBox_frontfeng02->Text = L"Finished task 02";
		this->checkBox_frontfeng02->UseVisualStyleBackColor = true;
		this->checkBox_frontfeng02->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontfeng02);
		// 
		// checkBox_frontfeng01
		// 
		this->checkBox_frontfeng01->AutoSize = true;
		this->checkBox_frontfeng01->Location = System::Drawing::Point(10, 220);
		this->checkBox_frontfeng01->Name = L"checkBox_frontfeng01";
		this->checkBox_frontfeng01->Size = System::Drawing::Size(103, 17);
		this->checkBox_frontfeng01->TabIndex = 424;
		this->checkBox_frontfeng01->Text = L"Finished task 01";
		this->checkBox_frontfeng01->UseVisualStyleBackColor = true;
		this->checkBox_frontfeng01->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontfeng01);
		// 
		// label143
		// 
		this->label143->AutoSize = true;
		this->label143->Location = System::Drawing::Point(111, 587);
		this->label143->Name = L"label143";
		this->label143->Size = System::Drawing::Size(93, 13);
		this->label143->TabIndex = 423;
		this->label143->Text = L"BangLing amount:";
		// 
		// textBox_blamon
		// 
		this->textBox_blamon->Location = System::Drawing::Point(206, 583);
		this->textBox_blamon->Name = L"textBox_blamon";
		this->textBox_blamon->Size = System::Drawing::Size(46, 20);
		this->textBox_blamon->TabIndex = 422;
		this->textBox_blamon->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_blamon);
		// 
		// checkBox_blded
		// 
		this->checkBox_blded->AutoSize = true;
		this->checkBox_blded->Location = System::Drawing::Point(115, 562);
		this->checkBox_blded->Name = L"checkBox_blded";
		this->checkBox_blded->Size = System::Drawing::Size(121, 17);
		this->checkBox_blded->TabIndex = 421;
		this->checkBox_blded->Text = L"BangLing deduction";
		this->checkBox_blded->UseVisualStyleBackColor = true;
		this->checkBox_blded->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_blded);
		// 
		// textBox_clanchmax
		// 
		this->textBox_clanchmax->Location = System::Drawing::Point(216, 536);
		this->textBox_clanchmax->Name = L"textBox_clanchmax";
		this->textBox_clanchmax->Size = System::Drawing::Size(46, 20);
		this->textBox_clanchmax->TabIndex = 420;
		this->textBox_clanchmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanchmax);
		// 
		// textBox_clanchmin
		// 
		this->textBox_clanchmin->Location = System::Drawing::Point(216, 514);
		this->textBox_clanchmin->Name = L"textBox_clanchmin";
		this->textBox_clanchmin->Size = System::Drawing::Size(46, 20);
		this->textBox_clanchmin->TabIndex = 419;
		this->textBox_clanchmin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanchmin);
		// 
		// label140
		// 
		this->label140->AutoSize = true;
		this->label140->Location = System::Drawing::Point(112, 542);
		this->label140->Name = L"label140";
		this->label140->Size = System::Drawing::Size(102, 13);
		this->label140->TabIndex = 418;
		this->label140->Text = L"Clan challenge max:";
		// 
		// label139
		// 
		this->label139->AutoSize = true;
		this->label139->Location = System::Drawing::Point(111, 517);
		this->label139->Name = L"label139";
		this->label139->Size = System::Drawing::Size(99, 13);
		this->label139->TabIndex = 417;
		this->label139->Text = L"Clan challenge min:";
		// 
		// textBox_clanmap
		// 
		this->textBox_clanmap->Location = System::Drawing::Point(551, 488);
		this->textBox_clanmap->Name = L"textBox_clanmap";
		this->textBox_clanmap->Size = System::Drawing::Size(46, 20);
		this->textBox_clanmap->TabIndex = 416;
		this->textBox_clanmap->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanmap);
		// 
		// label138
		// 
		this->label138->AutoSize = true;
		this->label138->Location = System::Drawing::Point(483, 490);
		this->label138->Name = L"label138";
		this->label138->Size = System::Drawing::Size(65, 13);
		this->label138->TabIndex = 415;
		this->label138->Text = L"Clan map id:";
		// 
		// textBox_clanskillid
		// 
		this->textBox_clanskillid->Location = System::Drawing::Point(551, 464);
		this->textBox_clanskillid->Name = L"textBox_clanskillid";
		this->textBox_clanskillid->Size = System::Drawing::Size(46, 20);
		this->textBox_clanskillid->TabIndex = 414;
		this->textBox_clanskillid->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanskillid);
		// 
		// label137
		// 
		this->label137->AutoSize = true;
		this->label137->Location = System::Drawing::Point(483, 467);
		this->label137->Name = L"label137";
		this->label137->Size = System::Drawing::Size(62, 13);
		this->label137->TabIndex = 413;
		this->label137->Text = L"Clan skill id:";
		// 
		// textBox_clanskillexpmax
		// 
		this->textBox_clanskillexpmax->Location = System::Drawing::Point(431, 488);
		this->textBox_clanskillexpmax->Name = L"textBox_clanskillexpmax";
		this->textBox_clanskillexpmax->Size = System::Drawing::Size(46, 20);
		this->textBox_clanskillexpmax->TabIndex = 412;
		this->textBox_clanskillexpmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanskillexpmax);
		// 
		// textBox_clanskillexpmin
		// 
		this->textBox_clanskillexpmin->Location = System::Drawing::Point(431, 464);
		this->textBox_clanskillexpmin->Name = L"textBox_clanskillexpmin";
		this->textBox_clanskillexpmin->Size = System::Drawing::Size(46, 20);
		this->textBox_clanskillexpmin->TabIndex = 411;
		this->textBox_clanskillexpmin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanskillexpmin);
		// 
		// label136
		// 
		this->label136->AutoSize = true;
		this->label136->Location = System::Drawing::Point(338, 491);
		this->label136->Name = L"label136";
		this->label136->Size = System::Drawing::Size(90, 13);
		this->label136->TabIndex = 410;
		this->label136->Text = L"Clan skill exp min:";
		// 
		// label135
		// 
		this->label135->AutoSize = true;
		this->label135->Location = System::Drawing::Point(338, 467);
		this->label135->Name = L"label135";
		this->label135->Size = System::Drawing::Size(90, 13);
		this->label135->TabIndex = 409;
		this->label135->Text = L"Clan skill exp min:";
		// 
		// textBox_clanskillmax
		// 
		this->textBox_clanskillmax->Location = System::Drawing::Point(286, 490);
		this->textBox_clanskillmax->Name = L"textBox_clanskillmax";
		this->textBox_clanskillmax->Size = System::Drawing::Size(46, 20);
		this->textBox_clanskillmax->TabIndex = 408;
		this->textBox_clanskillmax->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanskillmax);
		// 
		// textBox_clanskillmin
		// 
		this->textBox_clanskillmin->Location = System::Drawing::Point(286, 464);
		this->textBox_clanskillmin->Name = L"textBox_clanskillmin";
		this->textBox_clanskillmin->Size = System::Drawing::Size(46, 20);
		this->textBox_clanskillmin->TabIndex = 407;
		this->textBox_clanskillmin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanskillmin);
		// 
		// label133
		// 
		this->label133->AutoSize = true;
		this->label133->Location = System::Drawing::Point(193, 494);
		this->label133->Name = L"label133";
		this->label133->Size = System::Drawing::Size(90, 13);
		this->label133->TabIndex = 406;
		this->label133->Text = L"Clan skill Lvl max:";
		// 
		// label80
		// 
		this->label80->AutoSize = true;
		this->label80->Location = System::Drawing::Point(193, 467);
		this->label80->Name = L"label80";
		this->label80->Size = System::Drawing::Size(87, 13);
		this->label80->TabIndex = 405;
		this->label80->Text = L"Clan skill Lvl min:";
		// 
		// checkBox_clanlead
		// 
		this->checkBox_clanlead->AutoSize = true;
		this->checkBox_clanlead->Location = System::Drawing::Point(114, 494);
		this->checkBox_clanlead->Name = L"checkBox_clanlead";
		this->checkBox_clanlead->Size = System::Drawing::Size(79, 17);
		this->checkBox_clanlead->TabIndex = 404;
		this->checkBox_clanlead->Text = L"Clan leader";
		this->checkBox_clanlead->UseVisualStyleBackColor = true;
		this->checkBox_clanlead->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanlead);
		// 
		// checkBox_clanc
		// 
		this->checkBox_clanc->AutoSize = true;
		this->checkBox_clanc->Location = System::Drawing::Point(114, 468);
		this->checkBox_clanc->Name = L"checkBox_clanc";
		this->checkBox_clanc->Size = System::Drawing::Size(50, 17);
		this->checkBox_clanc->TabIndex = 403;
		this->checkBox_clanc->Text = L"Clan ";
		this->checkBox_clanc->UseVisualStyleBackColor = true;
		this->checkBox_clanc->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_clanc);
		// 
		// textBox_vtask
		// 
		this->textBox_vtask->Location = System::Drawing::Point(60, 581);
		this->textBox_vtask->Name = L"textBox_vtask";
		this->textBox_vtask->Size = System::Drawing::Size(46, 20);
		this->textBox_vtask->TabIndex = 402;
		this->textBox_vtask->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_vtask);
		// 
		// label77
		// 
		this->label77->AutoSize = true;
		this->label77->Location = System::Drawing::Point(4, 583);
		this->label77->Name = L"label77";
		this->label77->Size = System::Drawing::Size(56, 13);
		this->label77->TabIndex = 401;
		this->label77->Text = L"View task:";
		// 
		// label30
		// 
		this->label30->AutoSize = true;
		this->label30->Location = System::Drawing::Point(4, 561);
		this->label30->Name = L"label30";
		this->label30->Size = System::Drawing::Size(50, 13);
		this->label30->TabIndex = 400;
		this->label30->Text = L"View Lvl:";
		// 
		// textBox_viewlvl
		// 
		this->textBox_viewlvl->Location = System::Drawing::Point(60, 558);
		this->textBox_viewlvl->Name = L"textBox_viewlvl";
		this->textBox_viewlvl->Size = System::Drawing::Size(46, 20);
		this->textBox_viewlvl->TabIndex = 399;
		this->textBox_viewlvl->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_viewlvl);
		// 
		// checkBox_vunkn2
		// 
		this->checkBox_vunkn2->AutoSize = true;
		this->checkBox_vunkn2->Location = System::Drawing::Point(6, 536);
		this->checkBox_vunkn2->Name = L"checkBox_vunkn2";
		this->checkBox_vunkn2->Size = System::Drawing::Size(82, 17);
		this->checkBox_vunkn2->TabIndex = 398;
		this->checkBox_vunkn2->Text = L"View unkn2";
		this->checkBox_vunkn2->UseVisualStyleBackColor = true;
		this->checkBox_vunkn2->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_vunkn2);
		// 
		// checkBox_vunkn1
		// 
		this->checkBox_vunkn1->AutoSize = true;
		this->checkBox_vunkn1->Location = System::Drawing::Point(6, 513);
		this->checkBox_vunkn1->Name = L"checkBox_vunkn1";
		this->checkBox_vunkn1->Size = System::Drawing::Size(82, 17);
		this->checkBox_vunkn1->TabIndex = 397;
		this->checkBox_vunkn1->Text = L"View unkn1";
		this->checkBox_vunkn1->UseVisualStyleBackColor = true;
		this->checkBox_vunkn1->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_vunkn1);
		// 
		// checkBox_vpeach
		// 
		this->checkBox_vpeach->AutoSize = true;
		this->checkBox_vpeach->Location = System::Drawing::Point(6, 490);
		this->checkBox_vpeach->Name = L"checkBox_vpeach";
		this->checkBox_vpeach->Size = System::Drawing::Size(82, 17);
		this->checkBox_vpeach->TabIndex = 396;
		this->checkBox_vpeach->Text = L"View peach";
		this->checkBox_vpeach->UseVisualStyleBackColor = true;
		this->checkBox_vpeach->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_vpeach);
		// 
		// checkBox_vchef
		// 
		this->checkBox_vchef->AutoSize = true;
		this->checkBox_vchef->Location = System::Drawing::Point(7, 467);
		this->checkBox_vchef->Name = L"checkBox_vchef";
		this->checkBox_vchef->Size = System::Drawing::Size(73, 17);
		this->checkBox_vchef->TabIndex = 395;
		this->checkBox_vchef->Text = L"View chef";
		this->checkBox_vchef->UseVisualStyleBackColor = true;
		this->checkBox_vchef->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_vchef);
		// 
		// checkBox_view
		// 
		this->checkBox_view->AutoSize = true;
		this->checkBox_view->Location = System::Drawing::Point(8, 444);
		this->checkBox_view->Name = L"checkBox_view";
		this->checkBox_view->Size = System::Drawing::Size(74, 17);
		this->checkBox_view->TabIndex = 394;
		this->checkBox_view->Text = L"View view";
		this->checkBox_view->UseVisualStyleBackColor = true;
		this->checkBox_view->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_view);
		// 
		// checkBox_viewreq
		// 
		this->checkBox_viewreq->AutoSize = true;
		this->checkBox_viewreq->Location = System::Drawing::Point(9, 421);
		this->checkBox_viewreq->Name = L"checkBox_viewreq";
		this->checkBox_viewreq->Size = System::Drawing::Size(67, 17);
		this->checkBox_viewreq->TabIndex = 393;
		this->checkBox_viewreq->Text = L"View req";
		this->checkBox_viewreq->UseVisualStyleBackColor = true;
		this->checkBox_viewreq->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_viewreq);
		// 
		// checkBox_leavefail
		// 
		this->checkBox_leavefail->AutoSize = true;
		this->checkBox_leavefail->Location = System::Drawing::Point(423, 436);
		this->checkBox_leavefail->Name = L"checkBox_leavefail";
		this->checkBox_leavefail->Size = System::Drawing::Size(98, 17);
		this->checkBox_leavefail->TabIndex = 392;
		this->checkBox_leavefail->Text = L"Team leave fail";
		this->checkBox_leavefail->UseVisualStyleBackColor = true;
		this->checkBox_leavefail->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_leavefail);
		// 
		// checkBox_tamaccfail
		// 
		this->checkBox_tamaccfail->AutoSize = true;
		this->checkBox_tamaccfail->Location = System::Drawing::Point(332, 436);
		this->checkBox_tamaccfail->Name = L"checkBox_tamaccfail";
		this->checkBox_tamaccfail->Size = System::Drawing::Size(88, 17);
		this->checkBox_tamaccfail->TabIndex = 391;
		this->checkBox_tamaccfail->Text = L"Team join fail";
		this->checkBox_tamaccfail->UseVisualStyleBackColor = true;
		this->checkBox_tamaccfail->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_tamaccfail);
		// 
		// checkBox_leaderfail
		// 
		this->checkBox_leaderfail->AutoSize = true;
		this->checkBox_leaderfail->Location = System::Drawing::Point(233, 436);
		this->checkBox_leaderfail->Name = L"checkBox_leaderfail";
		this->checkBox_leaderfail->Size = System::Drawing::Size(101, 17);
		this->checkBox_leaderfail->TabIndex = 390;
		this->checkBox_leaderfail->Text = L"Team leader fail";
		this->checkBox_leaderfail->UseVisualStyleBackColor = true;
		this->checkBox_leaderfail->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_leaderfail);
		// 
		// checkBox_teamchecknum
		// 
		this->checkBox_teamchecknum->AutoSize = true;
		this->checkBox_teamchecknum->Location = System::Drawing::Point(119, 436);
		this->checkBox_teamchecknum->Name = L"checkBox_teamchecknum";
		this->checkBox_teamchecknum->Size = System::Drawing::Size(109, 17);
		this->checkBox_teamchecknum->TabIndex = 389;
		this->checkBox_teamchecknum->Text = L"Team check num";
		this->checkBox_teamchecknum->UseVisualStyleBackColor = true;
		this->checkBox_teamchecknum->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_teamchecknum);
		// 
		// checkBox_teamshare2
		// 
		this->checkBox_teamshare2->AutoSize = true;
		this->checkBox_teamshare2->Location = System::Drawing::Point(423, 413);
		this->checkBox_teamshare2->Name = L"checkBox_teamshare2";
		this->checkBox_teamshare2->Size = System::Drawing::Size(88, 17);
		this->checkBox_teamshare2->TabIndex = 388;
		this->checkBox_teamshare2->Text = L"Team share2";
		this->checkBox_teamshare2->UseVisualStyleBackColor = true;
		this->checkBox_teamshare2->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_teamshare2);
		// 
		// checkBox_teamunk
		// 
		this->checkBox_teamunk->AutoSize = true;
		this->checkBox_teamunk->Location = System::Drawing::Point(332, 413);
		this->checkBox_teamunk->Name = L"checkBox_teamunk";
		this->checkBox_teamunk->Size = System::Drawing::Size(74, 17);
		this->checkBox_teamunk->TabIndex = 387;
		this->checkBox_teamunk->Text = L"Team unk";
		this->checkBox_teamunk->UseVisualStyleBackColor = true;
		this->checkBox_teamunk->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_teamunk);
		// 
		// checkBox_teamshare
		// 
		this->checkBox_teamshare->AutoSize = true;
		this->checkBox_teamshare->Location = System::Drawing::Point(233, 413);
		this->checkBox_teamshare->Name = L"checkBox_teamshare";
		this->checkBox_teamshare->Size = System::Drawing::Size(82, 17);
		this->checkBox_teamshare->TabIndex = 386;
		this->checkBox_teamshare->Text = L"Team share";
		this->checkBox_teamshare->UseVisualStyleBackColor = true;
		// 
		// checkBox_teamask
		// 
		this->checkBox_teamask->AutoSize = true;
		this->checkBox_teamask->Location = System::Drawing::Point(119, 413);
		this->checkBox_teamask->Name = L"checkBox_teamask";
		this->checkBox_teamask->Size = System::Drawing::Size(78, 17);
		this->checkBox_teamask->TabIndex = 385;
		this->checkBox_teamask->Text = L"Team asks";
		this->checkBox_teamask->UseVisualStyleBackColor = true;
		this->checkBox_teamask->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_teamask);
		// 
		// label166
		// 
		this->label166->AutoSize = true;
		this->label166->Location = System::Drawing::Point(467, 560);
		this->label166->Name = L"label166";
		this->label166->Size = System::Drawing::Size(47, 13);
		this->label166->TabIndex = 384;
		this->label166->Text = L"Max PK:";
		// 
		// label164
		// 
		this->label164->AutoSize = true;
		this->label164->Location = System::Drawing::Point(467, 538);
		this->label164->Name = L"label164";
		this->label164->Size = System::Drawing::Size(44, 13);
		this->label164->TabIndex = 383;
		this->label164->Text = L"Min PK:";
		// 
		// textBox_maxpk
		// 
		this->textBox_maxpk->Location = System::Drawing::Point(520, 556);
		this->textBox_maxpk->Name = L"textBox_maxpk";
		this->textBox_maxpk->Size = System::Drawing::Size(42, 20);
		this->textBox_maxpk->TabIndex = 382;
		// 
		// textBox_minpk
		// 
		this->textBox_minpk->Location = System::Drawing::Point(520, 534);
		this->textBox_minpk->Name = L"textBox_minpk";
		this->textBox_minpk->Size = System::Drawing::Size(42, 20);
		this->textBox_minpk->TabIndex = 381;
		// 
		// checkBox_frontmarried
		// 
		this->checkBox_frontmarried->AutoSize = true;
		this->checkBox_frontmarried->Location = System::Drawing::Point(575, 418);
		this->checkBox_frontmarried->Name = L"checkBox_frontmarried";
		this->checkBox_frontmarried->Size = System::Drawing::Size(61, 17);
		this->checkBox_frontmarried->TabIndex = 380;
		this->checkBox_frontmarried->Text = L"Married";
		this->checkBox_frontmarried->UseVisualStyleBackColor = true;
		this->checkBox_frontmarried->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontmarried);
		// 
		// checkBox_frontunkn01
		// 
		this->checkBox_frontunkn01->AutoSize = true;
		this->checkBox_frontunkn01->Location = System::Drawing::Point(575, 395);
		this->checkBox_frontunkn01->Name = L"checkBox_frontunkn01";
		this->checkBox_frontunkn01->Size = System::Drawing::Size(72, 17);
		this->checkBox_frontunkn01->TabIndex = 379;
		this->checkBox_frontunkn01->Text = L"Unknown";
		this->checkBox_frontunkn01->UseVisualStyleBackColor = true;
		this->checkBox_frontunkn01->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontunkn01);
		// 
		// checkBox_licreq
		// 
		this->checkBox_licreq->AutoSize = true;
		this->checkBox_licreq->Location = System::Drawing::Point(575, 372);
		this->checkBox_licreq->Name = L"checkBox_licreq";
		this->checkBox_licreq->Size = System::Drawing::Size(58, 17);
		this->checkBox_licreq->TabIndex = 378;
		this->checkBox_licreq->Text = L"Lic req";
		this->checkBox_licreq->UseVisualStyleBackColor = true;
		this->checkBox_licreq->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_licreq);
		// 
		// checkBox_frontgen
		// 
		this->checkBox_frontgen->AutoSize = true;
		this->checkBox_frontgen->Location = System::Drawing::Point(575, 274);
		this->checkBox_frontgen->Name = L"checkBox_frontgen";
		this->checkBox_frontgen->Size = System::Drawing::Size(82, 17);
		this->checkBox_frontgen->TabIndex = 377;
		this->checkBox_frontgen->Text = L"Gender con";
		this->checkBox_frontgen->UseVisualStyleBackColor = true;
		this->checkBox_frontgen->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontgen);
		// 
		// textBox_unknfront
		// 
		this->textBox_unknfront->Location = System::Drawing::Point(578, 323);
		this->textBox_unknfront->Name = L"textBox_unknfront";
		this->textBox_unknfront->Size = System::Drawing::Size(65, 20);
		this->textBox_unknfront->TabIndex = 376;
		this->textBox_unknfront->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknfront);
		// 
		// checkBox_frontalliance
		// 
		this->checkBox_frontalliance->AutoSize = true;
		this->checkBox_frontalliance->Location = System::Drawing::Point(575, 349);
		this->checkBox_frontalliance->Name = L"checkBox_frontalliance";
		this->checkBox_frontalliance->Size = System::Drawing::Size(63, 17);
		this->checkBox_frontalliance->TabIndex = 375;
		this->checkBox_frontalliance->Text = L"Alliance";
		this->checkBox_frontalliance->UseVisualStyleBackColor = true;
		this->checkBox_frontalliance->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontalliance);
		// 
		// textBox_frontgender
		// 
		this->textBox_frontgender->Location = System::Drawing::Point(578, 297);
		this->textBox_frontgender->Name = L"textBox_frontgender";
		this->textBox_frontgender->Size = System::Drawing::Size(65, 20);
		this->textBox_frontgender->TabIndex = 374;
		this->textBox_frontgender->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_frontgender);
		// 
		// textBox_zhuanchonglv
		// 
		this->textBox_zhuanchonglv->Location = System::Drawing::Point(607, 248);
		this->textBox_zhuanchonglv->Name = L"textBox_zhuanchonglv";
		this->textBox_zhuanchonglv->Size = System::Drawing::Size(36, 20);
		this->textBox_zhuanchonglv->TabIndex = 372;
		this->textBox_zhuanchonglv->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_zhuanchonglv);
		// 
		// textBox_renwu
		// 
		this->textBox_renwu->Location = System::Drawing::Point(607, 207);
		this->textBox_renwu->Name = L"textBox_renwu";
		this->textBox_renwu->Size = System::Drawing::Size(36, 20);
		this->textBox_renwu->TabIndex = 370;
		this->textBox_renwu->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_renwu);
		// 
		// textBox_yaoqiu
		// 
		this->textBox_yaoqiu->Location = System::Drawing::Point(607, 168);
		this->textBox_yaoqiu->Name = L"textBox_yaoqiu";
		this->textBox_yaoqiu->Size = System::Drawing::Size(36, 20);
		this->textBox_yaoqiu->TabIndex = 367;
		this->textBox_yaoqiu->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_yaoqiu);
		// 
		// checkBox_tasknum
		// 
		this->checkBox_tasknum->AutoSize = true;
		this->checkBox_tasknum->Location = System::Drawing::Point(455, 120);
		this->checkBox_tasknum->Name = L"checkBox_tasknum";
		this->checkBox_tasknum->Size = System::Drawing::Size(73, 17);
		this->checkBox_tasknum->TabIndex = 366;
		this->checkBox_tasknum->Text = L"Task num";
		this->checkBox_tasknum->UseVisualStyleBackColor = true;
		this->checkBox_tasknum->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_tasknum);
		// 
		// textBox1
		// 
		this->textBox1->Location = System::Drawing::Point(325, 383);
		this->textBox1->Name = L"textBox1";
		this->textBox1->Size = System::Drawing::Size(107, 20);
		this->textBox1->TabIndex = 364;
		this->textBox1->TextChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::rename_job);
		// 
		// listBox_jobs
		// 
		this->listBox_jobs->FormattingEnabled = true;
		this->listBox_jobs->Location = System::Drawing::Point(326, 287);
		this->listBox_jobs->Name = L"listBox_jobs";
		this->listBox_jobs->Size = System::Drawing::Size(106, 95);
		this->listBox_jobs->TabIndex = 363;
		this->listBox_jobs->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_job);
		// 
		// label257
		// 
		this->label257->AutoSize = true;
		this->label257->Location = System::Drawing::Point(214, 764);
		this->label257->Name = L"label257";
		this->label257->Size = System::Drawing::Size(48, 13);
		this->label257->TabIndex = 362;
		this->label257->Text = L"Incense:";
		// 
		// label256
		// 
		this->label256->AutoSize = true;
		this->label256->Location = System::Drawing::Point(323, 723);
		this->label256->Name = L"label256";
		this->label256->Size = System::Drawing::Size(37, 13);
		this->label256->TabIndex = 361;
		this->label256->Text = L"Celan:";
		// 
		// label255
		// 
		this->label255->AutoSize = true;
		this->label255->Location = System::Drawing::Point(323, 701);
		this->label255->Name = L"label255";
		this->label255->Size = System::Drawing::Size(41, 13);
		this->label255->TabIndex = 360;
		this->label255->Text = L"Rayan:";
		// 
		// label254
		// 
		this->label254->AutoSize = true;
		this->label254->Location = System::Drawing::Point(323, 746);
		this->label254->Name = L"label254";
		this->label254->Size = System::Drawing::Size(34, 13);
		this->label254->TabIndex = 359;
		this->label254->Text = L"Forta:";
		// 
		// label253
		// 
		this->label253->AutoSize = true;
		this->label253->Location = System::Drawing::Point(323, 764);
		this->label253->Name = L"label253";
		this->label253->Size = System::Drawing::Size(37, 13);
		this->label253->TabIndex = 358;
		this->label253->Text = L"Voida:";
		// 
		// label252
		// 
		this->label252->AutoSize = true;
		this->label252->Location = System::Drawing::Point(323, 680);
		this->label252->Name = L"label252";
		this->label252->Size = System::Drawing::Size(38, 13);
		this->label252->TabIndex = 357;
		this->label252->Text = L"Arden:";
		// 
		// label251
		// 
		this->label251->AutoSize = true;
		this->label251->Location = System::Drawing::Point(323, 661);
		this->label251->Name = L"label251";
		this->label251->Size = System::Drawing::Size(31, 13);
		this->label251->TabIndex = 356;
		this->label251->Text = L"Balo:";
		// 
		// label250
		// 
		this->label250->AutoSize = true;
		this->label250->Location = System::Drawing::Point(550, 664);
		this->label250->Name = L"label250";
		this->label250->Size = System::Drawing::Size(51, 13);
		this->label250->TabIndex = 355;
		this->label250->Text = L"reserved:";
		// 
		// label249
		// 
		this->label249->AutoSize = true;
		this->label249->Location = System::Drawing::Point(552, 747);
		this->label249->Name = L"label249";
		this->label249->Size = System::Drawing::Size(45, 13);
		this->label249->TabIndex = 354;
		this->label249->Text = L"resrved:";
		// 
		// label248
		// 
		this->label248->AutoSize = true;
		this->label248->Location = System::Drawing::Point(550, 720);
		this->label248->Name = L"label248";
		this->label248->Size = System::Drawing::Size(51, 13);
		this->label248->TabIndex = 353;
		this->label248->Text = L"reserved:";
		// 
		// label247
		// 
		this->label247->AutoSize = true;
		this->label247->Location = System::Drawing::Point(550, 692);
		this->label247->Name = L"label247";
		this->label247->Size = System::Drawing::Size(51, 13);
		this->label247->TabIndex = 352;
		this->label247->Text = L"reserved:";
		// 
		// label246
		// 
		this->label246->AutoSize = true;
		this->label246->Location = System::Drawing::Point(428, 709);
		this->label246->Name = L"label246";
		this->label246->Size = System::Drawing::Size(53, 13);
		this->label246->TabIndex = 351;
		this->label246->Text = L"Expertise:";
		// 
		// label245
		// 
		this->label245->AutoSize = true;
		this->label245->Location = System::Drawing::Point(428, 687);
		this->label245->Name = L"label245";
		this->label245->Size = System::Drawing::Size(49, 13);
		this->label245->TabIndex = 350;
		this->label245->Text = L"Coulture:";
		// 
		// label244
		// 
		this->label244->AutoSize = true;
		this->label244->Location = System::Drawing::Point(428, 663);
		this->label244->Name = L"label244";
		this->label244->Size = System::Drawing::Size(56, 13);
		this->label244->TabIndex = 349;
		this->label244->Text = L"Romance:";
		// 
		// label243
		// 
		this->label243->AutoSize = true;
		this->label243->Location = System::Drawing::Point(428, 731);
		this->label243->Name = L"label243";
		this->label243->Size = System::Drawing::Size(33, 13);
		this->label243->TabIndex = 348;
		this->label243->Text = L"Piety:";
		// 
		// label87
		// 
		this->label87->AutoSize = true;
		this->label87->Location = System::Drawing::Point(214, 744);
		this->label87->Name = L"label87";
		this->label87->Size = System::Drawing::Size(36, 13);
		this->label87->TabIndex = 347;
		this->label87->Text = L"Lupin:";
		// 
		// label86
		// 
		this->label86->AutoSize = true;
		this->label86->Location = System::Drawing::Point(214, 722);
		this->label86->Name = L"label86";
		this->label86->Size = System::Drawing::Size(27, 13);
		this->label86->TabIndex = 346;
		this->label86->Text = L"Vim:";
		// 
		// label83
		// 
		this->label83->AutoSize = true;
		this->label83->Location = System::Drawing::Point(214, 701);
		this->label83->Name = L"label83";
		this->label83->Size = System::Drawing::Size(51, 13);
		this->label83->TabIndex = 345;
		this->label83->Text = L"Skysong:";
		// 
		// label69
		// 
		this->label69->AutoSize = true;
		this->label69->Location = System::Drawing::Point(214, 678);
		this->label69->Name = L"label69";
		this->label69->Size = System::Drawing::Size(45, 13);
		this->label69->TabIndex = 344;
		this->label69->Text = L"Jadeon:";
		// 
		// label58
		// 
		this->label58->AutoSize = true;
		this->label58->Location = System::Drawing::Point(215, 659);
		this->label58->Name = L"label58";
		this->label58->Size = System::Drawing::Size(37, 13);
		this->label58->TabIndex = 343;
		this->label58->Text = L"Modo:";
		// 
		// textBox_rep20
		// 
		this->textBox_rep20->Location = System::Drawing::Point(272, 762);
		this->textBox_rep20->Name = L"textBox_rep20";
		this->textBox_rep20->Size = System::Drawing::Size(48, 20);
		this->textBox_rep20->TabIndex = 342;
		this->textBox_rep20->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep20->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep20);
		// 
		// textBox_rep19
		// 
		this->textBox_rep19->Location = System::Drawing::Point(381, 719);
		this->textBox_rep19->Name = L"textBox_rep19";
		this->textBox_rep19->Size = System::Drawing::Size(47, 20);
		this->textBox_rep19->TabIndex = 341;
		this->textBox_rep19->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep19->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep19);
		// 
		// textBox_rep13
		// 
		this->textBox_rep13->Location = System::Drawing::Point(607, 660);
		this->textBox_rep13->Name = L"textBox_rep13";
		this->textBox_rep13->Size = System::Drawing::Size(45, 20);
		this->textBox_rep13->TabIndex = 340;
		this->textBox_rep13->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep13->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep13);
		// 
		// textBox_rep14
		// 
		this->textBox_rep14->Location = System::Drawing::Point(382, 655);
		this->textBox_rep14->Name = L"textBox_rep14";
		this->textBox_rep14->Size = System::Drawing::Size(46, 20);
		this->textBox_rep14->TabIndex = 339;
		this->textBox_rep14->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep14->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep14);
		// 
		// textBox_rep15
		// 
		this->textBox_rep15->Location = System::Drawing::Point(382, 676);
		this->textBox_rep15->Name = L"textBox_rep15";
		this->textBox_rep15->Size = System::Drawing::Size(46, 20);
		this->textBox_rep15->TabIndex = 338;
		this->textBox_rep15->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep15->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep15);
		// 
		// textBox_rep17
		// 
		this->textBox_rep17->Location = System::Drawing::Point(381, 740);
		this->textBox_rep17->Name = L"textBox_rep17";
		this->textBox_rep17->Size = System::Drawing::Size(47, 20);
		this->textBox_rep17->TabIndex = 337;
		this->textBox_rep17->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep17->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep17);
		// 
		// textBox_rep16
		// 
		this->textBox_rep16->Location = System::Drawing::Point(381, 761);
		this->textBox_rep16->Name = L"textBox_rep16";
		this->textBox_rep16->Size = System::Drawing::Size(47, 20);
		this->textBox_rep16->TabIndex = 336;
		this->textBox_rep16->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep16->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep16);
		// 
		// textBox_rep18
		// 
		this->textBox_rep18->Location = System::Drawing::Point(381, 698);
		this->textBox_rep18->Name = L"textBox_rep18";
		this->textBox_rep18->Size = System::Drawing::Size(47, 20);
		this->textBox_rep18->TabIndex = 335;
		this->textBox_rep18->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep18->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep18);
		// 
		// checkBox_hassheng
		// 
		this->checkBox_hassheng->AutoSize = true;
		this->checkBox_hassheng->Location = System::Drawing::Point(227, 629);
		this->checkBox_hassheng->Name = L"checkBox_hassheng";
		this->checkBox_hassheng->Size = System::Drawing::Size(80, 17);
		this->checkBox_hassheng->TabIndex = 334;
		this->checkBox_hassheng->Text = L"Honor/Rep";
		this->checkBox_hassheng->UseVisualStyleBackColor = true;
		this->checkBox_hassheng->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_hassheng);
		// 
		// textBox_rep7
		// 
		this->textBox_rep7->Location = System::Drawing::Point(486, 660);
		this->textBox_rep7->Name = L"textBox_rep7";
		this->textBox_rep7->Size = System::Drawing::Size(49, 20);
		this->textBox_rep7->TabIndex = 333;
		this->textBox_rep7->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep7->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep7);
		// 
		// textBox_rep8
		// 
		this->textBox_rep8->Location = System::Drawing::Point(486, 682);
		this->textBox_rep8->Name = L"textBox_rep8";
		this->textBox_rep8->Size = System::Drawing::Size(49, 20);
		this->textBox_rep8->TabIndex = 332;
		this->textBox_rep8->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep8->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep8);
		// 
		// textBox_rep12
		// 
		this->textBox_rep12->Location = System::Drawing::Point(607, 744);
		this->textBox_rep12->Name = L"textBox_rep12";
		this->textBox_rep12->Size = System::Drawing::Size(45, 20);
		this->textBox_rep12->TabIndex = 331;
		this->textBox_rep12->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep12->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep12);
		// 
		// textBox_rep11
		// 
		this->textBox_rep11->Location = System::Drawing::Point(607, 714);
		this->textBox_rep11->Name = L"textBox_rep11";
		this->textBox_rep11->Size = System::Drawing::Size(45, 20);
		this->textBox_rep11->TabIndex = 330;
		this->textBox_rep11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep11->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep11);
		// 
		// textBox_rep10
		// 
		this->textBox_rep10->Location = System::Drawing::Point(607, 687);
		this->textBox_rep10->Name = L"textBox_rep10";
		this->textBox_rep10->Size = System::Drawing::Size(45, 20);
		this->textBox_rep10->TabIndex = 329;
		this->textBox_rep10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep10->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep10);
		// 
		// textBox_rep9
		// 
		this->textBox_rep9->Location = System::Drawing::Point(486, 704);
		this->textBox_rep9->Name = L"textBox_rep9";
		this->textBox_rep9->Size = System::Drawing::Size(49, 20);
		this->textBox_rep9->TabIndex = 328;
		this->textBox_rep9->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep9->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep9);
		// 
		// textBox_rep6
		// 
		this->textBox_rep6->Location = System::Drawing::Point(486, 728);
		this->textBox_rep6->Name = L"textBox_rep6";
		this->textBox_rep6->Size = System::Drawing::Size(49, 20);
		this->textBox_rep6->TabIndex = 327;
		this->textBox_rep6->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep6->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep6);
		// 
		// textBox_rep5
		// 
		this->textBox_rep5->Location = System::Drawing::Point(272, 741);
		this->textBox_rep5->Name = L"textBox_rep5";
		this->textBox_rep5->Size = System::Drawing::Size(48, 20);
		this->textBox_rep5->TabIndex = 326;
		this->textBox_rep5->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep5->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep5);
		// 
		// textBox_rep4
		// 
		this->textBox_rep4->Location = System::Drawing::Point(272, 719);
		this->textBox_rep4->Name = L"textBox_rep4";
		this->textBox_rep4->Size = System::Drawing::Size(48, 20);
		this->textBox_rep4->TabIndex = 325;
		this->textBox_rep4->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep4->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep4);
		// 
		// textBox_rep3
		// 
		this->textBox_rep3->Location = System::Drawing::Point(272, 697);
		this->textBox_rep3->Name = L"textBox_rep3";
		this->textBox_rep3->Size = System::Drawing::Size(48, 20);
		this->textBox_rep3->TabIndex = 324;
		this->textBox_rep3->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep3->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep3);
		// 
		// textBox_rep2
		// 
		this->textBox_rep2->Location = System::Drawing::Point(272, 676);
		this->textBox_rep2->Name = L"textBox_rep2";
		this->textBox_rep2->Size = System::Drawing::Size(48, 20);
		this->textBox_rep2->TabIndex = 323;
		this->textBox_rep2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep2->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep2);
		// 
		// textBox_rep1
		// 
		this->textBox_rep1->Location = System::Drawing::Point(272, 655);
		this->textBox_rep1->Name = L"textBox_rep1";
		this->textBox_rep1->Size = System::Drawing::Size(48, 20);
		this->textBox_rep1->TabIndex = 322;
		this->textBox_rep1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox_rep1->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rep1);
		// 
		// checkBox_needgender
		// 
		this->checkBox_needgender->AutoSize = true;
		this->checkBox_needgender->Location = System::Drawing::Point(303, 608);
		this->checkBox_needgender->Name = L"checkBox_needgender";
		this->checkBox_needgender->Size = System::Drawing::Size(61, 17);
		this->checkBox_needgender->TabIndex = 320;
		this->checkBox_needgender->Text = L"Gender";
		this->checkBox_needgender->UseVisualStyleBackColor = true;
		this->checkBox_needgender->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_needgender);
		// 
		// textBox_autoval
		// 
		this->textBox_autoval->Location = System::Drawing::Point(389, 583);
		this->textBox_autoval->Name = L"textBox_autoval";
		this->textBox_autoval->Size = System::Drawing::Size(60, 20);
		this->textBox_autoval->TabIndex = 319;
		// 
		// checkBox_hasauto
		// 
		this->checkBox_hasauto->AutoSize = true;
		this->checkBox_hasauto->Location = System::Drawing::Point(303, 583);
		this->checkBox_hasauto->Name = L"checkBox_hasauto";
		this->checkBox_hasauto->Size = System::Drawing::Size(69, 17);
		this->checkBox_hasauto->TabIndex = 318;
		this->checkBox_hasauto->Text = L"Has auto";
		this->checkBox_hasauto->UseVisualStyleBackColor = true;
		this->checkBox_hasauto->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_hasauto);
		// 
		// textBox_workval
		// 
		this->textBox_workval->Location = System::Drawing::Point(389, 558);
		this->textBox_workval->Name = L"textBox_workval";
		this->textBox_workval->Size = System::Drawing::Size(60, 20);
		this->textBox_workval->TabIndex = 317;
		// 
		// checkBox_haswork
		// 
		this->checkBox_haswork->AutoSize = true;
		this->checkBox_haswork->Location = System::Drawing::Point(303, 560);
		this->checkBox_haswork->Name = L"checkBox_haswork";
		this->checkBox_haswork->Size = System::Drawing::Size(85, 17);
		this->checkBox_haswork->TabIndex = 316;
		this->checkBox_haswork->Text = L"Has working";
		this->checkBox_haswork->UseVisualStyleBackColor = true;
		this->checkBox_haswork->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_haswork);
		// 
		// checkBox_contribu
		// 
		this->checkBox_contribu->AutoSize = true;
		this->checkBox_contribu->Location = System::Drawing::Point(568, 559);
		this->checkBox_contribu->Name = L"checkBox_contribu";
		this->checkBox_contribu->Size = System::Drawing::Size(82, 17);
		this->checkBox_contribu->TabIndex = 315;
		this->checkBox_contribu->Text = L"Contribution";
		this->checkBox_contribu->UseVisualStyleBackColor = true;
		// 
		// checkBox_needclan
		// 
		this->checkBox_needclan->AutoSize = true;
		this->checkBox_needclan->Location = System::Drawing::Point(568, 536);
		this->checkBox_needclan->Name = L"checkBox_needclan";
		this->checkBox_needclan->Size = System::Drawing::Size(75, 17);
		this->checkBox_needclan->TabIndex = 314;
		this->checkBox_needclan->Text = L"Need clan";
		this->checkBox_needclan->UseVisualStyleBackColor = true;
		// 
		// textBox_hasnextval
		// 
		this->textBox_hasnextval->Location = System::Drawing::Point(389, 534);
		this->textBox_hasnextval->Name = L"textBox_hasnextval";
		this->textBox_hasnextval->Size = System::Drawing::Size(60, 20);
		this->textBox_hasnextval->TabIndex = 313;
		// 
		// checkBox_hasnext
		// 
		this->checkBox_hasnext->AutoSize = true;
		this->checkBox_hasnext->Location = System::Drawing::Point(304, 538);
		this->checkBox_hasnext->Name = L"checkBox_hasnext";
		this->checkBox_hasnext->Size = System::Drawing::Size(68, 17);
		this->checkBox_hasnext->TabIndex = 312;
		this->checkBox_hasnext->Text = L"Has next";
		this->checkBox_hasnext->UseVisualStyleBackColor = true;
		this->checkBox_hasnext->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_hasnext);
		// 
		// checkBox_rineedbag
		// 
		this->checkBox_rineedbag->AutoSize = true;
		this->checkBox_rineedbag->Location = System::Drawing::Point(9, 56);
		this->checkBox_rineedbag->Name = L"checkBox_rineedbag";
		this->checkBox_rineedbag->Size = System::Drawing::Size(103, 17);
		this->checkBox_rineedbag->TabIndex = 311;
		this->checkBox_rineedbag->Text = L"Need bag slots\?";
		this->checkBox_rineedbag->UseVisualStyleBackColor = true;
		this->checkBox_rineedbag->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rinedbag);
		// 
		// textBox_iunk11
		// 
		this->textBox_iunk11->Location = System::Drawing::Point(589, 128);
		this->textBox_iunk11->Name = L"textBox_iunk11";
		this->textBox_iunk11->Size = System::Drawing::Size(58, 20);
		this->textBox_iunk11->TabIndex = 310;
		this->textBox_iunk11->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_iunk11);
		// 
		// textBox_iunk10
		// 
		this->textBox_iunk10->Location = System::Drawing::Point(589, 102);
		this->textBox_iunk10->Name = L"textBox_iunk10";
		this->textBox_iunk10->Size = System::Drawing::Size(58, 20);
		this->textBox_iunk10->TabIndex = 309;
		this->textBox_iunk10->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_iunk10);
		// 
		// textBox_iunk09
		// 
		this->textBox_iunk09->Location = System::Drawing::Point(589, 76);
		this->textBox_iunk09->Name = L"textBox_iunk09";
		this->textBox_iunk09->Size = System::Drawing::Size(58, 20);
		this->textBox_iunk09->TabIndex = 308;
		this->textBox_iunk09->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_iunk09);
		// 
		// textBox_itunk07
		// 
		this->textBox_itunk07->Location = System::Drawing::Point(553, 50);
		this->textBox_itunk07->Name = L"textBox_itunk07";
		this->textBox_itunk07->Size = System::Drawing::Size(94, 20);
		this->textBox_itunk07->TabIndex = 307;
		this->textBox_itunk07->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_itunk07);
		// 
		// textBox_itunk06
		// 
		this->textBox_itunk06->Location = System::Drawing::Point(553, 17);
		this->textBox_itunk06->Name = L"textBox_itunk06";
		this->textBox_itunk06->Size = System::Drawing::Size(94, 20);
		this->textBox_itunk06->TabIndex = 306;
		this->textBox_itunk06->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_itunk06);
		// 
		// checkBox_iunk4
		// 
		this->checkBox_iunk4->AutoSize = true;
		this->checkBox_iunk4->Location = System::Drawing::Point(463, 19);
		this->checkBox_iunk4->Name = L"checkBox_iunk4";
		this->checkBox_iunk4->Size = System::Drawing::Size(84, 17);
		this->checkBox_iunk4->TabIndex = 305;
		this->checkBox_iunk4->Text = L"item_unk 04";
		this->checkBox_iunk4->UseVisualStyleBackColor = true;
		this->checkBox_iunk4->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_iunknown4);
		// 
		// checkBox_iunk5
		// 
		this->checkBox_iunk5->AutoSize = true;
		this->checkBox_iunk5->Location = System::Drawing::Point(463, 53);
		this->checkBox_iunk5->Name = L"checkBox_iunk5";
		this->checkBox_iunk5->Size = System::Drawing::Size(84, 17);
		this->checkBox_iunk5->TabIndex = 304;
		this->checkBox_iunk5->Text = L"item_unk 05";
		this->checkBox_iunk5->UseVisualStyleBackColor = true;
		this->checkBox_iunk5->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_iunknown5);
		// 
		// label222
		// 
		this->label222->AutoSize = true;
		this->label222->Location = System::Drawing::Point(329, 271);
		this->label222->Name = L"label222";
		this->label222->Size = System::Drawing::Size(93, 13);
		this->label222->TabIndex = 276;
		this->label222->Text = L"Required factions:";
		// 
		// label74
		// 
		this->label74->AutoSize = true;
		this->label74->Location = System::Drawing::Point(181, 885);
		this->label74->Name = L"label74";
		this->label74->Size = System::Drawing::Size(83, 13);
		this->label74->TabIndex = 275;
		this->label74->Text = L"Team Members:";
		// 
		// dataGridView_team_members
		// 
		this->dataGridView_team_members->AllowUserToAddRows = false;
		this->dataGridView_team_members->AllowUserToDeleteRows = false;
		this->dataGridView_team_members->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_team_members->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_team_members->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_team_members->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(10) {this->dataGridViewTextBoxColumn8, 
			this->dataGridViewTextBoxColumn9, this->dataGridViewTextBoxColumn10, this->dataGridViewTextBoxColumn11, this->dataGridViewTextBoxColumn12, 
			this->dataGridViewTextBoxColumn13, this->dataGridViewTextBoxColumn14, this->dataGridViewTextBoxColumn15, this->Column18, this->Column32});
		this->dataGridView_team_members->ContextMenuStrip = this->contextMenuStrip_team_members;
		this->dataGridView_team_members->Location = System::Drawing::Point(179, 908);
		this->dataGridView_team_members->MultiSelect = false;
		this->dataGridView_team_members->Name = L"dataGridView_team_members";
		this->dataGridView_team_members->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle58->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle58->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle58->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle58->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle58->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle58->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle58->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_team_members->RowHeadersDefaultCellStyle = dataGridViewCellStyle58;
		this->dataGridView_team_members->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_team_members->RowTemplate->Height = 18;
		this->dataGridView_team_members->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_team_members->Size = System::Drawing::Size(468, 81);
		this->dataGridView_team_members->TabIndex = 274;
		this->dataGridView_team_members->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_required_team_member_groups);
		// 
		// dataGridViewTextBoxColumn8
		// 
		this->dataGridViewTextBoxColumn8->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle50->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn8->DefaultCellStyle = dataGridViewCellStyle50;
		this->dataGridViewTextBoxColumn8->HeaderText = L"Min. LvL";
		this->dataGridViewTextBoxColumn8->Name = L"dataGridViewTextBoxColumn8";
		this->dataGridViewTextBoxColumn8->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn8->Width = 53;
		// 
		// dataGridViewTextBoxColumn9
		// 
		this->dataGridViewTextBoxColumn9->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle51->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn9->DefaultCellStyle = dataGridViewCellStyle51;
		this->dataGridViewTextBoxColumn9->HeaderText = L"Max. LvL";
		this->dataGridViewTextBoxColumn9->Name = L"dataGridViewTextBoxColumn9";
		this->dataGridViewTextBoxColumn9->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn9->Width = 56;
		// 
		// dataGridViewTextBoxColumn10
		// 
		this->dataGridViewTextBoxColumn10->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle52->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn10->DefaultCellStyle = dataGridViewCellStyle52;
		this->dataGridViewTextBoxColumn10->HeaderText = L"Race";
		this->dataGridViewTextBoxColumn10->Name = L"dataGridViewTextBoxColumn10";
		this->dataGridViewTextBoxColumn10->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn10->Width = 38;
		// 
		// dataGridViewTextBoxColumn11
		// 
		this->dataGridViewTextBoxColumn11->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle53->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn11->DefaultCellStyle = dataGridViewCellStyle53;
		this->dataGridViewTextBoxColumn11->HeaderText = L"Job";
		this->dataGridViewTextBoxColumn11->Name = L"dataGridViewTextBoxColumn11";
		this->dataGridViewTextBoxColumn11->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn11->Width = 29;
		// 
		// dataGridViewTextBoxColumn12
		// 
		this->dataGridViewTextBoxColumn12->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle54->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn12->DefaultCellStyle = dataGridViewCellStyle54;
		this->dataGridViewTextBoxColumn12->HeaderText = L"Gender";
		this->dataGridViewTextBoxColumn12->Name = L"dataGridViewTextBoxColumn12";
		this->dataGridViewTextBoxColumn12->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn12->Width = 47;
		// 
		// dataGridViewTextBoxColumn13
		// 
		this->dataGridViewTextBoxColumn13->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle55->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn13->DefaultCellStyle = dataGridViewCellStyle55;
		this->dataGridViewTextBoxColumn13->HeaderText = L"Min.#";
		this->dataGridViewTextBoxColumn13->Name = L"dataGridViewTextBoxColumn13";
		this->dataGridViewTextBoxColumn13->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn13->Width = 39;
		// 
		// dataGridViewTextBoxColumn14
		// 
		this->dataGridViewTextBoxColumn14->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle56->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn14->DefaultCellStyle = dataGridViewCellStyle56;
		this->dataGridViewTextBoxColumn14->HeaderText = L"Max.#";
		this->dataGridViewTextBoxColumn14->Name = L"dataGridViewTextBoxColumn14";
		this->dataGridViewTextBoxColumn14->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn14->Width = 42;
		// 
		// dataGridViewTextBoxColumn15
		// 
		this->dataGridViewTextBoxColumn15->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle57->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn15->DefaultCellStyle = dataGridViewCellStyle57;
		this->dataGridViewTextBoxColumn15->HeaderText = L"Quest";
		this->dataGridViewTextBoxColumn15->Name = L"dataGridViewTextBoxColumn15";
		this->dataGridViewTextBoxColumn15->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn15->Width = 40;
		// 
		// Column18
		// 
		this->Column18->HeaderText = L"unk1";
		this->Column18->Name = L"Column18";
		this->Column18->Width = 30;
		// 
		// Column32
		// 
		this->Column32->HeaderText = L"unk2";
		this->Column32->Name = L"Column32";
		this->Column32->Width = 40;
		// 
		// label68
		// 
		this->label68->AutoSize = true;
		this->label68->Location = System::Drawing::Point(7, 124);
		this->label68->Name = L"label68";
		this->label68->Size = System::Drawing::Size(63, 13);
		this->label68->TabIndex = 273;
		this->label68->Text = L"Given Items";
		// 
		// dataGridView_given_items
		// 
		this->dataGridView_given_items->AllowUserToAddRows = false;
		this->dataGridView_given_items->AllowUserToDeleteRows = false;
		this->dataGridView_given_items->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_given_items->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_given_items->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_given_items->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->dataGridViewTextBoxColumn24, 
			this->dataGridViewTextBoxColumn5, this->dataGridViewTextBoxColumn26, this->dataGridViewTextBoxColumn27, this->Column3});
		this->dataGridView_given_items->ContextMenuStrip = this->contextMenuStrip_given_items;
		this->dataGridView_given_items->Location = System::Drawing::Point(9, 143);
		this->dataGridView_given_items->MultiSelect = false;
		this->dataGridView_given_items->Name = L"dataGridView_given_items";
		this->dataGridView_given_items->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle63->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle63->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle63->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle63->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle63->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle63->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle63->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_given_items->RowHeadersDefaultCellStyle = dataGridViewCellStyle63;
		this->dataGridView_given_items->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_given_items->RowTemplate->Height = 18;
		this->dataGridView_given_items->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_given_items->Size = System::Drawing::Size(202, 70);
		this->dataGridView_given_items->TabIndex = 272;
		this->dataGridView_given_items->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_given_items);
		// 
		// dataGridViewTextBoxColumn24
		// 
		this->dataGridViewTextBoxColumn24->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle59->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn24->DefaultCellStyle = dataGridViewCellStyle59;
		this->dataGridViewTextBoxColumn24->HeaderText = L"ID";
		this->dataGridViewTextBoxColumn24->Name = L"dataGridViewTextBoxColumn24";
		this->dataGridViewTextBoxColumn24->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn24->Width = 23;
		// 
		// dataGridViewTextBoxColumn5
		// 
		this->dataGridViewTextBoxColumn5->HeaderText = L"Type";
		this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
		this->dataGridViewTextBoxColumn5->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->dataGridViewTextBoxColumn5->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->dataGridViewTextBoxColumn5->Width = 40;
		// 
		// dataGridViewTextBoxColumn26
		// 
		this->dataGridViewTextBoxColumn26->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle60->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn26->DefaultCellStyle = dataGridViewCellStyle60;
		this->dataGridViewTextBoxColumn26->HeaderText = L"#";
		this->dataGridViewTextBoxColumn26->Name = L"dataGridViewTextBoxColumn26";
		this->dataGridViewTextBoxColumn26->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn26->Width = 19;
		// 
		// dataGridViewTextBoxColumn27
		// 
		this->dataGridViewTextBoxColumn27->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle61->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn27->DefaultCellStyle = dataGridViewCellStyle61;
		this->dataGridViewTextBoxColumn27->HeaderText = L"%";
		this->dataGridViewTextBoxColumn27->Name = L"dataGridViewTextBoxColumn27";
		this->dataGridViewTextBoxColumn27->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn27->Width = 20;
		// 
		// Column3
		// 
		this->Column3->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle62->ForeColor = System::Drawing::Color::Gray;
		this->Column3->DefaultCellStyle = dataGridViewCellStyle62;
		this->Column3->HeaderText = L"Expire";
		this->Column3->Name = L"Column3";
		this->Column3->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// dataGridView_required_items
		// 
		this->dataGridView_required_items->AllowUserToAddRows = false;
		this->dataGridView_required_items->AllowUserToDeleteRows = false;
		this->dataGridView_required_items->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_required_items->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_required_items->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_required_items->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->dataGridViewTextBoxColumn4, 
			this->Column37, this->dataGridViewTextBoxColumn6, this->dataGridViewTextBoxColumn7, this->Column2});
		this->dataGridView_required_items->ContextMenuStrip = this->contextMenuStrip_required_items;
		this->dataGridView_required_items->Location = System::Drawing::Point(149, 37);
		this->dataGridView_required_items->MultiSelect = false;
		this->dataGridView_required_items->Name = L"dataGridView_required_items";
		this->dataGridView_required_items->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle68->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle68->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle68->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle68->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle68->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle68->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle68->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_items->RowHeadersDefaultCellStyle = dataGridViewCellStyle68;
		this->dataGridView_required_items->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_required_items->RowTemplate->Height = 18;
		this->dataGridView_required_items->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_items->Size = System::Drawing::Size(300, 87);
		this->dataGridView_required_items->TabIndex = 271;
		this->dataGridView_required_items->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_required_items);
		// 
		// dataGridViewTextBoxColumn4
		// 
		this->dataGridViewTextBoxColumn4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle64->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn4->DefaultCellStyle = dataGridViewCellStyle64;
		this->dataGridViewTextBoxColumn4->HeaderText = L"ID";
		this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
		this->dataGridViewTextBoxColumn4->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn4->Width = 23;
		// 
		// Column37
		// 
		this->Column37->HeaderText = L"Type";
		this->Column37->Name = L"Column37";
		this->Column37->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->Column37->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Automatic;
		this->Column37->Width = 40;
		// 
		// dataGridViewTextBoxColumn6
		// 
		this->dataGridViewTextBoxColumn6->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle65->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn6->DefaultCellStyle = dataGridViewCellStyle65;
		this->dataGridViewTextBoxColumn6->HeaderText = L"#";
		this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
		this->dataGridViewTextBoxColumn6->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn6->Width = 19;
		// 
		// dataGridViewTextBoxColumn7
		// 
		this->dataGridViewTextBoxColumn7->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle66->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn7->DefaultCellStyle = dataGridViewCellStyle66;
		this->dataGridViewTextBoxColumn7->HeaderText = L"%";
		this->dataGridViewTextBoxColumn7->Name = L"dataGridViewTextBoxColumn7";
		this->dataGridViewTextBoxColumn7->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn7->Width = 20;
		// 
		// Column2
		// 
		this->Column2->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle67->ForeColor = System::Drawing::Color::Gray;
		this->Column2->DefaultCellStyle = dataGridViewCellStyle67;
		this->Column2->HeaderText = L"Expire";
		this->Column2->Name = L"Column2";
		this->Column2->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// label2
		// 
		this->label2->AutoSize = true;
		this->label2->Location = System::Drawing::Point(214, 127);
		this->label2->Name = L"label2";
		this->label2->Size = System::Drawing::Size(55, 13);
		this->label2->TabIndex = 255;
		this->label2->Text = L"Get Items:";
		// 
		// dataGridView_required_get_items
		// 
		this->dataGridView_required_get_items->AllowUserToAddRows = false;
		this->dataGridView_required_get_items->AllowUserToDeleteRows = false;
		this->dataGridView_required_get_items->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_required_get_items->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_required_get_items->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_required_get_items->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->dataGridViewTextBoxColumn16, 
			this->dataGridViewTextBoxColumn17, this->dataGridViewTextBoxColumn18, this->dataGridViewTextBoxColumn19, this->Column17});
		this->dataGridView_required_get_items->ContextMenuStrip = this->contextMenuStrip_required_get_items;
		this->dataGridView_required_get_items->Location = System::Drawing::Point(217, 143);
		this->dataGridView_required_get_items->MultiSelect = false;
		this->dataGridView_required_get_items->Name = L"dataGridView_required_get_items";
		this->dataGridView_required_get_items->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle73->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle73->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle73->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle73->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle73->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle73->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle73->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_get_items->RowHeadersDefaultCellStyle = dataGridViewCellStyle73;
		this->dataGridView_required_get_items->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_required_get_items->RowTemplate->Height = 18;
		this->dataGridView_required_get_items->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_get_items->Size = System::Drawing::Size(219, 71);
		this->dataGridView_required_get_items->TabIndex = 256;
		this->dataGridView_required_get_items->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_required_get_items);
		// 
		// dataGridViewTextBoxColumn16
		// 
		this->dataGridViewTextBoxColumn16->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle69->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn16->DefaultCellStyle = dataGridViewCellStyle69;
		this->dataGridViewTextBoxColumn16->HeaderText = L"ID";
		this->dataGridViewTextBoxColumn16->Name = L"dataGridViewTextBoxColumn16";
		this->dataGridViewTextBoxColumn16->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn16->Width = 23;
		// 
		// dataGridViewTextBoxColumn17
		// 
		this->dataGridViewTextBoxColumn17->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->dataGridViewTextBoxColumn17->HeaderText = L"\?";
		this->dataGridViewTextBoxColumn17->Name = L"dataGridViewTextBoxColumn17";
		this->dataGridViewTextBoxColumn17->Resizable = System::Windows::Forms::DataGridViewTriState::True;
		this->dataGridViewTextBoxColumn17->Width = 18;
		// 
		// dataGridViewTextBoxColumn18
		// 
		this->dataGridViewTextBoxColumn18->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle70->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn18->DefaultCellStyle = dataGridViewCellStyle70;
		this->dataGridViewTextBoxColumn18->HeaderText = L"#";
		this->dataGridViewTextBoxColumn18->Name = L"dataGridViewTextBoxColumn18";
		this->dataGridViewTextBoxColumn18->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn18->Width = 19;
		// 
		// dataGridViewTextBoxColumn19
		// 
		this->dataGridViewTextBoxColumn19->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle71->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn19->DefaultCellStyle = dataGridViewCellStyle71;
		this->dataGridViewTextBoxColumn19->HeaderText = L"%";
		this->dataGridViewTextBoxColumn19->Name = L"dataGridViewTextBoxColumn19";
		this->dataGridViewTextBoxColumn19->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn19->Width = 20;
		// 
		// Column17
		// 
		this->Column17->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		dataGridViewCellStyle72->ForeColor = System::Drawing::Color::Gray;
		this->Column17->DefaultCellStyle = dataGridViewCellStyle72;
		this->Column17->HeaderText = L"Expire";
		this->Column17->Name = L"Column17";
		this->Column17->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		// 
		// label1
		// 
		this->label1->AutoSize = true;
		this->label1->Location = System::Drawing::Point(300, 791);
		this->label1->Name = L"label1";
		this->label1->Size = System::Drawing::Size(53, 13);
		this->label1->TabIndex = 251;
		this->label1->Text = L"Monsters:";
		// 
		// dataGridView_required_chases
		// 
		this->dataGridView_required_chases->AllowUserToAddRows = false;
		this->dataGridView_required_chases->AllowUserToDeleteRows = false;
		this->dataGridView_required_chases->BackgroundColor = System::Drawing::SystemColors::Window;
		this->dataGridView_required_chases->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_required_chases->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_required_chases->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {this->dataGridViewTextBoxColumn20, 
			this->dataGridViewTextBoxColumn21, this->dataGridViewTextBoxColumn22, this->dataGridViewTextBoxColumn23, this->Column14, this->Column15, 
			this->Column16});
		this->dataGridView_required_chases->ContextMenuStrip = this->contextMenuStrip_chases;
		this->dataGridView_required_chases->Location = System::Drawing::Point(300, 809);
		this->dataGridView_required_chases->MultiSelect = false;
		this->dataGridView_required_chases->Name = L"dataGridView_required_chases";
		this->dataGridView_required_chases->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle78->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle78->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle78->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle78->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle78->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle78->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle78->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_chases->RowHeadersDefaultCellStyle = dataGridViewCellStyle78;
		this->dataGridView_required_chases->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_required_chases->RowTemplate->Height = 18;
		this->dataGridView_required_chases->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_required_chases->Size = System::Drawing::Size(347, 80);
		this->dataGridView_required_chases->TabIndex = 252;
		this->dataGridView_required_chases->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_required_chases);
		// 
		// dataGridViewTextBoxColumn20
		// 
		this->dataGridViewTextBoxColumn20->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle74->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn20->DefaultCellStyle = dataGridViewCellStyle74;
		this->dataGridViewTextBoxColumn20->HeaderText = L"Mob";
		this->dataGridViewTextBoxColumn20->Name = L"dataGridViewTextBoxColumn20";
		this->dataGridViewTextBoxColumn20->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn20->Width = 33;
		// 
		// dataGridViewTextBoxColumn21
		// 
		this->dataGridViewTextBoxColumn21->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle75->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn21->DefaultCellStyle = dataGridViewCellStyle75;
		this->dataGridViewTextBoxColumn21->HeaderText = L"#";
		this->dataGridViewTextBoxColumn21->Name = L"dataGridViewTextBoxColumn21";
		this->dataGridViewTextBoxColumn21->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn21->Width = 19;
		// 
		// dataGridViewTextBoxColumn22
		// 
		this->dataGridViewTextBoxColumn22->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle76->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn22->DefaultCellStyle = dataGridViewCellStyle76;
		this->dataGridViewTextBoxColumn22->HeaderText = L"Drop";
		this->dataGridViewTextBoxColumn22->Name = L"dataGridViewTextBoxColumn22";
		this->dataGridViewTextBoxColumn22->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn22->Width = 35;
		// 
		// dataGridViewTextBoxColumn23
		// 
		this->dataGridViewTextBoxColumn23->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		dataGridViewCellStyle77->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
		this->dataGridViewTextBoxColumn23->DefaultCellStyle = dataGridViewCellStyle77;
		this->dataGridViewTextBoxColumn23->HeaderText = L"#";
		this->dataGridViewTextBoxColumn23->Name = L"dataGridViewTextBoxColumn23";
		this->dataGridViewTextBoxColumn23->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
		this->dataGridViewTextBoxColumn23->Width = 19;
		// 
		// Column14
		// 
		this->Column14->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->Column14->HeaderText = L"\?";
		this->Column14->Name = L"Column14";
		this->Column14->Width = 37;
		// 
		// Column15
		// 
		this->Column15->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->Column15->HeaderText = L"%";
		this->Column15->Name = L"Column15";
		this->Column15->Width = 39;
		// 
		// Column16
		// 
		this->Column16->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::AllCells;
		this->Column16->HeaderText = L"\?";
		this->Column16->Name = L"Column16";
		this->Column16->Width = 37;
		// 
		// textBox_required_quests_undone_1
		// 
		this->textBox_required_quests_undone_1->Location = System::Drawing::Point(119, 279);
		this->textBox_required_quests_undone_1->Name = L"textBox_required_quests_undone_1";
		this->textBox_required_quests_undone_1->Size = System::Drawing::Size(80, 20);
		this->textBox_required_quests_undone_1->TabIndex = 11;
		this->textBox_required_quests_undone_1->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_undone);
		// 
		// textBox_required_quests_undone_2
		// 
		this->textBox_required_quests_undone_2->Location = System::Drawing::Point(119, 305);
		this->textBox_required_quests_undone_2->Name = L"textBox_required_quests_undone_2";
		this->textBox_required_quests_undone_2->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_undone_2->TabIndex = 12;
		this->textBox_required_quests_undone_2->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_undone);
		// 
		// textBox_required_quests_undone_3
		// 
		this->textBox_required_quests_undone_3->Location = System::Drawing::Point(119, 331);
		this->textBox_required_quests_undone_3->Name = L"textBox_required_quests_undone_3";
		this->textBox_required_quests_undone_3->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_undone_3->TabIndex = 13;
		this->textBox_required_quests_undone_3->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_undone);
		// 
		// textBox_required_quests_undone_5
		// 
		this->textBox_required_quests_undone_5->Location = System::Drawing::Point(119, 383);
		this->textBox_required_quests_undone_5->Name = L"textBox_required_quests_undone_5";
		this->textBox_required_quests_undone_5->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_undone_5->TabIndex = 15;
		this->textBox_required_quests_undone_5->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_undone);
		// 
		// textBox_required_quests_undone_4
		// 
		this->textBox_required_quests_undone_4->Location = System::Drawing::Point(119, 357);
		this->textBox_required_quests_undone_4->Name = L"textBox_required_quests_undone_4";
		this->textBox_required_quests_undone_4->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_undone_4->TabIndex = 14;
		this->textBox_required_quests_undone_4->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_undone);
		// 
		// textBox_unknown_44
		// 
		this->textBox_unknown_44->Location = System::Drawing::Point(313, 629);
		this->textBox_unknown_44->Name = L"textBox_unknown_44";
		this->textBox_unknown_44->Size = System::Drawing::Size(334, 20);
		this->textBox_unknown_44->TabIndex = 207;
		this->textBox_unknown_44->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_44);
		// 
		// checkBox_required_be_gm
		// 
		this->checkBox_required_be_gm->AutoSize = true;
		this->checkBox_required_be_gm->Location = System::Drawing::Point(517, 413);
		this->checkBox_required_be_gm->Name = L"checkBox_required_be_gm";
		this->checkBox_required_be_gm->Size = System::Drawing::Size(43, 17);
		this->checkBox_required_be_gm->TabIndex = 193;
		this->checkBox_required_be_gm->Text = L"GM";
		this->checkBox_required_be_gm->UseVisualStyleBackColor = true;
		this->checkBox_required_be_gm->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_be_gm);
		// 
		// checkBox_required_be_married
		// 
		this->checkBox_required_be_married->AutoSize = true;
		this->checkBox_required_be_married->Location = System::Drawing::Point(470, 584);
		this->checkBox_required_be_married->Name = L"checkBox_required_be_married";
		this->checkBox_required_be_married->Size = System::Drawing::Size(61, 17);
		this->checkBox_required_be_married->TabIndex = 189;
		this->checkBox_required_be_married->Text = L"Married";
		this->checkBox_required_be_married->UseVisualStyleBackColor = true;
		this->checkBox_required_be_married->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_be_married);
		// 
		// comboBox_required_gender
		// 
		this->comboBox_required_gender->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
		this->comboBox_required_gender->FormattingEnabled = true;
		this->comboBox_required_gender->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"All", L"Male", L"Female"});
		this->comboBox_required_gender->Location = System::Drawing::Point(389, 604);
		this->comboBox_required_gender->Name = L"comboBox_required_gender";
		this->comboBox_required_gender->Size = System::Drawing::Size(60, 21);
		this->comboBox_required_gender->TabIndex = 182;
		this->comboBox_required_gender->SelectedIndexChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_gender);
		// 
		// textBox_required_quests_done_1
		// 
		this->textBox_required_quests_done_1->Location = System::Drawing::Point(10, 279);
		this->textBox_required_quests_done_1->Name = L"textBox_required_quests_done_1";
		this->textBox_required_quests_done_1->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_done_1->TabIndex = 165;
		this->textBox_required_quests_done_1->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_done);
		// 
		// textBox_required_quests_done_2
		// 
		this->textBox_required_quests_done_2->Location = System::Drawing::Point(10, 305);
		this->textBox_required_quests_done_2->Name = L"textBox_required_quests_done_2";
		this->textBox_required_quests_done_2->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_done_2->TabIndex = 166;
		this->textBox_required_quests_done_2->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_done);
		// 
		// textBox_required_quests_done_3
		// 
		this->textBox_required_quests_done_3->Location = System::Drawing::Point(10, 331);
		this->textBox_required_quests_done_3->Name = L"textBox_required_quests_done_3";
		this->textBox_required_quests_done_3->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_done_3->TabIndex = 167;
		this->textBox_required_quests_done_3->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_done);
		// 
		// textBox_required_quests_done_4
		// 
		this->textBox_required_quests_done_4->Location = System::Drawing::Point(10, 357);
		this->textBox_required_quests_done_4->Name = L"textBox_required_quests_done_4";
		this->textBox_required_quests_done_4->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_done_4->TabIndex = 168;
		this->textBox_required_quests_done_4->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_done);
		// 
		// textBox_required_quests_done_5
		// 
		this->textBox_required_quests_done_5->Location = System::Drawing::Point(10, 383);
		this->textBox_required_quests_done_5->Name = L"textBox_required_quests_done_5";
		this->textBox_required_quests_done_5->Size = System::Drawing::Size(81, 20);
		this->textBox_required_quests_done_5->TabIndex = 169;
		this->textBox_required_quests_done_5->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_quests_done);
		// 
		// textBox_required_items_unknown
		// 
		this->textBox_required_items_unknown->Location = System::Drawing::Point(8, 95);
		this->textBox_required_items_unknown->Name = L"textBox_required_items_unknown";
		this->textBox_required_items_unknown->Size = System::Drawing::Size(128, 20);
		this->textBox_required_items_unknown->TabIndex = 147;
		this->textBox_required_items_unknown->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_items_unknown);
		// 
		// label70
		// 
		this->label70->AutoSize = true;
		this->label70->Location = System::Drawing::Point(6, 79);
		this->label70->Name = L"label70";
		this->label70->Size = System::Drawing::Size(130, 13);
		this->label70->TabIndex = 146;
		this->label70->Text = L"Required Items Unknown:";
		// 
		// checkBox_unknown_27
		// 
		this->checkBox_unknown_27->AutoSize = true;
		this->checkBox_unknown_27->Location = System::Drawing::Point(9, 36);
		this->checkBox_unknown_27->Name = L"checkBox_unknown_27";
		this->checkBox_unknown_27->Size = System::Drawing::Size(97, 17);
		this->checkBox_unknown_27->TabIndex = 144;
		this->checkBox_unknown_27->Text = L"Required Items";
		this->checkBox_unknown_27->UseVisualStyleBackColor = true;
		this->checkBox_unknown_27->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_unknown_27);
		// 
		// textBox_instant_pay_coins
		// 
		this->textBox_instant_pay_coins->Location = System::Drawing::Point(103, 911);
		this->textBox_instant_pay_coins->Name = L"textBox_instant_pay_coins";
		this->textBox_instant_pay_coins->Size = System::Drawing::Size(48, 20);
		this->textBox_instant_pay_coins->TabIndex = 68;
		this->textBox_instant_pay_coins->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_instant_pay_coins);
		// 
		// label65
		// 
		this->label65->AutoSize = true;
		this->label65->Location = System::Drawing::Point(6, 914);
		this->label65->Name = L"label65";
		this->label65->Size = System::Drawing::Size(92, 13);
		this->label65->TabIndex = 67;
		this->label65->Text = L"Instant Pay Coins:";
		// 
		// dataGridView_tasknum
		// 
		this->dataGridView_tasknum->AllowUserToAddRows = false;
		this->dataGridView_tasknum->AllowUserToDeleteRows = false;
		this->dataGridView_tasknum->BackgroundColor = System::Drawing::SystemColors::ButtonHighlight;
		this->dataGridView_tasknum->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		this->dataGridView_tasknum->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
		this->dataGridView_tasknum->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {this->Column19, 
			this->Column20});
		this->dataGridView_tasknum->Location = System::Drawing::Point(443, 143);
		this->dataGridView_tasknum->MultiSelect = false;
		this->dataGridView_tasknum->Name = L"dataGridView_tasknum";
		this->dataGridView_tasknum->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Single;
		dataGridViewCellStyle79->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
		dataGridViewCellStyle79->BackColor = System::Drawing::SystemColors::Control;
		dataGridViewCellStyle79->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
		dataGridViewCellStyle79->ForeColor = System::Drawing::SystemColors::WindowText;
		dataGridViewCellStyle79->SelectionBackColor = System::Drawing::SystemColors::Highlight;
		dataGridViewCellStyle79->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
		dataGridViewCellStyle79->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_tasknum->RowHeadersDefaultCellStyle = dataGridViewCellStyle79;
		this->dataGridView_tasknum->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToFirstHeader;
		this->dataGridView_tasknum->RowTemplate->Height = 18;
		this->dataGridView_tasknum->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
		this->dataGridView_tasknum->Size = System::Drawing::Size(137, 125);
		this->dataGridView_tasknum->TabIndex = 365;
		this->dataGridView_tasknum->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &zxTASKeditWindow::change_tasknumgrid);
		// 
		// Column19
		// 
		this->Column19->HeaderText = L"ID";
		this->Column19->Name = L"Column19";
		this->Column19->Width = 30;
		// 
		// Column20
		// 
		this->Column20->HeaderText = L"num";
		this->Column20->Name = L"Column20";
		this->Column20->Width = 30;
		// 
		// tabPage6
		// 
		this->tabPage6->AutoScroll = true;
		this->tabPage6->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage6->Controls->Add(this->groupBox1);
		this->tabPage6->Controls->Add(this->groupBox_reward_items);
		this->tabPage6->Controls->Add(this->groupBox_reward);
		this->tabPage6->Controls->Add(this->groupBox_reward_selector);
		this->tabPage6->Location = System::Drawing::Point(4, 25);
		this->tabPage6->Margin = System::Windows::Forms::Padding(0);
		this->tabPage6->Name = L"tabPage6";
		this->tabPage6->Size = System::Drawing::Size(698, 578);
		this->tabPage6->TabIndex = 5;
		this->tabPage6->Text = L"Rewards";
		this->tabPage6->UseVisualStyleBackColor = true;
		// 
		// groupBox1
		// 
		this->groupBox1->Controls->Add(this->textBox_rewRenZong);
		this->groupBox1->Controls->Add(this->textBox_rewDiZong);
		this->groupBox1->Controls->Add(this->textBox_TianZong);
		this->groupBox1->Controls->Add(this->textBox_ZhanXun);
		this->groupBox1->Controls->Add(this->textBox_ZhanJi);
		this->groupBox1->Controls->Add(this->textBox_FenXiang);
		this->groupBox1->Controls->Add(this->textBox_TianHua);
		this->groupBox1->Controls->Add(this->textBox_HuaiGuang);
		this->groupBox1->Controls->Add(this->textBox_rewreptaihao);
		this->groupBox1->Controls->Add(this->textBox_ChenHuang);
		this->groupBox1->Controls->Add(this->textBox_LieShan);
		this->groupBox1->Controls->Add(this->textBox_rewJiuLi);
		this->groupBox1->Controls->Add(this->label203);
		this->groupBox1->Controls->Add(this->label202);
		this->groupBox1->Controls->Add(this->label201);
		this->groupBox1->Controls->Add(this->label200);
		this->groupBox1->Controls->Add(this->label199);
		this->groupBox1->Controls->Add(this->label195);
		this->groupBox1->Controls->Add(this->label194);
		this->groupBox1->Controls->Add(this->label191);
		this->groupBox1->Controls->Add(this->label190);
		this->groupBox1->Controls->Add(this->label189);
		this->groupBox1->Controls->Add(this->label176);
		this->groupBox1->Controls->Add(this->label175);
		this->groupBox1->Controls->Add(this->textBox_runk15);
		this->groupBox1->Controls->Add(this->label172);
		this->groupBox1->Controls->Add(this->textBox_rewrepfoyuan);
		this->groupBox1->Controls->Add(this->label165);
		this->groupBox1->Controls->Add(this->textBox_rewmoxing);
		this->groupBox1->Controls->Add(this->label163);
		this->groupBox1->Controls->Add(this->textBox_DaoXin);
		this->groupBox1->Controls->Add(this->label162);
		this->groupBox1->Controls->Add(this->textBox_rewshide);
		this->groupBox1->Controls->Add(this->label161);
		this->groupBox1->Controls->Add(this->textBox_rewwencai);
		this->groupBox1->Controls->Add(this->label160);
		this->groupBox1->Controls->Add(this->textBox_rewQingyuan);
		this->groupBox1->Controls->Add(this->label159);
		this->groupBox1->Controls->Add(this->textBox_rewrepzhongyi);
		this->groupBox1->Controls->Add(this->label158);
		this->groupBox1->Controls->Add(this->textBox_rewreplupi);
		this->groupBox1->Controls->Add(this->label157);
		this->groupBox1->Controls->Add(this->textBox_rewrepvim);
		this->groupBox1->Controls->Add(this->label156);
		this->groupBox1->Controls->Add(this->textBox_rewrepsky);
		this->groupBox1->Controls->Add(this->label155);
		this->groupBox1->Controls->Add(this->textBox_rewrepjad);
		this->groupBox1->Controls->Add(this->label154);
		this->groupBox1->Controls->Add(this->textBox_rewrepmodo);
		this->groupBox1->Controls->Add(this->label153);
		this->groupBox1->Controls->Add(this->textBox_runk25);
		this->groupBox1->Controls->Add(this->label193);
		this->groupBox1->Controls->Add(this->label223);
		this->groupBox1->Controls->Add(this->textBox_runk26);
		this->groupBox1->Location = System::Drawing::Point(3, 814);
		this->groupBox1->Name = L"groupBox1";
		this->groupBox1->Size = System::Drawing::Size(655, 232);
		this->groupBox1->TabIndex = 4;
		this->groupBox1->TabStop = false;
		this->groupBox1->Text = L"REP/Honor";
		// 
		// textBox_rewRenZong
		// 
		this->textBox_rewRenZong->Location = System::Drawing::Point(580, 45);
		this->textBox_rewRenZong->Name = L"textBox_rewRenZong";
		this->textBox_rewRenZong->Size = System::Drawing::Size(71, 20);
		this->textBox_rewRenZong->TabIndex = 373;
		this->textBox_rewRenZong->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewRenZong);
		// 
		// textBox_rewDiZong
		// 
		this->textBox_rewDiZong->Location = System::Drawing::Point(580, 162);
		this->textBox_rewDiZong->Name = L"textBox_rewDiZong";
		this->textBox_rewDiZong->Size = System::Drawing::Size(71, 20);
		this->textBox_rewDiZong->TabIndex = 372;
		this->textBox_rewDiZong->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewDiZong);
		// 
		// textBox_TianZong
		// 
		this->textBox_TianZong->Location = System::Drawing::Point(580, 132);
		this->textBox_TianZong->Name = L"textBox_TianZong";
		this->textBox_TianZong->Size = System::Drawing::Size(71, 20);
		this->textBox_TianZong->TabIndex = 371;
		this->textBox_TianZong->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_TianZong);
		// 
		// textBox_ZhanXun
		// 
		this->textBox_ZhanXun->Location = System::Drawing::Point(580, 104);
		this->textBox_ZhanXun->Name = L"textBox_ZhanXun";
		this->textBox_ZhanXun->Size = System::Drawing::Size(71, 20);
		this->textBox_ZhanXun->TabIndex = 370;
		this->textBox_ZhanXun->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ZhanXun);
		// 
		// textBox_ZhanJi
		// 
		this->textBox_ZhanJi->Location = System::Drawing::Point(580, 73);
		this->textBox_ZhanJi->Name = L"textBox_ZhanJi";
		this->textBox_ZhanJi->Size = System::Drawing::Size(71, 20);
		this->textBox_ZhanJi->TabIndex = 369;
		this->textBox_ZhanJi->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ZhanJi);
		// 
		// textBox_FenXiang
		// 
		this->textBox_FenXiang->Location = System::Drawing::Point(68, 169);
		this->textBox_FenXiang->Name = L"textBox_FenXiang";
		this->textBox_FenXiang->Size = System::Drawing::Size(100, 20);
		this->textBox_FenXiang->TabIndex = 368;
		this->textBox_FenXiang->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_FenXiang);
		// 
		// textBox_TianHua
		// 
		this->textBox_TianHua->Location = System::Drawing::Point(235, 108);
		this->textBox_TianHua->Name = L"textBox_TianHua";
		this->textBox_TianHua->Size = System::Drawing::Size(112, 20);
		this->textBox_TianHua->TabIndex = 367;
		this->textBox_TianHua->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_TianHua);
		// 
		// textBox_HuaiGuang
		// 
		this->textBox_HuaiGuang->Location = System::Drawing::Point(235, 76);
		this->textBox_HuaiGuang->Name = L"textBox_HuaiGuang";
		this->textBox_HuaiGuang->Size = System::Drawing::Size(112, 20);
		this->textBox_HuaiGuang->TabIndex = 366;
		this->textBox_HuaiGuang->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_HuaiGuang);
		// 
		// textBox_rewreptaihao
		// 
		this->textBox_rewreptaihao->Location = System::Drawing::Point(235, 135);
		this->textBox_rewreptaihao->Name = L"textBox_rewreptaihao";
		this->textBox_rewreptaihao->Size = System::Drawing::Size(112, 20);
		this->textBox_rewreptaihao->TabIndex = 365;
		this->textBox_rewreptaihao->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewreptaihao);
		// 
		// textBox_ChenHuang
		// 
		this->textBox_ChenHuang->Location = System::Drawing::Point(235, 166);
		this->textBox_ChenHuang->Name = L"textBox_ChenHuang";
		this->textBox_ChenHuang->Size = System::Drawing::Size(112, 20);
		this->textBox_ChenHuang->TabIndex = 364;
		this->textBox_ChenHuang->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_ChenHuang);
		// 
		// textBox_LieShan
		// 
		this->textBox_LieShan->Location = System::Drawing::Point(235, 45);
		this->textBox_LieShan->Name = L"textBox_LieShan";
		this->textBox_LieShan->Size = System::Drawing::Size(112, 20);
		this->textBox_LieShan->TabIndex = 363;
		this->textBox_LieShan->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_LieShan);
		// 
		// textBox_rewJiuLi
		// 
		this->textBox_rewJiuLi->Location = System::Drawing::Point(235, 16);
		this->textBox_rewJiuLi->Name = L"textBox_rewJiuLi";
		this->textBox_rewJiuLi->Size = System::Drawing::Size(112, 20);
		this->textBox_rewJiuLi->TabIndex = 362;
		this->textBox_rewJiuLi->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewJiuLi);
		// 
		// label203
		// 
		this->label203->AutoSize = true;
		this->label203->Location = System::Drawing::Point(174, 169);
		this->label203->Name = L"label203";
		this->label203->Size = System::Drawing::Size(37, 13);
		this->label203->TabIndex = 361;
		this->label203->Text = L"Voida:";
		// 
		// label202
		// 
		this->label202->AutoSize = true;
		this->label202->Location = System::Drawing::Point(174, 142);
		this->label202->Name = L"label202";
		this->label202->Size = System::Drawing::Size(34, 13);
		this->label202->TabIndex = 360;
		this->label202->Text = L"Forta:";
		// 
		// label201
		// 
		this->label201->AutoSize = true;
		this->label201->Location = System::Drawing::Point(174, 112);
		this->label201->Name = L"label201";
		this->label201->Size = System::Drawing::Size(37, 13);
		this->label201->TabIndex = 359;
		this->label201->Text = L"Celan:";
		// 
		// label200
		// 
		this->label200->AutoSize = true;
		this->label200->Location = System::Drawing::Point(174, 79);
		this->label200->Name = L"label200";
		this->label200->Size = System::Drawing::Size(41, 13);
		this->label200->TabIndex = 358;
		this->label200->Text = L"Rayan:";
		// 
		// label199
		// 
		this->label199->AutoSize = true;
		this->label199->Location = System::Drawing::Point(174, 48);
		this->label199->Name = L"label199";
		this->label199->Size = System::Drawing::Size(38, 13);
		this->label199->TabIndex = 357;
		this->label199->Text = L"Arden:";
		// 
		// label195
		// 
		this->label195->AutoSize = true;
		this->label195->Location = System::Drawing::Point(174, 19);
		this->label195->Name = L"label195";
		this->label195->Size = System::Drawing::Size(31, 13);
		this->label195->TabIndex = 356;
		this->label195->Text = L"Balo:";
		// 
		// label194
		// 
		this->label194->AutoSize = true;
		this->label194->Location = System::Drawing::Point(9, 173);
		this->label194->Name = L"label194";
		this->label194->Size = System::Drawing::Size(48, 13);
		this->label194->TabIndex = 355;
		this->label194->Text = L"Incense:";
		// 
		// label191
		// 
		this->label191->AutoSize = true;
		this->label191->Location = System::Drawing::Point(513, 169);
		this->label191->Name = L"label191";
		this->label191->Size = System::Drawing::Size(48, 13);
		this->label191->TabIndex = 353;
		this->label191->Text = L"Ar-Earth:";
		// 
		// label190
		// 
		this->label190->AutoSize = true;
		this->label190->Location = System::Drawing::Point(513, 138);
		this->label190->Name = L"label190";
		this->label190->Size = System::Drawing::Size(61, 13);
		this->label190->TabIndex = 352;
		this->label190->Text = L"Ar-Heaven:";
		// 
		// label189
		// 
		this->label189->AutoSize = true;
		this->label189->Location = System::Drawing::Point(513, 108);
		this->label189->Name = L"label189";
		this->label189->Size = System::Drawing::Size(41, 13);
		this->label189->TabIndex = 351;
		this->label189->Text = L"Exploit:";
		// 
		// label176
		// 
		this->label176->AutoSize = true;
		this->label176->Location = System::Drawing::Point(513, 76);
		this->label176->Name = L"label176";
		this->label176->Size = System::Drawing::Size(31, 13);
		this->label176->TabIndex = 350;
		this->label176->Text = L"Feat:";
		// 
		// label175
		// 
		this->label175->AutoSize = true;
		this->label175->Location = System::Drawing::Point(513, 48);
		this->label175->Name = L"label175";
		this->label175->Size = System::Drawing::Size(44, 13);
		this->label175->TabIndex = 349;
		this->label175->Text = L"Ar-Man:";
		// 
		// textBox_runk15
		// 
		this->textBox_runk15->Location = System::Drawing::Point(580, 16);
		this->textBox_runk15->Name = L"textBox_runk15";
		this->textBox_runk15->Size = System::Drawing::Size(71, 20);
		this->textBox_runk15->TabIndex = 348;
		this->textBox_runk15->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_runk15);
		// 
		// label172
		// 
		this->label172->AutoSize = true;
		this->label172->Location = System::Drawing::Point(513, 19);
		this->label172->Name = L"label172";
		this->label172->Size = System::Drawing::Size(50, 13);
		this->label172->TabIndex = 347;
		this->label172->Text = L"Courage:";
		// 
		// textBox_rewrepfoyuan
		// 
		this->textBox_rewrepfoyuan->Location = System::Drawing::Point(421, 196);
		this->textBox_rewrepfoyuan->Name = L"textBox_rewrepfoyuan";
		this->textBox_rewrepfoyuan->Size = System::Drawing::Size(86, 20);
		this->textBox_rewrepfoyuan->TabIndex = 346;
		this->textBox_rewrepfoyuan->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepfoyuan);
		// 
		// label165
		// 
		this->label165->AutoSize = true;
		this->label165->Location = System::Drawing::Point(353, 199);
		this->label165->Name = L"label165";
		this->label165->Size = System::Drawing::Size(36, 13);
		this->label165->TabIndex = 345;
		this->label165->Text = L"Fuwa:";
		// 
		// textBox_rewmoxing
		// 
		this->textBox_rewmoxing->Location = System::Drawing::Point(421, 166);
		this->textBox_rewmoxing->Name = L"textBox_rewmoxing";
		this->textBox_rewmoxing->Size = System::Drawing::Size(86, 20);
		this->textBox_rewmoxing->TabIndex = 344;
		this->textBox_rewmoxing->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewmoxing);
		// 
		// label163
		// 
		this->label163->AutoSize = true;
		this->label163->Location = System::Drawing::Point(352, 169);
		this->label163->Name = L"label163";
		this->label163->Size = System::Drawing::Size(38, 13);
		this->label163->TabIndex = 343;
		this->label163->Text = L"Felkin:";
		// 
		// textBox_DaoXin
		// 
		this->textBox_DaoXin->Location = System::Drawing::Point(421, 135);
		this->textBox_DaoXin->Name = L"textBox_DaoXin";
		this->textBox_DaoXin->Size = System::Drawing::Size(86, 20);
		this->textBox_DaoXin->TabIndex = 342;
		this->textBox_DaoXin->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_DaoXin);
		// 
		// label162
		// 
		this->label162->AutoSize = true;
		this->label162->Location = System::Drawing::Point(353, 139);
		this->label162->Name = L"label162";
		this->label162->Size = System::Drawing::Size(41, 13);
		this->label162->TabIndex = 341;
		this->label162->Text = L"Dagos:";
		// 
		// textBox_rewshide
		// 
		this->textBox_rewshide->Location = System::Drawing::Point(421, 105);
		this->textBox_rewshide->Name = L"textBox_rewshide";
		this->textBox_rewshide->Size = System::Drawing::Size(86, 20);
		this->textBox_rewshide->TabIndex = 340;
		this->textBox_rewshide->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewshide);
		// 
		// label161
		// 
		this->label161->AutoSize = true;
		this->label161->Location = System::Drawing::Point(353, 108);
		this->label161->Name = L"label161";
		this->label161->Size = System::Drawing::Size(53, 13);
		this->label161->TabIndex = 339;
		this->label161->Text = L"Expertise:";
		// 
		// textBox_rewwencai
		// 
		this->textBox_rewwencai->Location = System::Drawing::Point(421, 76);
		this->textBox_rewwencai->Name = L"textBox_rewwencai";
		this->textBox_rewwencai->Size = System::Drawing::Size(86, 20);
		this->textBox_rewwencai->TabIndex = 338;
		this->textBox_rewwencai->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewwencai);
		// 
		// label160
		// 
		this->label160->AutoSize = true;
		this->label160->Location = System::Drawing::Point(353, 79);
		this->label160->Name = L"label160";
		this->label160->Size = System::Drawing::Size(49, 13);
		this->label160->TabIndex = 337;
		this->label160->Text = L"Coulture:";
		// 
		// textBox_rewQingyuan
		// 
		this->textBox_rewQingyuan->Location = System::Drawing::Point(421, 45);
		this->textBox_rewQingyuan->Name = L"textBox_rewQingyuan";
		this->textBox_rewQingyuan->Size = System::Drawing::Size(86, 20);
		this->textBox_rewQingyuan->TabIndex = 336;
		this->textBox_rewQingyuan->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewQingyuan);
		// 
		// label159
		// 
		this->label159->AutoSize = true;
		this->label159->Location = System::Drawing::Point(353, 48);
		this->label159->Name = L"label159";
		this->label159->Size = System::Drawing::Size(56, 13);
		this->label159->TabIndex = 335;
		this->label159->Text = L"Romance:";
		// 
		// textBox_rewrepzhongyi
		// 
		this->textBox_rewrepzhongyi->Location = System::Drawing::Point(421, 16);
		this->textBox_rewrepzhongyi->Name = L"textBox_rewrepzhongyi";
		this->textBox_rewrepzhongyi->Size = System::Drawing::Size(86, 20);
		this->textBox_rewrepzhongyi->TabIndex = 334;
		this->textBox_rewrepzhongyi->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepzhongyi);
		// 
		// label158
		// 
		this->label158->AutoSize = true;
		this->label158->Location = System::Drawing::Point(353, 19);
		this->label158->Name = L"label158";
		this->label158->Size = System::Drawing::Size(33, 13);
		this->label158->TabIndex = 333;
		this->label158->Text = L"Piety:";
		// 
		// textBox_rewreplupi
		// 
		this->textBox_rewreplupi->Location = System::Drawing::Point(68, 139);
		this->textBox_rewreplupi->Name = L"textBox_rewreplupi";
		this->textBox_rewreplupi->Size = System::Drawing::Size(100, 20);
		this->textBox_rewreplupi->TabIndex = 332;
		this->textBox_rewreplupi->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewreplupi);
		// 
		// label157
		// 
		this->label157->AutoSize = true;
		this->label157->Location = System::Drawing::Point(9, 142);
		this->label157->Name = L"label157";
		this->label157->Size = System::Drawing::Size(36, 13);
		this->label157->TabIndex = 331;
		this->label157->Text = L"Lupin:";
		// 
		// textBox_rewrepvim
		// 
		this->textBox_rewrepvim->Location = System::Drawing::Point(68, 108);
		this->textBox_rewrepvim->Name = L"textBox_rewrepvim";
		this->textBox_rewrepvim->Size = System::Drawing::Size(100, 20);
		this->textBox_rewrepvim->TabIndex = 330;
		this->textBox_rewrepvim->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepvim);
		// 
		// label156
		// 
		this->label156->AutoSize = true;
		this->label156->Location = System::Drawing::Point(9, 111);
		this->label156->Name = L"label156";
		this->label156->Size = System::Drawing::Size(27, 13);
		this->label156->TabIndex = 329;
		this->label156->Text = L"Vim:";
		// 
		// textBox_rewrepsky
		// 
		this->textBox_rewrepsky->Location = System::Drawing::Point(68, 76);
		this->textBox_rewrepsky->Name = L"textBox_rewrepsky";
		this->textBox_rewrepsky->Size = System::Drawing::Size(100, 20);
		this->textBox_rewrepsky->TabIndex = 328;
		this->textBox_rewrepsky->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepsky);
		// 
		// label155
		// 
		this->label155->AutoSize = true;
		this->label155->Location = System::Drawing::Point(8, 79);
		this->label155->Name = L"label155";
		this->label155->Size = System::Drawing::Size(51, 13);
		this->label155->TabIndex = 327;
		this->label155->Text = L"Skysong:";
		// 
		// textBox_rewrepjad
		// 
		this->textBox_rewrepjad->Location = System::Drawing::Point(68, 45);
		this->textBox_rewrepjad->Name = L"textBox_rewrepjad";
		this->textBox_rewrepjad->Size = System::Drawing::Size(100, 20);
		this->textBox_rewrepjad->TabIndex = 326;
		this->textBox_rewrepjad->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepjad);
		// 
		// label154
		// 
		this->label154->AutoSize = true;
		this->label154->Location = System::Drawing::Point(8, 48);
		this->label154->Name = L"label154";
		this->label154->Size = System::Drawing::Size(45, 13);
		this->label154->TabIndex = 325;
		this->label154->Text = L"Jadeon:";
		// 
		// textBox_rewrepmodo
		// 
		this->textBox_rewrepmodo->Location = System::Drawing::Point(68, 16);
		this->textBox_rewrepmodo->Name = L"textBox_rewrepmodo";
		this->textBox_rewrepmodo->Size = System::Drawing::Size(100, 20);
		this->textBox_rewrepmodo->TabIndex = 324;
		this->textBox_rewrepmodo->Leave += gcnew System::EventHandler(this, &zxTASKeditWindow::change_rewrepmodo);
		// 
		// label153
		// 
		this->label153->AutoSize = true;
		this->label153->Location = System::Drawing::Point(8, 19);
		this->label153->Name = L"label153";
		this->label153->Size = System::Drawing::Size(37, 13);
		this->label153->TabIndex = 324;
		this->label153->Text = L"Modo:";
		// 
		// groupBox_reward_items
		// 
		this->groupBox_reward_items->Controls->Add(this->textBox_expfname);
		this->groupBox_reward_items->Controls->Add(this->label276);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval2len);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval1len);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval4h);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval3h);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval2h);
		this->groupBox_reward_items->Controls->Add(this->textBox_rnval1h);
		this->groupBox_reward_items->Controls->Add(this->checkBox_rnval);
		this->groupBox_reward_items->Controls->Add(this->textBox_runkn4);
		this->groupBox_reward_items->Controls->Add(this->label274);
		this->groupBox_reward_items->Controls->Add(this->textBox_runkn3);
		this->groupBox_reward_items->Controls->Add(this->label273);
		this->groupBox_reward_items->Controls->Add(this->textBox_runkn2);
		this->groupBox_reward_items->Controls->Add(this->label272);
		this->groupBox_reward_items->Controls->Add(this->textBox_runkn1);
		this->groupBox_reward_items->Controls->Add(this->label271);
		this->groupBox_reward_items->Controls->Add(this->textBox_runk49);
		this->groupBox_reward_items->Controls->Add(this->label220);
		this->groupBox_reward_items->Controls->Add(this->textBox_rvalc);
		this->groupBox_reward_items->Controls->Add(this->textBox_rval3);
		this->groupBox_reward_items->Controls->Add(this->textBox_rval2);
		this->groupBox_reward_items->Controls->Add(this->textBox_val1);
		this->groupBox_reward_items->Controls->Add(this->label219);
		this->groupBox_reward_items->Controls->Add(this->label218);
		this->groupBox_reward_items->Controls->Add(this->textBox_runk48);
		this->groupBox_reward_items->Controls->Add(this->listBox_reward_timed);
		this->groupBox_reward_items->Location = System::Drawing::Point(3, 1052);
		this->groupBox_reward_items->Name = L"groupBox_reward_items";
		this->groupBox_reward_items->Size = System::Drawing::Size(655, 345);
		this->groupBox_reward_items->TabIndex = 3;
		this->groupBox_reward_items->TabStop = false;
		this->groupBox_reward_items->Text = L"Unknowns";
		// 
		// textBox_expfname
		// 
		this->textBox_expfname->Enabled = false;
		this->textBox_expfname->Location = System::Drawing::Point(110, 176);
		this->textBox_expfname->Name = L"textBox_expfname";
		this->textBox_expfname->Size = System::Drawing::Size(101, 20);
		this->textBox_expfname->TabIndex = 402;
		// 
		// label276
		// 
		this->label276->AutoSize = true;
		this->label276->Location = System::Drawing::Point(7, 179);
		this->label276->Name = L"label276";
		this->label276->Size = System::Drawing::Size(100, 13);
		this->label276->TabIndex = 401;
		this->label276->Text = L"EXP Formula name:";
		// 
		// textBox_rnval2len
		// 
		this->textBox_rnval2len->Enabled = false;
		this->textBox_rnval2len->Location = System::Drawing::Point(586, 103);
		this->textBox_rnval2len->Name = L"textBox_rnval2len";
		this->textBox_rnval2len->Size = System::Drawing::Size(65, 20);
		this->textBox_rnval2len->TabIndex = 400;
		// 
		// textBox_rnval1len
		// 
		this->textBox_rnval1len->Enabled = false;
		this->textBox_rnval1len->Location = System::Drawing::Point(586, 75);
		this->textBox_rnval1len->Name = L"textBox_rnval1len";
		this->textBox_rnval1len->Size = System::Drawing::Size(65, 20);
		this->textBox_rnval1len->TabIndex = 399;
		// 
		// textBox_rnval4h
		// 
		this->textBox_rnval4h->Location = System::Drawing::Point(155, 150);
		this->textBox_rnval4h->Name = L"textBox_rnval4h";
		this->textBox_rnval4h->Size = System::Drawing::Size(425, 20);
		this->textBox_rnval4h->TabIndex = 398;
		// 
		// textBox_rnval3h
		// 
		this->textBox_rnval3h->Enabled = false;
		this->textBox_rnval3h->Location = System::Drawing::Point(155, 129);
		this->textBox_rnval3h->Name = L"textBox_rnval3h";
		this->textBox_rnval3h->Size = System::Drawing::Size(425, 20);
		this->textBox_rnval3h->TabIndex = 397;
		// 
		// textBox_rnval2h
		// 
		this->textBox_rnval2h->Enabled = false;
		this->textBox_rnval2h->Location = System::Drawing::Point(155, 103);
		this->textBox_rnval2h->Name = L"textBox_rnval2h";
		this->textBox_rnval2h->Size = System::Drawing::Size(425, 20);
		this->textBox_rnval2h->TabIndex = 396;
		// 
		// textBox_rnval1h
		// 
		this->textBox_rnval1h->Enabled = false;
		this->textBox_rnval1h->Location = System::Drawing::Point(155, 75);
		this->textBox_rnval1h->Name = L"textBox_rnval1h";
		this->textBox_rnval1h->Size = System::Drawing::Size(425, 20);
		this->textBox_rnval1h->TabIndex = 395;
		// 
		// checkBox_rnval
		// 
		this->checkBox_rnval->AutoSize = true;
		this->checkBox_rnval->Enabled = false;
		this->checkBox_rnval->Location = System::Drawing::Point(10, 47);
		this->checkBox_rnval->Name = L"checkBox_rnval";
		this->checkBox_rnval->Size = System::Drawing::Size(62, 17);
		this->checkBox_rnval->TabIndex = 394;
		this->checkBox_rnval->Text = L"Is N val";
		this->checkBox_rnval->UseVisualStyleBackColor = true;
		// 
		// textBox_runkn4
		// 
		this->textBox_runkn4->Enabled = false;
		this->textBox_runkn4->Location = System::Drawing::Point(88, 147);
		this->textBox_runkn4->Name = L"textBox_runkn4";
		this->textBox_runkn4->Size = System::Drawing::Size(54, 20);
		this->textBox_runkn4->TabIndex = 387;
		// 
		// label274
		// 
		this->label274->AutoSize = true;
		this->label274->Location = System::Drawing::Point(10, 150);
		this->label274->Name = L"label274";
		this->label274->Size = System::Drawing::Size(73, 13);
		this->label274->TabIndex = 386;
		this->label274->Text = L"Unknown N4:";
		// 
		// textBox_runkn3
		// 
		this->textBox_runkn3->Enabled = false;
		this->textBox_runkn3->Location = System::Drawing::Point(88, 124);
		this->textBox_runkn3->Name = L"textBox_runkn3";
		this->textBox_runkn3->Size = System::Drawing::Size(54, 20);
		this->textBox_runkn3->TabIndex = 385;
		// 
		// label273
		// 
		this->label273->AutoSize = true;
		this->label273->Location = System::Drawing::Point(10, 127);
		this->label273->Name = L"label273";
		this->label273->Size = System::Drawing::Size(73, 13);
		this->label273->TabIndex = 384;
		this->label273->Text = L"Unknown N3:";
		// 
		// textBox_runkn2
		// 
		this->textBox_runkn2->Enabled = false;
		this->textBox_runkn2->Location = System::Drawing::Point(88, 100);
		this->textBox_runkn2->Name = L"textBox_runkn2";
		this->textBox_runkn2->Size = System::Drawing::Size(54, 20);
		this->textBox_runkn2->TabIndex = 383;
		// 
		// label272
		// 
		this->label272->AutoSize = true;
		this->label272->Location = System::Drawing::Point(11, 103);
		this->label272->Name = L"label272";
		this->label272->Size = System::Drawing::Size(73, 13);
		this->label272->TabIndex = 382;
		this->label272->Text = L"Unknown N2:";
		// 
		// textBox_runkn1
		// 
		this->textBox_runkn1->Enabled = false;
		this->textBox_runkn1->Location = System::Drawing::Point(88, 75);
		this->textBox_runkn1->Name = L"textBox_runkn1";
		this->textBox_runkn1->Size = System::Drawing::Size(54, 20);
		this->textBox_runkn1->TabIndex = 381;
		// 
		// label271
		// 
		this->label271->AutoSize = true;
		this->label271->Location = System::Drawing::Point(11, 78);
		this->label271->Name = L"label271";
		this->label271->Size = System::Drawing::Size(73, 13);
		this->label271->TabIndex = 380;
		this->label271->Text = L"Unknown N1:";
		// 
		// textBox_runk49
		// 
		this->textBox_runk49->Location = System::Drawing::Point(159, 45);
		this->textBox_runk49->Name = L"textBox_runk49";
		this->textBox_runk49->Size = System::Drawing::Size(492, 20);
		this->textBox_runk49->TabIndex = 379;
		// 
		// label220
		// 
		this->label220->AutoSize = true;
		this->label220->Location = System::Drawing::Point(85, 48);
		this->label220->Name = L"label220";
		this->label220->Size = System::Drawing::Size(68, 13);
		this->label220->TabIndex = 378;
		this->label220->Text = L"Unknown49:";
		// 
		// textBox_rvalc
		// 
		this->textBox_rvalc->Enabled = false;
		this->textBox_rvalc->Location = System::Drawing::Point(508, 19);
		this->textBox_rvalc->Name = L"textBox_rvalc";
		this->textBox_rvalc->Size = System::Drawing::Size(30, 20);
		this->textBox_rvalc->TabIndex = 377;
		// 
		// textBox_rval3
		// 
		this->textBox_rval3->Enabled = false;
		this->textBox_rval3->Location = System::Drawing::Point(478, 19);
		this->textBox_rval3->Name = L"textBox_rval3";
		this->textBox_rval3->Size = System::Drawing::Size(30, 20);
		this->textBox_rval3->TabIndex = 376;
		// 
		// textBox_rval2
		// 
		this->textBox_rval2->Enabled = false;
		this->textBox_rval2->Location = System::Drawing::Point(447, 19);
		this->textBox_rval2->Name = L"textBox_rval2";
		this->textBox_rval2->Size = System::Drawing::Size(30, 20);
		this->textBox_rval2->TabIndex = 375;
		// 
		// textBox_val1
		// 
		this->textBox_val1->Enabled = false;
		this->textBox_val1->Location = System::Drawing::Point(417, 19);
		this->textBox_val1->Name = L"textBox_val1";
		this->textBox_val1->Size = System::Drawing::Size(30, 20);
		this->textBox_val1->TabIndex = 374;
		// 
		// label219
		// 
		this->label219->AutoSize = true;
		this->label219->Location = System::Drawing::Point(367, 22);
		this->label219->Name = L"label219";
		this->label219->Size = System::Drawing::Size(54, 13);
		this->label219->TabIndex = 374;
		this->label219->Text = L"Value set:";
		// 
		// label218
		// 
		this->label218->AutoSize = true;
		this->label218->Location = System::Drawing::Point(11, 22);
		this->label218->Name = L"label218";
		this->label218->Size = System::Drawing::Size(68, 13);
		this->label218->TabIndex = 374;
		this->label218->Text = L"Unknown48:";
		// 
		// textBox_runk48
		// 
		this->textBox_runk48->Location = System::Drawing::Point(88, 19);
		this->textBox_runk48->Name = L"textBox_runk48";
		this->textBox_runk48->Size = System::Drawing::Size(255, 20);
		this->textBox_runk48->TabIndex = 374;
		this->textBox_runk48->TextChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::textBox_runk48_TextChanged);
		// 
		// tabPage7
		// 
		this->tabPage7->AutoScroll = true;
		this->tabPage7->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->tabPage7->Controls->Add(this->groupBox_answers);
		this->tabPage7->Controls->Add(this->groupBox_conversation);
		this->tabPage7->Controls->Add(this->groupBox_questions);
		this->tabPage7->Controls->Add(this->groupBox_dialogs);
		this->tabPage7->Location = System::Drawing::Point(4, 25);
		this->tabPage7->Margin = System::Windows::Forms::Padding(0);
		this->tabPage7->Name = L"tabPage7";
		this->tabPage7->Size = System::Drawing::Size(698, 578);
		this->tabPage7->TabIndex = 6;
		this->tabPage7->Text = L"Conversation";
		this->tabPage7->UseVisualStyleBackColor = true;
		// 
		// tabPage5
		// 
		this->tabPage5->AutoScroll = true;
		this->tabPage5->Controls->Add(this->textBox_unkn134);
		this->tabPage5->Controls->Add(this->label259);
		this->tabPage5->Controls->Add(this->textBox_sval5);
		this->tabPage5->Controls->Add(this->label258);
		this->tabPage5->Controls->Add(this->textBox_sval4len);
		this->tabPage5->Controls->Add(this->textBox_sval3len);
		this->tabPage5->Controls->Add(this->textBox_svallen2);
		this->tabPage5->Controls->Add(this->textBox_sva1len);
		this->tabPage5->Controls->Add(this->textBox_sval4);
		this->tabPage5->Controls->Add(this->textBox_sval3);
		this->tabPage5->Controls->Add(this->label188);
		this->tabPage5->Controls->Add(this->label231);
		this->tabPage5->Controls->Add(this->label232);
		this->tabPage5->Controls->Add(this->label233);
		this->tabPage5->Controls->Add(this->textBox_sval2);
		this->tabPage5->Controls->Add(this->textBox_sval1);
		this->tabPage5->Controls->Add(this->checkBox_conval2);
		this->tabPage5->Controls->Add(this->textBox_unkn133);
		this->tabPage5->Controls->Add(this->label187);
		this->tabPage5->Controls->Add(this->textBox_unkn132);
		this->tabPage5->Controls->Add(this->label184);
		this->tabPage5->Controls->Add(this->textBox_unkn131);
		this->tabPage5->Controls->Add(this->label182);
		this->tabPage5->Controls->Add(this->textBox_unkn130);
		this->tabPage5->Controls->Add(this->label181);
		this->tabPage5->Controls->Add(this->textBox_unkn23456);
		this->tabPage5->Controls->Add(this->label177);
		this->tabPage5->Controls->Add(this->textBox_unkn129);
		this->tabPage5->Controls->Add(this->label174);
		this->tabPage5->Controls->Add(this->textBox_unkn1628);
		this->tabPage5->Controls->Add(this->label150);
		this->tabPage5->Controls->Add(this->textBox_unkn128);
		this->tabPage5->Controls->Add(this->label148);
		this->tabPage5->Controls->Add(this->textBox_val4len);
		this->tabPage5->Controls->Add(this->textBox_val3len);
		this->tabPage5->Controls->Add(this->textBox_val2len);
		this->tabPage5->Controls->Add(this->textBox_val1len);
		this->tabPage5->Controls->Add(this->textBox_nval4);
		this->tabPage5->Controls->Add(this->textBox_nval3);
		this->tabPage5->Controls->Add(this->label147);
		this->tabPage5->Controls->Add(this->label146);
		this->tabPage5->Controls->Add(this->label145);
		this->tabPage5->Controls->Add(this->label144);
		this->tabPage5->Controls->Add(this->textBox_nval2);
		this->tabPage5->Controls->Add(this->textBox_nval1);
		this->tabPage5->Controls->Add(this->checkBox_nval);
		this->tabPage5->Controls->Add(this->textBox_unkn127);
		this->tabPage5->Controls->Add(this->label72);
		this->tabPage5->Controls->Add(this->textBox_unkn126);
		this->tabPage5->Controls->Add(this->label142);
		this->tabPage5->Controls->Add(this->textBox_unkn125);
		this->tabPage5->Controls->Add(this->label141);
		this->tabPage5->Controls->Add(this->textBox_ubkb124);
		this->tabPage5->Controls->Add(this->label78);
		this->tabPage5->Controls->Add(this->textBox_unkn123);
		this->tabPage5->Controls->Add(this->label170);
		this->tabPage5->Controls->Add(this->textBox_unkn122);
		this->tabPage5->Controls->Add(this->label169);
		this->tabPage5->Controls->Add(this->textBox_unkn121);
		this->tabPage5->Controls->Add(this->label168);
		this->tabPage5->Controls->Add(this->textBox_unkn120);
		this->tabPage5->Controls->Add(this->label134);
		this->tabPage5->Controls->Add(this->textBox_unkn119);
		this->tabPage5->Controls->Add(this->label132);
		this->tabPage5->Controls->Add(this->textBox_unkn118);
		this->tabPage5->Controls->Add(this->label167);
		this->tabPage5->Controls->Add(this->checkBox_unknown31);
		this->tabPage5->Controls->Add(this->textBox_unkn116);
		this->tabPage5->Controls->Add(this->label75);
		this->tabPage5->Controls->Add(this->label54);
		this->tabPage5->Controls->Add(this->textBox_unkn115);
		this->tabPage5->Location = System::Drawing::Point(4, 25);
		this->tabPage5->Name = L"tabPage5";
		this->tabPage5->Size = System::Drawing::Size(698, 578);
		this->tabPage5->TabIndex = 7;
		this->tabPage5->Text = L"Unknowns";
		this->tabPage5->UseVisualStyleBackColor = true;
		// 
		// textBox_unkn134
		// 
		this->textBox_unkn134->Location = System::Drawing::Point(473, 3);
		this->textBox_unkn134->Name = L"textBox_unkn134";
		this->textBox_unkn134->Size = System::Drawing::Size(203, 20);
		this->textBox_unkn134->TabIndex = 480;
		// 
		// label259
		// 
		this->label259->AutoSize = true;
		this->label259->Location = System::Drawing::Point(375, 6);
		this->label259->Name = L"label259";
		this->label259->Size = System::Drawing::Size(92, 13);
		this->label259->TabIndex = 479;
		this->label259->Text = L"UNKNOWN_134:";
		// 
		// textBox_sval5
		// 
		this->textBox_sval5->Location = System::Drawing::Point(101, 554);
		this->textBox_sval5->Name = L"textBox_sval5";
		this->textBox_sval5->Size = System::Drawing::Size(575, 20);
		this->textBox_sval5->TabIndex = 478;
		// 
		// label258
		// 
		this->label258->AutoSize = true;
		this->label258->Location = System::Drawing::Point(79, 557);
		this->label258->Name = L"label258";
		this->label258->Size = System::Drawing::Size(16, 13);
		this->label258->TabIndex = 477;
		this->label258->Text = L"5:";
		// 
		// textBox_sval4len
		// 
		this->textBox_sval4len->Location = System::Drawing::Point(601, 531);
		this->textBox_sval4len->Name = L"textBox_sval4len";
		this->textBox_sval4len->Size = System::Drawing::Size(75, 20);
		this->textBox_sval4len->TabIndex = 476;
		// 
		// textBox_sval3len
		// 
		this->textBox_sval3len->Location = System::Drawing::Point(601, 506);
		this->textBox_sval3len->Name = L"textBox_sval3len";
		this->textBox_sval3len->Size = System::Drawing::Size(75, 20);
		this->textBox_sval3len->TabIndex = 475;
		// 
		// textBox_svallen2
		// 
		this->textBox_svallen2->Location = System::Drawing::Point(601, 477);
		this->textBox_svallen2->Name = L"textBox_svallen2";
		this->textBox_svallen2->Size = System::Drawing::Size(75, 20);
		this->textBox_svallen2->TabIndex = 474;
		// 
		// textBox_sva1len
		// 
		this->textBox_sva1len->Location = System::Drawing::Point(601, 451);
		this->textBox_sva1len->Name = L"textBox_sva1len";
		this->textBox_sva1len->Size = System::Drawing::Size(75, 20);
		this->textBox_sva1len->TabIndex = 473;
		// 
		// textBox_sval4
		// 
		this->textBox_sval4->Location = System::Drawing::Point(101, 532);
		this->textBox_sval4->Name = L"textBox_sval4";
		this->textBox_sval4->Size = System::Drawing::Size(494, 20);
		this->textBox_sval4->TabIndex = 472;
		// 
		// textBox_sval3
		// 
		this->textBox_sval3->Location = System::Drawing::Point(101, 506);
		this->textBox_sval3->Name = L"textBox_sval3";
		this->textBox_sval3->Size = System::Drawing::Size(494, 20);
		this->textBox_sval3->TabIndex = 471;
		// 
		// label188
		// 
		this->label188->AutoSize = true;
		this->label188->Location = System::Drawing::Point(79, 535);
		this->label188->Name = L"label188";
		this->label188->Size = System::Drawing::Size(16, 13);
		this->label188->TabIndex = 470;
		this->label188->Text = L"4:";
		// 
		// label231
		// 
		this->label231->AutoSize = true;
		this->label231->Location = System::Drawing::Point(79, 509);
		this->label231->Name = L"label231";
		this->label231->Size = System::Drawing::Size(16, 13);
		this->label231->TabIndex = 469;
		this->label231->Text = L"3:";
		// 
		// label232
		// 
		this->label232->AutoSize = true;
		this->label232->Location = System::Drawing::Point(79, 480);
		this->label232->Name = L"label232";
		this->label232->Size = System::Drawing::Size(16, 13);
		this->label232->TabIndex = 468;
		this->label232->Text = L"2:";
		// 
		// label233
		// 
		this->label233->AutoSize = true;
		this->label233->Location = System::Drawing::Point(79, 454);
		this->label233->Name = L"label233";
		this->label233->Size = System::Drawing::Size(16, 13);
		this->label233->TabIndex = 467;
		this->label233->Text = L"1:";
		// 
		// textBox_sval2
		// 
		this->textBox_sval2->Location = System::Drawing::Point(101, 477);
		this->textBox_sval2->Name = L"textBox_sval2";
		this->textBox_sval2->Size = System::Drawing::Size(494, 20);
		this->textBox_sval2->TabIndex = 466;
		// 
		// textBox_sval1
		// 
		this->textBox_sval1->Location = System::Drawing::Point(101, 451);
		this->textBox_sval1->Name = L"textBox_sval1";
		this->textBox_sval1->Size = System::Drawing::Size(494, 20);
		this->textBox_sval1->TabIndex = 465;
		// 
		// checkBox_conval2
		// 
		this->checkBox_conval2->AutoSize = true;
		this->checkBox_conval2->Location = System::Drawing::Point(3, 440);
		this->checkBox_conval2->Name = L"checkBox_conval2";
		this->checkBox_conval2->Size = System::Drawing::Size(82, 17);
		this->checkBox_conval2->TabIndex = 463;
		this->checkBox_conval2->Text = L"Control val2";
		this->checkBox_conval2->UseVisualStyleBackColor = true;
		// 
		// textBox_unkn133
		// 
		this->textBox_unkn133->Location = System::Drawing::Point(101, 417);
		this->textBox_unkn133->Name = L"textBox_unkn133";
		this->textBox_unkn133->Size = System::Drawing::Size(575, 20);
		this->textBox_unkn133->TabIndex = 462;
		// 
		// label187
		// 
		this->label187->AutoSize = true;
		this->label187->Location = System::Drawing::Point(3, 424);
		this->label187->Name = L"label187";
		this->label187->Size = System::Drawing::Size(92, 13);
		this->label187->TabIndex = 461;
		this->label187->Text = L"UNKNOWN_133:";
		// 
		// textBox_unkn132
		// 
		this->textBox_unkn132->Location = System::Drawing::Point(398, 363);
		this->textBox_unkn132->Name = L"textBox_unkn132";
		this->textBox_unkn132->Size = System::Drawing::Size(278, 20);
		this->textBox_unkn132->TabIndex = 460;
		// 
		// label184
		// 
		this->label184->AutoSize = true;
		this->label184->Location = System::Drawing::Point(304, 366);
		this->label184->Name = L"label184";
		this->label184->Size = System::Drawing::Size(92, 13);
		this->label184->TabIndex = 459;
		this->label184->Text = L"UNKNOWN_132:";
		// 
		// textBox_unkn131
		// 
		this->textBox_unkn131->Location = System::Drawing::Point(101, 390);
		this->textBox_unkn131->Name = L"textBox_unkn131";
		this->textBox_unkn131->Size = System::Drawing::Size(575, 20);
		this->textBox_unkn131->TabIndex = 458;
		// 
		// label182
		// 
		this->label182->AutoSize = true;
		this->label182->Location = System::Drawing::Point(3, 393);
		this->label182->Name = L"label182";
		this->label182->Size = System::Drawing::Size(92, 13);
		this->label182->TabIndex = 457;
		this->label182->Text = L"UNKNOWN_131:";
		// 
		// textBox_unkn130
		// 
		this->textBox_unkn130->Location = System::Drawing::Point(101, 363);
		this->textBox_unkn130->Name = L"textBox_unkn130";
		this->textBox_unkn130->Size = System::Drawing::Size(197, 20);
		this->textBox_unkn130->TabIndex = 456;
		// 
		// label181
		// 
		this->label181->AutoSize = true;
		this->label181->Location = System::Drawing::Point(3, 370);
		this->label181->Name = L"label181";
		this->label181->Size = System::Drawing::Size(92, 13);
		this->label181->TabIndex = 455;
		this->label181->Text = L"UNKNOWN_130:";
		// 
		// textBox_unkn23456
		// 
		this->textBox_unkn23456->Location = System::Drawing::Point(522, 333);
		this->textBox_unkn23456->Name = L"textBox_unkn23456";
		this->textBox_unkn23456->Size = System::Drawing::Size(154, 20);
		this->textBox_unkn23456->TabIndex = 454;
		// 
		// label177
		// 
		this->label177->AutoSize = true;
		this->label177->Location = System::Drawing::Point(409, 338);
		this->label177->Name = L"label177";
		this->label177->Size = System::Drawing::Size(107, 13);
		this->label177->TabIndex = 453;
		this->label177->Text = L"UNKNOWN_234556";
		// 
		// textBox_unkn129
		// 
		this->textBox_unkn129->Location = System::Drawing::Point(283, 335);
		this->textBox_unkn129->Name = L"textBox_unkn129";
		this->textBox_unkn129->Size = System::Drawing::Size(120, 20);
		this->textBox_unkn129->TabIndex = 452;
		// 
		// label174
		// 
		this->label174->AutoSize = true;
		this->label174->Location = System::Drawing::Point(188, 338);
		this->label174->Name = L"label174";
		this->label174->Size = System::Drawing::Size(92, 13);
		this->label174->TabIndex = 451;
		this->label174->Text = L"UNKNOWN_129:";
		// 
		// textBox_unkn1628
		// 
		this->textBox_unkn1628->Location = System::Drawing::Point(101, 335);
		this->textBox_unkn1628->Name = L"textBox_unkn1628";
		this->textBox_unkn1628->Size = System::Drawing::Size(81, 20);
		this->textBox_unkn1628->TabIndex = 450;
		// 
		// label150
		// 
		this->label150->AutoSize = true;
		this->label150->Location = System::Drawing::Point(3, 342);
		this->label150->Name = L"label150";
		this->label150->Size = System::Drawing::Size(86, 13);
		this->label150->TabIndex = 449;
		this->label150->Text = L"Unknown_1628:";
		// 
		// textBox_unkn128
		// 
		this->textBox_unkn128->Location = System::Drawing::Point(101, 307);
		this->textBox_unkn128->Name = L"textBox_unkn128";
		this->textBox_unkn128->Size = System::Drawing::Size(575, 20);
		this->textBox_unkn128->TabIndex = 448;
		// 
		// label148
		// 
		this->label148->AutoSize = true;
		this->label148->Location = System::Drawing::Point(3, 312);
		this->label148->Name = L"label148";
		this->label148->Size = System::Drawing::Size(92, 13);
		this->label148->TabIndex = 447;
		this->label148->Text = L"UNKNOWN_128:";
		// 
		// textBox_val4len
		// 
		this->textBox_val4len->Location = System::Drawing::Point(601, 281);
		this->textBox_val4len->Name = L"textBox_val4len";
		this->textBox_val4len->Size = System::Drawing::Size(75, 20);
		this->textBox_val4len->TabIndex = 446;
		// 
		// textBox_val3len
		// 
		this->textBox_val3len->Location = System::Drawing::Point(601, 255);
		this->textBox_val3len->Name = L"textBox_val3len";
		this->textBox_val3len->Size = System::Drawing::Size(75, 20);
		this->textBox_val3len->TabIndex = 445;
		// 
		// textBox_val2len
		// 
		this->textBox_val2len->Location = System::Drawing::Point(601, 226);
		this->textBox_val2len->Name = L"textBox_val2len";
		this->textBox_val2len->Size = System::Drawing::Size(75, 20);
		this->textBox_val2len->TabIndex = 444;
		// 
		// textBox_val1len
		// 
		this->textBox_val1len->Location = System::Drawing::Point(601, 200);
		this->textBox_val1len->Name = L"textBox_val1len";
		this->textBox_val1len->Size = System::Drawing::Size(75, 20);
		this->textBox_val1len->TabIndex = 443;
		// 
		// textBox_nval4
		// 
		this->textBox_nval4->Location = System::Drawing::Point(101, 281);
		this->textBox_nval4->Name = L"textBox_nval4";
		this->textBox_nval4->Size = System::Drawing::Size(494, 20);
		this->textBox_nval4->TabIndex = 442;
		// 
		// textBox_nval3
		// 
		this->textBox_nval3->Location = System::Drawing::Point(101, 255);
		this->textBox_nval3->Name = L"textBox_nval3";
		this->textBox_nval3->Size = System::Drawing::Size(494, 20);
		this->textBox_nval3->TabIndex = 441;
		// 
		// label147
		// 
		this->label147->AutoSize = true;
		this->label147->Location = System::Drawing::Point(79, 284);
		this->label147->Name = L"label147";
		this->label147->Size = System::Drawing::Size(16, 13);
		this->label147->TabIndex = 440;
		this->label147->Text = L"4:";
		// 
		// label146
		// 
		this->label146->AutoSize = true;
		this->label146->Location = System::Drawing::Point(79, 258);
		this->label146->Name = L"label146";
		this->label146->Size = System::Drawing::Size(16, 13);
		this->label146->TabIndex = 439;
		this->label146->Text = L"3:";
		// 
		// label145
		// 
		this->label145->AutoSize = true;
		this->label145->Location = System::Drawing::Point(79, 229);
		this->label145->Name = L"label145";
		this->label145->Size = System::Drawing::Size(16, 13);
		this->label145->TabIndex = 438;
		this->label145->Text = L"2:";
		// 
		// label144
		// 
		this->label144->AutoSize = true;
		this->label144->Location = System::Drawing::Point(79, 203);
		this->label144->Name = L"label144";
		this->label144->Size = System::Drawing::Size(16, 13);
		this->label144->TabIndex = 437;
		this->label144->Text = L"1:";
		// 
		// textBox_nval2
		// 
		this->textBox_nval2->Location = System::Drawing::Point(101, 226);
		this->textBox_nval2->Name = L"textBox_nval2";
		this->textBox_nval2->Size = System::Drawing::Size(494, 20);
		this->textBox_nval2->TabIndex = 436;
		// 
		// textBox_nval1
		// 
		this->textBox_nval1->Location = System::Drawing::Point(101, 200);
		this->textBox_nval1->Name = L"textBox_nval1";
		this->textBox_nval1->Size = System::Drawing::Size(494, 20);
		this->textBox_nval1->TabIndex = 435;
		// 
		// checkBox_nval
		// 
		this->checkBox_nval->AutoSize = true;
		this->checkBox_nval->Location = System::Drawing::Point(6, 205);
		this->checkBox_nval->Name = L"checkBox_nval";
		this->checkBox_nval->Size = System::Drawing::Size(76, 17);
		this->checkBox_nval->TabIndex = 434;
		this->checkBox_nval->Text = L"Control val";
		this->checkBox_nval->UseVisualStyleBackColor = true;
		// 
		// textBox_unkn127
		// 
		this->textBox_unkn127->Location = System::Drawing::Point(101, 174);
		this->textBox_unkn127->Name = L"textBox_unkn127";
		this->textBox_unkn127->Size = System::Drawing::Size(575, 20);
		this->textBox_unkn127->TabIndex = 399;
		// 
		// label72
		// 
		this->label72->AutoSize = true;
		this->label72->Location = System::Drawing::Point(3, 177);
		this->label72->Name = L"label72";
		this->label72->Size = System::Drawing::Size(92, 13);
		this->label72->TabIndex = 398;
		this->label72->Text = L"UNKNOWN_127:";
		// 
		// textBox_unkn126
		// 
		this->textBox_unkn126->Location = System::Drawing::Point(304, 145);
		this->textBox_unkn126->Name = L"textBox_unkn126";
		this->textBox_unkn126->Size = System::Drawing::Size(372, 20);
		this->textBox_unkn126->TabIndex = 397;
		// 
		// label142
		// 
		this->label142->AutoSize = true;
		this->label142->Location = System::Drawing::Point(206, 148);
		this->label142->Name = L"label142";
		this->label142->Size = System::Drawing::Size(92, 13);
		this->label142->TabIndex = 396;
		this->label142->Text = L"UNKNOWN_126:";
		// 
		// textBox_unkn125
		// 
		this->textBox_unkn125->Location = System::Drawing::Point(101, 145);
		this->textBox_unkn125->Name = L"textBox_unkn125";
		this->textBox_unkn125->Size = System::Drawing::Size(99, 20);
		this->textBox_unkn125->TabIndex = 395;
		// 
		// label141
		// 
		this->label141->AutoSize = true;
		this->label141->Location = System::Drawing::Point(3, 148);
		this->label141->Name = L"label141";
		this->label141->Size = System::Drawing::Size(92, 13);
		this->label141->TabIndex = 394;
		this->label141->Text = L"UNKNOWN_125:";
		// 
		// textBox_ubkb124
		// 
		this->textBox_ubkb124->Location = System::Drawing::Point(507, 114);
		this->textBox_ubkb124->Name = L"textBox_ubkb124";
		this->textBox_ubkb124->Size = System::Drawing::Size(169, 20);
		this->textBox_ubkb124->TabIndex = 393;
		// 
		// label78
		// 
		this->label78->AutoSize = true;
		this->label78->Location = System::Drawing::Point(409, 117);
		this->label78->Name = L"label78";
		this->label78->Size = System::Drawing::Size(92, 13);
		this->label78->TabIndex = 392;
		this->label78->Text = L"UNKNOWN_124:";
		// 
		// textBox_unkn123
		// 
		this->textBox_unkn123->Location = System::Drawing::Point(304, 114);
		this->textBox_unkn123->Name = L"textBox_unkn123";
		this->textBox_unkn123->Size = System::Drawing::Size(99, 20);
		this->textBox_unkn123->TabIndex = 391;
		// 
		// label170
		// 
		this->label170->AutoSize = true;
		this->label170->Location = System::Drawing::Point(206, 117);
		this->label170->Name = L"label170";
		this->label170->Size = System::Drawing::Size(92, 13);
		this->label170->TabIndex = 390;
		this->label170->Text = L"UNKNOWN_123:";
		// 
		// textBox_unkn122
		// 
		this->textBox_unkn122->Location = System::Drawing::Point(101, 114);
		this->textBox_unkn122->Name = L"textBox_unkn122";
		this->textBox_unkn122->Size = System::Drawing::Size(99, 20);
		this->textBox_unkn122->TabIndex = 389;
		// 
		// label169
		// 
		this->label169->AutoSize = true;
		this->label169->Location = System::Drawing::Point(3, 117);
		this->label169->Name = L"label169";
		this->label169->Size = System::Drawing::Size(92, 13);
		this->label169->TabIndex = 388;
		this->label169->Text = L"UNKNOWN_122:";
		// 
		// textBox_unkn121
		// 
		this->textBox_unkn121->Location = System::Drawing::Point(486, 85);
		this->textBox_unkn121->Name = L"textBox_unkn121";
		this->textBox_unkn121->Size = System::Drawing::Size(190, 20);
		this->textBox_unkn121->TabIndex = 387;
		// 
		// label168
		// 
		this->label168->AutoSize = true;
		this->label168->Location = System::Drawing::Point(388, 88);
		this->label168->Name = L"label168";
		this->label168->Size = System::Drawing::Size(92, 13);
		this->label168->TabIndex = 386;
		this->label168->Text = L"UNKNOWN_121:";
		// 
		// textBox_unkn120
		// 
		this->textBox_unkn120->Location = System::Drawing::Point(101, 85);
		this->textBox_unkn120->Name = L"textBox_unkn120";
		this->textBox_unkn120->Size = System::Drawing::Size(281, 20);
		this->textBox_unkn120->TabIndex = 385;
		// 
		// label134
		// 
		this->label134->AutoSize = true;
		this->label134->Location = System::Drawing::Point(3, 88);
		this->label134->Name = L"label134";
		this->label134->Size = System::Drawing::Size(92, 13);
		this->label134->TabIndex = 384;
		this->label134->Text = L"UNKNOWN_120:";
		// 
		// textBox_unkn119
		// 
		this->textBox_unkn119->Location = System::Drawing::Point(378, 56);
		this->textBox_unkn119->Name = L"textBox_unkn119";
		this->textBox_unkn119->Size = System::Drawing::Size(298, 20);
		this->textBox_unkn119->TabIndex = 383;
		// 
		// label132
		// 
		this->label132->AutoSize = true;
		this->label132->Location = System::Drawing::Point(280, 61);
		this->label132->Name = L"label132";
		this->label132->Size = System::Drawing::Size(92, 13);
		this->label132->TabIndex = 382;
		this->label132->Text = L"UNKNOWN_119:";
		// 
		// textBox_unkn118
		// 
		this->textBox_unkn118->Location = System::Drawing::Point(101, 58);
		this->textBox_unkn118->Name = L"textBox_unkn118";
		this->textBox_unkn118->Size = System::Drawing::Size(173, 20);
		this->textBox_unkn118->TabIndex = 381;
		// 
		// label167
		// 
		this->label167->AutoSize = true;
		this->label167->Location = System::Drawing::Point(3, 63);
		this->label167->Name = L"label167";
		this->label167->Size = System::Drawing::Size(92, 13);
		this->label167->TabIndex = 380;
		this->label167->Text = L"UNKNOWN_118:";
		// 
		// checkBox_unknown31
		// 
		this->checkBox_unknown31->AutoSize = true;
		this->checkBox_unknown31->Location = System::Drawing::Point(280, 5);
		this->checkBox_unknown31->Name = L"checkBox_unknown31";
		this->checkBox_unknown31->Size = System::Drawing::Size(102, 17);
		this->checkBox_unknown31->TabIndex = 379;
		this->checkBox_unknown31->Text = L"UNKNOWN_31";
		this->checkBox_unknown31->UseVisualStyleBackColor = true;
		// 
		// textBox_unkn116
		// 
		this->textBox_unkn116->Location = System::Drawing::Point(101, 32);
		this->textBox_unkn116->Name = L"textBox_unkn116";
		this->textBox_unkn116->Size = System::Drawing::Size(575, 20);
		this->textBox_unkn116->TabIndex = 378;
		// 
		// label75
		// 
		this->label75->AutoSize = true;
		this->label75->Location = System::Drawing::Point(3, 35);
		this->label75->Name = L"label75";
		this->label75->Size = System::Drawing::Size(92, 13);
		this->label75->TabIndex = 377;
		this->label75->Text = L"UNKNOWN_116:";
		// 
		// label54
		// 
		this->label54->AutoSize = true;
		this->label54->Location = System::Drawing::Point(3, 6);
		this->label54->Name = L"label54";
		this->label54->Size = System::Drawing::Size(92, 13);
		this->label54->TabIndex = 376;
		this->label54->Text = L"UNKNOWN_115:";
		// 
		// textBox_unkn115
		// 
		this->textBox_unkn115->Location = System::Drawing::Point(101, 3);
		this->textBox_unkn115->Name = L"textBox_unkn115";
		this->textBox_unkn115->Size = System::Drawing::Size(173, 20);
		this->textBox_unkn115->TabIndex = 375;
		// 
		// contextMenuStrip_reward_pq_specials
		// 
		this->contextMenuStrip_reward_pq_specials->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem42, 
			this->toolStripMenuItem43});
		this->contextMenuStrip_reward_pq_specials->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_pq_specials->Size = System::Drawing::Size(150, 48);
		// 
		// toolStripMenuItem42
		// 
		this->toolStripMenuItem42->Name = L"toolStripMenuItem42";
		this->toolStripMenuItem42->Size = System::Drawing::Size(149, 22);
		this->toolStripMenuItem42->Text = L"Add Special";
		// 
		// toolStripMenuItem43
		// 
		this->toolStripMenuItem43->Name = L"toolStripMenuItem43";
		this->toolStripMenuItem43->Size = System::Drawing::Size(149, 22);
		this->toolStripMenuItem43->Text = L"Remove Special";
		// 
		// contextMenuStrip_reward_pq_items
		// 
		this->contextMenuStrip_reward_pq_items->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem22, 
			this->toolStripMenuItem23});
		this->contextMenuStrip_reward_pq_items->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_pq_items->Size = System::Drawing::Size(139, 48);
		// 
		// toolStripMenuItem22
		// 
		this->toolStripMenuItem22->Name = L"toolStripMenuItem22";
		this->toolStripMenuItem22->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem22->Text = L"Add Item";
		// 
		// toolStripMenuItem23
		// 
		this->toolStripMenuItem23->Name = L"toolStripMenuItem23";
		this->toolStripMenuItem23->Size = System::Drawing::Size(138, 22);
		this->toolStripMenuItem23->Text = L"Remove Item";
		// 
		// contextMenuStrip_reward_pq_chases
		// 
		this->contextMenuStrip_reward_pq_chases->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem20, 
			this->toolStripMenuItem21});
		this->contextMenuStrip_reward_pq_chases->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_pq_chases->Size = System::Drawing::Size(147, 48);
		// 
		// toolStripMenuItem20
		// 
		this->toolStripMenuItem20->Name = L"toolStripMenuItem20";
		this->toolStripMenuItem20->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem20->Text = L"Add Chase";
		// 
		// toolStripMenuItem21
		// 
		this->toolStripMenuItem21->Name = L"toolStripMenuItem21";
		this->toolStripMenuItem21->Size = System::Drawing::Size(146, 22);
		this->toolStripMenuItem21->Text = L"Remove Chase";
		// 
		// contextMenuStrip_reward_pq_messages
		// 
		this->contextMenuStrip_reward_pq_messages->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem44, 
			this->toolStripMenuItem45});
		this->contextMenuStrip_reward_pq_messages->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_pq_messages->Size = System::Drawing::Size(159, 48);
		// 
		// toolStripMenuItem44
		// 
		this->toolStripMenuItem44->Name = L"toolStripMenuItem44";
		this->toolStripMenuItem44->Size = System::Drawing::Size(158, 22);
		this->toolStripMenuItem44->Text = L"Add Message";
		// 
		// toolStripMenuItem45
		// 
		this->toolStripMenuItem45->Name = L"toolStripMenuItem45";
		this->toolStripMenuItem45->Size = System::Drawing::Size(158, 22);
		this->toolStripMenuItem45->Text = L"Remove Message";
		// 
		// contextMenuStrip_reward_pq_scripts
		// 
		this->contextMenuStrip_reward_pq_scripts->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem40, 
			this->toolStripMenuItem41});
		this->contextMenuStrip_reward_pq_scripts->Name = L"contextMenuStrip1";
		this->contextMenuStrip_reward_pq_scripts->Size = System::Drawing::Size(144, 48);
		// 
		// toolStripMenuItem40
		// 
		this->toolStripMenuItem40->Name = L"toolStripMenuItem40";
		this->toolStripMenuItem40->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem40->Text = L"Add Script";
		// 
		// toolStripMenuItem41
		// 
		this->toolStripMenuItem41->Name = L"toolStripMenuItem41";
		this->toolStripMenuItem41->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem41->Text = L"Remove Script";
		// 
		// contextMenuStrip_pq_scripts
		// 
		this->contextMenuStrip_pq_scripts->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripMenuItem32, 
			this->toolStripMenuItem33});
		this->contextMenuStrip_pq_scripts->Name = L"contextMenuStrip1";
		this->contextMenuStrip_pq_scripts->Size = System::Drawing::Size(144, 48);
		// 
		// toolStripMenuItem32
		// 
		this->toolStripMenuItem32->Name = L"toolStripMenuItem32";
		this->toolStripMenuItem32->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem32->Text = L"Add Script";
		// 
		// toolStripMenuItem33
		// 
		this->toolStripMenuItem33->Name = L"toolStripMenuItem33";
		this->toolStripMenuItem33->Size = System::Drawing::Size(143, 22);
		this->toolStripMenuItem33->Text = L"Remove Script";
		// 
		// zxTASKeditWindow
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->ClientSize = System::Drawing::Size(931, 654);
		this->Controls->Add(this->tabControl1);
		this->Controls->Add(this->comboBox_search);
		this->Controls->Add(this->treeView_tasks);
		this->Controls->Add(this->button_search);
		this->Controls->Add(this->progressBar_progress);
		this->Controls->Add(this->menuStrip_mainMenu);
		this->Controls->Add(this->textBox_search);
		this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
		this->MaximizeBox = false;
		this->MinimizeBox = true;
		this->MinimumSize = System::Drawing::Size(920, 680);
		this->Name = L"zxTASKeditWindow";
		this->ShowIcon = false;
		this->ShowInTaskbar = false;
		this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
		this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
		this->Text = L" zxTASKedit     Copyright (c) 2010-2020";
		this->menuStrip_mainMenu->ResumeLayout(false);
		this->menuStrip_mainMenu->PerformLayout();
		this->contextMenuStrip_date_spans->ResumeLayout(false);
		this->contextMenuStrip_task->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_date_spans))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_reward_item_group_items))->EndInit();
		this->contextMenuStrip_reward_items->ResumeLayout(false);
		this->groupBox_reward_selector->ResumeLayout(false);
		this->groupBox_reward_selector->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown_reward_item_groups_count))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown_time_factor))->EndInit();
		this->contextMenuStrip_reward_timed->ResumeLayout(false);
		this->groupBox_reward->ResumeLayout(false);
		this->groupBox_reward->PerformLayout();
		this->groupBox_conversation->ResumeLayout(false);
		this->groupBox_conversation->PerformLayout();
		this->groupBox_answers->ResumeLayout(false);
		this->groupBox_answers->PerformLayout();
		this->contextMenuStrip_conversation_answer->ResumeLayout(false);
		this->contextMenuStrip_conversation_question->ResumeLayout(false);
		this->groupBox_questions->ResumeLayout(false);
		this->groupBox_questions->PerformLayout();
		this->groupBox_dialogs->ResumeLayout(false);
		this->groupBox_dialogs->PerformLayout();
		this->groupBox_basic_2->ResumeLayout(false);
		this->groupBox_basic_2->PerformLayout();
		this->contextMenuStrip_pq_messages->ResumeLayout(false);
		this->contextMenuStrip_pq_special_scripts->ResumeLayout(false);
		this->contextMenuStrip_pq_chases->ResumeLayout(false);
		this->contextMenuStrip_pq_script_infos->ResumeLayout(false);
		this->contextMenuStrip_pq_location->ResumeLayout(false);
		this->groupBox_basic_1->ResumeLayout(false);
		this->groupBox_basic_1->PerformLayout();
		this->contextMenuStrip_valid_location->ResumeLayout(false);
		this->contextMenuStrip_fail_location->ResumeLayout(false);
		this->groupBox_trigger_location->ResumeLayout(false);
		this->groupBox_trigger_location->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_trigger_location_spans))->EndInit();
		this->contextMenuStrip_trigger_location->ResumeLayout(false);
		this->groupBox_flags->ResumeLayout(false);
		this->groupBox_flags->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_globals))->EndInit();
		this->contextMenuStrip_morai_pk->ResumeLayout(false);
		this->contextMenuStrip_team_members->ResumeLayout(false);
		this->contextMenuStrip_given_items->ResumeLayout(false);
		this->contextMenuStrip_required_items->ResumeLayout(false);
		this->contextMenuStrip_required_get_items->ResumeLayout(false);
		this->contextMenuStrip_chases->ResumeLayout(false);
		this->groupBox_general->ResumeLayout(false);
		this->groupBox_general->PerformLayout();
		this->groupBox_reach_location->ResumeLayout(false);
		this->groupBox_reach_location->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_reach_location_spans))->EndInit();
		this->contextMenuStrip_reach_location->ResumeLayout(false);
		this->tabControl1->ResumeLayout(false);
		this->tabPage1->ResumeLayout(false);
		this->tabPage2->ResumeLayout(false);
		this->groupBox_fail_location->ResumeLayout(false);
		this->groupBox_fail_location->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_fail_location_spans))->EndInit();
		this->tabPage3->ResumeLayout(false);
		this->tabPage4->ResumeLayout(false);
		this->groupBox_requirements->ResumeLayout(false);
		this->groupBox_requirements->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_dynamics))->EndInit();
		this->contextMenuStrip1->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_team_members))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_given_items))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_items))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_get_items))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_required_chases))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView_tasknum))->EndInit();
		this->tabPage6->ResumeLayout(false);
		this->groupBox1->ResumeLayout(false);
		this->groupBox1->PerformLayout();
		this->groupBox_reward_items->ResumeLayout(false);
		this->groupBox_reward_items->PerformLayout();
		this->tabPage7->ResumeLayout(false);
		this->tabPage5->ResumeLayout(false);
		this->tabPage5->PerformLayout();
		this->contextMenuStrip_reward_pq_specials->ResumeLayout(false);
		this->contextMenuStrip_reward_pq_items->ResumeLayout(false);
		this->contextMenuStrip_reward_pq_chases->ResumeLayout(false);
		this->contextMenuStrip_reward_pq_messages->ResumeLayout(false);
		this->contextMenuStrip_reward_pq_scripts->ResumeLayout(false);
		this->contextMenuStrip_pq_scripts->ResumeLayout(false);
		this->ResumeLayout(false);
		this->PerformLayout();

	}

#pragma endregion

#pragma region I/O FUNCTIONS
	private: System::Void load_taskReadSub(String^  filetaskName, int version){
		FileStream^ fr = File::OpenRead(filetaskName);
		BinaryReader^ br = gcnew BinaryReader(fr);
		int stamp = br->ReadInt32();
		int count = br->ReadInt32();
		int tmp=Tasks->Length;
		int count2=count+tmp;
		//if(count<0) MessageBox::Show("debug[] " + Tasks->Length+"c:"+count);
		Array::Resize(Tasks, count2); //<Task^>
        Array::Resize(ItemStreamPositions, count2);
		for(int i=tmp; i<count2; i++)
		{
			ItemStreamPositions[i] = br->ReadInt32();
		}

		for(int i=tmp; i<count2; i++)
		{
			Application::DoEvents();
			//MessageBox::Show("debug sdas["+i+"] " + ItemStreamPositions[i]+"c:"+count2);
			Tasks[i] = gcnew Task(version, br, ItemStreamPositions[i],treeView_tasks->Nodes);
		}

		br->Close();
		fr->Close();
	}
	private: System::Void click_load(System::Object^  sender, System::EventArgs^  e)
	{
		OpenFileDialog^ load = gcnew OpenFileDialog();
		String^ fileName2;
        String^ extension;
		load->Filter = "Tasks File (*.data)|*.data|All Files (*.*)|*.*";
		if(load->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(load->FileName))
		{
			try
			{
				extension = Path::GetExtension(load->FileName);
                fileName2 = Path::GetFileName(load->FileName);
				//MessageBox::Show("debug " + extension +";"+fileName2);
				if((".data" == extension) && ("tasks.data" == fileName2)){
					Cursor = Windows::Forms::Cursors::WaitCursor;
					progressBar_progress->Style = ProgressBarStyle::Marquee;

					treeView_tasks->Nodes->Clear();
					treeView_tasks->BeginUpdate();

					FileStream^ fr2 = File::OpenRead(load->FileName);
					BinaryReader^ br2 = gcnew BinaryReader(fr2);

					int stamp = br2->ReadInt32();
					version = br2->ReadInt32();

					if(!versions->Contains(version))
					{
						br2->Close();
						fr2->Close();
						treeView_tasks->EndUpdate();
						progressBar_progress->Style = ProgressBarStyle::Continuous;
						Cursor = Windows::Forms::Cursors::Default;
						MessageBox::Show("Version Unsupported: " + version.ToString());
						return;
					}
					int num5 = br2->ReadInt32();
					int num4 = br2->ReadInt32();
					int num2 = br2->ReadInt32();

					ItemStreamPositions = gcnew array<int>(0);
					Tasks = gcnew array<Task^>(0);
					for(int num=0;num<num2;num++){
						Application::DoEvents();
						int tmp=num + 1;//;
						load_taskReadSub(String::Concat(load->FileName, tmp.ToString()),version);
					}
					
					br2->Close();
					fr2->Close();
					
					treeView_tasks->EndUpdate();
					treeView_tasks->SelectedNode = treeView_tasks->Nodes[0];

					this->Text = " jTASKedit (" + load->FileName + ")";

					progressBar_progress->Style = ProgressBarStyle::Continuous;
					Cursor = Windows::Forms::Cursors::Default;

					MessageBox::Show("Version: " + version.ToString() + "\n\n", "Loading Complete");
				}
				else{
					//treeView_tasks->Nodes->Clear();
					//treeView_tasks->BeginUpdate();
					//ItemStreamPositions = gcnew array<int>(0);
					//Tasks = gcnew array<Task^>(0);
					treeView_tasks->EndUpdate();
					MessageBox::Show("You need to open tasks.data file.");
					//int version = 143;
					//load_taskReadSub(load->FileName,version);
					//treeView_tasks->EndUpdate();
					//treeView_tasks->SelectedNode = treeView_tasks->Nodes[0];
					//MessageBox::Show("Version: 143\n\n", "Loading Complete");
				}
			}
			catch(Exception^ e)
			{
				treeView_tasks->EndUpdate();
				progressBar_progress->Style = ProgressBarStyle::Continuous;
				Cursor = Windows::Forms::Cursors::Default;
				MessageBox::Show("IMPORT ERROR!\n\n" + e->Message);
			}
		}
	}
	private: array<unsigned char>^ GetFileMD5(String^ filePath)
    {
            FileStream^ stream =  File::OpenRead(filePath);
            MD5CryptoServiceProvider^ provider = gcnew MD5CryptoServiceProvider();
			array<unsigned char>^ hash = gcnew array<unsigned char>(32);
            int num3 = 16;
			array<unsigned char>^ arr = gcnew array<unsigned char>(num3);
            provider->Initialize();
            int num2 = 0;
            while (true)
            {
                if (num2 >= stream->Length)
                {
                    break;
                }
                __int64 num = num3;
                if ((num2 + num) > stream->Length)
                {
                    num = (stream->Length - num2);
                }
                stream->Read(arr, 0, Convert::ToInt32(num));
                if ((num2 + num) < stream->Length)
                {
                    provider->TransformBlock(arr, 0, Convert::ToInt32(num), arr, 0);
                }
                else
                {
                    provider->TransformFinalBlock(arr, 0, Convert::ToInt32(num));
                }
                num2 += num3;
            }
            stream->Close();
            hash = provider->Hash;
            provider->Clear();
            return hash;
    }
	private: System::Void click_save(System::Object^  sender, System::EventArgs^  e)
	{
		// Leave Focus to ensure all changes are saved...
		menuStrip_mainMenu->Focus();

		SaveFileDialog^ save = gcnew SaveFileDialog();
		save->Filter = "Tasks File (*.data)|*.data|All Files (*.*)|*.*";
		if(save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
		{
			try
			{
				Cursor = Windows::Forms::Cursors::WaitCursor;
				progressBar_progress->Style = ProgressBarStyle::Marquee;
				//int version=159;
				int task_packs=Tasks->Length/300;
				int modulo=Tasks->Length%300;
				if(modulo>0) task_packs++;
				//MessageBox::Show("save test " + task_packs + "-" + modulo);
				int stamp = 110314836;
				int task_limit = 300;
				int current_task=0;
				for(int t=1;t<=task_packs;t++){
					FileStream^ fs = gcnew FileStream(save->FileName+t.ToString(), FileMode::Create, FileAccess::Write);
					BinaryWriter^ bw = gcnew BinaryWriter(fs);
					if(modulo>0 && t == task_packs) task_limit=modulo;
					bw->Write(stamp);
					bw->Write(task_limit);

					array<int>^ ItemStreamPositions = gcnew array<int>(task_limit);
					Application::DoEvents();
					// write placeholder
					bw->Write(gcnew array<unsigned char>(4*task_limit));
					for(int i=0; i<task_limit; i++)
					{
						ItemStreamPositions[i] = (int)bw->BaseStream->Position;
						Tasks[current_task]->Save(version, bw);
						current_task++;
					}
					
					// insert into placeholder
					bw->BaseStream->Position = 8; 
					for(int i=0; i<ItemStreamPositions->Length; i++)
					{
						bw->Write(ItemStreamPositions[i]);
					}
					
					bw->Close();
					fs->Close();
				}
				FileStream^ fs = gcnew FileStream(save->FileName, FileMode::Create, FileAccess::Write);
				BinaryWriter^ bw = gcnew BinaryWriter(fs);
				stamp = 1765016324;
				bw->Write(stamp);
				bw->Write(version);
				bw->Write(1);
				bw->Write(Tasks->Length);
				bw->Write(task_packs);
				//array<unsigned char>^ hash = gcnew array<unsigned char>(32);
				for(int t=1;t<=task_packs;t++)
				{
					bw->Write(GetFileMD5(save->FileName+t.ToString()));
				}
				bw->Close();
				fs->Close();
				MessageBox::Show("Tasks saved!");
				progressBar_progress->Style = ProgressBarStyle::Continuous;
				Cursor = Windows::Forms::Cursors::Default;
			}
			catch(Exception^ e)
			{
				progressBar_progress->Style = ProgressBarStyle::Continuous;
				Cursor = Windows::Forms::Cursors::Default;
				MessageBox::Show("EXPORT ERROR!\n\n" + e->Message);
			}
		}
	}
	private: System::Void click_ImportTask(System::Object^  sender, System::EventArgs^  e)
	{
		OpenFileDialog^ qLoad = gcnew OpenFileDialog();
		qLoad->Filter = "Task File (*.data)|*.data|All Files (*.*)|*.*";
		if(qLoad->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(qLoad->FileName))
		{
			try
			{
				Cursor = Windows::Forms::Cursors::AppStarting;

				FileStream^ fr = File::OpenRead(qLoad->FileName);
				BinaryReader^ br = gcnew BinaryReader(fr);

				br->ReadInt32(); // stamp
				int version = br->ReadInt32();
				int count = br->ReadInt32();

				// offset list
				// 4*count bytes

				array<Task^>^ temp = gcnew array<Task^>(Tasks->Length+1);
				temp[temp->Length-1] = gcnew Task(version, br, 4*count+12, treeView_tasks->Nodes);
				Array::Copy(Tasks, 0, temp, 0, Tasks->Length);
				Tasks = temp;

				br->Close();
				fr->Close();

				treeView_tasks->SelectedNode = treeView_tasks->Nodes[treeView_tasks->Nodes->Count-1];

				Cursor = Windows::Forms::Cursors::Default;
			}
			catch(...)
			{
				MessageBox::Show("IMPORT ERROR!");
				Cursor = Windows::Forms::Cursors::Default;
			}
		}
	}
	private: System::Void click_ExportTask(System::Object^  sender, System::EventArgs^  e)
	{
		SaveFileDialog^ qSave = gcnew SaveFileDialog();
		qSave->Filter = "Task File (*.data)|*.data|All Files (*.*)|*.*";
		if(qSave->ShowDialog() == Windows::Forms::DialogResult::OK && qSave->FileName != "")
		{
			try
			{
				Cursor = Windows::Forms::Cursors::AppStarting;

				FileStream^ fs = gcnew FileStream(qSave->FileName, FileMode::Create, FileAccess::Write);
				BinaryWriter^ bw = gcnew BinaryWriter(fs);

				int version = 9999;
				bw->Write(0); // stamp
				bw->Write(version);
				bw->Write((int)1);

				// offset list
				bw->Write((int)16);

				SelectedTask->Save(version, bw);

				bw->Close();
				fs->Close();

				Cursor = Windows::Forms::Cursors::Default;
			}
			catch(...)
			{
				MessageBox::Show("EXPORT ERROR!\nExporting item to binary file failed!");
				Cursor = Windows::Forms::Cursors::Default;
			}
		}
	}

#pragma endregion

#pragma region SELECTOR FUNCTIONS

	private: System::Void select_task(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e)
	{
		ArrayList^ task_index_path = gcnew ArrayList();
		TreeNode^ node = ((TreeView^)sender)->SelectedNode;
		radioButton_success->Checked=true;
		while(node->Parent != nullptr)
		{
			task_index_path->Add(node->Index);
			node = node->Parent;
		}
		SelectedTask = Tasks[node->Index];
		//MessageBox::Show("task select:"+node->Index+"="+SelectedTask->sub_quest_count);
		for(int i=task_index_path->Count-1; i>=0; i--)
		{
			//MessageBox::Show("sub select:"+task_index_path[i]+";sub_tasks:"+SelectedTask->sub_quests->Length);
			SelectedTask = SelectedTask->sub_quests[(int)task_index_path[i]];
		}
		//MessageBox::Show("sub["+SelectedTask->ID+"]"+SelectedTask->Name);
		// general
		try{
			textBox_id->Text = SelectedTask->ID.ToString();
			textBox_name->Text = SelectedTask->Name;
			checkBox_author_mode->Checked = SelectedTask->author_mode;
			textBox_author_text->Text = SelectedTask->AuthorText;
			textBox_unknown_01->Text = SelectedTask->UNKNOWN_01.ToString();
			textBox_type->Text = SelectedTask->type.ToString();
			textBox_time_limit->Text = SelectedTask->time_limit.ToString();
			checkBox_unknown_02->Checked = SelectedTask->UNKNOWN_02;
			textBox_xunhan->Text = SelectedTask->xunhanType.ToString();
			textBox_cooldown->Text = SelectedTask->cooldown.ToString();
			textBox_resettype->Text = SelectedTask->reset_type.ToString();
			textBox_resetcycle->Text = SelectedTask->reset_cycle.ToString();
			checkBox_trigger_on->Checked = SelectedTask->has_trigger_on;
			checkBox_unknown_03->Checked = SelectedTask->val_unknown_01;
			checkBox_has_date_spans->Checked = SelectedTask->has_date_spans;
			dataGridView_date_spans->Rows->Clear();
			for(int r=0; r<SelectedTask->date_spans->Length; r++)
			{
				try
				{
					dataGridView_date_spans->Rows->Add(gcnew array<String^>
					{
						SelectedTask->date_spans[r]->from->year.ToString(),
						SelectedTask->date_spans[r]->from->month.ToString(),
						SelectedTask->date_spans[r]->from->day.ToString(),
						SelectedTask->date_spans[r]->from->hour.ToString(),
						SelectedTask->date_spans[r]->from->minute.ToString(),
						Column6->Items[SelectedTask->date_spans[r]->from->weekday]->ToString(),
						"to",
						SelectedTask->date_spans[r]->to->year.ToString(),
						SelectedTask->date_spans[r]->to->month.ToString(),
						SelectedTask->date_spans[r]->to->day.ToString(),
						SelectedTask->date_spans[r]->to->hour.ToString(),
						SelectedTask->date_spans[r]->to->minute.ToString(),
						Column13->Items[SelectedTask->date_spans[r]->to->weekday]->ToString()
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Date Span!"+SelectedTask->date_spans->Length);
				}
			}
			textBox_autounk->Text = SelectedTask->autotask_unknown.ToString();
			textBox_cleartask->Text = SelectedTask->cleartask_unknown.ToString();
			textBox_unknown_04->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_157);
			textBox_unknown_05->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_ZEROS);
			textBox_unknown_06->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_112_1);
			textBox_unknown_07->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_113);
			textBox_unknown_08->Text = ByteArray_to_HexString(SelectedTask->GlobalVal1);
			textBox_unknown_09->Text = ByteArray_to_HexString(SelectedTask->GlobalVal2);
			checkBox_has_date_fail->Checked =  SelectedTask->val_unknown_02;
			// flags
			checkBox_activate_first_subquest->Checked = SelectedTask->activate_first_subquest;
			checkBox_activate_random_subquest->Checked = SelectedTask->activate_random_subquest;
			checkBox_activate_next_subquest->Checked = SelectedTask->activate_next_subquest;
			checkBox_on_give_up_parent_fails->Checked = SelectedTask->on_give_up_parent_fails;
			checkBox_on_success_parent_success->Checked = SelectedTask->on_success_parent_success;
			checkBox_can_give_up->Checked = SelectedTask->can_give_up;
			checkBox_repeatable->Checked = SelectedTask->repeatable;
			checkBox_repeatable_after_failure->Checked = SelectedTask->repeatable_after_failure;
			checkBox_fail_on_death->Checked = SelectedTask->fail_on_death;
			checkBox_on_fail_parent_fail->Checked = SelectedTask->on_fail_parent_fail;
			checkBox_unknown_10->Checked = SelectedTask->UNKNOWN_10;
			textBox_player_limit->Text = SelectedTask->player_limit.ToString();
			// trigger locations
			checkBox_trigger_locations_has_spans->Checked = SelectedTask->trigger_locations->has_location;
			textBox_trigger_locations_map_id->Text = SelectedTask->trigger_locations->map_id.ToString();
			dataGridView_trigger_location_spans->Rows->Clear();
			for(int r=0; r<SelectedTask->trigger_locations->spans->Length; r++)
			{
				try
				{
					dataGridView_trigger_location_spans->Rows->Add(gcnew array<String^>
					{
						SelectedTask->trigger_locations->spans[r]->north.ToString("F3"),
						SelectedTask->trigger_locations->spans[r]->south.ToString("F3"),
						SelectedTask->trigger_locations->spans[r]->west.ToString("F3"),
						SelectedTask->trigger_locations->spans[r]->east.ToString("F3"),
						SelectedTask->trigger_locations->spans[r]->bottom.ToString("F3"),
						SelectedTask->trigger_locations->spans[r]->top.ToString("F3")
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Trigger Location Span!");
				}
			}
		
			// fail locations
			checkBox_fail_locations_has_spans->Checked = SelectedTask->fail_locations->has_location;
			textBox_fail_locations_map_id->Text = SelectedTask->fail_locations->map_id.ToString();
			dataGridView_fail_location_spans->Rows->Clear();
			for(int r=0; r<SelectedTask->fail_locations->spans->Length; r++)
			{
				try
				{
					dataGridView_fail_location_spans->Rows->Add(gcnew array<String^>
					{
						SelectedTask->fail_locations->spans[r]->north.ToString("F3"),
						SelectedTask->fail_locations->spans[r]->south.ToString("F3"),
						SelectedTask->fail_locations->spans[r]->west.ToString("F3"),
						SelectedTask->fail_locations->spans[r]->east.ToString("F3"),
						SelectedTask->fail_locations->spans[r]->bottom.ToString("F3"),
						SelectedTask->fail_locations->spans[r]->top.ToString("F3")
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Fail Location Span!");
				}
			}
		
			// basic I
			checkBox_unknown_17->Checked = SelectedTask->UNKNOWN_17;
			checkBox_has_instant_teleport->Checked = SelectedTask->has_instant_teleport;
			textBox_instant_teleport_location_map_id->Text = SelectedTask->instant_teleport_location->map_id.ToString();
			textBox_instant_teleport_location_x->Text = SelectedTask->instant_teleport_location->x.ToString("F3");
			textBox_instant_teleport_location_alt->Text = SelectedTask->instant_teleport_location->altitude.ToString("F3");
			textBox_instant_teleport_location_z->Text = SelectedTask->instant_teleport_location->z.ToString("F3");
			textBox_ai_trigger->Text = SelectedTask->ai_trigger.ToString();
			checkBox_unknown_18->Checked = SelectedTask->UNKNOWN_18;
			checkBox_unknown_19->Checked = SelectedTask->UNKNOWN_19;
			checkBox_unknown_20->Checked = SelectedTask->UNKNOWN_20;
			checkBox_unknown_21->Checked = SelectedTask->UNKNOWN_21;
			checkBox_unknown_22->Checked = SelectedTask->UNKNOWN_22;
			checkBox_unknown_23->Checked = SelectedTask->UNKNOWN_23;
			textBox_component->Text = SelectedTask->component.ToString();
			textBox_classify->Text = SelectedTask->classify.ToString();
			checkBox_clantask->Checked = SelectedTask->clan_task;
			checkBox_plimit->Checked = SelectedTask->player_limit_on;
			checkBox_allzone->Checked = SelectedTask->all_zone;
			textBox_allzonenum->Text = SelectedTask->UNKNOWN_LEVEL.ToString();
			textBox_plimitnum->Text = SelectedTask->player_limit_num.ToString();
			textBox_unkpenalty->Text = SelectedTask->unkpenalty.ToString();
			textBox_playerpenalty->Text = SelectedTask->playerpenalty.ToString();
			textBox_allkillnum->Text = SelectedTask->allkillnum.ToString();
			checkBox_haslimit->Checked = SelectedTask->haslimit;

			textBox_unknown_level->Text = SelectedTask->UNKNOWN_LEVEL.ToString();
			checkBox_mark_available_icon->Checked = SelectedTask->mark_available_icon;
			checkBox_mark_available_point->Checked = SelectedTask->mark_available_point;
			textBox_quest_npc->Text = SelectedTask->quest_npc.ToString();
			textBox_reward_npc->Text = SelectedTask->reward_npc.ToString();
			textBox_newtype->Text = SelectedTask->new_type.ToString();
			textBox_newtype2->Text = SelectedTask->new_type2.ToString();
			textBox_level_min->Text = SelectedTask->level_min.ToString();
			textBox_level_max->Text = SelectedTask->level_max.ToString();
			checkBox_showlvl->Checked = SelectedTask->has_show_level;
			textBox_bldbmin->Text = SelectedTask->bloodbound_min.ToString();
			textBox_bldbmax->Text = SelectedTask->bloodbound_max.ToString();
			checkBox_unknown_27->Checked = SelectedTask->UNKNOWN_27;
			checkBox_valunk3->Checked = SelectedTask->val_unknown_03;
			checkBox_iunk4->Checked = SelectedTask->items_unknown_4;
			checkBox_iunk5->Checked = SelectedTask->items_unknown_5;
			textBox_itunk06->Text = SelectedTask->items_unknown_6.ToString();
			textBox_itunk07->Text = SelectedTask->items_unknown_7.ToString();
			textBox_iunk09->Text = SelectedTask->items_unknown_9.ToString();
			textBox_iunk10->Text = SelectedTask->items_unknown_10.ToString();
			textBox_iunk11->Text = SelectedTask->items_unknown_11.ToString();
			checkBox_tasknum->Checked = SelectedTask->has_show_tasks;
			dataGridView_tasknum->Rows->Clear();
			for(int r=0; r<SelectedTask->front_tasks_a->Length; r++)
			{
				try
				{
					dataGridView_tasknum->Rows->Add(gcnew array<String^>
					{
						SelectedTask->front_tasks_a[r].ToString(),
						SelectedTask->front_tasks_num[r].ToString()
					});
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading task nums!");
				}
			}
			textBox_yaoqiu->Text = SelectedTask->front_tasks_yaoqiu.ToString();
			textBox_renwu->Text = SelectedTask->front_tasks_renwu.ToString();
			textBox_zhuanchonglv->Text = SelectedTask->front_tasks_zhuanchonglv.ToString();
			textBox_frontgender->Text = SelectedTask->front_tasks_xingbie.ToString();
			textBox_unknfront->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_114);
			checkBox_frontalliance->Checked =  SelectedTask->front_tasks_showbangpai;
			checkBox_frontgen->Checked =  SelectedTask->front_tasks_showgender;
			checkBox_licreq->Checked =  SelectedTask->front_tasks_yaoqiubanpai;
			checkBox_frontunkn01->Checked =  SelectedTask->how_unknown01;
			//checkBox_craft_skill->Checked = SelectedTask->craft_skill;
			//checkBox_unknown_24->Checked = SelectedTask->UNKNOWN_24;
			//checkBox_unknown_25->Checked = SelectedTask->UNKNOWN_25;
			//checkBox_unknown_26->Checked = SelectedTask->UNKNOWN_26;
			//textBox_unknown_26_01->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_26_01);
			// required items
			listBox_jobs->Items->Clear();
			for(int j=0; j<SelectedTask->jobs->Length; j++ )//conuter check?
			{	
				listBox_jobs->Items->Add(SelectedTask->jobs[j].ToString());
			}
			dataGridView_required_items->Rows->Clear();
			for(int r=0; r<SelectedTask->required_items->Length; r++)
			{
				try
				{
					dataGridView_required_items->Rows->Add(gcnew array<String^>
					{
						SelectedTask->required_items[r]->id.ToString(),
						SelectedTask->required_items[r]->unknown.ToString(),
						SelectedTask->required_items[r]->amount.ToString(),
						SelectedTask->required_items[r]->probability.ToString("F6"),
						SelectedTask->required_items[r]->expiration.ToString()
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Required Items!");
				}
			}
		
			textBox_required_items_unknown->Text = SelectedTask->required_items_unknown.ToString();
		
			// given items
			dataGridView_given_items->Rows->Clear();
			for(int r=0; r<SelectedTask->given_items->Length; r++)
			{
				try
				{
					dataGridView_given_items->Rows->Add(gcnew array<String^>
					{
						SelectedTask->given_items[r]->id.ToString(),
						SelectedTask->given_items[r]->unknown.ToString(),
						SelectedTask->given_items[r]->amount.ToString(),
						SelectedTask->given_items[r]->probability.ToString("F6"),
						SelectedTask->given_items[r]->expiration.ToString()
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Given Items!");
				}
			}
			
			//textBox_unknown_29->Text = SelectedTask->UNKNOWN_29.ToString();
			//textBox_unknown_30->Text = SelectedTask->UNKNOWN_30.ToString();
			checkBox_rineedbag->Checked = SelectedTask->required_items_bags;
			textBox_hasnextval->Text = SelectedTask->required_need_next.ToString();
			checkBox_hasnext->Checked = SelectedTask->has_next;
			checkBox_needclan->Checked = SelectedTask->clan_required;
			checkBox_contribu->Checked = SelectedTask->has_contribution;
			textBox_workval->Text = SelectedTask->required_need_working.ToString();
			checkBox_haswork->Checked = SelectedTask->has_working;
			textBox_autoval->Text = SelectedTask->required_need_xianji.ToString();
			checkBox_hasauto->Checked =  SelectedTask->has_xianji;
			comboBox_required_gender->SelectedIndex = SelectedTask->required_need_gender;
			checkBox_needgender->Checked = SelectedTask->has_gender;
			textBox_rep1->Text = SelectedTask->shengwang_guidao.ToString();
			textBox_rep2->Text = SelectedTask->shengwang_qingyun.ToString();
			textBox_rep3->Text = SelectedTask->shengwang_tianyin.ToString();
			textBox_rep4->Text = SelectedTask->shengwang_guiwang.ToString();
			textBox_rep5->Text = SelectedTask->shengwang_hehuan.ToString();
			textBox_rep6->Text = SelectedTask->shengwang_zhongyi.ToString();
			textBox_rep7->Text = SelectedTask->shengwang_qingyuan.ToString();
			textBox_rep8->Text = SelectedTask->shengwang_wencai.ToString();
			textBox_rep9->Text = SelectedTask->shengwang_shide.ToString();
			textBox_rep10->Text = SelectedTask->shengwang_yuliu_01.ToString();
			textBox_rep11->Text = SelectedTask->shengwang_yuliu_02.ToString();
			textBox_rep12->Text = SelectedTask->shengwang_yuliu_03.ToString();
			textBox_rep13->Text = SelectedTask->shengwang_yuliu_04.ToString();
			textBox_rep14->Text = SelectedTask->shengwang_jiuli.ToString();
			textBox_rep15->Text = SelectedTask->shengwang_lieshan.ToString();
			textBox_rep16->Text = SelectedTask->shengwang_chenhuang.ToString();
			textBox_rep17->Text = SelectedTask->shengwang_taihao.ToString();
			textBox_rep18->Text = SelectedTask->shengwang_huaiguang.ToString();
			textBox_rep19->Text = SelectedTask->shengwang_tianhua.ToString();
			textBox_rep20->Text = SelectedTask->shengwang_fenxiang.ToString();
			textBox_unknown_44->Text = ByteArray_to_HexString(SelectedTask->shengwang_unknown_48);
			checkBox_hassheng->Checked = SelectedTask->has_shengwang;
			textBox_required_quests_done_1->Text = SelectedTask->front_tasks[0].ToString();
			textBox_required_quests_done_2->Text = SelectedTask->front_tasks[1].ToString();
			textBox_required_quests_done_3->Text = SelectedTask->front_tasks[2].ToString();
			textBox_required_quests_done_4->Text = SelectedTask->front_tasks[3].ToString();
			textBox_required_quests_done_5->Text = SelectedTask->front_tasks[4].ToString();
			textBox_required_quests_undone_1->Text = SelectedTask->mutexs[0].ToString();
			textBox_required_quests_undone_2->Text = SelectedTask->mutexs[1].ToString();
			textBox_required_quests_undone_3->Text = SelectedTask->mutexs[2].ToString();
			textBox_required_quests_undone_4->Text = SelectedTask->mutexs[3].ToString();
			textBox_required_quests_undone_5->Text = SelectedTask->mutexs[4].ToString();
			// reach locations
			checkBox_reach_locations_has_spans->Checked = SelectedTask->reach_locations->has_location;
			textBox_reach_locations_map_id->Text = SelectedTask->reach_locations->map_id.ToString();
			dataGridView_reach_location_spans->Rows->Clear();
			for(int r=0; r<SelectedTask->reach_locations->spans->Length; r++)
			{
				try
				{
					dataGridView_reach_location_spans->Rows->Add(gcnew array<String^>
					{
						SelectedTask->reach_locations->spans[r]->north.ToString("F3"),
						SelectedTask->reach_locations->spans[r]->south.ToString("F3"),
						SelectedTask->reach_locations->spans[r]->west.ToString("F3"),
						SelectedTask->reach_locations->spans[r]->east.ToString("F3"),
						SelectedTask->reach_locations->spans[r]->bottom.ToString("F3"),
						SelectedTask->reach_locations->spans[r]->top.ToString("F3")
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Reach Location Span!");
				}
			}
			textBox_instant_pay_coins->Text = SelectedTask->instant_pay_coins.ToString();
			//checkBox_unknown_31->Checked = SelectedTask->UNKNOWN_31;
			// reward
			//MessageBox::Show("sd4!");
			/*
			listBox_reward_timed->Items->Clear();
			for(int i=0; i<SelectedTask->rewards_timed->Length; i++)
			{
				listBox_reward_timed->Items->Add("Success Time [sec]: " + (SelectedTask->time_limit*SelectedTask->rewards_timed_factors[i]));
			}
			*/
			select_reward(nullptr, nullptr);
			//MessageBox::Show("sdfdd34!"); done
			// conversation
			textBox_conversation_prompt_text->Text = SelectedTask->conversation->PromptText;
			textBox_conversation_general_text->Text = SelectedTask->conversation->GeneralText;
			textBox_agernmess->Text = SelectedTask->conversation->AgernText;
			textBox_xanfumess->Text = SelectedTask->conversation->XuanfuText;
			textBox_newtxt->Text = SelectedTask->conversation->NewText;
			// answers
			textBox_conversation_answer_text->Clear();
			textBox_conversation_answer_question_link->Clear();
			textBox_conversation_answer_task_link->Clear();
			listBox_conversation_answers->Items->Clear();
			// questions
			textBox_conversation_question_text->Clear();
			textBox_conversation_question_id->Clear();
			textBox_conversation_question_previous->Clear();
			listBox_conversation_questions->Items->Clear();
			// dialog
			textBox_conversation_dialog_text->Clear();
			textBox_conversation_dialog_unknown->Clear();
			listBox_conversation_dialogs->SelectedIndex = -1;
		}
		catch(Exception^ e)
		{
			MessageBox::Show("ERROR Loading task data! "+ e->Message);
		}
		// team members
		dataGridView_team_members->Rows->Clear();
		for(int r=0; r<SelectedTask->required_team_member_groups->Length; r++)
		{
			try
			{
				dataGridView_team_members->Rows->Add(gcnew array<String^>
				{
					SelectedTask->required_team_member_groups[r]->level_min.ToString(),
					SelectedTask->required_team_member_groups[r]->level_max.ToString(),
					SelectedTask->required_team_member_groups[r]->race.ToString(),
					SelectedTask->required_team_member_groups[r]->job.ToString(),
					SelectedTask->required_team_member_groups[r]->gender.ToString(),
					SelectedTask->required_team_member_groups[r]->amount_min.ToString(),
					SelectedTask->required_team_member_groups[r]->amount_max.ToString(),
					SelectedTask->required_team_member_groups[r]->quest.ToString(),
					SelectedTask->required_team_member_groups[r]->Unknown_01.ToString(),
					SelectedTask->required_team_member_groups[r]->Unknown_02.ToString()
					//bool Unknown_02;
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Team Members!");
			}
		}
		dataGridView_globals->Rows->Clear();
		dataGridView_globals->Rows->Add(gcnew array<String^>
				{
					SelectedTask->global_value_set1->val_1.ToString(),
					SelectedTask->global_value_set1->val_2.ToString(),
					SelectedTask->global_value_set1->val_3.ToString(),
					SelectedTask->global_value_set1->val_count.ToString()
				});
		dataGridView_globals->Rows->Add(gcnew array<String^>
				{
					SelectedTask->global_value_set2->val_1.ToString(),
					SelectedTask->global_value_set2->val_2.ToString(),
					SelectedTask->global_value_set2->val_3.ToString(),
					SelectedTask->global_value_set2->val_count.ToString()
				});
		dataGridView_globals->Rows->Add(gcnew array<String^>
				{
					SelectedTask->global_value_set3->val_1.ToString(),
					SelectedTask->global_value_set3->val_2.ToString(),
					SelectedTask->global_value_set3->val_3.ToString(),
					SelectedTask->global_value_set3->val_count.ToString()
				});
		textBox_next_quest->Text = SelectedTask->next_quest.ToString();
		checkBox_frontmarried->Checked = SelectedTask->front_tasks_marry;
		checkBox_teamask->Checked = SelectedTask->Team_asks;
		checkBox_teamshare->Checked = SelectedTask->Team_share_again;
		checkBox_teamunk->Checked = SelectedTask->Team_UNKNUWN_01;
		checkBox_teamshare2->Checked = SelectedTask->Team_share_now;
		checkBox_teamchecknum->Checked = SelectedTask->Team_check_mem;
		checkBox_leaderfail->Checked = SelectedTask->Team_duizhang_shibai;
		checkBox_tamaccfail->Checked = SelectedTask->Team_quan_shibai;
		checkBox_leavefail->Checked = SelectedTask->Team_leave_shibai;
		checkBox_viewreq->Checked = SelectedTask->Shitu_yaoqiu;
		checkBox_view->Checked = SelectedTask->Shitu_shitu;
		checkBox_vchef->Checked = SelectedTask->Shitu_chushi;
		checkBox_vpeach->Checked = SelectedTask->Shitu_pantaohui;
		checkBox_vunkn1->Checked = SelectedTask->Shitu_UNKNOWN_01;
		checkBox_vunkn2->Checked = SelectedTask->Shitu_UNKNOWN_02;
		checkBox_clanc->Checked = SelectedTask->Jiazu_jiazu_tasks;
		checkBox_clanlead->Checked = SelectedTask->Jiazu_zuzhang_tasks;
		checkBox_blded->Checked = SelectedTask->BangLing_kouchu;
		checkBox_frontfeng01->Checked = SelectedTask->front_tasks_feisheng_01;
		checkBox_frontfeng02->Checked = SelectedTask->front_tasks_feisheng_02;
		checkBox_frongfang03->Checked = SelectedTask->front_tasks_feisheng;
		checkBox_nval->Checked = SelectedTask->is_val;
		checkBox_soulminom->Checked = SelectedTask->Soul_min_on;
		checkBox_soulmaxon->Checked = SelectedTask->Soul_max_on;
		checkBox_conval2->Checked = SelectedTask->is_val_1;
		checkBox_unknown31->Checked = SelectedTask->UNKNOWN_31;
		
		textBox_unkn115->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_115);
		textBox_unkn116->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_116);
		textBox_unkn118->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_118);
		textBox_unkn119->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_119);
		textBox_unkn120->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_120);
		textBox_unkn121->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_121);
		textBox_unkn122->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_122);
		textBox_unkn123->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_123);
		textBox_ubkb124->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_124);
		textBox_unkn125->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_125);
		textBox_unkn126->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_126);
		textBox_unkn127->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_127);
		textBox_nval1->Text = ByteArray_to_HexString(SelectedTask->Nval_1);
		textBox_nval2->Text = ByteArray_to_HexString(SelectedTask->Nval_2);
		textBox_nval3->Text = ByteArray_to_HexString(SelectedTask->Nval_3);
		textBox_nval4->Text = ByteArray_to_HexString(SelectedTask->Nval_4);
		textBox_unkn128->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_128);
		textBox_unkn1628->Text = ByteArray_to_HexString(SelectedTask->Unknown_1628);
		textBox_unkn129->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_129);
		textBox_unkn23456->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_234556);
		textBox_unkn130->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_130);
		textBox_unkn131->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_131);
		textBox_unkn132->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_132);
		textBox_unkn133->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_133);
		textBox_sval1->Text = ByteArray_to_HexString(SelectedTask->sNval_1);
		textBox_sval2->Text = ByteArray_to_HexString(SelectedTask->sNval_2);
		textBox_sval3->Text = ByteArray_to_HexString(SelectedTask->sNval_3);
		textBox_sval4->Text = ByteArray_to_HexString(SelectedTask->sNval_4);
		textBox_sval5->Text = ByteArray_to_HexString(SelectedTask->sNval_5);
		textBox_unkn134->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_134);

		textBox_minpk->Text = SelectedTask->pk_min.ToString();
		textBox_maxpk->Text = SelectedTask->pk_max.ToString();
		textBox_viewlvl->Text = SelectedTask->Shitu_lv.ToString();
		textBox_vtask->Text = SelectedTask->Shitu_tasks.ToString();
		textBox_clanskillmin->Text = SelectedTask->Jiazu_jineng_lv_min.ToString();
		textBox_clanskillmax->Text = SelectedTask->Jiazu_jineng_lv_max.ToString();
		textBox_clanskillexpmin->Text = SelectedTask->Jiazu_jineng_shulian_min.ToString();
		textBox_clanskillexpmax->Text = SelectedTask->Jiazu_jineng_shulian_max.ToString();
		textBox_clanskillid->Text = SelectedTask->Jiazu_jineng_id.ToString();
		textBox_clanmap->Text = SelectedTask->Jiazu_map_id.ToString();
		textBox_clanchmin->Text = SelectedTask->Jiazu_tiaozhan_min.ToString();
		textBox_clanchmax->Text = SelectedTask->Jiazu_tiaozhan_max.ToString();
		textBox_blamon->Text = SelectedTask->BangLing_shuliang.ToString();
		textBox_zhenying->Text = SelectedTask->zhenying.ToString();
		textBox_ingotmin->Text = SelectedTask->Ingot_min.ToString();
		textBox_ingotmax->Text = SelectedTask->Ingot_max.ToString();
		textBox_val1len->Text = SelectedTask->Nval_1_len.ToString();
		textBox_val2len->Text = SelectedTask->Nval_2_len.ToString();
		textBox_val3len->Text = SelectedTask->Nval_3_len.ToString();
		textBox_val4len->Text = SelectedTask->Nval_4_len.ToString();
		textBox_soulunkn1->Text = SelectedTask->UNKNOWN_Soul_01.ToString();
		textBox_soulmin->Text = SelectedTask->Soul_min.ToString();
		textBox_soulmax->Text = SelectedTask->Soul_max.ToString();
		textBox_method->Text = SelectedTask->JinXingFangShi.ToString();
		textBox_completetype->Text = SelectedTask->WanChengLeiXing.ToString();
		textBox_noncash->Text = SelectedTask->HuaFeiJinQian.ToString();
		textBox_safenpc->Text = SelectedTask->SafeNPC.ToString();
		textBox_npctime->Text = SelectedTask->SafeNpcTime.ToString();
		textBox_waittime->Text = SelectedTask->required_wait_time.ToString();

		listBox_titles->Items->Clear();
		for(int j=0; j<SelectedTask->TitleS->Length; j++ )
		{	
				listBox_titles->Items->Add(SelectedTask->TitleS[j].ToString());
		}
		textBox_needlvl->Text = SelectedTask->needlv.ToString();
		textBox_sva1len->Text = SelectedTask->sNval_1_len.ToString();
		textBox_svallen2->Text = SelectedTask->sNval_2_len.ToString();
		textBox_sval3len->Text = SelectedTask->sNval_3_len.ToString();
		textBox_sval4len->Text = SelectedTask->sNval_4_len.ToString();
		textBox_rewardtype->Text = SelectedTask->reward_type.ToString();
		textBox_rewardtype2->Text = SelectedTask->reward_type2.ToString();

		textBox_un13301->Text = SelectedTask->unkn_1330_1.ToString();
		textBox_un13303->Text = SelectedTask->unkn_1330_3.ToString();
		textBox_un13304->Text = SelectedTask->unkn_1330_4.ToString();
		checkBox_un13302->Checked = SelectedTask->unkn_1330_2;

		textBox_parent_quest->Text = SelectedTask->parent_quest.ToString();
		textBox_previous_quest->Text = SelectedTask->previous_quest.ToString();
		textBox_sub_quest_first->Text = SelectedTask->sub_quest_first.ToString();
		listBox_tops->Items->Clear();
		for(int j=0; j<SelectedTask->tops->Length; j++ )
		{	
				listBox_tops->Items->Add(SelectedTask->tops[j].ToString());
		}
		// chases
		dataGridView_required_chases->Rows->Clear();
		for(int r=0; r<SelectedTask->required_chases->Length; r++)
		{
			try
			{
				dataGridView_required_chases->Rows->Add(gcnew array<String^>
				{
					SelectedTask->required_chases[r]->id_monster.ToString(),
					SelectedTask->required_chases[r]->amount_monster.ToString(),
					SelectedTask->required_chases[r]->id_drop.ToString(),
					SelectedTask->required_chases[r]->amount_drop.ToString(),
					SelectedTask->required_chases[r]->unknown_1.ToString(),
					SelectedTask->required_chases[r]->probability.ToString("F6"),
					SelectedTask->required_chases[r]->unknown_2.ToString()
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Chases!");
			}
		}
		dataGridView_dynamics->Rows->Clear();
			for(int r=0; r<SelectedTask->dynamics->Length; r++)
			{
				try
				{
					dataGridView_dynamics->Rows->Add(gcnew array<String^>
					{
						SelectedTask->dynamics[r]->id.ToString(),
						SelectedTask->dynamics[r]->point->map_id.ToString(),
						SelectedTask->dynamics[r]->point->x.ToString("F3"),
						SelectedTask->dynamics[r]->point->altitude.ToString("F3"),
						SelectedTask->dynamics[r]->point->z.ToString("F3"),
						SelectedTask->dynamics[r]->times.ToString(),
						SelectedTask->dynamics[r]->unknown_1.ToString(),
						SelectedTask->dynamics[r]->unknown_2.ToString(),
						SelectedTask->dynamics[r]->unknown_3.ToString(),
						SelectedTask->dynamics[r]->unknown_4.ToString(),
						SelectedTask->dynamics[r]->unknown_5.ToString()
					}
					);
				}
				catch(...)
				{
					MessageBox::Show("ERROR Loading Required Items!");
				}
			}
		// get items
		dataGridView_required_get_items->Rows->Clear();
		for(int r=0; r<SelectedTask->required_get_items->Length; r++)
		{
			try
			{
				dataGridView_required_get_items->Rows->Add(gcnew array<String^>
				{
					SelectedTask->required_get_items[r]->id.ToString(),
					SelectedTask->required_get_items[r]->unknown.ToString(),
					SelectedTask->required_get_items[r]->amount.ToString(),
					SelectedTask->required_get_items[r]->probability.ToString("F6"),
					SelectedTask->required_get_items[r]->expiration.ToString()
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Get Items!");
			}
		}
		checkBox_required_be_gm->Checked = SelectedTask->required_be_gm;
		//
		/*
		//textBox_unknown_32->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_32);
		checkBox_unknown_33->Checked = SelectedTask->UNKNOWN_33;
		textBox_required_quests_done_1->Text = SelectedTask->required_quests_done[0].ToString();
		textBox_required_quests_done_2->Text = SelectedTask->required_quests_done[1].ToString();
		textBox_required_quests_done_3->Text = SelectedTask->required_quests_done[2].ToString();
		textBox_required_quests_done_4->Text = SelectedTask->required_quests_done[3].ToString();
		textBox_required_quests_done_5->Text = SelectedTask->required_quests_done[4].ToString();
		//textBox_unknown_34->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_34);
		checkBox_unknown_35->Checked = SelectedTask->UNKNOWN_35;
		textBox_unknown_36->Text = SelectedTask->UNKNOWN_36.ToString();
		MessageBox::Show("sdfddfg234fdsghjdh!");
		switch(SelectedTask->required_cultivation)
		{
			case 0:		comboBox_required_cultivation->SelectedIndex = 0;
						break;
			case 1:		comboBox_required_cultivation->SelectedIndex = 1;
						break;
			case 2:		comboBox_required_cultivation->SelectedIndex = 2;
						break;
			case 3:		comboBox_required_cultivation->SelectedIndex = 3;
						break;
			case 4:		comboBox_required_cultivation->SelectedIndex = 4;
						break;
			case 5:		comboBox_required_cultivation->SelectedIndex = 5;
						break;
			case 6:		comboBox_required_cultivation->SelectedIndex = 6;
						break;
			case 7:		comboBox_required_cultivation->SelectedIndex = 7;
						break;
			case 8:		comboBox_required_cultivation->SelectedIndex = 8;
						break;
			case 20:	comboBox_required_cultivation->SelectedIndex = 9;
						break;
			case 30:	comboBox_required_cultivation->SelectedIndex = 10;
						break;
			case 21:	comboBox_required_cultivation->SelectedIndex = 11;
						break;
			case 31:	comboBox_required_cultivation->SelectedIndex = 12;
						break;
			case 22:	comboBox_required_cultivation->SelectedIndex = 13;
						break;
			case 32:	comboBox_required_cultivation->SelectedIndex = 14;
						break;
		}
		textBox_unknown_37->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_37);
		textBox_unknown_38->Text = SelectedTask->UNKNOWN_38.ToString();
		checkBox_unknown_39->Checked = SelectedTask->UNKNOWN_39;
		comboBox_required_gender->SelectedIndex = SelectedTask->required_gender;
		checkBox_unknown_40->Checked = SelectedTask->UNKNOWN_40;
		checkBox_unknown_41->Checked = SelectedTask->UNKNOWN_41;
		checkBox_required_be_married->Checked = SelectedTask->required_be_married;
		checkBox_unknown_42->Checked = SelectedTask->UNKNOWN_42;
		checkBox_unknown_42_1->Checked = SelectedTask->UNKNOWN_42_1;
		checkBox_unknown_42_2->Checked = SelectedTask->UNKNOWN_42_2;
		checkBox_unknown_43->Checked = SelectedTask->UNKNOWN_43;
		// occupations
		this->checkBox_occupation_blademaster->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_wizard->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_psychic->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_venomancer->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_barbarian->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_assassin->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_archer->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_cleric->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_seeker->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_mystic->CheckedChanged -= gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		checkBox_occupation_blademaster->Checked = false;
		checkBox_occupation_wizard->Checked = false;
		checkBox_occupation_psychic->Checked = false;
		checkBox_occupation_venomancer->Checked = false;
		checkBox_occupation_barbarian->Checked = false;
		checkBox_occupation_assassin->Checked = false;
		checkBox_occupation_archer->Checked = false;
		checkBox_occupation_cleric->Checked = false;
		checkBox_occupation_seeker->Checked = false;
		checkBox_occupation_mystic->Checked = false;
		if(Array::IndexOf(SelectedTask->required_occupations, 0) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 0) > -1){ checkBox_occupation_blademaster->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 1) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 1) > -1){ checkBox_occupation_wizard->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 2) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 2) > -1){ checkBox_occupation_psychic->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 3) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 3) > -1){ checkBox_occupation_venomancer->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 4) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 4) > -1){ checkBox_occupation_barbarian->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 5) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 5) > -1){ checkBox_occupation_assassin->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 6) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 6) > -1){ checkBox_occupation_archer->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 7) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 7) > -1){ checkBox_occupation_cleric->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 7) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 8) > -1){ checkBox_occupation_seeker->Checked = true; }
		if(Array::IndexOf(SelectedTask->required_occupations, 7) < SelectedTask->required_occupations_count && Array::IndexOf(SelectedTask->required_occupations, 9) > -1){ checkBox_occupation_mystic->Checked = true; }
		this->checkBox_occupation_blademaster->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_wizard->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_psychic->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_venomancer->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_barbarian->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_assassin->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_archer->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_cleric->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_seeker->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		this->checkBox_occupation_mystic->CheckedChanged += gcnew System::EventHandler(this, &zxTASKeditWindow::change_required_occupations);
		//
		textBox_unknown_44->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_44);
		// date unknown
		dataGridView_date_unknown->Rows->Clear();
		for(int r=0; r<1; r++)
		{
			try
			{
				dataGridView_date_unknown->Rows->Add(gcnew array<String^>
				{
					SelectedTask->date_unknown->year.ToString(),
					SelectedTask->date_unknown->month.ToString(),
					SelectedTask->date_unknown->day.ToString(),
					SelectedTask->date_unknown->hour.ToString(),
					SelectedTask->date_unknown->minute.ToString(),
					Column_date_unknown_weekday->Items[SelectedTask->date_unknown->weekday]->ToString()
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Date Unknown!");
			}
		}
		textBox_unknown_45->Text = SelectedTask->UNKNOWN_45.ToString();
		checkBox_unknown_46->Checked = SelectedTask->UNKNOWN_46;
		textBox_unknown_47->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_47);
		// quests undone
		textBox_required_quests_undone_1->Text = SelectedTask->required_quests_undone[0].ToString();
		textBox_required_quests_undone_2->Text = SelectedTask->required_quests_undone[1].ToString();
		textBox_required_quests_undone_3->Text = SelectedTask->required_quests_undone[2].ToString();
		textBox_required_quests_undone_4->Text = SelectedTask->required_quests_undone[3].ToString();
		textBox_required_quests_undone_5->Text = SelectedTask->required_quests_undone[4].ToString();
		// skill levels
		textBox_required_blacksmith_level->Text = SelectedTask->required_blacksmith_level.ToString();
		textBox_required_tailor_level->Text = SelectedTask->required_tailor_level.ToString();
		textBox_required_craftsman_level->Text = SelectedTask->required_craftsman_level.ToString();
		textBox_required_apothecary_level->Text = SelectedTask->required_apothecary_level.ToString();
		//
		textBox_unknown_48->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_48);
		textBox_unknown_49->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_49);
		textBox_unknown_50->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_50);
		checkBox_unknown_51->Checked = SelectedTask->UNKNOWN_51;
		textBox_unknown_52->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_52);
		textBox_resource_pq_audit_id->Text = SelectedTask->resource_pq_audit_id.ToString();
		textBox_unknown_53->Text = SelectedTask->UNKNOWN_53.ToString();
		textBox_unknown_54->Text = SelectedTask->UNKNOWN_54.ToString();
		textBox_required_pq_contribution->Text = SelectedTask->required_pq_contribution.ToString();
		textBox_unknown_55->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55);
		textBox_unknown_55_02_01->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_02_01);
		comboBox_required_force->SelectedIndex = Math::Max(0, SelectedTask->required_force-1003);
		textBox_unknown_55_02_02->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_02_02);
		textBox_required_prestige->Text = SelectedTask->required_prestige.ToString();
		textBox_unknown_55_03->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_03);
		textBox_required_influence_fee->Text = SelectedTask->required_influence_fee.ToString();
		textBox_unknown_55_04->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_04);
		textBox_unknown_55_05->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_05);
		textBox_unknown_55_06->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_55_06);
		textBox_required_success_type->Text = SelectedTask->required_success_type.ToString();
		textBox_required_npc_type->Text = SelectedTask->required_npc_type.ToString();
		// morai pk
		dataGridView_required_morai_pk->Rows->Clear();
		for(int r=0; r<SelectedTask->required_morai_pk->Length; r++)
		{
			try
			{
				dataGridView_required_morai_pk->Rows->Add(gcnew array<String^>
				{
					SelectedTask->required_morai_pk[r]->unknown_1.ToString(),
					SelectedTask->required_morai_pk[r]->unknown_2.ToString(),
					SelectedTask->required_morai_pk[r]->unknown_3.ToString(),
					SelectedTask->required_morai_pk[r]->unknown_4.ToString(),
					SelectedTask->required_morai_pk[r]->probability.ToString("F6"),
					SelectedTask->required_morai_pk[r]->class_mask.ToString(),
					SelectedTask->required_morai_pk[r]->level_min.ToString(),
					SelectedTask->required_morai_pk[r]->level_max.ToString(),
					SelectedTask->required_morai_pk[r]->unknown_5.ToString(),
					Column28->Items[SelectedTask->required_morai_pk[r]->type]->ToString()
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Morai PK!");
			}
		}
		textBox_required_morai_pk_unknown->Text = ByteArray_to_HexString(SelectedTask->required_morai_pk_unknown);
		
		textBox_required_chases_unknown->Text = ByteArray_to_HexString(SelectedTask->required_chases_unknown);
		
		textBox_required_get_items_unknown->Text = ByteArray_to_HexString(SelectedTask->required_get_items_unknown);
		//
		textBox_required_coins->Text = SelectedTask->required_coins.ToString();
		textBox_unknown_56->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_56);
		textBox_unknown_57->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_57);
		//
		textBox_required_wait_time->Text = SelectedTask->required_wait_time.ToString();
		textBox_unknown_57_01->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_57_01);
		// pq script infos
		dataGridView_pq_script_infos->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->script_infos->Length; r++)
		{
			try
			{
				dataGridView_pq_script_infos->Rows->Add(gcnew array<String^>
				{
					SelectedTask->pq->script_infos[r]->id.ToString(),
					SelectedTask->pq->script_infos[r]->unknown_1.ToString(),
					ByteArray_to_HexString(SelectedTask->pq->script_infos[r]->unknown_2)
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading PQ Script Infos!");
			}
		}
		textBox_pq_unknown_1->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_1);
		textBox_pq_unknown_2->Text = SelectedTask->pq->unknown_2.ToString();
		textBox_pq_unknown_3->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_3);
		// pq scripts
		dataGridView_pq_scripts->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->scripts->Length; r++)
		{
			try
			{
				dataGridView_pq_scripts->Rows->Add(gcnew array<String^>
				{
					SelectedTask->pq->scripts[r]->Name,
					SelectedTask->pq->scripts[r]->count.ToString(),
					SelectedTask->pq->scripts[r]->id.ToString(),
					ByteArray_to_HexString(SelectedTask->pq->scripts[r]->seperator),
					SelectedTask->pq->scripts[r]->reference_id.ToString("F0"),
					SelectedTask->pq->scripts[r]->Code
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading PQ Scripts!");
			}
		}
		textBox_pq_unknown_4->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_4);
		textBox_pq_unknown_5->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_5);
		textBox_pq_unknown_6->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_6);
		// pq chases
		dataGridView_pq_chases->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->chases->Length; r++)
		{
			try
			{
				dataGridView_pq_chases->Rows->Add(gcnew array<String^>
				{
					SelectedTask->pq->chases[r]->id_monster.ToString(),
					SelectedTask->pq->chases[r]->amount_1.ToString(),
					SelectedTask->pq->chases[r]->contribution.ToString(),
					SelectedTask->pq->chases[r]->amount_3.ToString()
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading PQ Chases!");
			}
		}
		textBox_pq_unknown_7->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_7);
		textBox_pq_required_quests_completed->Text = SelectedTask->pq->required_quests_completed.ToString();
		textBox_pq_unknown_8->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_8);
		textBox_pq_unknown_9->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_9);
		textBox_pq_unknown_10->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_10);
		textBox_pq_unknown_11->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_11);
		// pq locations
		checkBox_pq_location_has_spans->Checked = SelectedTask->pq->location->has_location;
		textBox_pq_location_map_id->Text = SelectedTask->pq->location->map_id.ToString();
		textBox_pq_location_unknown_1->Text = ByteArray_to_HexString(SelectedTask->pq->location->unknown_1);
		dataGridView_pq_location_spans->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->location->spans->Length; r++)
		{
			try
			{
				dataGridView_pq_location_spans->Rows->Add(gcnew array<String^>
				{
					SelectedTask->pq->location->spans[r]->north.ToString("F3"),
					SelectedTask->pq->location->spans[r]->south.ToString("F3"),
					SelectedTask->pq->location->spans[r]->west.ToString("F3"),
					SelectedTask->pq->location->spans[r]->east.ToString("F3"),
					SelectedTask->pq->location->spans[r]->bottom.ToString("F3"),
					SelectedTask->pq->location->spans[r]->top.ToString("F3")
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading PQ Location Span!");
			}
		}
		textBox_pq_unknown_12->Text = SelectedTask->pq->unknown_12.ToString();
		textBox_pq_unknown_13->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_13);
		textBox_pq_id_script->Text = SelectedTask->pq->id_script.ToString();
		textBox_pq_unknown_14->Text = SelectedTask->pq->unknown_14.ToString();
		textBox_pq_unknown_15->Text = SelectedTask->pq->unknown_15.ToString();
		textBox_pq_unknown_16->Text = SelectedTask->pq->unknown_16.ToString();
		textBox_pq_unknown_17->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_17);
		// pq special scripts
		dataGridView_pq_special_scripts->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->special_scripts->Length; r++)
		{
			try
			{
				dataGridView_pq_special_scripts->Rows->Add(gcnew array<String^>
				{
					SelectedTask->pq->special_scripts[r]->Name,
					SelectedTask->pq->special_scripts[r]->count.ToString(),
					SelectedTask->pq->special_scripts[r]->id.ToString(),
					ByteArray_to_HexString(SelectedTask->pq->special_scripts[r]->seperator),
					SelectedTask->pq->special_scripts[r]->reference_id.ToString("F0"),
					SelectedTask->pq->special_scripts[r]->Code
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading Special Scripts!");
			}
		}
		textBox_pq_unknown_18->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_18);
		textBox_pq_unknown_19->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_19);
		// pq messages
		dataGridView_pq_messages->Rows->Clear();
		for(int r=0; r<SelectedTask->pq->messages->Length; r++)
		{
			try
			{
				dataGridView_pq_messages->Rows->Add(gcnew array<String^>
				{
					ByteArray_to_UnicodeString(SelectedTask->pq->messages[r])
				}
				);
			}
			catch(...)
			{
				MessageBox::Show("ERROR Loading PQ Message!");
			}
		}
		textBox_pq_unknown_20->Text = ByteArray_to_HexString(SelectedTask->pq->unknown_20);
		//
		textBox_unknown_58->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_58);
		textBox_unknown_59->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_59);

		checkBox_unknown_60->Checked = SelectedTask->UNKNOWN_60;
		textBox_receive_quest_probability->Text = SelectedTask->receive_quest_probability.ToString("F3");
		checkBox_unknown_60_01->Checked = SelectedTask->UNKNOWN_60_01;
		
		//
		textBox_unknown_61->Text = ByteArray_to_HexString(SelectedTask->UNKNOWN_61);
		*/
	}
	private: System::Void select_reward(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedReward = nullptr;
			if(radioButton_success->Checked)
			{
				listBox_reward_timed->Enabled = false;
				numericUpDown_time_factor->Enabled = false;
				numericUpDown_time_factor->Value = 0;
				SelectedReward = SelectedTask->reward_success;
			}
			if(radioButton_failed->Checked)
			{
				listBox_reward_timed->Enabled = false;
				numericUpDown_time_factor->Enabled = false;
				numericUpDown_time_factor->Value = 0;
				SelectedReward = SelectedTask->reward_failed;
			}
			radioButton_timed->Enabled=false;
			radioButton_teammem->Enabled=false;
			radioButton_team_lead->Enabled=false;
			radioButton_teacher->Enabled=false;
			if(SelectedTask->reward_success->is_special_reward){
				if (SelectedTask->reward_success->special_flg == 1)
				{
					radioButton_teammem->Enabled=true;
				}
				if (SelectedTask->reward_success->special_flg == 2)
				{
					radioButton_team_lead->Enabled=true;
				}
				if (SelectedTask->reward_success->special_flg == 4)
				{
					radioButton_teacher->Enabled=true;
				}
			}
			if(radioButton_teammem->Checked && radioButton_teammem->Enabled == true)
			{
				listBox_reward_timed->Enabled = false;
				numericUpDown_time_factor->Enabled = false;
				numericUpDown_time_factor->Value = 0;
				SelectedReward = SelectedTask->reward_team_member;
			}
			if(radioButton_team_lead->Checked && radioButton_team_lead->Enabled == true)
			{
				listBox_reward_timed->Enabled = false;
				numericUpDown_time_factor->Enabled = false;
				numericUpDown_time_factor->Value = 0;
				SelectedReward = SelectedTask->reward_team_leader;
			}
			if(radioButton_teacher->Checked && radioButton_teacher->Enabled == true)
			{
				listBox_reward_timed->Enabled = false;
				numericUpDown_time_factor->Enabled = false;
				numericUpDown_time_factor->Value = 0;
				SelectedReward = SelectedTask->reward_teacher;
			}
			if (SelectedTask->reward_type == 2)
			{
				radioButton_timed->Enabled=true;
			}
			if(radioButton_timed->Enabled==true && radioButton_timed->Checked)
			{
				listBox_reward_timed->Enabled = true;
				numericUpDown_time_factor->Enabled = true;
				if(listBox_reward_timed->SelectedIndex>-1)
				{
					SelectedReward = SelectedTask->rewards_timed[listBox_reward_timed->SelectedIndex];
					numericUpDown_time_factor->Value = Convert::ToDecimal(SelectedTask->rewards_timed_factors[listBox_reward_timed->SelectedIndex]);
				}
				else
				{
					numericUpDown_time_factor->Value = 0;
				}
			}

			if(SelectedReward)
			{
				textBox_reward_coins->Text = SelectedReward->coins.ToString();
				textBox_reward_experience->Text = SelectedReward->experience.ToString();
				textBox_reward_teleport_map_id->Text = SelectedReward->teleport->map_id.ToString();
				textBox_reward_teleport_x->Text = SelectedReward->teleport->x.ToString("F1");
				textBox_reward_teleport_altitude->Text = SelectedReward->teleport->altitude.ToString("F1");
				textBox_reward_teleport_z->Text = SelectedReward->teleport->z.ToString("F1");
				textBox_reward_ai_trigger->Text = SelectedReward->ai_trigger.ToString();
				//textBox_reward_unknown_2a->Text = ByteArray_to_HexString(SelectedReward->UNKNOWN_2a);
				//textBox_reward_unknown_2b->Text = ByteArray_to_HexString(SelectedReward->UNKNOWN_2b);
				//textBox_reward_seperator->Text = ByteArray_to_HexString(SelectedReward->SEPERATOR);
				textBox_rewardExpLv->Text = SelectedReward->ExpLv.ToString("F1");
				textBox_reward_rclvl->Text = SelectedReward->RCLv.ToString("F1");
				textBox_reward_APLv->Text = SelectedReward->APLv.ToString("F1");
				textBox_reward_newtask->Text = SelectedReward->new_quest.ToString();

				textBox_rew1330_6->Text = SelectedReward->unkn_1330_6.ToString();
				textBox_rew1330_7->Text = SelectedReward->unkn_1330_7.ToString();
				textBox_rew1330_8->Text = SelectedReward->unkn_1330_8.ToString();
				textBox_rew1330_9->Text = SelectedReward->unkn_1330_9.ToString();

				checkBox_rew1330_10->Checked =  SelectedReward->unkn_1330_10;

				textBox_un01->Text = SelectedReward->unknow_01.ToString();
				textBox_un02->Text = SelectedReward->unknow_02.ToString();
				textBox_un03->Text = SelectedReward->unknow_03.ToString();
				textBox_run04->Text = SelectedReward->unknow_04.ToString();
				textBox_run05->Text = SelectedReward->unknow_05.ToString();
				textBox_run06->Text = SelectedReward->unknow_06.ToString();
				textBox_run07->Text = SelectedReward->unknow_07.ToString();
				textBox_run08->Text = SelectedReward->unknow_08.ToString();
				textBox_run09->Text = SelectedReward->unknow_09.ToString();
				textBox_run10->Text = SelectedReward->unknow_10.ToString();
				textBox_run12->Text = SelectedReward->unknow_12.ToString();
				textBox_grint->Text = SelectedReward->group_integral.ToString();
				textBox_run121->Text = SelectedReward->unknow_12_1.ToString();
				textBox_rewclancont->Text = SelectedReward->fation_gongxian.ToString();
				textBox_clancontribut->Text = SelectedReward->family_gongxian.ToString();
				textBox_run13->Text = SelectedReward->unknow_13.ToString();
				textBox_rewardtitle->Text = SelectedReward->title.ToString();
				textBox_clearpk->Text = SelectedReward->removePK.ToString();
				textBox_rewrepmodo->Text = SelectedReward->sw->GuiDao.ToString();
				textBox_rewrepjad->Text = SelectedReward->sw->QingYun.ToString();
				textBox_rewrepsky->Text = SelectedReward->sw->TianYin.ToString();
				textBox_rewrepvim->Text = SelectedReward->sw->GuiWang.ToString();
				textBox_rewreplupi->Text = SelectedReward->sw->HeHuan.ToString();
				textBox_rewrepzhongyi->Text = SelectedReward->sw->ZhongYi.ToString();
				textBox_rewQingyuan->Text = SelectedReward->sw->QingYuan.ToString();
				textBox_rewwencai->Text = SelectedReward->sw->WenCai.ToString();
				textBox_rewshide->Text = SelectedReward->sw->ShiDe.ToString();
				textBox_DaoXin->Text = SelectedReward->sw->DaoXin.ToString();
				textBox_rewmoxing->Text = SelectedReward->sw->MoXing.ToString();
				textBox_rewrepfoyuan->Text = SelectedReward->sw->FoYuan.ToString();
				textBox_runk15->Text = SelectedReward->sw->unknow_15.ToString();
				textBox_rewJiuLi->Text = SelectedReward->sw->JiuLi.ToString();
				textBox_LieShan->Text = SelectedReward->sw->LieShan.ToString();
				textBox_ChenHuang->Text = SelectedReward->sw->ChenHuang.ToString();
				textBox_rewreptaihao->Text = SelectedReward->sw->TaiHao.ToString();
				textBox_HuaiGuang->Text = SelectedReward->sw->HuaiGuang.ToString();
				textBox_TianHua->Text = SelectedReward->sw->TianHua.ToString();
				textBox_FenXiang->Text = SelectedReward->sw->FenXiang.ToString();
				textBox_ZhanJi->Text = SelectedReward->sw->ZhanJi.ToString();
				textBox_ZhanXun->Text = SelectedReward->sw->ZhanXun.ToString();
				textBox_TianZong->Text = SelectedReward->sw->TianZong.ToString();
				textBox_rewDiZong->Text = SelectedReward->sw->DiZong.ToString();
				textBox_rewRenZong->Text = SelectedReward->sw->RenZong.ToString();

				textBox_run20->Text = SelectedReward->unknow_20.ToString();
				textBox_run21->Text = SelectedReward->unknow_21.ToString();
				textBox_run22->Text = SelectedReward->unknow_22.ToString();
				textBox_run23->Text = SelectedReward->unknow_23.ToString();
				textBox_run31->Text = SelectedReward->unknow_31.ToString();
				textBox_runk25->Text = SelectedReward->unknow_25.ToString();
				textBox_runk26->Text = SelectedReward->unknow_26.ToString();
				textBox_runk27->Text = SelectedReward->unknow_27.ToString();
				textBox_runk28->Text = SelectedReward->unknow_28.ToString();
				textBox_runk29->Text = SelectedReward->unknow_29.ToString();
				textBox_runk30->Text = SelectedReward->unknow_30.ToString();
				textBox_runk301->Text = SelectedReward->unknow_31_1.ToString();
				textBox_runk32->Text = SelectedReward->unknow_32.ToString();
				textBox_runk33->Text = SelectedReward->unknow_33.ToString();
				textBox_runk35->Text = SelectedReward->unknow_35.ToString();
				textBox_runk36->Text = SelectedReward->unknow_36.ToString();
				textBox_runk40->Text = SelectedReward->unknow_40.ToString();
				textBox_runk42->Text = SelectedReward->unknow_42.ToString();
				textBox_runk43->Text = SelectedReward->unknow_43.ToString();
				textBox_tunk46->Text = SelectedReward->unknow_46.ToString();
				textBox_runk48->Text = ByteArray_to_HexString(SelectedReward->unknow_48);
				textBox_runk49->Text = ByteArray_to_HexString(SelectedReward->unknow_49);
				textBox_run50->Text = SelectedReward->unknow_50.ToString();
				textBox_runk51->Text = SelectedReward->unknow_51.ToString();
				checkBox_runk52->Checked = SelectedReward->unknow_52;
				textBox_runk53->Text = SelectedReward->unknown_53.ToString();
				textBox_runk55->Text = SelectedReward->unknown_55.ToString();
				textBox_runk56->Text = SelectedReward->unknown_56.ToString();
				textBox_runkn1->Text = SelectedReward->unknown_n1.ToString();
				textBox_runkn2->Text = SelectedReward->unknown_n2.ToString();
				textBox_runkn3->Text = SelectedReward->unknown_n3.ToString("F2");
				textBox_runkn4->Text = SelectedReward->unknown_n4.ToString();
				textBox_rnval1h->Text = ByteArray_to_HexString(SelectedReward->nval_1);
				textBox_rnval2h->Text = ByteArray_to_HexString(SelectedReward->nval_2);
				textBox_rnval3h->Text = ByteArray_to_HexString(SelectedReward->nval_3);
				textBox_rnval4h->Text = ByteArray_to_HexString(SelectedReward->nval_4);
				textBox_rnval1len->Text = SelectedReward->nval_len_1.ToString();
				textBox_rnval2len->Text = SelectedReward->nval_len_2.ToString();
				textBox_rewstash->Text = SelectedReward->Cangku.ToString();
				textBox_rewbag->Text = SelectedReward->Beibao.ToString();
				textBox_rewpetbag->Text = SelectedReward->CWBeibao.ToString();
				textBox_petslot->Text = SelectedReward->CWlan.ToString();
				textBox_mountbag->Text = SelectedReward->ZuoqiBeibao.ToString();
				textBox_rsetjob->Text = SelectedReward->SetJob.ToString();
				textBox_proff->Text = SelectedReward->ShuLianDu.ToString();
				textBox_rskill1->Text = SelectedReward->SkillsLV.ToString();
				textBox_rclanskillid->Text = SelectedReward->family_skill_id.ToString();
				textBox_rclanunk->Text = SelectedReward->family_lingqi.ToString();
				textBox_rfacunk->Text = SelectedReward->fation_lingqi.ToString();
				textBox_rnotch->Text = SelectedReward->Notice_Channel.ToString();
				textBox_rremtask->Text = SelectedReward->removetasks.ToString();
				textBox_rdbltime->Text = SelectedReward->DoubleTimes.ToString();
				textBox_rcampv->Text = SelectedReward->Camp.ToString();
				textBox_rskillcnt->Text = SelectedReward->skillcount.ToString();
				textBox_risexpf->Text = SelectedReward->is_exp_formula.ToString();
				//textBox_val1->Text = SelectedReward->value_set->val_1.ToString();
				//textBox_rval2->Text = SelectedReward->value_set->val_2.ToString();
				//textBox_rval3->Text = SelectedReward->value_set->val_3.ToString();
				//textBox_rvalc->Text = SelectedReward->value_set->val_count.ToString();
				textBox_rtransf->Text = SelectedReward->Bianshen.ToString();
				textBox_transftime->Text = SelectedReward->BianshenTime.ToString();
				textBox_rsound->Text = SelectedReward->YuanShen.ToString();
				textBox_rhuli->Text = SelectedReward->HongLiIngot.ToString();
				textBox_rspecialflg->Text = SelectedReward->special_flg.ToString();
				textBox_rjobup->Text = SelectedReward->jobtmp_up.ToString();

				//textBox_expfname->Text = SelectedReward->exp_val->name.ToString();

				checkBox_rewremallinf->Checked = SelectedReward->clearPK;
				checkBox_rewdivorce->Checked = SelectedReward->Divorce;
				checkBox_craftup->Checked = SelectedReward->ShengChan_UP;
				
				checkBox_triggeron->Checked = SelectedReward->trigger_on;
				checkBox_rnotice->Checked = SelectedReward->Notice;
				checkBox_rchushi->Checked = SelectedReward->ChuShi;
				checkBox_rpanshi->Checked = SelectedReward->PanShi;
				checkBox_rview1->Checked = SelectedReward->Shitu_01;
				checkBox_rview2->Checked = SelectedReward->Shitu_02;
				checkBox_rspecialaward->Checked = SelectedReward->is_special_reward;
				checkBox_rcampon->Checked = SelectedReward->Camp_on;
				checkBox_rskillon->Checked = SelectedReward->skill_on;
				checkBox_rtomeon->Checked = SelectedReward->skill_Tianshu_on;
				checkBox_rsound->Checked = SelectedReward->YuanShenlv;
				checkBox_openreason->Checked = SelectedReward->OpenYuanYing;
				checkBox_rnval->Checked = SelectedReward->is_nval;
				if(SelectedReward->Notice&&SelectedReward->noticetext_len>0) textBox_noticemess->Text = Encoding::Unicode->GetString(SelectedReward->noticetext);

				numericUpDown_reward_item_groups_count->Value = Convert::ToDecimal(SelectedReward->item_groups_count);

				checkedListBox_reward_item_groups_flag->Items->Clear();
				dataGridView_reward_item_group_items->Rows->Clear();
				Column_reward_item_groups->Items->Clear();

				for(int g=0; g<SelectedReward->item_groups->Length; g++)
				{
					Column_reward_item_groups->Items->Add(g.ToString());
					checkedListBox_reward_item_groups_flag->Items->Add("Group[" + g.ToString() + "]",Convert::ToBoolean(SelectedReward->item_groups[g]->type));
					for(int r=0; r<SelectedReward->item_groups[g]->items->Length; r++)
					{
						dataGridView_reward_item_group_items->Rows->Add(gcnew array<String^>
						{
							g.ToString(),
							SelectedReward->item_groups[g]->items[r]->id.ToString(),
							SelectedReward->item_groups[g]->items[r]->unknown.ToString(),
							SelectedReward->item_groups[g]->items[r]->amount.ToString(),
							SelectedReward->item_groups[g]->items[r]->probability.ToString("F4"),
							SelectedReward->item_groups[g]->items[r]->flg.ToString(),
							SelectedReward->item_groups[g]->items[r]->expiration.ToString(),
							SelectedReward->item_groups[g]->items[r]->RefLevel.ToString(),
							SelectedReward->item_groups[g]->items[r]->unkown_01.ToString(),
							Convert::ToInt16(SelectedReward->item_groups[g]->items[r]->unkown_02).ToString(),
							SelectedReward->item_groups[g]->items[r]->unkown_03.ToString(),
						}
						);
						// set row background color depending on item group
						if(g%2 == 0)
						{
							dataGridView_reward_item_group_items->Rows[dataGridView_reward_item_group_items->Rows->Count-1]->DefaultCellStyle->BackColor = Color::White;
						}
						else
						{
							dataGridView_reward_item_group_items->Rows[dataGridView_reward_item_group_items->Rows->Count-1]->DefaultCellStyle->BackColor = Color::AliceBlue;
						}
					}
				}
			}
			else
			{
				textBox_reward_coins->Clear();
				textBox_reward_experience->Clear();
				textBox_reward_teleport_map_id->Clear();
				textBox_reward_teleport_x->Clear();
				textBox_reward_teleport_altitude->Clear();
				textBox_reward_teleport_z->Clear();
				textBox_reward_ai_trigger->Clear();
				// items
				numericUpDown_reward_item_groups_count->Value = 0;
				checkedListBox_reward_item_groups_flag->Items->Clear();
				dataGridView_reward_item_group_items->Rows->Clear();
				Column_reward_item_groups->Items->Clear();
			}
		}
	}
	private: System::Void select_dialog(System::Object^  sender, System::EventArgs^  e)
	{
		int d = listBox_conversation_dialogs->SelectedIndex;

		if(SelectedTask && d>-1)
		{
			textBox_conversation_dialog_unknown->Text = SelectedTask->conversation->dialogs[d]->unknown.ToString();
			textBox_conversation_dialog_text->Text = SelectedTask->conversation->dialogs[d]->DialogText;
			textBox_conversation_question_text->Clear();
			textBox_conversation_question_id->Clear();
			textBox_conversation_question_previous->Clear();
			listBox_conversation_questions->Items->Clear();
			for(int q=0; q<SelectedTask->conversation->dialogs[d]->question_count; q++)
			{
				listBox_conversation_questions->Items->Add("[" + SelectedTask->conversation->dialogs[d]->questions[q]->question_id + "] Question");
			}
			textBox_conversation_answer_text->Clear();
			listBox_conversation_answers->Items->Clear();
		}
	}
	private: System::Void select_question(System::Object^  sender, System::EventArgs^  e)
	{
		int d = listBox_conversation_dialogs->SelectedIndex;
		int q = listBox_conversation_questions->SelectedIndex;

		if(SelectedTask && d>-1 && q>-1)
		{
			textBox_conversation_question_text->Text = SelectedTask->conversation->dialogs[d]->questions[q]->QuestionText;
			textBox_conversation_question_id->Text = SelectedTask->conversation->dialogs[d]->questions[q]->question_id.ToString();
			textBox_conversation_question_previous->Text = SelectedTask->conversation->dialogs[d]->questions[q]->previous_question.ToString();
			textBox_conversation_answer_text->Clear();
			textBox_conversation_answer_question_link->Clear();
			textBox_conversation_answer_task_link->Clear();
			listBox_conversation_answers->Items->Clear();
			for(int a=0; a<SelectedTask->conversation->dialogs[d]->questions[q]->answer_count; a++)
			{
				listBox_conversation_answers->Items->Add("[" + (a+1) + "] Answer");
			}
		}
	}
	private: System::Void select_answer(System::Object^  sender, System::EventArgs^  e)
	{
		int d = listBox_conversation_dialogs->SelectedIndex;
		int q = listBox_conversation_questions->SelectedIndex;
		int a = listBox_conversation_answers->SelectedIndex;

		if(SelectedTask && d>-1 && q>-1 && a>-1)
		{
			textBox_conversation_answer_text->Text = SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText;
			textBox_conversation_answer_question_link->Text = SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->question_link.ToString();
			textBox_conversation_answer_task_link->Text = SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->task_link.ToString();
		}
	}

#pragma endregion

#pragma region EDIT FUNCTIONS
	private: System::Void click_export_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(treeView_tasks->SelectedNode)
		{
			try
			{
				ArrayList^ task_index_path = gcnew ArrayList();
				TreeNode^ node = ((TreeView^)contextMenuStrip_task->SourceControl)->SelectedNode;
				SelectedTask = Tasks[node->Index];

				while(node->Parent != nullptr)
				{
					task_index_path->Add(node->Index);
					node = node->Parent;
				}
				SelectedTask = Tasks[node->Index];
				for(int i=task_index_path->Count-1; i>=0; i--)
				{
					SelectedTask = SelectedTask->sub_quests[(int)task_index_path[i]];
				}

				//if(task_index_path->Count == 0)
				//{
					//MessageBox::Show("Single quest detected!");
					SaveFileDialog^ save = gcnew SaveFileDialog();
					save->InitialDirectory = Environment::CurrentDirectory;
					save->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
					if(save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
					{
						try
						{
							StreamWriter^ sw = gcnew StreamWriter(save->FileName, false, Encoding::Unicode);
							System::Text::Encoding^ enc = System::Text::Encoding::GetEncoding("Unicode");
							sw->WriteLine(SelectedTask->ID+"-0\t"+SelectedTask->Name->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-1\t"+SelectedTask->AuthorText->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-2\t"+SelectedTask->conversation->PromptText->Replace("\r\n","\\r")->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-3\t"+SelectedTask->conversation->AgernText->Replace("\r\n","\\r")->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-4\t"+SelectedTask->conversation->GeneralText->Replace("\r\n","\\r")->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-5\t"+SelectedTask->conversation->XuanfuText->Replace("\r\n","\\r")->Replace("\0",""));
							sw->WriteLine(SelectedTask->ID+"-6\t"+SelectedTask->conversation->NewText->Replace("\r\n","\\r")->Replace("\0",""));
							for(int d=0; d<SelectedTask->conversation->dialogs->Length; d++)
								{
									sw->WriteLine(SelectedTask->ID+"-7-"+d+"\t"+SelectedTask->conversation->dialogs[d]->DialogText->Replace("\r\n","\\r")->Replace("\0",""));
									for(int q=0; q<SelectedTask->conversation->dialogs[d]->questions->Length; q++)
									{
										sw->WriteLine(SelectedTask->ID+"-8-"+d+"-"+q+"\t"+SelectedTask->conversation->dialogs[d]->questions[q]->QuestionText->Replace("\r\n","\\r")->Replace("\0",""));
										for(int a=0; a<SelectedTask->conversation->dialogs[d]->questions[q]->answer_count; a++)
										{
											sw->WriteLine(SelectedTask->ID+"-9-"+d+"-"+q+"-"+a+"\t"+SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText->Replace("\r\n","\\r")->Replace("\0",""));
										}
									}
								}
							for(int i=0; i<SelectedTask->sub_quest_count; i++){
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-0\t"+SelectedTask->sub_quests[i]->Name->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-1\t"+SelectedTask->sub_quests[i]->AuthorText->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-2\t"+SelectedTask->sub_quests[i]->conversation->PromptText->Replace("\r\n","\\r")->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-3\t"+SelectedTask->sub_quests[i]->conversation->AgernText->Replace("\r\n","\\r")->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-4\t"+SelectedTask->sub_quests[i]->conversation->GeneralText->Replace("\r\n","\\r")->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-5\t"+SelectedTask->sub_quests[i]->conversation->XuanfuText->Replace("\r\n","\\r")->Replace("\0",""));
								sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-6\t"+SelectedTask->sub_quests[i]->conversation->NewText->Replace("\r\n","\\r")->Replace("\0",""));
								for(int d=0; d<SelectedTask->sub_quests[i]->conversation->dialogs->Length; d++)
									{
										sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-7-"+d+"\t"+SelectedTask->sub_quests[i]->conversation->dialogs[d]->DialogText->Replace("\r\n","\\r")->Replace("\0",""));
										for(int q=0; q<SelectedTask->sub_quests[i]->conversation->dialogs[d]->questions->Length; q++)
										{
											sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-8-"+d+"-"+q+"\t"+SelectedTask->sub_quests[i]->conversation->dialogs[d]->questions[q]->QuestionText->Replace("\r\n","\\r")->Replace("\0",""));
											for(int a=0; a<SelectedTask->sub_quests[i]->conversation->dialogs[d]->questions[q]->answer_count; a++)
											{
												sw->WriteLine(SelectedTask->sub_quests[i]->ID+"-9-"+d+"-"+q+"-"+a+"\t"+SelectedTask->sub_quests[i]->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText->Replace("\r\n","\\r")->Replace("\0",""));
											}
										}
									}
							}
							sw->Close();
						}
						catch(...)
						{
							MessageBox::Show("SAVING ERROR!");
						}
					}
				//}
				//else
				//{
				//	MessageBox::Show("Sub Quests detected!");
				//}							
			}
			catch(Exception^ e)
			{
				MessageBox::Show("ERROR\n" + e->Message);
			}
		}
	}
	private: System::Void click_import_text(System::Object^  sender, System::EventArgs^  e)
	{
		OpenFileDialog^ load = gcnew OpenFileDialog();
			load->InitialDirectory = Environment::CurrentDirectory;
			load->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
			if(load->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(load->FileName))
			{
				try
				{
					StreamReader^ sr = gcnew StreamReader(load->FileName);

					String^ line;
					//int counter;
					array<String^>^ pair;
					array<String^>^ pair2;
					array<String^>^ seperator = gcnew array<String^>{"\t"};
					array<String^>^ seperator2 = gcnew array<String^>{"-"};
					int quest_id;
					//int task_index=-1;
					while(!sr->EndOfStream)
					{
						line = sr->ReadLine();
						if(!line->StartsWith("#") && line->Contains("\t"))
						{
							pair = line->Split(seperator, StringSplitOptions::RemoveEmptyEntries);
							if(pair->Length == 2)
							{
								pair2 = pair[0]->Split(seperator2, StringSplitOptions::RemoveEmptyEntries);
								quest_id=Convert::ToInt32(pair2[0]);
								if(!SelectedTask||SelectedTask->ID!=quest_id){
									for(int i=0;i<Tasks->Length;i++){
										if(Tasks[i]->ID==quest_id) SelectedTask=Tasks[i];
										else{
											for(int j=0;j<Tasks[i]->sub_quest_count;j++){
												if(Tasks[i]->sub_quests[j]->ID==quest_id) SelectedTask=Tasks[i]->sub_quests[j];
												else{
													for(int o=0;o<Tasks[i]->sub_quests[j]->sub_quest_count;o++){
														if(Tasks[i]->sub_quests[j]->sub_quests[o]->ID==quest_id) SelectedTask=Tasks[i]->sub_quests[j]->sub_quests[o];
														else{

														}
													}
												}
											}
										}
									}
								}
								//MessageBox::Show("task_index:"+task_index+";id:"+quest_id);
								switch(Convert::ToInt32(pair2[1])){
									case 1:
										SelectedTask->AuthorText=pair[1];
									break;
									case 2:
										SelectedTask->conversation->PromptText=pair[1]->Replace("\\r","\r\n");
									break;
									case 3:
										SelectedTask->conversation->AgernText=pair[1]->Replace("\\r","\r\n");
									break;
									case 4:
										SelectedTask->conversation->GeneralText=pair[1]->Replace("\\r","\r\n");
									break;
									case 5:
										SelectedTask->conversation->XuanfuText=pair[1]->Replace("\\r","\r\n");
									break;
									case 6:
										SelectedTask->conversation->NewText=pair[1]->Replace("\\r","\r\n");
									break;
									case 7:
										SelectedTask->conversation->dialogs[Convert::ToInt32(pair2[2])]->DialogText=pair[1]->Replace("\\r","\r\n");
									break;
									case 8:
										SelectedTask->conversation->dialogs[Convert::ToInt32(pair2[2])]->questions[Convert::ToInt32(pair2[3])]->QuestionText=pair[1]->Replace("\\r","\r\n");
									break;
									case 9:
										SelectedTask->conversation->dialogs[Convert::ToInt32(pair2[2])]->questions[Convert::ToInt32(pair2[3])]->answers[Convert::ToInt32(pair2[4])]->AnswerText=pair[1]->Replace("\\r","\r\n");
									break;
									default:
										SelectedTask->Name=pair[1];
										//MessageBox::Show("0");
									break;
								}
							}
						}
					}
					sr->Close();
				}
				catch(...)
				{
					MessageBox::Show("import ERROR!");
				}
			}
	}
	private: System::Void click_cloneTask(System::Object^  sender, System::EventArgs^  e)
	{
		if(treeView_tasks->SelectedNode)
		{
			try
			{
				ArrayList^ task_index_path = gcnew ArrayList();
				TreeNode^ node = ((TreeView^)contextMenuStrip_task->SourceControl)->SelectedNode;

				while(node->Parent != nullptr)
				{
					task_index_path->Add(node->Index);
					node = node->Parent;
				}

				if(task_index_path->Count == 0)
				{
					array<Task^>^ temp = gcnew array<Task^>(Tasks->Length+1);
					temp[0] = Tasks[node->Index]->Clone();
					Array::Copy(Tasks, 0, temp, 1, Tasks->Length);
					Tasks = temp;

					treeView_tasks->Nodes->Insert(0, (TreeNode^)treeView_tasks->SelectedNode->Clone());
					treeView_tasks->SelectedNode = treeView_tasks->Nodes[0];
				}
				else
				{
					Task^ parent = Tasks[node->Index];
		
					for(int i=task_index_path->Count-1; i>0; i--)
					{
						parent = parent->sub_quests[(int)task_index_path[i]];
					}

					parent->sub_quest_count++;
					array<Task^>^ temp = gcnew array<Task^>(parent->sub_quests->Length+1);
					Array::Copy(parent->sub_quests, 0, temp, 0, parent->sub_quests->Length);
					temp[temp->Length-1] = parent->sub_quests[(int)task_index_path[0]]->Clone();
					parent->sub_quests = temp;

					MessageBox::Show("NOTE:\nCheck association like \"Next Quest\" and \"Previous Quest\" of all affected quests!");

					treeView_tasks->SelectedNode->Parent->Nodes->Add((TreeNode^)treeView_tasks->SelectedNode->Clone());
				}							
			}
			catch(Exception^ e)
			{
				MessageBox::Show("ERROR\n" + e->Message);
			}
		}
	}
	private: System::Void click_deleteTask(System::Object^  sender, System::EventArgs^  e)
	{
		if(treeView_tasks->SelectedNode)
		{
			try
			{
				ArrayList^ task_index_path = gcnew ArrayList();
				TreeNode^ node = ((TreeView^)contextMenuStrip_task->SourceControl)->SelectedNode;

				while(node->Parent != nullptr)
				{
					task_index_path->Add(node->Index);
					node = node->Parent;
				}

				if(task_index_path->Count == 0)
				{
					array<Task^>^ temp = gcnew array<Task^>(Tasks->Length-1);
					Array::Copy(Tasks, 0, temp, 0, node->Index);
					Array::Copy(Tasks, node->Index+1, temp, node->Index, Tasks->Length-1 - node->Index);
					Tasks = temp;
				}
				else
				{
					Task^ parent = Tasks[node->Index];
		
					for(int i=task_index_path->Count-1; i>0; i--)
					{
						parent = parent->sub_quests[(int)task_index_path[i]];
					}

					parent->sub_quest_count--;
					array<Task^>^ temp = gcnew array<Task^>(parent->sub_quests->Length-1);
					Array::Copy(parent->sub_quests, 0, temp, 0, (int)task_index_path[0]);
					Array::Copy(parent->sub_quests, (int)task_index_path[0]+1, temp, (int)task_index_path[0], parent->sub_quests->Length-1 - (int)task_index_path[0]);
					parent->sub_quests = temp;

// Update all links (parent, subquest, next, previous) for all affected quests...
					MessageBox::Show("NOTE:\nCheck association like \"Next Quest\" and \"Previous Quest\" of all affected quests!");
				}				

				// Remove node from treeview
				treeView_tasks->SelectedNode->Remove();
			}
			catch(Exception^ e)
			{
				MessageBox::Show("ERROR\n" + e->Message);
			}
		}
		// after deletion: select task above or below if possible...
	}

	private: System::Void click_split(System::Object^  sender, System::EventArgs^  e)
	{
		OpenFileDialog^ load = gcnew OpenFileDialog();
		load->Filter = "Tasks File (*.data)|*.data|All Files (*.*)|*.*";
		if(load->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(load->FileName))
		{
			FolderBrowserDialog^ save = gcnew FolderBrowserDialog();

			if(save->ShowDialog() == Windows::Forms::DialogResult::OK && Directory::Exists(save->SelectedPath))
			{
				try
				{
					Cursor = Windows::Forms::Cursors::WaitCursor;
					progressBar_progress->Style = ProgressBarStyle::Marquee;

					int count;
					int value;

					FileStream^ fr = File::OpenRead(load->FileName);
					BinaryReader^ br = gcnew BinaryReader(fr);

					value = br->ReadInt32();
					value = br->ReadInt32()-1;
					count = br->ReadInt32();

					array<int>^ ItemStreamPositions = gcnew array<int>(count);

					for(int i=0; i<count; i++)
					{
						ItemStreamPositions[i] = br->ReadInt32();
					}

					for(int i=0; i<count; i++)
					{
						br->BaseStream->Position = ItemStreamPositions[i];
						int id = br->ReadInt32();

						br->BaseStream->Position = ItemStreamPositions[i];

						FileStream^ fw = gcnew FileStream(save->SelectedPath + "\\" + id + ".data", FileMode::Create, FileAccess::Write);
						//FileStream^ fw = gcnew FileStream(save->SelectedPath + "\\cn.client.tasks." + i, FileMode::Create, FileAccess::Write);
						BinaryWriter^ bw = gcnew BinaryWriter(fw);

						int bytes = (int)(br->BaseStream->Length-1 - ItemStreamPositions[i]);
						if(i<count-1)
						{
							bytes = ItemStreamPositions[i+1] - ItemStreamPositions[i];
						}

						bw->Write(br->ReadBytes(bytes));

						bw->Close();
						fw->Close();
					}

					br->Close();
					fr->Close();

					progressBar_progress->Style = ProgressBarStyle::Continuous;
					Cursor = Windows::Forms::Cursors::Default;
				}
				catch(...)
				{
					progressBar_progress->Style = ProgressBarStyle::Continuous;
					Cursor = Windows::Forms::Cursors::Default;
					MessageBox::Show("EXPORT ERROR!\nNo information available");
				}
			}
		}
	}

 	private: System::Void save_CreatureBuilder(Task^ t, StreamWriter^ sw)
	{
		String^ line = "";
		if(t->ai_trigger>0 || t->reward_success->ai_trigger>0 || t->reward_failed->ai_trigger>0)
		{
			if(t->Name->StartsWith("^"))
			{
				line += t->ID.ToString() + "\t" + t->Name->Substring(7);
			}
			else
			{
				line += t->ID.ToString() + "\t" + t->Name;
			}
			
			if(t->ai_trigger>0)
			{
				line += "\t" + t->ai_trigger.ToString();
			}
			else
			{
				line += "\t-";
			}
			if(t->reward_success->ai_trigger>0)
			{
				line += "\t" + t->reward_success->ai_trigger.ToString();
			}
			else
			{
				line += "\t-";
			}
			if(t->reward_failed->ai_trigger>0)
			{
				line += "\t" + t->reward_failed->ai_trigger.ToString();
			}
			else
			{
				line += "\t-";
			}
		}
		if(line != "")
		{
			sw->WriteLine(line);
		}
		for(int i=0; i<t->sub_quests->Length; i++)
		{
			save_CreatureBuilder(t->sub_quests[i], sw);
		}
	}
	private: System::Void click_creatureBuilder(System::Object^  sender, System::EventArgs^  e)
	{
		SaveFileDialog^ save = gcnew SaveFileDialog();
		save->InitialDirectory = Environment::CurrentDirectory;
		save->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
		if(Tasks && save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
		{
			try
			{
				Cursor = Windows::Forms::Cursors::AppStarting;

				StreamWriter^ sw = gcnew StreamWriter(save->FileName, false, Encoding::Unicode);

				for(int i=Tasks->Length-1; i>-1; i--)
				{
					save_CreatureBuilder(Tasks[i], sw);
				}

				sw->Close();

				Cursor = Windows::Forms::Cursors::Default;
			}
			catch(...)
			{
				MessageBox::Show("SAVING ERROR!");
				Cursor = Windows::Forms::Cursors::Default;
			}
		}
	}

#pragma endregion

#pragma region SEARCH FUNCTIONS

	// search
	private: System::Void keyPress_search(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
	{
		if(e->KeyChar == (wchar_t)Keys::Enter)
		{
			click_search(nullptr, nullptr);
		}
	}
	private: System::Void click_search(System::Object^  sender, System::EventArgs^  e)
	{
		if(treeView_tasks->Nodes->Count>0)
		{
			array<int>^ indices = next_treeNode();

			// while there is a next task
			while(indices->Length > 0)
			{
				// search success
				if(search(indices))
				{
					return;
				}
				indices = next_task(indices);
			}

			MessageBox::Show("Search reached end of List without result");
		}
	}

	// return an array of indices for the next node after the selected in the tree view
	private: array<int>^ next_treeNode()
	{
		array<int>^ indices = gcnew array<int>{0};

		if(treeView_tasks->SelectedNode->Index > -1)
		{
			indices = gcnew array<int>(treeView_tasks->SelectedNode->Level+1);
			bool foundNext = false;
			TreeNode^ node = treeView_tasks->SelectedNode;

			// search through parents
			for(int i=indices->Length-1; i>-1; i--)
			{
				indices[i] = node->Index;

				if(node == treeView_tasks->SelectedNode && treeView_tasks->SelectedNode->Nodes->Count > 0)
				{
					foundNext = true;
				}
				if(!foundNext)
				{
					if(node->NextNode)
					{
						indices[i]++;
						foundNext = true;
					}
					else
					{
						Array::Resize(indices, indices->Length-1);
					}
				}

				node = node->Parent;
			}

			node = treeView_tasks->SelectedNode;

			// search through children
			while(node->Nodes->Count > 0)
			{
				Array::Resize(indices, indices->Length+1);
				indices[indices->Length-1] = 0;
				node = node->Nodes[0];
			}
		}

		return indices;
	}

	// returns the task depending on the array of indices to the current path
	private: Task^ current_task(array<int>^ currentIndices)
	{
		if(currentIndices->Length > 0)
		{
			// get the task from indices path
			Task^ t = Tasks[currentIndices[0]];
			for(int i=1; i<currentIndices->Length; i++)
			{
				t = t->sub_quests[currentIndices[i]];
			}
			return t;
		}
		return nullptr;
	}

	// return an array of indices for the next task depending on the array of indices to the current path
	private: array<int>^ next_task(array<int>^ currentIndices)
	{
		if(currentIndices->Length > 0)
		{
			array<int>^ indices = gcnew array<int>(currentIndices->Length);

			// get the task from indices path
			Task^ t = current_task(currentIndices);

			currentIndices->CopyTo(indices, 0);

			// select the next subtask, if available
			if(t->sub_quest_count > 0)
			{
				Array::Resize(indices, indices->Length+1);
				indices[indices->Length-1] = 0;
				return indices;
			}

			// search through parents
			for(int i=indices->Length-1; i>0; i--)
			{
				// check if the current task has a successor
				if(current_task(indices)->next_quest != 0)
				{
					indices[i]++;
					return indices;
				}
				else
				{
					Array::Resize(indices, indices->Length-1);
				}
			}

			// we end up in root task tree
			// at this point indices should have a length of exactly 1
			// we return incremented indices[0] if possible
			if(indices[0] < Tasks->Length-1)
			{
				indices[0]++;
				return indices;
			}
		}

		return gcnew array<int>(0);
	}

	// return the path of indices to the child task that contains the pattern
	private: bool search(array<int>^ indices)
	{
		if(indices->Length > 0)
		{
			// get the task from indices path
			Task^ t = current_task(indices);
			if(!t)
			{
				return false;
			}
			// check search options for this path
			if(comboBox_search->SelectedItem == "ID" && t->ID.ToString() == textBox_search->Text)
			{
				select_treeNode(indices);
				return true;
			}
			if(comboBox_search->SelectedItem == "Name" && t->Name->Contains(textBox_search->Text))
			{
				select_treeNode(indices);
				return true;
			}
			if( (comboBox_search->SelectedItem == "Creature Builder ID") && ((t->ai_trigger.ToString() == textBox_search->Text) || (t->reward_success->ai_trigger.ToString() == textBox_search->Text) || (t->reward_failed->ai_trigger.ToString() == textBox_search->Text)) )
			{
				select_treeNode(indices);
				return true;
			}
		}

		return false; // no entry found
	}

	// select the treenode that corresponds to array of indices
	private: System::Void select_treeNode(array<int>^ indices)
	{
		if(indices->Length > 0)
		{
			TreeNode^ node = treeView_tasks->Nodes[indices[0]];
			for(int i=1; i<indices->Length; i++)
			{
				node = node->Nodes[indices[i]];
			}
			treeView_tasks->SelectedNode = node;
		}
	}

#pragma endregion

//################### NEW #########################

#pragma region DEBUG TRIGGER FUNCTION

	// debug: change information
	private: void TriggerDebug(Control^ sender)
	{
		if(changeConfirmationToolStripMenuItem->Checked)
		{
			MessageBox::Show("Sender: " + sender->Name, "Change Trigger");
		}
	}

#pragma endregion

#pragma region CHANGE EVENT FUNCTIONS

	// general
	private: System::Void change_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->ID != Convert::ToInt32(textBox_id->Text))
				{
					if(MessageBox::Show("Changing a task id is dangerous and can lead to inconsistence. Please check all answers and other (child)tasks that are connected to this id!\n\nChange task id anyway?", "NOTE!", MessageBoxButtons::YesNo) == Windows::Forms::DialogResult::Yes)
					{
						SelectedTask->ID = Convert::ToInt32(textBox_id->Text);

						// Update node in treeview
						String^ name = SelectedTask->Name;
						Drawing::Color c = Drawing::Color::White;
						treeView_tasks->BeginUpdate();
						if(name->StartsWith("^"))
						{
							try
							{
								c = Drawing::Color::FromArgb(int::Parse(name->Substring(1, 6), Globalization::NumberStyles::HexNumber));
								name = name->Substring(7);
							}
							catch(...)
							{
								c = Drawing::Color::White;
							}
						}
						treeView_tasks->SelectedNode->ForeColor = c;
						treeView_tasks->SelectedNode->Text = SelectedTask->ID.ToString() + " - " + name;
						treeView_tasks->EndUpdate();

						// call debug function
						TriggerDebug((Control^)sender);
					}
					else
					{
						textBox_id->Text = SelectedTask->ID.ToString();
					}
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_name(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Name != textBox_name->Text)
				{
					String^ node = textBox_name->Text;
					SelectedTask->Name = node;

					// Update node in treeview
					Drawing::Color c = Drawing::Color::White;
					treeView_tasks->BeginUpdate();
					if(node->StartsWith("^"))
					{
						try
						{
							c = Drawing::Color::FromArgb(int::Parse(node->Substring(1, 6), Globalization::NumberStyles::HexNumber));
							node = node->Substring(7);
						}
						catch(...)
						{
							c = Drawing::Color::White;
						}
					}
					treeView_tasks->SelectedNode->ForeColor = c;
					treeView_tasks->SelectedNode->Text = SelectedTask->ID.ToString() + " - " + node;
					treeView_tasks->EndUpdate();

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_author_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->AuthorText != textBox_author_text->Text)
				{
					if(textBox_author_text->Text == "")
					{
						checkBox_author_mode->Checked = false;
						SelectedTask->author_mode = false;
					}
					else
					{
						checkBox_author_mode->Checked = true;
						SelectedTask->author_mode = true;
					}

					SelectedTask->AuthorText = textBox_author_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void checkBox_trigger_on_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_trigger_on != checkBox_trigger_on->Checked)
				{
					SelectedTask->has_trigger_on = checkBox_trigger_on->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_01(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_01 != Convert::ToInt32(textBox_unknown_01->Text))
				{
					SelectedTask->UNKNOWN_01 = Convert::ToInt32(textBox_unknown_01->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep20(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_fenxiang != Convert::ToInt32(textBox_rep20->Text))
				{
					SelectedTask->shengwang_fenxiang = Convert::ToInt32(textBox_rep20->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep19(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_tianhua != Convert::ToInt32(textBox_rep19->Text))
				{
					SelectedTask->shengwang_tianhua = Convert::ToInt32(textBox_rep19->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep18(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_huaiguang != Convert::ToInt32(textBox_rep18->Text))
				{
					SelectedTask->shengwang_huaiguang = Convert::ToInt32(textBox_rep18->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep17(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_taihao != Convert::ToInt32(textBox_rep17->Text))
				{
					SelectedTask->shengwang_taihao = Convert::ToInt32(textBox_rep17->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep16(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_chenhuang != Convert::ToInt32(textBox_rep16->Text))
				{
					SelectedTask->shengwang_chenhuang = Convert::ToInt32(textBox_rep16->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep15(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_lieshan != Convert::ToInt32(textBox_rep15->Text))
				{
					SelectedTask->shengwang_lieshan = Convert::ToInt32(textBox_rep15->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep14(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_jiuli != Convert::ToInt32(textBox_rep14->Text))
				{
					SelectedTask->shengwang_jiuli = Convert::ToInt32(textBox_rep14->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep13(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_yuliu_04 != Convert::ToInt32(textBox_rep13->Text))
				{
					SelectedTask->shengwang_yuliu_04 = Convert::ToInt32(textBox_rep13->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep12(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_yuliu_03 != Convert::ToInt32(textBox_rep12->Text))
				{
					SelectedTask->shengwang_yuliu_03 = Convert::ToInt32(textBox_rep12->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep11(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_yuliu_02 != Convert::ToInt32(textBox_rep11->Text))
				{
					SelectedTask->shengwang_yuliu_02 = Convert::ToInt32(textBox_rep11->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep10(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_yuliu_01 != Convert::ToInt32(textBox_rep10->Text))
				{
					SelectedTask->shengwang_yuliu_01 = Convert::ToInt32(textBox_rep10->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep9(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_shide != Convert::ToInt32(textBox_rep9->Text))
				{
					SelectedTask->shengwang_shide = Convert::ToInt32(textBox_rep9->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep8(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_wencai != Convert::ToInt32(textBox_rep8->Text))
				{
					SelectedTask->shengwang_wencai = Convert::ToInt32(textBox_rep8->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep7(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_qingyuan != Convert::ToInt32(textBox_rep7->Text))
				{
					SelectedTask->shengwang_qingyuan = Convert::ToInt32(textBox_rep7->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep6(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_zhongyi != Convert::ToInt32(textBox_rep6->Text))
				{
					SelectedTask->shengwang_zhongyi = Convert::ToInt32(textBox_rep6->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep5(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_hehuan != Convert::ToInt32(textBox_rep5->Text))
				{
					SelectedTask->shengwang_hehuan = Convert::ToInt32(textBox_rep5->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep4(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_guiwang != Convert::ToInt32(textBox_rep4->Text))
				{
					SelectedTask->shengwang_guiwang = Convert::ToInt32(textBox_rep4->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep3(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_tianyin != Convert::ToInt32(textBox_rep3->Text))
				{
					SelectedTask->shengwang_tianyin = Convert::ToInt32(textBox_rep3->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep2(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_qingyun != Convert::ToInt32(textBox_rep2->Text))
				{
					SelectedTask->shengwang_qingyun = Convert::ToInt32(textBox_rep2->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rep1(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->shengwang_guidao != Convert::ToInt32(textBox_rep1->Text))
				{
					SelectedTask->shengwang_guidao = Convert::ToInt32(textBox_rep1->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_type(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->type != Convert::ToInt32(textBox_type->Text))
				{
					SelectedTask->type = Convert::ToInt32(textBox_type->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	private: System::Void change_time_limit(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->time_limit != Convert::ToInt32(textBox_time_limit->Text))
				{
					SelectedTask->time_limit = Convert::ToInt32(textBox_time_limit->Text);
					// Recalculate time for time based rewards...
					for(int i=0; i<listBox_reward_timed->Items->Count; i++)
					{
						listBox_reward_timed->Items[i] = "Success Time [sec]: " + (SelectedTask->time_limit*SelectedTask->rewards_timed_factors[i]);
					}

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_02(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_02 != checkBox_unknown_02->Checked)
				{
					SelectedTask->UNKNOWN_02 = checkBox_unknown_02->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rinedbag(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_items_bags != checkBox_rineedbag->Checked)
				{
					SelectedTask->required_items_bags = checkBox_rineedbag->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_has_date_fail(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->val_unknown_02 != checkBox_has_date_fail->Checked)
				{
					SelectedTask->val_unknown_02 = checkBox_has_date_fail->Checked;
					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	/*
	private: System::Void change_date_fail(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_date_fail->CurrentCell->RowIndex;
				switch (dataGridView_date_fail->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->date_fail->year = Convert::ToInt32(dataGridView_date_fail->CurrentCell->Value);
								break;
					case 1:		SelectedTask->date_fail->month = Convert::ToInt32(dataGridView_date_fail->CurrentCell->Value);
								break;
					case 2:		SelectedTask->date_fail->day = Convert::ToInt32(dataGridView_date_fail->CurrentCell->Value);
								break;
					case 3:		SelectedTask->date_fail->hour = Convert::ToInt32(dataGridView_date_fail->CurrentCell->Value);
								break;
					case 4:		SelectedTask->date_fail->minute = Convert::ToInt32(dataGridView_date_fail->CurrentCell->Value);
								break;
					case 5:		SelectedTask->date_fail->weekday = Column_date_fail_weekday->Items->IndexOf(dataGridView_date_fail->CurrentCell->Value);
								break;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	*/
	private: System::Void change_unknown_03(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_03 != checkBox_unknown_03->Checked)
				{
					SelectedTask->UNKNOWN_03 = checkBox_unknown_03->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_has_date_spans(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_date_spans != checkBox_has_date_spans->Checked)
				{
					SelectedTask->has_date_spans = checkBox_has_date_spans->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_date_span(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_date_spans->CurrentCell->RowIndex;
				switch(dataGridView_date_spans->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->date_spans[r]->from->year = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 1:		SelectedTask->date_spans[r]->from->month = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 2:		SelectedTask->date_spans[r]->from->day = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 3:		SelectedTask->date_spans[r]->from->hour = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 4:		SelectedTask->date_spans[r]->from->minute = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 5:		SelectedTask->date_spans[r]->from->weekday = Column6->Items->IndexOf(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 7:		SelectedTask->date_spans[r]->to->year = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 8:		SelectedTask->date_spans[r]->to->month = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 9:		SelectedTask->date_spans[r]->to->day = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 10:	SelectedTask->date_spans[r]->to->hour = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 11:	SelectedTask->date_spans[r]->to->minute = Convert::ToInt32(dataGridView_date_spans->CurrentCell->Value);
								break;
					case 12:	SelectedTask->date_spans[r]->to->weekday = Column13->Items->IndexOf(dataGridView_date_spans->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_date_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->date_spans_count++;
			Array::Resize(SelectedTask->date_spans, SelectedTask->date_spans->Length+1);
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1] = gcnew DateSpan();

			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from = gcnew Date();
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->year = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->month = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->day = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->hour = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->minute = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->from->weekday = 0;

			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to = gcnew Date();
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->year = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->month = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->day = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->hour = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->minute = 0;
			SelectedTask->date_spans[SelectedTask->date_spans->Length-1]->to->weekday = 0;

			dataGridView_date_spans->Rows->Add(gcnew array<String^>{"0", "0", "0", "0", "0", Column6->Items[0]->ToString(), "to", "0", "0", "0", "0", "0", Column13->Items[0]->ToString()});
		}
	}
	private: System::Void change_tasknum(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_show_tasks != checkBox_tasknum->Checked)
				{
					SelectedTask->has_show_tasks = checkBox_tasknum->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void remove_date_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->date_spans_count>0 && dataGridView_date_spans->CurrentCell->RowIndex>-1)
			{
				SelectedTask->date_spans_count--;

				array<DateSpan^>^ temp = gcnew array<DateSpan^>(SelectedTask->date_spans_count);
				Array::Copy(SelectedTask->date_spans, 0, temp, 0, dataGridView_date_spans->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->date_spans, dataGridView_date_spans->CurrentCell->RowIndex+1, temp, dataGridView_date_spans->CurrentCell->RowIndex, SelectedTask->date_spans_count - dataGridView_date_spans->CurrentCell->RowIndex);
				SelectedTask->date_spans = temp;

				dataGridView_date_spans->Rows->RemoveAt(dataGridView_date_spans->CurrentCell->RowIndex);
			}
		}
	}
	private: System::Void change_iunknown4(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_4 != checkBox_iunk4->Checked)
				{
					SelectedTask->items_unknown_4 = checkBox_iunk4->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_iunknown5(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_5 != checkBox_iunk5->Checked)
				{
					SelectedTask->items_unknown_5 = checkBox_iunk5->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_04(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->UNKNOWN_157) != textBox_unknown_04->Text)
				{
					SelectedTask->UNKNOWN_157 = HexString_to_ByteArray(textBox_unknown_04->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_05(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->UNKNOWN_ZEROS) != textBox_unknown_05->Text)
				{
					SelectedTask->UNKNOWN_ZEROS = HexString_to_ByteArray(textBox_unknown_05->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_06(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->UNKNOWN_112_1) != textBox_unknown_06->Text)
				{
					SelectedTask->UNKNOWN_112_1 = HexString_to_ByteArray(textBox_unknown_06->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_07(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->UNKNOWN_113) != textBox_unknown_07->Text)
				{
					SelectedTask->UNKNOWN_113 = HexString_to_ByteArray(textBox_unknown_07->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_08(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->GlobalVal1) != textBox_unknown_08->Text)
				{
					SelectedTask->GlobalVal1 = HexString_to_ByteArray(textBox_unknown_08->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_unknown_09(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->GlobalVal2) != textBox_unknown_09->Text)
				{
					SelectedTask->GlobalVal2 = HexString_to_ByteArray(textBox_unknown_09->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// flags
	private: System::Void change_activate_first_subquest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->activate_first_subquest != checkBox_activate_first_subquest->Checked)
				{
					SelectedTask->activate_first_subquest = checkBox_activate_first_subquest->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_activate_random_subquest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->activate_random_subquest != checkBox_activate_random_subquest->Checked)
				{
					SelectedTask->activate_random_subquest = checkBox_activate_random_subquest->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_activate_next_subquest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->activate_next_subquest != checkBox_activate_next_subquest->Checked)
				{
					SelectedTask->activate_next_subquest = checkBox_activate_next_subquest->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_on_give_up_parent_fails(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->on_give_up_parent_fails != checkBox_on_give_up_parent_fails->Checked)
				{
					SelectedTask->on_give_up_parent_fails = checkBox_on_give_up_parent_fails->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_on_success_parent_success(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->on_success_parent_success != checkBox_on_success_parent_success->Checked)
				{
					SelectedTask->on_success_parent_success = checkBox_on_success_parent_success->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_can_give_up(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->can_give_up != checkBox_can_give_up->Checked)
				{
					SelectedTask->can_give_up = checkBox_can_give_up->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_haslimit(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->haslimit != checkBox_haslimit->Checked)
				{
					SelectedTask->haslimit = checkBox_haslimit->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontfeng03(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_feisheng != checkBox_frongfang03->Checked)
				{
					SelectedTask->front_tasks_feisheng = checkBox_frongfang03->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontfeng02(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_feisheng_02 != checkBox_frontfeng02->Checked)
				{
					SelectedTask->front_tasks_feisheng_02 = checkBox_frontfeng02->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontfeng01(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_feisheng_01 != checkBox_frontfeng01->Checked)
				{
					SelectedTask->front_tasks_feisheng_01 = checkBox_frontfeng01->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontmarried(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_marry != checkBox_frontmarried->Checked)
				{
					SelectedTask->front_tasks_marry = checkBox_frontmarried->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_needlvl(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->needlv  != Convert::ToInt32(textBox_needlvl->Text))
				{
					SelectedTask->needlv  = Convert::ToInt32(textBox_needlvl->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_npctime(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->SafeNpcTime  != Convert::ToInt32(textBox_npctime->Text))
				{
					SelectedTask->SafeNpcTime  = Convert::ToInt32(textBox_npctime->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_safenpc(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->SafeNPC  != Convert::ToInt32(textBox_safenpc->Text))
				{
					SelectedTask->SafeNPC  = Convert::ToInt32(textBox_safenpc->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_completetype(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->WanChengLeiXing != Convert::ToInt32(textBox_completetype->Text))
				{
					SelectedTask->WanChengLeiXing = Convert::ToInt32(textBox_completetype->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_method(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->JinXingFangShi != Convert::ToInt32(textBox_method->Text))
				{
					SelectedTask->JinXingFangShi = Convert::ToInt32(textBox_method->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_soulmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Soul_max != Convert::ToInt32(textBox_soulmax->Text))
				{
					SelectedTask->Soul_max = Convert::ToInt32(textBox_soulmax->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_soulmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Soul_min != Convert::ToInt32(textBox_soulmin->Text))
				{
					SelectedTask->Soul_min = Convert::ToInt32(textBox_soulmin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_soulunkn1(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_Soul_01 != Convert::ToInt32(textBox_soulunkn1->Text))
				{
					SelectedTask->UNKNOWN_Soul_01 = Convert::ToInt32(textBox_soulunkn1->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ingotmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Ingot_max != Convert::ToInt32(textBox_ingotmax->Text))
				{
					SelectedTask->Ingot_max = Convert::ToInt32(textBox_ingotmax->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ingotmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Ingot_min != Convert::ToInt32(textBox_ingotmin->Text))
				{
					SelectedTask->Ingot_min = Convert::ToInt32(textBox_ingotmin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_zhenying(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->zhenying != Convert::ToInt32(textBox_zhenying->Text))
				{
					SelectedTask->zhenying = Convert::ToInt32(textBox_zhenying->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_blamon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->BangLing_shuliang != Convert::ToInt32(textBox_blamon->Text))
				{
					SelectedTask->BangLing_shuliang = Convert::ToInt32(textBox_blamon->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_vtask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_tasks != Convert::ToInt32(textBox_vtask->Text))
				{
					SelectedTask->Shitu_tasks = Convert::ToInt32(textBox_vtask->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rsound(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->YuanShenlv != checkBox_rsound->Checked)
				{
					SelectedReward->YuanShenlv = checkBox_rsound->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rew1330_10(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unkn_1330_10 != checkBox_rew1330_10->Checked)
				{
					SelectedReward->unkn_1330_10 = checkBox_rew1330_10->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rskillon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->skill_on != checkBox_rskillon->Checked)
				{
					SelectedReward->skill_on = checkBox_rskillon->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rcampon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Camp_on != checkBox_rcampon->Checked)
				{
					SelectedReward->Camp_on = checkBox_rcampon->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rtomeon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->skill_Tianshu_on != checkBox_rtomeon->Checked)
				{
					SelectedReward->skill_Tianshu_on = checkBox_rtomeon->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rview2(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Shitu_02 != checkBox_rview2->Checked)
				{
					SelectedReward->Shitu_02 = checkBox_rview2->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rview1(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Shitu_01 != checkBox_rview1->Checked)
				{
					SelectedReward->Shitu_01 = checkBox_rview1->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk52(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_52 != checkBox_runk52->Checked)
				{
					SelectedReward->unknow_52 = checkBox_runk52->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rnotice(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Notice != checkBox_rnotice->Checked)
				{
					SelectedReward->Notice = checkBox_rnotice->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_triggeron(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->trigger_on != checkBox_triggeron->Checked)
				{
					SelectedReward->trigger_on = checkBox_triggeron->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_openreason(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->OpenYuanYing != checkBox_openreason->Checked)
				{
					SelectedReward->OpenYuanYing = checkBox_openreason->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rchushi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ChuShi != checkBox_rchushi->Checked)
				{
					SelectedReward->ChuShi = checkBox_rchushi->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rpanshi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->PanShi != checkBox_rpanshi->Checked)
				{
					SelectedReward->PanShi = checkBox_rpanshi->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_craftup(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ShengChan_UP != checkBox_craftup->Checked)
				{
					SelectedReward->ShengChan_UP = checkBox_craftup->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewdivorce(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Divorce != checkBox_rewdivorce->Checked)
				{
					SelectedReward->Divorce = checkBox_rewdivorce->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewremallinf(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->clearPK != checkBox_rewremallinf->Checked)
				{
					SelectedReward->clearPK = checkBox_rewremallinf->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_viewlvl(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_lv != Convert::ToInt32(textBox_viewlvl->Text))
				{
					SelectedTask->Shitu_lv = Convert::ToInt32(textBox_viewlvl->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanchmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_tiaozhan_max != Convert::ToInt32(textBox_clanchmax->Text))
				{
					SelectedTask->Jiazu_tiaozhan_max = Convert::ToInt32(textBox_clanchmax->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanchmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_tiaozhan_min != Convert::ToInt32(textBox_clanchmin->Text))
				{
					SelectedTask->Jiazu_tiaozhan_min = Convert::ToInt32(textBox_clanchmin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanmap(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_map_id != Convert::ToInt32(textBox_clanmap->Text))
				{
					SelectedTask->Jiazu_map_id = Convert::ToInt32(textBox_clanmap->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanskillid(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jineng_id != Convert::ToInt32(textBox_clanskillid->Text))
				{
					SelectedTask->Jiazu_jineng_id = Convert::ToInt32(textBox_clanskillid->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanskillexpmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jineng_shulian_max != Convert::ToInt32(textBox_clanskillexpmax->Text))
				{
					SelectedTask->Jiazu_jineng_shulian_max = Convert::ToInt32(textBox_clanskillexpmax->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanskillexpmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jineng_shulian_min != Convert::ToInt32(textBox_clanskillexpmin->Text))
				{
					SelectedTask->Jiazu_jineng_shulian_min = Convert::ToInt32(textBox_clanskillexpmin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanskillmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jineng_lv_max != Convert::ToInt32(textBox_clanskillmax->Text))
				{
					SelectedTask->Jiazu_jineng_lv_max = Convert::ToInt32(textBox_clanskillmax->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanskillmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jineng_lv_min != Convert::ToInt32(textBox_clanskillmin->Text))
				{
					SelectedTask->Jiazu_jineng_lv_min = Convert::ToInt32(textBox_clanskillmin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontunkn01(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->how_unknown01 != checkBox_frontunkn01->Checked)
				{
					SelectedTask->how_unknown01 = checkBox_frontunkn01->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_licreq(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_yaoqiubanpai != checkBox_licreq->Checked)
				{
					SelectedTask->front_tasks_yaoqiubanpai = checkBox_licreq->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontalliance(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_showbangpai != checkBox_frontalliance->Checked)
				{
					SelectedTask->front_tasks_showbangpai = checkBox_frontalliance->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_soulmaxon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Soul_max_on != checkBox_soulmaxon->Checked)
				{
					SelectedTask->Soul_max_on = checkBox_soulmaxon->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_soulminom(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Soul_min_on != checkBox_soulminom->Checked)
				{
					SelectedTask->Soul_min_on = checkBox_soulminom->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_hassheng(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_shengwang != checkBox_hassheng->Checked)
				{
					SelectedTask->has_shengwang = checkBox_hassheng->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_needgender(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_gender != checkBox_needgender->Checked)
				{
					SelectedTask->has_gender = checkBox_needgender->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_hasauto(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_xianji != checkBox_hasauto->Checked)
				{
					SelectedTask->has_xianji = checkBox_hasauto->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_haswork(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_working != checkBox_haswork->Checked)
				{
					SelectedTask->has_working = checkBox_haswork->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_hasnext(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_next != checkBox_hasnext->Checked)
				{
					SelectedTask->has_next = checkBox_hasnext->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_blded(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->BangLing_kouchu != checkBox_blded->Checked)
				{
					SelectedTask->BangLing_kouchu = checkBox_blded->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanlead(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_zuzhang_tasks != checkBox_clanlead->Checked)
				{
					SelectedTask->Jiazu_zuzhang_tasks = checkBox_clanlead->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clanc(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Jiazu_jiazu_tasks != checkBox_clanc->Checked)
				{
					SelectedTask->Jiazu_jiazu_tasks = checkBox_clanc->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_vunkn2(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_UNKNOWN_02 != checkBox_vunkn2->Checked)
				{
					SelectedTask->Shitu_UNKNOWN_02 = checkBox_vunkn2->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_vunkn1(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_UNKNOWN_01 != checkBox_vunkn1->Checked)
				{
					SelectedTask->Shitu_UNKNOWN_01 = checkBox_vunkn1->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_vpeach(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_pantaohui != checkBox_vpeach->Checked)
				{
					SelectedTask->Shitu_pantaohui = checkBox_vpeach->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_vchef(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_chushi != checkBox_vchef->Checked)
				{
					SelectedTask->Shitu_chushi = checkBox_vchef->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_view(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_shitu != checkBox_view->Checked)
				{
					SelectedTask->Shitu_shitu = checkBox_view->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_viewreq(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Shitu_yaoqiu != checkBox_viewreq->Checked)
				{
					SelectedTask->Shitu_yaoqiu = checkBox_viewreq->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontgen(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_showgender != checkBox_frontgen->Checked)
				{
					SelectedTask->front_tasks_showgender = checkBox_frontgen->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_rclvl(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->RCLv != Convert::ToSingle(textBox_reward_rclvl->Text))
				{
					SelectedReward->RCLv = Convert::ToSingle(textBox_reward_rclvl->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewardExpLv(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ExpLv != Convert::ToSingle(textBox_rewardExpLv->Text))
				{
					SelectedReward->ExpLv = Convert::ToSingle(textBox_rewardExpLv->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clearpk(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->removePK != Convert::ToInt32(textBox_clearpk->Text))
				{
					SelectedReward->removePK = Convert::ToInt32(textBox_clearpk->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewardtitle(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->title != Convert::ToInt32(textBox_rewardtitle->Text))
				{
					SelectedReward->title = Convert::ToInt32(textBox_rewardtitle->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clancontribut(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->family_gongxian != Convert::ToInt32(textBox_clancontribut->Text))
				{
					SelectedReward->family_gongxian = Convert::ToInt32(textBox_clancontribut->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewclancont(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->fation_gongxian != Convert::ToInt32(textBox_rewclancont->Text))
				{
					SelectedReward->fation_gongxian = Convert::ToInt32(textBox_rewclancont->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_grint(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->group_integral != Convert::ToInt32(textBox_grint->Text))
				{
					SelectedReward->group_integral = Convert::ToInt32(textBox_grint->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_newtask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->new_quest != Convert::ToInt32(textBox_reward_newtask->Text))
				{
					SelectedReward->new_quest = Convert::ToInt32(textBox_reward_newtask->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_APLv(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->APLv != Convert::ToSingle(textBox_reward_APLv->Text))
				{
					SelectedReward->APLv = Convert::ToSingle(textBox_reward_APLv->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_itunk06(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_6 != Convert::ToInt32(textBox_itunk06->Text))
				{
					SelectedTask->items_unknown_6 = Convert::ToInt32(textBox_itunk06->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_itunk07(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_7 != Convert::ToInt32(textBox_itunk07->Text))
				{
					SelectedTask->items_unknown_7 = Convert::ToInt32(textBox_itunk07->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_iunk09(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_9 != Convert::ToInt32(textBox_iunk09->Text))
				{
					SelectedTask->items_unknown_9 = Convert::ToInt32(textBox_iunk09->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_iunk10(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_10 != Convert::ToInt32(textBox_iunk10->Text))
				{
					SelectedTask->items_unknown_10 = Convert::ToInt32(textBox_iunk10->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_iunk11(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->items_unknown_11 != Convert::ToInt32(textBox_iunk11->Text))
				{
					SelectedTask->items_unknown_11 = Convert::ToInt32(textBox_iunk11->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_repeatable(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->repeatable != checkBox_repeatable->Checked)
				{
					SelectedTask->repeatable = checkBox_repeatable->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_repeatable_after_failure(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->repeatable_after_failure != checkBox_repeatable_after_failure->Checked)
				{
					SelectedTask->repeatable_after_failure = checkBox_repeatable_after_failure->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_fail_on_death(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->fail_on_death != checkBox_fail_on_death->Checked)
				{
					SelectedTask->fail_on_death = checkBox_fail_on_death->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_on_fail_parent_fail(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->on_fail_parent_fail != checkBox_on_fail_parent_fail->Checked)
				{
					SelectedTask->on_fail_parent_fail = checkBox_on_fail_parent_fail->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_10(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_10 != checkBox_unknown_10->Checked)
				{
					SelectedTask->UNKNOWN_10 = checkBox_unknown_10->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_cooldown(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->cooldown != Convert::ToInt32(textBox_cooldown->Text))
				{
					SelectedTask->cooldown = Convert::ToInt32(textBox_cooldown->Text);
					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_resettype(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->reset_type != Convert::ToInt32(textBox_resettype->Text))
				{
					SelectedTask->reset_type = Convert::ToInt32(textBox_resettype->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_resetcycle(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->reset_cycle != Convert::ToInt32(textBox_resetcycle->Text))
				{
					SelectedTask->reset_cycle = Convert::ToInt32(textBox_resetcycle->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_bldbmin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->bloodbound_min != Convert::ToInt32(textBox_bldbmin->Text))
				{
					SelectedTask->bloodbound_min = Convert::ToInt32(textBox_bldbmin->Text);
					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_bldbmax(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->bloodbound_max != Convert::ToInt32(textBox_bldbmax->Text))
				{
					SelectedTask->bloodbound_max = Convert::ToInt32(textBox_bldbmax->Text);
					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_newtype2(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->new_type2 != Convert::ToInt32(textBox_newtype2->Text))
				{
					SelectedTask->new_type2 = Convert::ToInt32(textBox_newtype2->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_newtype(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->new_type != Convert::ToInt32(textBox_newtype->Text))
				{
					SelectedTask->new_type = Convert::ToInt32(textBox_newtype->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_classify(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->classify != Convert::ToInt32(textBox_classify->Text))
				{
					SelectedTask->classify = Convert::ToInt32(textBox_classify->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_component(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->component != Convert::ToSingle(textBox_component->Text))
				{
					SelectedTask->component = Convert::ToSingle(textBox_component->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un13304(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->unkn_1330_4 != Convert::ToInt32(textBox_un13304->Text))
				{
					SelectedTask->unkn_1330_4 = Convert::ToInt32(textBox_un13304->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un13303(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->unkn_1330_3 != Convert::ToInt32(textBox_un13303->Text))
				{
					SelectedTask->unkn_1330_3 = Convert::ToInt32(textBox_un13303->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_cleartask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->cleartask_unknown != Convert::ToInt32(textBox_cleartask->Text))
				{
					SelectedTask->cleartask_unknown = Convert::ToInt32(textBox_cleartask->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_autounk(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->autotask_unknown != Convert::ToInt32(textBox_autounk->Text))
				{
					SelectedTask->autotask_unknown = Convert::ToInt32(textBox_autounk->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_player_limit(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->player_limit != Convert::ToInt32(textBox_player_limit->Text))
				{
					SelectedTask->player_limit = Convert::ToInt32(textBox_player_limit->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_valunknown3(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->val_unknown_03 != checkBox_valunk3->Checked)
				{
					SelectedTask->val_unknown_03 = checkBox_valunk3->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// trigger locations (start on enter)
	private: System::Void change_trigger_locations_has_spans(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->trigger_locations->has_location != checkBox_trigger_locations_has_spans->Checked)
				{
					SelectedTask->trigger_locations->has_location = checkBox_trigger_locations_has_spans->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_trigger_locations_map_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->trigger_locations->map_id != Convert::ToInt32(textBox_trigger_locations_map_id->Text))
				{
					SelectedTask->trigger_locations->map_id = Convert::ToInt32(textBox_trigger_locations_map_id->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_allzone(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->all_zone != checkBox_allzone->Checked)
				{
					SelectedTask->all_zone = checkBox_allzone->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un13302(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->unkn_1330_2 != checkBox_un13302->Checked)
				{
					SelectedTask->unkn_1330_2 = checkBox_un13302->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un13301(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->unkn_1330_1 != Convert::ToInt32(textBox_un13301->Text))
				{
					SelectedTask->unkn_1330_1 = Convert::ToInt32(textBox_un13301->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_tasknumgrid(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_tasknum->CurrentCell->RowIndex;
				switch(dataGridView_tasknum->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->front_tasks_a[r] = Convert::ToInt32(dataGridView_tasknum->CurrentCell->Value);
								break;
					case 1:		SelectedTask->front_tasks_num[r] = Convert::ToInt32(dataGridView_tasknum->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_trigger_locations_span(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_trigger_location_spans->CurrentCell->RowIndex;
				switch(dataGridView_trigger_location_spans->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->trigger_locations->spans[r]->north = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
					case 1:		SelectedTask->trigger_locations->spans[r]->south = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
					case 2:		SelectedTask->trigger_locations->spans[r]->west = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
					case 3:		SelectedTask->trigger_locations->spans[r]->east = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
					case 4:		SelectedTask->trigger_locations->spans[r]->bottom = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
					case 5:		SelectedTask->trigger_locations->spans[r]->top = Convert::ToSingle(dataGridView_trigger_location_spans->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_trigger_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->trigger_locations->count++;
			Array::Resize(SelectedTask->trigger_locations->spans, SelectedTask->trigger_locations->spans->Length+1);
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1] = gcnew Span();

			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->north = 0.0;
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->south = 0.0;
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->west = 0.0;
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->east = 0.0;
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->bottom = 0.0;
			SelectedTask->trigger_locations->spans[SelectedTask->trigger_locations->spans->Length-1]->top = 0.0;

			dataGridView_trigger_location_spans->Rows->Add(gcnew array<String^>{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0"});
		}
	}
	private: System::Void add_tops(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			//if(SelectedTask->required_reputation>0)
			//{
				SelectedTask->required_reputation++;
				Array::Resize(SelectedTask->tops, SelectedTask->required_reputation);
				SelectedTask->tops[SelectedTask->required_reputation-1]=0;

				listBox_tops->Items->Clear();
				for(int j=0; j<SelectedTask->tops->Length; j++ )
				{	
					listBox_tops->Items->Add(SelectedTask->tops[j].ToString());
				}
			//}
		}
	}
	private: System::Void remove_tops(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->required_reputation>0 && listBox_tops->SelectedIndex>-1)
			{
				SelectedTask->required_reputation--;

				array<__int16>^ temp = gcnew array<__int16>(SelectedTask->required_reputation);
				Array::Copy(SelectedTask->tops, 0, temp, 0, listBox_tops->SelectedIndex);
				Array::Copy(SelectedTask->tops, listBox_tops->SelectedIndex+1, temp, listBox_tops->SelectedIndex, SelectedTask->required_reputation - listBox_tops->SelectedIndex);
				SelectedTask->tops = temp;

				listBox_tops->Items->Clear();
				for(int j=0; j<SelectedTask->tops->Length; j++ )
				{	
					listBox_tops->Items->Add(SelectedTask->tops[j].ToString());
				}
			}
		}
	}
	private: System::Void remove_trigger_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->trigger_locations->count>0 && dataGridView_trigger_location_spans->CurrentCell->RowIndex>-1)
			{
				SelectedTask->trigger_locations->count--;

				array<Span^>^ temp = gcnew array<Span^>(SelectedTask->trigger_locations->count);
				Array::Copy(SelectedTask->trigger_locations->spans, 0, temp, 0, dataGridView_trigger_location_spans->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->trigger_locations->spans, dataGridView_trigger_location_spans->CurrentCell->RowIndex+1, temp, dataGridView_trigger_location_spans->CurrentCell->RowIndex, SelectedTask->trigger_locations->count - dataGridView_trigger_location_spans->CurrentCell->RowIndex);
				SelectedTask->trigger_locations->spans = temp;

				dataGridView_trigger_location_spans->Rows->RemoveAt(dataGridView_trigger_location_spans->CurrentCell->RowIndex);
			}
		}
	}
	private: System::Void change_showlvl(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_show_level != checkBox_showlvl ->Checked)
				{
					SelectedTask->has_show_level = checkBox_showlvl ->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_clantask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->clan_task != checkBox_clantask->Checked)
				{
					SelectedTask->clan_task = checkBox_clantask->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// fail locations (fail on enter)
	private: System::Void change_fail_locations_has_spans(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->fail_locations->has_location != checkBox_fail_locations_has_spans->Checked)
				{
					SelectedTask->fail_locations->has_location = checkBox_fail_locations_has_spans->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_plimitnum(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->player_limit_num != Convert::ToInt32(textBox_plimitnum->Text))
				{
					SelectedTask->player_limit_num = Convert::ToInt32(textBox_plimitnum->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_fail_locations_map_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->fail_locations->map_id != Convert::ToInt32(textBox_fail_locations_map_id->Text))
				{
					SelectedTask->fail_locations->map_id = Convert::ToInt32(textBox_fail_locations_map_id->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_fail_locations_span(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_fail_location_spans->CurrentCell->RowIndex;
				switch(dataGridView_fail_location_spans->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->fail_locations->spans[r]->north = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
					case 1:		SelectedTask->fail_locations->spans[r]->south = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
					case 2:		SelectedTask->fail_locations->spans[r]->west = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
					case 3:		SelectedTask->fail_locations->spans[r]->east = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
					case 4:		SelectedTask->fail_locations->spans[r]->bottom = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
					case 5:		SelectedTask->fail_locations->spans[r]->top = Convert::ToSingle(dataGridView_fail_location_spans->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_fail_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->fail_locations->count++;
			Array::Resize(SelectedTask->fail_locations->spans, SelectedTask->fail_locations->spans->Length+1);
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1] = gcnew Span();

			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->north = 0.0;
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->south = 0.0;
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->west = 0.0;
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->east = 0.0;
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->bottom = 0.0;
			SelectedTask->fail_locations->spans[SelectedTask->fail_locations->spans->Length-1]->top = 0.0;

			dataGridView_fail_location_spans->Rows->Add(gcnew array<String^>{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0"});
		}
	}
	private: System::Void remove_fail_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->fail_locations->count>0 && dataGridView_fail_location_spans->CurrentCell->RowIndex>-1)
			{
				SelectedTask->fail_locations->count--;

				array<Span^>^ temp = gcnew array<Span^>(SelectedTask->fail_locations->count);
				Array::Copy(SelectedTask->fail_locations->spans, 0, temp, 0, dataGridView_fail_location_spans->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->fail_locations->spans, dataGridView_fail_location_spans->CurrentCell->RowIndex+1, temp, dataGridView_fail_location_spans->CurrentCell->RowIndex, SelectedTask->fail_locations->count - dataGridView_fail_location_spans->CurrentCell->RowIndex);
				SelectedTask->fail_locations->spans = temp;

				dataGridView_fail_location_spans->Rows->RemoveAt(dataGridView_fail_location_spans->CurrentCell->RowIndex);
			}
		}
	}
	// reach locations
	private: System::Void change_reach_locations_has_spans(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->reach_locations->has_location != checkBox_reach_locations_has_spans->Checked)
				{
					SelectedTask->reach_locations->has_location = checkBox_reach_locations_has_spans->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewDiZong(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->DiZong != Convert::ToInt32(textBox_rewDiZong->Text))
				{
					SelectedReward->sw->DiZong = Convert::ToInt32(textBox_rewDiZong->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_TianZong(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->TianZong != Convert::ToInt32(textBox_TianZong->Text))
				{
					SelectedReward->sw->TianZong = Convert::ToInt32(textBox_TianZong->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ZhanXun(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->ZhanXun != Convert::ToInt32(textBox_ZhanXun->Text))
				{
					SelectedReward->sw->ZhanXun = Convert::ToInt32(textBox_ZhanXun->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ZhanJi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->ZhanJi != Convert::ToInt32(textBox_ZhanJi->Text))
				{
					SelectedReward->sw->ZhanJi = Convert::ToInt32(textBox_ZhanJi->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewRenZong(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->RenZong != Convert::ToInt32(textBox_rewRenZong->Text))
				{
					SelectedReward->sw->RenZong = Convert::ToInt32(textBox_rewRenZong->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk15(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->unknow_15 != Convert::ToInt32(textBox_runk15->Text))
				{
					SelectedReward->sw->unknow_15 = Convert::ToInt32(textBox_runk15->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepfoyuan(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->FoYuan != Convert::ToInt32(textBox_rewrepfoyuan->Text))
				{
					SelectedReward->sw->FoYuan = Convert::ToInt32(textBox_rewrepfoyuan->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewmoxing(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->MoXing != Convert::ToInt32(textBox_rewmoxing->Text))
				{
					SelectedReward->sw->MoXing = Convert::ToInt32(textBox_rewmoxing->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_DaoXin(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->DaoXin != Convert::ToInt32(textBox_DaoXin->Text))
				{
					SelectedReward->sw->DaoXin = Convert::ToInt32(textBox_DaoXin->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewshide(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->ShiDe != Convert::ToInt32(textBox_rewshide->Text))
				{
					SelectedReward->sw->ShiDe = Convert::ToInt32(textBox_rewshide->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewwencai(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->WenCai != Convert::ToInt32(textBox_rewwencai->Text))
				{
					SelectedReward->sw->WenCai = Convert::ToInt32(textBox_rewwencai->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewQingyuan(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->QingYuan != Convert::ToInt32(textBox_rewQingyuan->Text))
				{
					SelectedReward->sw->QingYuan = Convert::ToInt32(textBox_rewQingyuan->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepzhongyi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->ZhongYi != Convert::ToInt32(textBox_rewrepzhongyi->Text))
				{
					SelectedReward->sw->ZhongYi = Convert::ToInt32(textBox_rewrepzhongyi->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ChenHuang(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->ChenHuang != Convert::ToInt32(textBox_ChenHuang->Text))
				{
					SelectedReward->sw->ChenHuang = Convert::ToInt32(textBox_ChenHuang->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewreptaihao(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->TaiHao != Convert::ToInt32(textBox_rewreptaihao->Text))
				{
					SelectedReward->sw->TaiHao = Convert::ToInt32(textBox_rewreptaihao->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_TianHua(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->TianHua != Convert::ToInt32(textBox_TianHua->Text))
				{
					SelectedReward->sw->TianHua = Convert::ToInt32(textBox_TianHua->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_HuaiGuang(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->HuaiGuang != Convert::ToInt32(textBox_HuaiGuang->Text))
				{
					SelectedReward->sw->HuaiGuang = Convert::ToInt32(textBox_HuaiGuang->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_LieShan(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->LieShan != Convert::ToInt32(textBox_LieShan->Text))
				{
					SelectedReward->sw->LieShan = Convert::ToInt32(textBox_LieShan->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewJiuLi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->JiuLi != Convert::ToInt32(textBox_rewJiuLi->Text))
				{
					SelectedReward->sw->JiuLi = Convert::ToInt32(textBox_rewJiuLi->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_FenXiang(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->FenXiang != Convert::ToInt32(textBox_FenXiang->Text))
				{
					SelectedReward->sw->FenXiang = Convert::ToInt32(textBox_FenXiang->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewreplupi(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->HeHuan != Convert::ToInt32(textBox_rewreplupi->Text))
				{
					SelectedReward->sw->HeHuan = Convert::ToInt32(textBox_rewreplupi->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepvim(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->GuiWang != Convert::ToInt32(textBox_rewrepvim->Text))
				{
					SelectedReward->sw->GuiWang = Convert::ToInt32(textBox_rewrepvim->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepsky(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->TianYin != Convert::ToInt32(textBox_rewrepsky->Text))
				{
					SelectedReward->sw->TianYin = Convert::ToInt32(textBox_rewrepsky->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepjad(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->QingYun != Convert::ToInt32(textBox_rewrepjad->Text))
				{
					SelectedReward->sw->QingYun = Convert::ToInt32(textBox_rewrepjad->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewrepmodo(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->sw->GuiDao != Convert::ToInt32(textBox_rewrepmodo->Text))
				{
					SelectedReward->sw->GuiDao = Convert::ToInt32(textBox_rewrepmodo->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rhuli(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->HongLiIngot != Convert::ToInt32(textBox_rhuli->Text))
				{
					SelectedReward->HongLiIngot = Convert::ToInt32(textBox_rhuli->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk56(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknown_56 != Convert::ToInt32(textBox_runk56->Text))
				{
					SelectedReward->unknown_56 = Convert::ToInt32(textBox_runk56->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk55(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknown_55 != Convert::ToInt32(textBox_runk55->Text))
				{
					SelectedReward->unknown_55 = Convert::ToInt32(textBox_runk55->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk53(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknown_53 != Convert::ToInt32(textBox_runk53->Text))
				{
					SelectedReward->unknown_53 = Convert::ToInt32(textBox_runk53->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk51(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_51 != Convert::ToInt32(textBox_runk51->Text))
				{
					SelectedReward->unknow_51 = Convert::ToInt32(textBox_runk51->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run50(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_50 != Convert::ToInt32(textBox_run50->Text))
				{
					SelectedReward->unknow_50 = Convert::ToInt32(textBox_run50->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_tunk46(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_46 != Convert::ToInt32(textBox_tunk46->Text))
				{
					SelectedReward->unknow_46 = Convert::ToInt32(textBox_tunk46->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk43(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_43 != Convert::ToInt32(textBox_runk43->Text))
				{
					SelectedReward->unknow_43 = Convert::ToInt32(textBox_runk43->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk42(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_42 != Convert::ToInt32(textBox_runk42->Text))
				{
					SelectedReward->unknow_42 = Convert::ToInt32(textBox_runk42->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk40(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_40 != Convert::ToInt32(textBox_runk40->Text))
				{
					SelectedReward->unknow_40 = Convert::ToInt32(textBox_runk40->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk36(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_36 != Convert::ToInt32(textBox_runk36->Text))
				{
					SelectedReward->unknow_36 = Convert::ToInt32(textBox_runk36->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk35(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_35 != Convert::ToInt32(textBox_runk35->Text))
				{
					SelectedReward->unknow_35 = Convert::ToInt32(textBox_runk35->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk33(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_33 != Convert::ToInt32(textBox_runk33->Text))
				{
					SelectedReward->unknow_33 = Convert::ToInt32(textBox_runk33->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk32(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_32 != Convert::ToInt32(textBox_runk32->Text))
				{
					SelectedReward->unknow_32 = Convert::ToInt32(textBox_runk32->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk30(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_30 != Convert::ToInt32(textBox_runk30->Text))
				{
					SelectedReward->unknow_30 = Convert::ToInt32(textBox_runk30->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk29(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_29 != Convert::ToInt32(textBox_runk29->Text))
				{
					SelectedReward->unknow_29 = Convert::ToInt32(textBox_runk29->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk28(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_28 != Convert::ToInt32(textBox_runk28->Text))
				{
					SelectedReward->unknow_28 = Convert::ToInt32(textBox_runk28->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk27(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_27 != Convert::ToInt32(textBox_runk27->Text))
				{
					SelectedReward->unknow_27 = Convert::ToInt32(textBox_runk27->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk26(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_26 != Convert::ToInt32(textBox_runk26->Text))
				{
					SelectedReward->unknow_26 = Convert::ToInt32(textBox_runk26->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run31(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_31 != Convert::ToInt32(textBox_run31->Text))
				{
					SelectedReward->unknow_31 = Convert::ToInt32(textBox_run31->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_runk25(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_25 != Convert::ToInt32(textBox_runk25->Text))
				{
					SelectedReward->unknow_25 = Convert::ToInt32(textBox_runk25->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run23(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_23 != Convert::ToInt32(textBox_run23->Text))
				{
					SelectedReward->unknow_23 = Convert::ToInt32(textBox_run23->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run22(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_22 != Convert::ToInt32(textBox_run22->Text))
				{
					SelectedReward->unknow_22 = Convert::ToInt32(textBox_run22->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run21(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_21 != Convert::ToInt32(textBox_run21->Text))
				{
					SelectedReward->unknow_21 = Convert::ToInt32(textBox_run21->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run20(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_20 != Convert::ToInt32(textBox_run20->Text))
				{
					SelectedReward->unknow_20 = Convert::ToInt32(textBox_run20->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run13(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_13 != Convert::ToInt32(textBox_run13->Text))
				{
					SelectedReward->unknow_13 = Convert::ToInt32(textBox_run13->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run121(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_12_1 != Convert::ToInt32(textBox_run121->Text))
				{
					SelectedReward->unknow_12_1 = Convert::ToInt32(textBox_run121->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run12(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_12 != Convert::ToInt32(textBox_run12->Text))
				{
					SelectedReward->unknow_12 = Convert::ToInt32(textBox_run12->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run10(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_10 != Convert::ToInt32(textBox_run10->Text))
				{
					SelectedReward->unknow_10 = Convert::ToInt32(textBox_run10->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run09(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_09 != Convert::ToInt32(textBox_run09->Text))
				{
					SelectedReward->unknow_09 = Convert::ToInt32(textBox_run09->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run08(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_08 != Convert::ToInt32(textBox_run08->Text))
				{
					SelectedReward->unknow_08 = Convert::ToInt32(textBox_run08->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run07(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_07 != Convert::ToInt32(textBox_run07->Text))
				{
					SelectedReward->unknow_07 = Convert::ToInt32(textBox_run07->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run06(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_06 != Convert::ToInt32(textBox_run06->Text))
				{
					SelectedReward->unknow_06 = Convert::ToInt32(textBox_run06->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run05(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_05 != Convert::ToInt32(textBox_run05->Text))
				{
					SelectedReward->unknow_05 = Convert::ToInt32(textBox_run05->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_run04(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_04 != Convert::ToInt32(textBox_run04->Text))
				{
					SelectedReward->unknow_04 = Convert::ToInt32(textBox_run04->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un03(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_03 != Convert::ToInt32(textBox_un03->Text))
				{
					SelectedReward->unknow_03 = Convert::ToInt32(textBox_un03->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un02(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_02 != Convert::ToInt32(textBox_un02->Text))
				{
					SelectedReward->unknow_02 = Convert::ToInt32(textBox_un02->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_un01(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unknow_01 != Convert::ToInt32(textBox_un01->Text))
				{
					SelectedReward->unknow_01 = Convert::ToInt32(textBox_un01->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rjobup(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->jobtmp_up != Convert::ToInt32(textBox_rjobup->Text))
				{
					SelectedReward->jobtmp_up = Convert::ToInt32(textBox_rjobup->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rfacunk(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->fation_lingqi != Convert::ToInt32(textBox_rfacunk->Text))
				{
					SelectedReward->fation_lingqi = Convert::ToInt32(textBox_rfacunk->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rclanunk(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->family_lingqi != Convert::ToInt32(textBox_rclanunk->Text))
				{
					SelectedReward->family_lingqi = Convert::ToInt32(textBox_rclanunk->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rclanskillid(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->family_skill_id != Convert::ToInt32(textBox_rclanskillid->Text))
				{
					SelectedReward->family_skill_id = Convert::ToInt32(textBox_rclanskillid->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rskill1(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->SkillsLV != Convert::ToInt32(textBox_rskill1->Text))
				{
					SelectedReward->SkillsLV = Convert::ToInt32(textBox_rskill1->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_proff(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ShuLianDu != Convert::ToInt32(textBox_proff->Text))
				{
					SelectedReward->ShuLianDu = Convert::ToInt32(textBox_proff->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rsetjob(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->SetJob != Convert::ToInt32(textBox_rsetjob->Text))
				{
					SelectedReward->SetJob = Convert::ToInt32(textBox_rsetjob->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_mountbag(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ZuoqiBeibao != Convert::ToInt32(textBox_mountbag->Text))
				{
					SelectedReward->ZuoqiBeibao = Convert::ToInt32(textBox_mountbag->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_petslot(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->CWlan != Convert::ToInt32(textBox_petslot->Text))
				{
					SelectedReward->CWlan = Convert::ToInt32(textBox_petslot->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewpetbag(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->CWBeibao != Convert::ToInt32(textBox_rewpetbag->Text))
				{
					SelectedReward->CWBeibao = Convert::ToInt32(textBox_rewpetbag->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewbag(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Beibao != Convert::ToInt32(textBox_rewbag->Text))
				{
					SelectedReward->Beibao = Convert::ToInt32(textBox_rewbag->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rewstash(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Cangku != Convert::ToInt32(textBox_rewstash->Text))
				{
					SelectedReward->Cangku = Convert::ToInt32(textBox_rewstash->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rsoundt(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->YuanShen != Convert::ToInt32(textBox_rsound->Text))
				{
					SelectedReward->YuanShen = Convert::ToInt32(textBox_rsound->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rew1330_9(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unkn_1330_9 != Convert::ToInt32(textBox_rew1330_9->Text))
				{
					SelectedReward->unkn_1330_9 = Convert::ToInt32(textBox_rew1330_9->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rew1330_8(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unkn_1330_8 != Convert::ToInt32(textBox_rew1330_8->Text))
				{
					SelectedReward->unkn_1330_8 = Convert::ToInt32(textBox_rew1330_8->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rew1330_7(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unkn_1330_7 != Convert::ToInt32(textBox_rew1330_7->Text))
				{
					SelectedReward->unkn_1330_7 = Convert::ToInt32(textBox_rew1330_7->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rew1330_6(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->unkn_1330_6 != Convert::ToInt32(textBox_rew1330_6->Text))
				{
					SelectedReward->unkn_1330_6 = Convert::ToInt32(textBox_rew1330_6->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_transftime(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->BianshenTime != Convert::ToInt32(textBox_transftime->Text))
				{
					SelectedReward->BianshenTime = Convert::ToInt32(textBox_transftime->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rtransf(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Bianshen != Convert::ToInt32(textBox_rtransf->Text))
				{
					SelectedReward->Bianshen = Convert::ToInt32(textBox_rtransf->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rskillcnt(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->skillcount != Convert::ToInt32(textBox_rskillcnt->Text))
				{
					SelectedReward->skillcount = Convert::ToInt32(textBox_rskillcnt->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rcampv(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Camp != Convert::ToInt32(textBox_rcampv->Text))
				{
					SelectedReward->Camp = Convert::ToInt32(textBox_rcampv->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rdbltime(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->DoubleTimes != Convert::ToInt32(textBox_rdbltime->Text))
				{
					SelectedReward->DoubleTimes = Convert::ToInt32(textBox_rdbltime->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rremtask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->removetasks != Convert::ToInt32(textBox_rremtask->Text))
				{
					SelectedReward->removetasks = Convert::ToInt32(textBox_rremtask->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_rnotch(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->Notice_Channel != Convert::ToInt32(textBox_rnotch->Text))
				{
					SelectedReward->Notice_Channel = Convert::ToInt32(textBox_rnotch->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reach_locations_map_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->reach_locations->map_id != Convert::ToInt32(textBox_reach_locations_map_id->Text))
				{
					SelectedTask->reach_locations->map_id = Convert::ToInt32(textBox_reach_locations_map_id->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reach_locations_span(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_reach_location_spans->CurrentCell->RowIndex;
				switch(dataGridView_reach_location_spans->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->reach_locations->spans[r]->north = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
					case 1:		SelectedTask->reach_locations->spans[r]->south = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
					case 2:		SelectedTask->reach_locations->spans[r]->west = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
					case 3:		SelectedTask->reach_locations->spans[r]->east = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
					case 4:		SelectedTask->reach_locations->spans[r]->bottom = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
					case 5:		SelectedTask->reach_locations->spans[r]->top = Convert::ToSingle(dataGridView_reach_location_spans->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_reach_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->reach_locations->count++;
			Array::Resize(SelectedTask->reach_locations->spans, SelectedTask->reach_locations->spans->Length+1);
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1] = gcnew Span();

			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->north = 0.0;
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->south = 0.0;
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->west = 0.0;
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->east = 0.0;
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->bottom = 0.0;
			SelectedTask->reach_locations->spans[SelectedTask->reach_locations->spans->Length-1]->top = 0.0;

			dataGridView_reach_location_spans->Rows->Add(gcnew array<String^>{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0"});
		}
	}
	private: System::Void remove_reach_locations_span(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->reach_locations->count>0 && dataGridView_reach_location_spans->CurrentCell->RowIndex>-1)
			{
				SelectedTask->reach_locations->count--;

				array<Span^>^ temp = gcnew array<Span^>(SelectedTask->reach_locations->count);
				Array::Copy(SelectedTask->reach_locations->spans, 0, temp, 0, dataGridView_reach_location_spans->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->reach_locations->spans, dataGridView_reach_location_spans->CurrentCell->RowIndex+1, temp, dataGridView_reach_location_spans->CurrentCell->RowIndex, SelectedTask->reach_locations->count - dataGridView_reach_location_spans->CurrentCell->RowIndex);
				SelectedTask->reach_locations->spans = temp;

				dataGridView_reach_location_spans->Rows->RemoveAt(dataGridView_reach_location_spans->CurrentCell->RowIndex);
			}
		}
	}
	// basic I
	private: System::Void change_unknown_17(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_17 != checkBox_unknown_17->Checked)
				{
					SelectedTask->UNKNOWN_17 = checkBox_unknown_17->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_has_instant_teleport(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->has_instant_teleport != checkBox_has_instant_teleport->Checked)
				{
					SelectedTask->has_instant_teleport = checkBox_has_instant_teleport->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_instant_teleport_location_map_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->instant_teleport_location->map_id != Convert::ToInt32(textBox_instant_teleport_location_map_id->Text))
				{
					SelectedTask->instant_teleport_location->map_id = Convert::ToInt32(textBox_instant_teleport_location_map_id->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_instant_teleport_location_x(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->instant_teleport_location->x != Convert::ToSingle(textBox_instant_teleport_location_x->Text))
				{
					SelectedTask->instant_teleport_location->x = Convert::ToSingle(textBox_instant_teleport_location_x->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_instant_teleport_location_alt(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->instant_teleport_location->altitude != Convert::ToSingle(textBox_instant_teleport_location_alt->Text))
				{
					SelectedTask->instant_teleport_location->altitude = Convert::ToSingle(textBox_instant_teleport_location_alt->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_instant_teleport_location_z(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->instant_teleport_location->z != Convert::ToSingle(textBox_instant_teleport_location_z->Text))
				{
					SelectedTask->instant_teleport_location->z = Convert::ToSingle(textBox_instant_teleport_location_z->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_waittime(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_wait_time != Convert::ToInt32(textBox_waittime->Text))
				{
					SelectedTask->required_wait_time = Convert::ToInt32(textBox_waittime->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_ai_trigger(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->ai_trigger != Convert::ToInt32(textBox_ai_trigger->Text))
				{
					SelectedTask->ai_trigger = Convert::ToInt32(textBox_ai_trigger->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_leavefail(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_leave_shibai != checkBox_leavefail->Checked)
				{
					SelectedTask->Team_leave_shibai = checkBox_leavefail->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_tamaccfail(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_quan_shibai != checkBox_tamaccfail->Checked)
				{
					SelectedTask->Team_quan_shibai = checkBox_tamaccfail->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknfront(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->UNKNOWN_114) != textBox_unknfront->Text)
				{
					SelectedTask->UNKNOWN_114 = HexString_to_ByteArray(textBox_unknfront->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_frontgender(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_xingbie != Convert::ToInt32(textBox_frontgender->Text))
				{
					SelectedTask->front_tasks_xingbie = Convert::ToInt32(textBox_frontgender->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_zhuanchonglv(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_zhuanchonglv != Convert::ToInt32(textBox_zhuanchonglv->Text))
				{
					SelectedTask->front_tasks_zhuanchonglv = Convert::ToInt32(textBox_zhuanchonglv->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_renwu(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_renwu != Convert::ToInt32(textBox_renwu->Text))
				{
					SelectedTask->front_tasks_renwu = Convert::ToInt32(textBox_renwu->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_yaoqiu(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->front_tasks_yaoqiu != Convert::ToInt32(textBox_yaoqiu->Text))
				{
					SelectedTask->front_tasks_yaoqiu = Convert::ToInt32(textBox_yaoqiu->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_title(System::Object^  sender,  System::EventArgs^  e)
		{
			if(SelectedTask && listBox_titles->SelectedIndex>-1)
			{
				if(listBox_titles->SelectedItem->ToString() != textBox_titlechange->Text)
				{
					textBox_titlechange->Text = listBox_titles->Items[listBox_titles->SelectedIndex]->ToString();
				}
			}
		}

	private: System::Void rename_title(System::Object^  sender, System::EventArgs^  e)
		{
			if(listBox_titles->SelectedIndex>-1 && textBox_titlechange->Text!="")
			{
				SelectedTask->TitleS[listBox_titles->SelectedIndex]= Convert::ToInt32(textBox_titlechange->Text);
				listBox_titles->Items[listBox_titles->SelectedIndex] = textBox_titlechange->Text;

				//recalculate
				SelectedTask->Title_count=0;
				for(int i=0;i<SelectedTask->TitleS->Length;i++){
					if(SelectedTask->TitleS[i]>0) SelectedTask->Title_count++;
				}
			}
		}
	private: System::Void change_job(System::Object^  sender,  System::EventArgs^  e)
		{
			if(SelectedTask && listBox_jobs->SelectedIndex>-1)
			{
				if(listBox_jobs->SelectedItem->ToString() != textBox1->Text)
				{
					textBox1->Text = listBox_jobs->Items[listBox_jobs->SelectedIndex]->ToString();
				}
			}
		}

	private: System::Void rename_job(System::Object^  sender, System::EventArgs^  e)
		{
			if(listBox_jobs->SelectedIndex>-1 && textBox1->Text!="")
			{
				SelectedTask->jobs[listBox_jobs->SelectedIndex]= Convert::ToInt32(textBox1->Text);
				listBox_jobs->Items[listBox_jobs->SelectedIndex] = textBox1->Text;

				//recalculate
				SelectedTask->job_nums=0;
				for(int i=0;i<SelectedTask->jobs->Length;i++){
					if(SelectedTask->jobs[i]>0) SelectedTask->job_nums++;
				}
			}
		}
	private: System::Void change_tops(System::Object^  sender,  System::EventArgs^  e)
		{
			if(SelectedTask && listBox_tops->SelectedIndex>-1)
			{
				if(listBox_tops->SelectedItem->ToString() != textBox3->Text)
				{
					textBox3->Text = listBox_tops->Items[listBox_tops->SelectedIndex]->ToString();
				}
			}
		}

	private: System::Void rename_tops(System::Object^  sender, System::EventArgs^  e)
		{
			if(listBox_tops->SelectedIndex>-1 && textBox3->Text!="")
			{
				SelectedTask->tops[listBox_tops->SelectedIndex]= Convert::ToInt16(textBox3->Text);
				listBox_tops->Items[listBox_tops->SelectedIndex] = textBox3->Text;
			}
		}
	private: System::Void change_leaderfail(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_duizhang_shibai != checkBox_leaderfail->Checked)
				{
					SelectedTask->Team_duizhang_shibai = checkBox_leaderfail->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_teamchecknum(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_check_mem != checkBox_teamchecknum->Checked)
				{
					SelectedTask->Team_check_mem = checkBox_teamchecknum->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_teamshare2(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_share_now != checkBox_teamshare2->Checked)
				{
					SelectedTask->Team_share_now = checkBox_teamshare2->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_teamunk(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_UNKNUWN_01 != checkBox_teamunk->Checked)
				{
					SelectedTask->Team_UNKNUWN_01 = checkBox_teamunk->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_teamshare(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_share_again != checkBox_teamshare->Checked)
				{
					SelectedTask->Team_share_again = checkBox_teamshare->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_teamask(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->Team_asks != checkBox_teamask->Checked)
				{
					SelectedTask->Team_asks = checkBox_teamask->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_18(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_18 != checkBox_unknown_18->Checked)
				{
					SelectedTask->UNKNOWN_18 = checkBox_unknown_18->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_19(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_19 != checkBox_unknown_19->Checked)
				{
					SelectedTask->UNKNOWN_19 = checkBox_unknown_19->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_20(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_20 != checkBox_unknown_20->Checked)
				{
					SelectedTask->UNKNOWN_20 = checkBox_unknown_20->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_21(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_21 != checkBox_unknown_21->Checked)
				{
					SelectedTask->UNKNOWN_21 = checkBox_unknown_21->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_22(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_22 != checkBox_unknown_22->Checked)
				{
					SelectedTask->UNKNOWN_22 = checkBox_unknown_22->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_plimit(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->player_limit_on != checkBox_plimit->Checked)
				{
					SelectedTask->player_limit_on = checkBox_plimit->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_23(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_23 != checkBox_unknown_23->Checked)
				{
					SelectedTask->UNKNOWN_23 = checkBox_unknown_23->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unknown_level(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_LEVEL != Convert::ToInt32(textBox_unknown_level->Text))
				{
					SelectedTask->UNKNOWN_LEVEL = Convert::ToInt32(textBox_unknown_level->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_mark_available_icon(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->mark_available_icon != checkBox_mark_available_icon->Checked)
				{
					SelectedTask->mark_available_icon = checkBox_mark_available_icon->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_mark_available_point(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->mark_available_point != checkBox_mark_available_point->Checked)
				{
					SelectedTask->mark_available_point = checkBox_mark_available_point->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_quest_npc(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->quest_npc != Convert::ToInt32(textBox_quest_npc->Text))
				{
					SelectedTask->quest_npc = Convert::ToInt32(textBox_quest_npc->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_reward_npc(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->reward_npc != Convert::ToInt32(textBox_reward_npc->Text))
				{
					SelectedTask->reward_npc = Convert::ToInt32(textBox_reward_npc->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}


	private: System::Void change_level_min(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->level_min != Convert::ToInt32(textBox_level_min->Text))
				{
					SelectedTask->level_min = Convert::ToInt32(textBox_level_min->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_level_max(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->level_max != Convert::ToInt32(textBox_level_max->Text))
				{
					SelectedTask->level_max = Convert::ToInt32(textBox_level_max->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// requirements
	private: System::Void change_unknown_27(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_27 != checkBox_unknown_27->Checked)
				{
					SelectedTask->UNKNOWN_27 = checkBox_unknown_27->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// required items
	private: System::Void change_required_items(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_required_items->CurrentCell->RowIndex;
				switch(dataGridView_required_items->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->required_items[r]->id = Convert::ToInt32(dataGridView_required_items->CurrentCell->Value);
								break;
					case 1:		SelectedTask->required_items[r]->unknown = Convert::ToBoolean(dataGridView_required_items->CurrentCell->Value);
								break;
					case 2:		SelectedTask->required_items[r]->amount = Convert::ToInt32(dataGridView_required_items->CurrentCell->Value);
								break;
					case 3:		SelectedTask->required_items[r]->probability = Convert::ToSingle(dataGridView_required_items->CurrentCell->Value);
								break;
					case 4:		SelectedTask->required_items[r]->expiration = Convert::ToInt32(dataGridView_required_items->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_dynamicsvals(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_dynamics->CurrentCell->RowIndex;
				switch(dataGridView_dynamics->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->dynamics[r]->id = Convert::ToInt32(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 1:		SelectedTask->dynamics[r]->point->map_id = Convert::ToInt32(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 2:		SelectedTask->dynamics[r]->point->x = Convert::ToSingle(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 3:		SelectedTask->dynamics[r]->point->altitude = Convert::ToSingle(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 4:		SelectedTask->dynamics[r]->point->z = Convert::ToSingle(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 5:		SelectedTask->dynamics[r]->times = Convert::ToInt32(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 6:		SelectedTask->dynamics[r]->unknown_1 = Convert::ToBoolean(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 7:		SelectedTask->dynamics[r]->unknown_2 = Convert::ToBoolean(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 8:		SelectedTask->dynamics[r]->unknown_3 = Convert::ToBoolean(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 9:		SelectedTask->dynamics[r]->unknown_4 = Convert::ToBoolean(dataGridView_dynamics->CurrentCell->Value);
								break;
					case 10:	SelectedTask->dynamics[r]->unknown_5 = Convert::ToBoolean(dataGridView_dynamics->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_required_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->required_items_count++;
			Array::Resize(SelectedTask->required_items, SelectedTask->required_items->Length+1);
			SelectedTask->required_items[SelectedTask->required_items->Length-1] = gcnew Item();

			SelectedTask->required_items[SelectedTask->required_items->Length-1]->id = 0;
			SelectedTask->required_items[SelectedTask->required_items->Length-1]->unknown = true;
			SelectedTask->required_items[SelectedTask->required_items->Length-1]->amount = 0;
			SelectedTask->required_items[SelectedTask->required_items->Length-1]->probability = 1.0;
			SelectedTask->required_items[SelectedTask->required_items->Length-1]->expiration = 0;

			dataGridView_required_items->Rows->Add(gcnew array<String^>{"0", "True", "0", "1.0", "0"});
		}
	}
	private: System::Void remove_required_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->required_items_count>0 && dataGridView_required_items->CurrentCell->RowIndex>-1)
			{
				SelectedTask->required_items_count--;

				array<Item^>^ temp = gcnew array<Item^>(SelectedTask->required_items_count);
				Array::Copy(SelectedTask->required_items, 0, temp, 0, dataGridView_required_items->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->required_items, dataGridView_required_items->CurrentCell->RowIndex+1, temp, dataGridView_required_items->CurrentCell->RowIndex, SelectedTask->required_items_count - dataGridView_required_items->CurrentCell->RowIndex);
				SelectedTask->required_items = temp;

				dataGridView_required_items->Rows->RemoveAt(dataGridView_required_items->CurrentCell->RowIndex);
			}
		}
	}
	private: System::Void change_required_items_unknown(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_items_unknown != Convert::ToInt32(textBox_required_items_unknown->Text))
				{
					SelectedTask->required_items_unknown = Convert::ToInt32(textBox_required_items_unknown->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	private: System::Void change_allkillnum(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->allkillnum != Convert::ToInt32(textBox_allkillnum->Text))
				{
					SelectedTask->allkillnum = Convert::ToInt32(textBox_allkillnum->Text);
					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	
	// given items
	private: System::Void change_given_items(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_given_items->CurrentCell->RowIndex;
				switch(dataGridView_given_items->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->given_items[r]->id = Convert::ToInt32(dataGridView_given_items->CurrentCell->Value);
								break;
					case 1:		SelectedTask->given_items[r]->unknown = Convert::ToBoolean(dataGridView_given_items->CurrentCell->Value);
								break;
					case 2:		SelectedTask->given_items[r]->amount = Convert::ToInt32(dataGridView_given_items->CurrentCell->Value);
								break;
					case 3:		SelectedTask->given_items[r]->probability = Convert::ToSingle(dataGridView_given_items->CurrentCell->Value);
								break;
					case 4:		SelectedTask->given_items[r]->expiration = Convert::ToInt32(dataGridView_given_items->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_given_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->given_items_count++;
			Array::Resize(SelectedTask->given_items, SelectedTask->given_items->Length+1);
			SelectedTask->given_items[SelectedTask->given_items->Length-1] = gcnew Item();

			SelectedTask->given_items[SelectedTask->given_items->Length-1]->id = 0;
			SelectedTask->given_items[SelectedTask->given_items->Length-1]->unknown = true;
			SelectedTask->given_items[SelectedTask->given_items->Length-1]->amount = 0;
			SelectedTask->given_items[SelectedTask->given_items->Length-1]->probability = 1.0;
			SelectedTask->given_items[SelectedTask->given_items->Length-1]->expiration = 0;

			dataGridView_given_items->Rows->Add(gcnew array<String^>{"0", "True", "0", "1.0", "0"});
		}
	}
	private: System::Void remove_given_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->given_items_count>0 && dataGridView_given_items->CurrentCell->RowIndex>-1)
			{
				SelectedTask->given_items_count--;

				array<Item^>^ temp = gcnew array<Item^>(SelectedTask->given_items_count);
				Array::Copy(SelectedTask->given_items, 0, temp, 0, dataGridView_given_items->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->given_items, dataGridView_given_items->CurrentCell->RowIndex+1, temp, dataGridView_given_items->CurrentCell->RowIndex, SelectedTask->given_items_count - dataGridView_given_items->CurrentCell->RowIndex);
				SelectedTask->given_items = temp;

				dataGridView_given_items->Rows->RemoveAt(dataGridView_given_items->CurrentCell->RowIndex);
			}
		}
	}
	//

	private: System::Void change_xunhan_type(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->xunhanType != Convert::ToInt32(textBox_xunhan->Text))
				{
					SelectedTask->xunhanType = Convert::ToInt32(textBox_xunhan->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_instant_pay_coins(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->instant_pay_coins != Convert::ToInt32(textBox_instant_pay_coins->Text))
				{
					SelectedTask->instant_pay_coins = Convert::ToInt32(textBox_instant_pay_coins->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	/*
	private: System::Void change_unknown_31(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_31 != checkBox_unknown_31->Checked)
				{
					SelectedTask->UNKNOWN_31 = checkBox_unknown_31->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	*/
	// quests done
	private: System::Void change_required_quests_done(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int i = 0;
				SelectedTask->required_quests_done = gcnew array<int>(5);

				if(textBox_required_quests_done_1->Text != "0")	{ SelectedTask->required_quests_done[i] = Convert::ToInt32(textBox_required_quests_done_1->Text); i++; }
				if(textBox_required_quests_done_2->Text != "0")	{ SelectedTask->required_quests_done[i] = Convert::ToInt32(textBox_required_quests_done_2->Text); i++; }
				if(textBox_required_quests_done_3->Text != "0")	{ SelectedTask->required_quests_done[i] = Convert::ToInt32(textBox_required_quests_done_3->Text); i++; }
				if(textBox_required_quests_done_4->Text != "0")	{ SelectedTask->required_quests_done[i] = Convert::ToInt32(textBox_required_quests_done_4->Text); i++; }
				if(textBox_required_quests_done_5->Text != "0")	{ SelectedTask->required_quests_done[i] = Convert::ToInt32(textBox_required_quests_done_5->Text); i++; }

				SelectedTask->required_quests_done_count = i;

				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	//
	
	private: System::Void change_required_gender(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_gender != comboBox_required_gender->SelectedIndex)
				{
					SelectedTask->required_gender = comboBox_required_gender->SelectedIndex;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_required_be_married(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_be_married != checkBox_required_be_married->Checked)
				{
					SelectedTask->required_be_married = checkBox_required_be_married->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_required_be_gm(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->required_be_gm != checkBox_required_be_gm->Checked)
				{
					SelectedTask->required_be_gm = checkBox_required_be_gm->Checked;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	// occupations
			 /*
	private: System::Void change_required_occupations(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int i = 0;
				SelectedTask->required_occupations = gcnew array<int>(10);

				if(checkBox_occupation_blademaster->Checked)	{ SelectedTask->required_occupations[i] = 0; i++; }
				if(checkBox_occupation_wizard->Checked)			{ SelectedTask->required_occupations[i] = 1; i++; }
				if(checkBox_occupation_psychic->Checked)		{ SelectedTask->required_occupations[i] = 2; i++; }
				if(checkBox_occupation_venomancer->Checked)		{ SelectedTask->required_occupations[i] = 3; i++; }
				if(checkBox_occupation_barbarian->Checked)		{ SelectedTask->required_occupations[i] = 4; i++; }
				if(checkBox_occupation_assassin->Checked)		{ SelectedTask->required_occupations[i] = 5; i++; }
				if(checkBox_occupation_archer->Checked)			{ SelectedTask->required_occupations[i] = 6; i++; }
				if(checkBox_occupation_cleric->Checked)			{ SelectedTask->required_occupations[i] = 7; i++; }
				if(checkBox_occupation_seeker->Checked)			{ SelectedTask->required_occupations[i] = 8; i++; }
				if(checkBox_occupation_mystic->Checked)			{ SelectedTask->required_occupations[i] = 9; i++; }

				SelectedTask->required_occupations_count = i;

				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	*/
	//
	private: System::Void change_unknown_44(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(ByteArray_to_HexString(SelectedTask->shengwang_unknown_48) != textBox_unknown_44->Text)
				{
					SelectedTask->shengwang_unknown_48 = HexString_to_ByteArray(textBox_unknown_44->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	// quests undone
	private: System::Void change_required_quests_undone(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int i = 0;
				SelectedTask->required_quests_undone = gcnew array<int>(5);

				if(textBox_required_quests_undone_1->Text != "0")	{ SelectedTask->required_quests_undone[i] = Convert::ToInt32(textBox_required_quests_undone_1->Text); i++; }
				if(textBox_required_quests_undone_2->Text != "0")	{ SelectedTask->required_quests_undone[i] = Convert::ToInt32(textBox_required_quests_undone_2->Text); i++; }
				if(textBox_required_quests_undone_3->Text != "0")	{ SelectedTask->required_quests_undone[i] = Convert::ToInt32(textBox_required_quests_undone_3->Text); i++; }
				if(textBox_required_quests_undone_4->Text != "0")	{ SelectedTask->required_quests_undone[i] = Convert::ToInt32(textBox_required_quests_undone_4->Text); i++; }
				if(textBox_required_quests_undone_5->Text != "0")	{ SelectedTask->required_quests_undone[i] = Convert::ToInt32(textBox_required_quests_undone_5->Text); i++; }

				SelectedTask->required_quests_undone_count = i;

				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_playerpenalty(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->playerpenalty != Convert::ToInt32(textBox_playerpenalty->Text))
				{
					SelectedTask->playerpenalty = Convert::ToInt32(textBox_playerpenalty->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_unkpenalty(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->unkpenalty != Convert::ToInt32(textBox_unkpenalty->Text))
				{
					SelectedTask->unkpenalty = Convert::ToInt32(textBox_unkpenalty->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_allzonenum(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->UNKNOWN_LEVEL != Convert::ToInt32(textBox_allzonenum->Text))
				{
					SelectedTask->UNKNOWN_LEVEL = Convert::ToInt32(textBox_allzonenum->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	// team members
	private: System::Void change_required_team_member_groups(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_team_members->CurrentCell->RowIndex;
				switch(dataGridView_team_members->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->required_team_member_groups[r]->level_min = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 1:		SelectedTask->required_team_member_groups[r]->level_max = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 2:		SelectedTask->required_team_member_groups[r]->job = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 3:		SelectedTask->required_team_member_groups[r]->quest = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 4:		SelectedTask->required_team_member_groups[r]->Unknown_01 = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 5:		SelectedTask->required_team_member_groups[r]->amount_min = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 6:		SelectedTask->required_team_member_groups[r]->amount_max = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					case 7:		SelectedTask->required_team_member_groups[r]->quest = Convert::ToInt32(dataGridView_team_members->CurrentCell->Value);
								break;
					// bool Unknown_02;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_required_team_member_group(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(contextMenuStrip_team_members->SourceControl == dataGridView_team_members)
			{
				SelectedTask->required_team_member_groups_count++;
				Array::Resize(SelectedTask->required_team_member_groups, SelectedTask->required_team_member_groups->Length+1);
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1] = gcnew TeamMembers();
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->level_min = 1;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->level_max = 150;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->gender = 0;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->job = 0;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->race = 0;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->amount_min = 1;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->amount_max = 5;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->quest = 0;
				SelectedTask->required_team_member_groups[SelectedTask->required_team_member_groups->Length-1]->Unknown_01 = 0;
				//bool Unknown_02;

				dataGridView_team_members->Rows->Add(gcnew array<String^>{"1", "150", "0", "0", "0", "1", "5", "0","0"});
			}
			if(contextMenuStrip_team_members->SourceControl == dataGridView_dynamics)
			{
				SelectedTask->dynamics_count++;
				Array::Resize(SelectedTask->dynamics, SelectedTask->dynamics->Length+1);
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1] = gcnew DynamicsTask();
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->id = 0;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->unknown_1 = false;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->unknown_2 = false;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->unknown_3 = false;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->unknown_4 = false;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->unknown_5 = false;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->times = 0;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->point = gcnew Map_Location();
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->point->map_id = 0;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->point->x = 0;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->point->altitude = 0;
				SelectedTask->dynamics[SelectedTask->dynamics->Length-1]->point->z = 0;
				

				dataGridView_dynamics->Rows->Add(gcnew array<String^>{"0", "0", "0", "0", "0", "0", "False","False","False","False","False"});
			}
		}
	}
	private: System::Void remove_required_team_member_group(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(contextMenuStrip_team_members->SourceControl == dataGridView_team_members)
			{
				if(SelectedTask->required_team_member_groups_count>0 && dataGridView_team_members->CurrentCell->RowIndex>-1)
				{
					SelectedTask->required_team_member_groups_count--;

					array<TeamMembers^>^ temp = gcnew array<TeamMembers^>(SelectedTask->required_team_member_groups_count);
					Array::Copy(SelectedTask->required_team_member_groups, 0, temp, 0, dataGridView_team_members->CurrentCell->RowIndex);
					Array::Copy(SelectedTask->required_team_member_groups, dataGridView_team_members->CurrentCell->RowIndex+1, temp, dataGridView_team_members->CurrentCell->RowIndex, SelectedTask->required_team_member_groups_count - dataGridView_team_members->CurrentCell->RowIndex);
					SelectedTask->required_team_member_groups = temp;

					dataGridView_team_members->Rows->RemoveAt(dataGridView_team_members->CurrentCell->RowIndex);
				}
			}
			if(contextMenuStrip_team_members->SourceControl == dataGridView_dynamics)
			{
				if(SelectedTask->dynamics_count>0 && dataGridView_dynamics->CurrentCell->RowIndex>-1)
				{
					SelectedTask->dynamics_count--;

					array<DynamicsTask^>^ temp = gcnew array<DynamicsTask^>(SelectedTask->dynamics_count);
					Array::Copy(SelectedTask->dynamics, 0, temp, 0, dataGridView_dynamics->CurrentCell->RowIndex);
					Array::Copy(SelectedTask->dynamics, dataGridView_dynamics->CurrentCell->RowIndex+1, temp, dataGridView_dynamics->CurrentCell->RowIndex, SelectedTask->dynamics_count - dataGridView_dynamics->CurrentCell->RowIndex);
					SelectedTask->dynamics = temp;

					dataGridView_dynamics->Rows->RemoveAt(dataGridView_dynamics->CurrentCell->RowIndex);
				}
			}
		}
	}


	// chases
	private: System::Void change_required_chases(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_required_chases->CurrentCell->RowIndex;
				switch(dataGridView_required_chases->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->required_chases[r]->id_monster = Convert::ToInt32(dataGridView_required_chases->CurrentCell->Value);
								break;
					case 1:		SelectedTask->required_chases[r]->amount_monster = Convert::ToInt32(dataGridView_required_chases->CurrentCell->Value);
								break;
					case 2:		SelectedTask->required_chases[r]->id_drop = Convert::ToInt32(dataGridView_required_chases->CurrentCell->Value);
								break;
					case 3:		SelectedTask->required_chases[r]->amount_drop = Convert::ToInt32(dataGridView_required_chases->CurrentCell->Value);
								break;
					case 4:		SelectedTask->required_chases[r]->unknown_1 = Convert::ToByte(dataGridView_required_items->CurrentCell->Value);
								break;
					case 5:		SelectedTask->required_chases[r]->probability = Convert::ToSingle(dataGridView_required_items->CurrentCell->Value);
								break;
					case 6:		SelectedTask->required_chases[r]->unknown_2 = Convert::ToByte(dataGridView_required_items->CurrentCell->Value);
								break;
					case 7:		SelectedTask->required_chases[r]->unknown_3 = HexString_to_ByteArray(Convert::ToString(dataGridView_required_items->CurrentCell->Value));
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_required_chase(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->required_chases_count++;
			Array::Resize(SelectedTask->required_chases, SelectedTask->required_chases->Length+1);
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1] = gcnew Chase();

			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->id_monster = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->amount_monster = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->id_drop = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->amount_drop = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->unknown_1 = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->probability = 1.0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->unknown_2 = 0;
			SelectedTask->required_chases[SelectedTask->required_chases->Length-1]->unknown_3 = gcnew array<unsigned char>(8);

			dataGridView_required_chases->Rows->Add(gcnew array<String^>{"0", "0", "0", "0", "0", "1.0", "0", "00-00-00-00-00-00-00-00"});
		}
	}
	private: System::Void remove_required_chases(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->required_chases_count>0 && dataGridView_required_chases->CurrentCell->RowIndex>-1)
			{
				SelectedTask->required_chases_count--;

				array<Chase^>^ temp = gcnew array<Chase^>(SelectedTask->required_chases_count);
				Array::Copy(SelectedTask->required_chases, 0, temp, 0, dataGridView_required_chases->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->required_chases, dataGridView_required_chases->CurrentCell->RowIndex+1, temp, dataGridView_required_chases->CurrentCell->RowIndex, SelectedTask->required_chases_count - dataGridView_required_chases->CurrentCell->RowIndex);
				SelectedTask->required_chases = temp;

				dataGridView_required_chases->Rows->RemoveAt(dataGridView_required_chases->CurrentCell->RowIndex);
			}
		}
	}
	// get items
	private: System::Void change_required_get_items(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int r = dataGridView_required_get_items->CurrentCell->RowIndex;
				switch(dataGridView_required_get_items->CurrentCell->ColumnIndex)
				{
					case 0:		SelectedTask->required_get_items[r]->id = Convert::ToInt32(dataGridView_required_get_items->CurrentCell->Value);
								break;
					case 1:		SelectedTask->required_get_items[r]->unknown = Convert::ToBoolean(dataGridView_required_get_items->CurrentCell->Value);
								break;
					case 2:		SelectedTask->required_get_items[r]->amount = Convert::ToInt32(dataGridView_required_get_items->CurrentCell->Value);
								break;
					case 3:		SelectedTask->required_get_items[r]->probability = Convert::ToSingle(dataGridView_required_get_items->CurrentCell->Value);
								break;
					case 4:		SelectedTask->required_get_items[r]->expiration = Convert::ToInt32(dataGridView_required_get_items->CurrentCell->Value);
								break;
				}
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_required_get_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			SelectedTask->required_get_items_count++;
			Array::Resize(SelectedTask->required_get_items, SelectedTask->required_get_items->Length+1);
			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1] = gcnew Item();

			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1]->id = 0;
			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1]->unknown = true;
			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1]->amount = 0;
			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1]->probability = 1.0;
			SelectedTask->required_get_items[SelectedTask->required_get_items->Length-1]->expiration = 0;

			dataGridView_required_get_items->Rows->Add(gcnew array<String^>{"0", "True", "0", "1.0", "0"});
		}
	}
	private: System::Void remove_required_get_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			if(SelectedTask->required_get_items_count>0 && dataGridView_required_get_items->CurrentCell->RowIndex>-1)
			{
				SelectedTask->required_get_items_count--;

				array<Item^>^ temp = gcnew array<Item^>(SelectedTask->required_get_items_count);
				Array::Copy(SelectedTask->required_get_items, 0, temp, 0, dataGridView_required_get_items->CurrentCell->RowIndex);
				Array::Copy(SelectedTask->required_get_items, dataGridView_required_get_items->CurrentCell->RowIndex+1, temp, dataGridView_required_get_items->CurrentCell->RowIndex, SelectedTask->required_get_items_count - dataGridView_required_get_items->CurrentCell->RowIndex);
				SelectedTask->required_get_items = temp;

				dataGridView_required_get_items->Rows->RemoveAt(dataGridView_required_get_items->CurrentCell->RowIndex);
			}
		}
	}
	
	private: System::Void change_parent_quest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->parent_quest != Convert::ToInt32(textBox_parent_quest->Text))
				{
					SelectedTask->parent_quest = Convert::ToInt32(textBox_parent_quest->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_previous_quest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->previous_quest != Convert::ToInt32(textBox_previous_quest->Text))
				{
					SelectedTask->previous_quest = Convert::ToInt32(textBox_previous_quest->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_next_quest(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->next_quest != Convert::ToInt32(textBox_next_quest->Text))
				{
					SelectedTask->next_quest = Convert::ToInt32(textBox_next_quest->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_sub_quest_first(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->sub_quest_first != Convert::ToInt32(textBox_sub_quest_first->Text))
				{
					SelectedTask->sub_quest_first = Convert::ToInt32(textBox_sub_quest_first->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	// reward
	private: System::Void change_reward_time_factor(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int i = listBox_reward_timed->SelectedIndex;
				if(radioButton_timed->Checked && i > -1)
				{
					// check if value really changed
					if(SelectedTask->rewards_timed_factors[i] != Convert::ToSingle(numericUpDown_time_factor->Value))
					{
						SelectedTask->rewards_timed_factors[i] = Convert::ToSingle(numericUpDown_time_factor->Value);
						listBox_reward_timed->Items[i] = "Success Time [sec]: " + (SelectedTask->time_limit*SelectedTask->rewards_timed_factors[i]);

						// call debug function
						TriggerDebug((Control^)sender);
					}
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_reward_timed(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			/*
			SelectedReward->pq->chase_count++;
			Array::Resize(SelectedReward->pq->chases, SelectedReward->pq->chases->Length+1);
			SelectedReward->pq->chases[SelectedReward->pq->chases->Length-1] = gcnew PQ_Chase();

			SelectedReward->pq->chases[SelectedReward->pq->chases->Length-1]->id_monster = 0;
			SelectedReward->pq->chases[SelectedReward->pq->chases->Length-1]->amount_monster = 0;
			SelectedReward->pq->chases[SelectedReward->pq->chases->Length-1]->probability = 1.0;
			SelectedReward->pq->chases[SelectedReward->pq->chases->Length-1]->amount_unknown = 0;

			dataGridView_reward_pq_chases->Rows->Add(gcnew array<String^>{"0", "0", "1.0", "0"});
			*/
			MessageBox::Show("Currently not Supported!");
		}
	}
	private: System::Void remove_reward_timed(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			/*
			if(SelectedReward->pq->chase_count>0 && dataGridView_reward_pq_chases->CurrentCell->RowIndex>-1)
			{
				SelectedReward->pq->chase_count--;

				array<PQ_Chase^>^ temp = gcnew array<PQ_Chase^>(SelectedReward->pq->chase_count);
				Array::Copy(SelectedReward->pq->chases, 0, temp, 0, dataGridView_reward_pq_chases->CurrentCell->RowIndex);
				Array::Copy(SelectedReward->pq->chases, dataGridView_reward_pq_chases->CurrentCell->RowIndex+1, temp, dataGridView_reward_pq_chases->CurrentCell->RowIndex, SelectedReward->pq->chase_count - dataGridView_reward_pq_chases->CurrentCell->RowIndex);
				SelectedReward->pq->chases = temp;

				dataGridView_reward_pq_chases->Rows->RemoveAt(dataGridView_reward_pq_chases->CurrentCell->RowIndex);
			}
			*/
			MessageBox::Show("Currently not Supported!");
		}
	}
	private: System::Void change_reward_coins(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->coins != Convert::ToInt32(textBox_reward_coins->Text))
				{
					SelectedReward->coins = Convert::ToInt32(textBox_reward_coins->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_experience(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->experience != Convert::ToInt64(textBox_reward_experience->Text))
				{
					SelectedReward->experience = Convert::ToInt64(textBox_reward_experience->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}

	private: System::Void change_reward_teleport_map_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->teleport->map_id != Convert::ToInt32(textBox_reward_teleport_map_id->Text))
				{
					SelectedReward->teleport->map_id = Convert::ToInt32(textBox_reward_teleport_map_id->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_teleport_x(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->teleport->x != Convert::ToSingle(textBox_reward_teleport_x->Text))
				{
					SelectedReward->teleport->x = Convert::ToSingle(textBox_reward_teleport_x->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_teleport_altitude(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->teleport->altitude != Convert::ToSingle(textBox_reward_teleport_altitude->Text))
				{
					SelectedReward->teleport->altitude = Convert::ToSingle(textBox_reward_teleport_altitude->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_teleport_z(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->teleport->z != Convert::ToSingle(textBox_reward_teleport_z->Text))
				{
					SelectedReward->teleport->z = Convert::ToSingle(textBox_reward_teleport_z->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_reward_ai_trigger(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->ai_trigger != Convert::ToInt32(textBox_reward_ai_trigger->Text))
				{
					SelectedReward->ai_trigger = Convert::ToInt32(textBox_reward_ai_trigger->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	// reward items
	private: System::Void change_reward_item_groups_count(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->item_groups_count != Convert::ToInt32(numericUpDown_reward_item_groups_count->Value))
				{
					SelectedReward->item_groups_count = Convert::ToInt32(numericUpDown_reward_item_groups_count->Value);

					// Group Added
					if(SelectedReward->item_groups->Length < SelectedReward->item_groups_count)
					{
						Array::Resize(SelectedReward->item_groups, SelectedReward->item_groups_count);
						// fill itemgroups
						for(int i=Column_reward_item_groups->Items->Count; i<SelectedReward->item_groups_count; i++)
						{
							// create empty itemgroup
							SelectedReward->item_groups[i] = gcnew ItemGroup();
							SelectedReward->item_groups[i]->type=false;
							SelectedReward->item_groups[i]->items_count=0;
							SelectedReward->item_groups[i]->items = gcnew array<RewardItem^>(0);
							// add itemgroup to datagridview's combobox-column
							Column_reward_item_groups->Items->Add(i.ToString());
							// add itemgroup to checked lidtbox
							checkedListBox_reward_item_groups_flag->Items->Add("Group[" + i.ToString() + "]", false);
						}

						MessageBox::Show("HINT: Added Item Group(s) contains no Items!");
					}

					// Group Removed
					if(SelectedReward->item_groups->Length > SelectedReward->item_groups_count)
					{
						// remove disposed itemgroups
						array<ItemGroup^>^ temp = gcnew array<ItemGroup^>(SelectedReward->item_groups_count);
						Array::Copy(SelectedReward->item_groups, temp, temp->Length);
						SelectedReward->item_groups = temp;

						// remove rows from datagrid
						for(int i=0; i<dataGridView_reward_item_group_items->RowCount; i++)
						{
							if(Convert::ToInt32(dataGridView_reward_item_group_items->Rows[i]->Cells[0]->Value) >= SelectedReward->item_groups_count)
							{
								dataGridView_reward_item_group_items->Rows->RemoveAt(i);
								i--;
							}
						}

						// remove group from combobox-column
						for(int i=SelectedReward->item_groups_count; i<Column_reward_item_groups->Items->Count; i++)
						{
							checkedListBox_reward_item_groups_flag->Items->RemoveAt(i);
							Column_reward_item_groups->Items->RemoveAt(Column_reward_item_groups->Items->Count-1);
						}
					}

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	private: System::Void change_reward_item_group_flag(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// check if value really changed
				if(SelectedReward->item_groups[e->Index]->type != (e->NewValue == CheckState::Checked))
				{
					SelectedReward->item_groups[e->Index]->type = (e->NewValue == CheckState::Checked);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	
	private: System::Void change_reward_items(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
	{
		if(SelectedReward)
		{
			try
			{
				// Rebuild all reward groups, instead of managing moving items between groups...
				SelectedReward->item_groups_count = Convert::ToInt32(numericUpDown_reward_item_groups_count->Value);
				SelectedReward->item_groups = gcnew array<ItemGroup^>(SelectedReward->item_groups_count);

				// create empty itemgroups
				for(int g=0; g<SelectedReward->item_groups->Length; g++)
				{
					SelectedReward->item_groups[g] = gcnew ItemGroup();
					/*
					if(checkedListBox_reward_item_groups_flag->CheckedItems->Contains(checkedListBox_reward_item_groups_flag->Items[g]))
					{
						SelectedReward->item_groups[g]->type = true;
					}
					else
					{
						SelectedReward->item_groups[g]->type = false;
					}
					*/
					SelectedReward->item_groups[g]->type = 0;
					SelectedReward->item_groups[g]->items_count = 0;
					SelectedReward->item_groups[g]->items = gcnew array<RewardItem^>(0);
				}
				// scan all datagridview lines and add items to the corresponding itemgroups
				for(int r=0; r<dataGridView_reward_item_group_items->RowCount; r++)
				{
					int g = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[0]->Value);

					SelectedReward->item_groups[g]->items_count++;
					Array::Resize(SelectedReward->item_groups[g]->items, SelectedReward->item_groups[g]->items->Length+1);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1] = gcnew RewardItem();
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->id = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[1]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unknown = Convert::ToBoolean(dataGridView_reward_item_group_items->Rows[r]->Cells[2]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->amount = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[3]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->probability = Convert::ToSingle(dataGridView_reward_item_group_items->Rows[r]->Cells[4]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->flg = Convert::ToBoolean(dataGridView_reward_item_group_items->Rows[r]->Cells[5]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->expiration = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[6]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_01 = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[8]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_02 = Convert::ToByte(dataGridView_reward_item_group_items->Rows[r]->Cells[9]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->RefLevel = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[7]->Value);
					SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_03 = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[10]->Value);
				}
				//MessageBox::Show("test3!");
				// call debug function
				TriggerDebug((Control^)sender);
			}
			catch(...)
			{
				MessageBox::Show("FORMAT2 ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_reward_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			// add item to the last item group
			int g = Convert::ToInt32(numericUpDown_reward_item_groups_count->Value-1);
			if(g > -1)
			{
				// add item directly to itemgroup instead of rebuilding all groups: change_reward_items(nullptr, nullptr)
				SelectedReward->item_groups[g]->items_count++;
				Array::Resize(SelectedReward->item_groups[g]->items, SelectedReward->item_groups[g]->items->Length+1);
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1] = gcnew RewardItem();

				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->id = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unknown = true;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->amount = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->probability = 1.0000;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->flg = true;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->expiration = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_01 = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_02 = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->RefLevel = 0;
				SelectedReward->item_groups[g]->items[SelectedReward->item_groups[g]->items->Length-1]->unkown_03 = 0;
				dataGridView_reward_item_group_items->Rows->Add(gcnew array<String^>{g.ToString(), "0", "True", "0", "1.0000","True", "0","0","0","0","0"});
			}
			else
			{
				MessageBox::Show("No Item Group found!\nPlease increase the Number of Item Groups!");
			}
		}
	}
	private: System::Void remove_reward_item(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedReward)
		{
			int r = dataGridView_reward_item_group_items->CurrentCell->RowIndex;
			int g = Convert::ToInt32(dataGridView_reward_item_group_items->Rows[r]->Cells[0]->Value);

			if(SelectedReward->item_groups[g]->items_count>0 && dataGridView_reward_item_group_items->CurrentCell->RowIndex>-1)
			{
				dataGridView_reward_item_group_items->Rows->RemoveAt(r);

				// datagridview row index != item index, which make it hard to find the removed item in the corresponding item group
				// rebuilding item groups instead of removing
				change_reward_items(nullptr, nullptr);

				// check for blank item group
				if(SelectedReward->item_groups[g]->items_count == 0)
				{
					MessageBox::Show("WARNING: Item Groups[" + g.ToString() + "] contains no Items!");
				}
			}
		}
	}
	// conversation
	private: System::Void change_conversation_new(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->conversation->NewText != textBox_newtxt->Text)
				{
					SelectedTask->conversation->NewText = textBox_newtxt->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_xanfu(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->conversation->XuanfuText != textBox_xanfumess->Text)
				{
					SelectedTask->conversation->XuanfuText = textBox_xanfumess->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_agern(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->conversation->AgernText != textBox_agernmess->Text)
				{
					SelectedTask->conversation->AgernText = textBox_agernmess->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private:
	private: System::Void change_conversation_prompt_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->conversation->PromptText != textBox_conversation_prompt_text->Text)
				{
					SelectedTask->conversation->PromptText = textBox_conversation_prompt_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_general_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				// check if value really changed
				if(SelectedTask->conversation->GeneralText != textBox_conversation_general_text->Text)
				{
					SelectedTask->conversation->GeneralText = textBox_conversation_general_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_dialog_unknown(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;

				// check if value really changed
				if(d > -1 && SelectedTask->conversation->dialogs[d]->unknown != Convert::ToInt32(textBox_conversation_dialog_unknown->Text))
				{
					SelectedTask->conversation->dialogs[d]->unknown = Convert::ToInt32(textBox_conversation_dialog_unknown->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_dialog_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;

				// check if value really changed
				if(d > -1 && SelectedTask->conversation->dialogs[d]->DialogText != textBox_conversation_dialog_text->Text)
				{
					SelectedTask->conversation->dialogs[d]->DialogText = textBox_conversation_dialog_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_question_id(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->question_id != Convert::ToInt32(textBox_conversation_question_id->Text))
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->question_id = Convert::ToInt32(textBox_conversation_question_id->Text);
					listBox_conversation_questions->Items[q] = "[" + textBox_conversation_question_id->Text + "] Question";

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_question_previous(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->previous_question != Convert::ToInt32(textBox_conversation_question_previous->Text))
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->previous_question = Convert::ToInt32(textBox_conversation_question_previous->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_question_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->QuestionText != textBox_conversation_question_text->Text)
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->QuestionText = textBox_conversation_question_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_question(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			int d = listBox_conversation_dialogs->SelectedIndex;

			if(d > -1)
			{
				SelectedTask->conversation->dialogs[d]->question_count++;
				Array::Resize(SelectedTask->conversation->dialogs[d]->questions, SelectedTask->conversation->dialogs[d]->questions->Length+1);

				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1] = gcnew Question();
				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->crypt_key = SelectedTask->conversation->crypt_key;
				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->question_id = SelectedTask->conversation->dialogs[d]->questions->Length;
				if(SelectedTask->conversation->dialogs[d]->questions->Length > 1)
				{
					SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->previous_question = SelectedTask->conversation->dialogs[d]->questions->Length-1;
				}
				else
				{
					SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->previous_question = -1;
				}
				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->answer_count = 0;
				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->answers = gcnew array<Answer^>(0);
				SelectedTask->conversation->dialogs[d]->questions[SelectedTask->conversation->dialogs[d]->questions->Length-1]->QuestionText = "BLANK";

				listBox_conversation_questions->Items->Add("[" + SelectedTask->conversation->dialogs[d]->questions->Length + "] Question");
			}
		}
	}
	private: System::Void remove_question(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			int d = listBox_conversation_dialogs->SelectedIndex;
			int q = listBox_conversation_questions->SelectedIndex;

			if(d > -1 && q > -1 && SelectedTask->conversation->dialogs[d]->question_count>0)
			{
				SelectedTask->conversation->dialogs[d]->question_count--;

				array<Question^>^ temp = gcnew array<Question^>(SelectedTask->conversation->dialogs[d]->question_count);
				Array::Copy(SelectedTask->conversation->dialogs[d]->questions, 0, temp, 0, listBox_conversation_questions->SelectedIndex);
				Array::Copy(SelectedTask->conversation->dialogs[d]->questions, listBox_conversation_questions->SelectedIndex+1, temp, listBox_conversation_questions->SelectedIndex, SelectedTask->conversation->dialogs[d]->question_count - listBox_conversation_questions->SelectedIndex);
				SelectedTask->conversation->dialogs[d]->questions = temp;

				// try to automatic updating all id's and previous_links...
				for(int i=0; i<SelectedTask->conversation->dialogs[d]->questions->Length; i++)
				{
					SelectedTask->conversation->dialogs[d]->questions[i]->question_id = i+1;
					if(i > 0)
					{
						SelectedTask->conversation->dialogs[d]->questions[i]->previous_question = i;
					}
					else
					{
						SelectedTask->conversation->dialogs[d]->questions[i]->previous_question = -1;
					}
				}

				// refresh question listbox
				select_dialog(nullptr, nullptr);
			}
		}
	}
	private: System::Void change_conversation_answer_question_link(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;
				int a = listBox_conversation_answers->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && a > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->question_link != Convert::ToInt32(textBox_conversation_answer_question_link->Text))
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->question_link = Convert::ToInt32(textBox_conversation_answer_question_link->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_answer_task_link(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;
				int a = listBox_conversation_answers->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && a > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->task_link != Convert::ToInt32(textBox_conversation_answer_task_link->Text))
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->task_link = Convert::ToInt32(textBox_conversation_answer_task_link->Text);

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void change_conversation_answer_text(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			try
			{
				int d = listBox_conversation_dialogs->SelectedIndex;
				int q = listBox_conversation_questions->SelectedIndex;
				int a = listBox_conversation_answers->SelectedIndex;

				// check if value really changed
				if(d > -1 && q > -1 && a > -1 && SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText != textBox_conversation_answer_text->Text)
				{
					SelectedTask->conversation->dialogs[d]->questions[q]->answers[a]->AnswerText = textBox_conversation_answer_text->Text;

					// call debug function
					TriggerDebug((Control^)sender);
				}
			}
			catch(...)
			{
				MessageBox::Show("FORMAT ERROR\nInput value must be in the correct format!");
			}
		}
	}
	private: System::Void add_answer(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			int d = listBox_conversation_dialogs->SelectedIndex;
			int q = listBox_conversation_questions->SelectedIndex;

			if(d > -1 && q > -1)
			{
				SelectedTask->conversation->dialogs[d]->questions[q]->answer_count++;
				Array::Resize(SelectedTask->conversation->dialogs[d]->questions[q]->answers, SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length+1);

				SelectedTask->conversation->dialogs[d]->questions[q]->answers[SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length-1] = gcnew Answer();
				SelectedTask->conversation->dialogs[d]->questions[q]->answers[SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length-1]->crypt_key = SelectedTask->conversation->crypt_key;
				SelectedTask->conversation->dialogs[d]->questions[q]->answers[SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length-1]->question_link = -2147483642;
				SelectedTask->conversation->dialogs[d]->questions[q]->answers[SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length-1]->task_link = SelectedTask->ID;
				SelectedTask->conversation->dialogs[d]->questions[q]->answers[SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length-1]->AnswerText = "OK";

				listBox_conversation_answers->Items->Add("[" + SelectedTask->conversation->dialogs[d]->questions[q]->answers->Length + "] Answer");
			}
		}
	}
	private: System::Void remove_answer(System::Object^  sender, System::EventArgs^  e)
	{
		if(SelectedTask)
		{
			int d = listBox_conversation_dialogs->SelectedIndex;
			int q = listBox_conversation_questions->SelectedIndex;
			int a = listBox_conversation_answers->SelectedIndex;

			if(d > -1 && q > -1 && a > -1 && SelectedTask->conversation->dialogs[d]->question_count>0)
			{
				SelectedTask->conversation->dialogs[d]->questions[q]->answer_count--;

				array<Answer^>^ temp = gcnew array<Answer^>(SelectedTask->conversation->dialogs[d]->questions[q]->answer_count);
				Array::Copy(SelectedTask->conversation->dialogs[d]->questions[q]->answers, 0, temp, 0, listBox_conversation_answers->SelectedIndex);
				Array::Copy(SelectedTask->conversation->dialogs[d]->questions[q]->answers, listBox_conversation_answers->SelectedIndex+1, temp, listBox_conversation_answers->SelectedIndex, SelectedTask->conversation->dialogs[d]->questions[q]->answer_count - listBox_conversation_answers->SelectedIndex);
				SelectedTask->conversation->dialogs[d]->questions[q]->answers = temp;

				// refresh question listbox
				select_question(nullptr, nullptr);
			}
		}
	}

#pragma endregion

#pragma region DEVELOPER FUNCTIONS

	// returns the id of the current task, if selected condition is true
	private: int developer_check_condition(Task^ task)
	{
		String^ condition = toolStripComboBox_developer_search->SelectedItem->ToString();

		if(condition == "Required be GM")
		{
			if(task->required_be_gm)
			{
				return task->ID;
			}
		}

		if(condition == "UNKNOWN_43")
		{
			if(task->UNKNOWN_43)
			{
				return task->ID;
			}
		}

		if(condition == "Prestige")
		{
			if(task->reward_success->prestige != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->prestige != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->prestige != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Influence")
		{
			if(task->reward_success->influence != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->influence != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->influence != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Event Gold")
		{
			if(task->reward_success->pq->event_gold != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->pq->event_gold != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->pq->event_gold != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward PQ Chases")
		{
			if(task->reward_success->pq->chase_count != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->pq->chase_count != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->pq->chase_count != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward PQ Items")
		{
			if(task->reward_success->pq->item_count != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->pq->item_count != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->pq->item_count != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward PQ Scripts")
		{
			if(task->reward_success->pq->script_count != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->pq->script_count != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->pq->script_count != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward PQ Messages")
		{
			if(task->reward_success->pq->message_count != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->pq->message_count != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->pq->message_count != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward Unknown 5")
		{
			if(task->reward_success->UNKNOWN_5 != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->UNKNOWN_5 != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->UNKNOWN_5 != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward Unknown 6")
		{
			if(task->reward_success->UNKNOWN_6 != 0)
			{
				return task->ID;
			}
			if(task->reward_failed->UNKNOWN_6 != 0)
			{
				return task->ID;
			}
			for(int i=0; i<task->rewards_timed->Length; i++)
			{
				if(task->rewards_timed[i]->UNKNOWN_6 != 0)
				{
					return task->ID;
				}
			}
		}

		if(condition == "Reward Timed")
		{
			if(task->rewards_timed->Length > 0)
			{
				return task->ID;
			}
		}

		return 0;
	}
	// returns a list of id's where selected condition is true
	// scans recursive into sub tasks
	private: String^ developer_scan(array<Task^>^ tasks)
	{
		String^log = "";

		for(int i=0; i<tasks->Length; i++)
		{
			int result = developer_check_condition(tasks[i]);
			if(result > 0)
			{
				log += result.ToString() + "\r\n";
			}
			if(tasks[i]->sub_quests->Length > 0)
			{
				log += developer_scan(tasks[i]->sub_quests);
			}
		}

		return log;
	}
	private: System::Void click_developer_search(System::Object^  sender, System::EventArgs^  e)
	{
		if(Tasks)
		{
			String^ log = "";
			log = developer_scan(Tasks);

			gcnew DebugWindow("Tasks Found", log);
		}
		else
		{
			MessageBox::Show("Please Load a File!");
		}
	}

#pragma endregion
private: System::Void textBox_runk48_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
};