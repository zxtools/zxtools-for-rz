#pragma once

#include "zxAIview\zxAIviewWindow.h"
#include "zxCLOTHfix\zxCLOTHfixWindow.h"
#include "zxELedit\\zxELeditWindow.h"
#include "zxGSHOPedit\zxGSHOPeditWindow.h"
#include "zxMAPtool\zxMAPtoolWindow.h"
#include "zxNPCedit\zxNPCeditWindow.h"
#include "zxTASKedit\zxTASKeditWindow.h"

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for zxtools_menu
	/// </summary>
	public ref class zxtools_menu : public System::Windows::Forms::Form
	{
	public:
		zxtools_menu(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~zxtools_menu()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(13, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(105, 27);
			this->button1->TabIndex = 0;
			this->button1->Text = L"zxAIview";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &zxtools_menu::button1_Click);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(124, 12);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(105, 27);
			this->button2->TabIndex = 1;
			this->button2->Text = L"zxCLOTHfix";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &zxtools_menu::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button3->Location = System::Drawing::Point(235, 12);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(105, 27);
			this->button3->TabIndex = 2;
			this->button3->Text = L"zxELedit";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &zxtools_menu::button3_Click);
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(346, 12);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(105, 27);
			this->button4->TabIndex = 3;
			this->button4->Text = L"zxGSHOPedit";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &zxtools_menu::button4_Click);
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button5->Location = System::Drawing::Point(55, 45);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(105, 27);
			this->button5->TabIndex = 4;
			this->button5->Text = L"zxMAPtool";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &zxtools_menu::button5_Click);
			// 
			// button6
			// 
			this->button6->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button6->Location = System::Drawing::Point(176, 45);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(105, 27);
			this->button6->TabIndex = 5;
			this->button6->Text = L"zxNPCedit";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &zxtools_menu::button6_Click);
			// 
			// button7
			// 
			this->button7->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button7->Location = System::Drawing::Point(296, 45);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(105, 27);
			this->button7->TabIndex = 6;
			this->button7->Text = L"zxTASKedit";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &zxtools_menu::button7_Click);
			// 
			// zxtools_menu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(464, 83);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"zxtools_menu";
			this->ShowIcon = false;
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"     zxtools                                                                 Copy" 
				L"right (c) 2010-2020";
			this->TopMost = true;
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxAIviewWindow ^zxAIWindow = gcnew zxAIviewWindow;
				 zxAIWindow->Show(this);
			 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxCLOTHfixWindow ^zxCLOTHWindow = gcnew zxCLOTHfixWindow;
				 zxCLOTHWindow->Show(this);
			 }
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxELeditWindow ^zxELWindow = gcnew zxELeditWindow;
				 zxELWindow->Show(this);
			 }
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxGSHOPeditWindow ^zxGSHOPWindow = gcnew zxGSHOPeditWindow;
				 zxGSHOPWindow->Show(this);
			 }
	private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxMAPtoolWindow ^zxMAPWindow = gcnew zxMAPtoolWindow;
				 zxMAPWindow->Show(this);
			 }
	private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxNPCeditWindow ^zxNPCWindow = gcnew zxNPCeditWindow;
				 zxNPCWindow->Show(this);
			 }
	private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 zxTASKeditWindow ^zxTASKWindow = gcnew zxTASKeditWindow;
				 zxTASKWindow->Show(this);
			 }
};