# zxtools  --  Copyright (c) 2010-2020 #

 -- A set of tools for viewing/editing some client / server files

### **Requires:** ###

**Visual Studio 2010 Professional**


## Credits go to RaGEZONE members: ##

### **ronny1982 _____  comper  _____ DNC** ###



Review the LICENSE: You are not permitted to sell, trade, barter, receive donations, goods, services, or any other repayment / recoupment. Your rights would be revoked permanently.

Sharing is Caring.



## Editors / Tools: ##

1. **zxACTedit** (for PW)

2. **zxAIview**

3. **zxCLOTHfix**

4. **zxELedit** (by default the configs folder .cfg file is set to the 1378 version)

5. **zxGSHOPedit**

6. **zxMAPtool**

7. **zxMODfix** (for PW)

8. **zxNPCedit**

9. **zxTASKedit**

10. **sPATCH**

11. **sPCK**

12. **zxPCK**

13. **pwAdmin**