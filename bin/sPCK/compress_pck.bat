@echo off

sPCK.exe -pw -c "building.pck.files"
sPCK.exe -pw -c "configs.pck.files"
sPCK.exe -pw -c "facedata.pck.files"
sPCK.exe -pw -c "gfx.pck.files"
sPCK.exe -pw -c "grasses.pck.files"
sPCK.exe -pw -c "interfaces.pck.files"
sPCK.exe -pw -c "litmodels.pck.files"
sPCK.exe -pw -c "loddata.pck.files"
sPCK.exe -pw -c "models.pck.files"
sPCK.exe -pw -c "sfx.pck.files"
sPCK.exe -pw -c "shaders.pck.files"
sPCK.exe -pw -c "surfaces.pck.files"
sPCK.exe -pw -c "textures.pck.files"
sPCK.exe -pw -c "trees.pck.files"
sPCK.exe -pw -c "help\script.pck.files"

pause