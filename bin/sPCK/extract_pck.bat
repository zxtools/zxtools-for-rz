@echo off

sPCK.exe -pw -x "building.pck"
sPCK.exe -pw -x "configs.pck"
sPCK.exe -pw -x "facedata.pck"
sPCK.exe -pw -x "gfx.pck"
sPCK.exe -pw -x "grasses.pck"
sPCK.exe -pw -x "interfaces.pck"
sPCK.exe -pw -x "litmodels.pck"
sPCK.exe -pw -x "loddata.pck"
sPCK.exe -pw -x "models.pck"
sPCK.exe -pw -x "sfx.pck"
sPCK.exe -pw -x "shaders.pck"
sPCK.exe -pw -x "surfaces.pck"
sPCK.exe -pw -x "textures.pck"
sPCK.exe -pw -x "trees.pck"
sPCK.exe -pw -x "help\script.pck"

pause